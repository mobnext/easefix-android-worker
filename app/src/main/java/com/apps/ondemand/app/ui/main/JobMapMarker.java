package com.apps.ondemand.app.ui.main;


import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.ScheduleJobModel;
import com.apps.ondemand.common.utils.AppUtils;


public class JobMapMarker extends RelativeLayout {


    public JobMapMarker(Context context) {
        super(context);
    }

    public JobMapMarker(Context context, ScheduleJobModel mover) {
        super(context);

        initView(mover);
    }

    private void initView(ScheduleJobModel mover) {
        inflate(getContext(), R.layout.custom_job_marker, this);

        ImageView mImage = findViewById(R.id.image_service);
        mImage.getLayoutParams().width = (int) getResources().getDimension(R.dimen.dimen_50);
        mImage.getLayoutParams().height = (int) getResources().getDimension(R.dimen.dimen_50);

        TextView milesOnMap = findViewById(R.id.txt_map_miles);
        String miles = AppUtils.formatNumberToTwoDigits(mover.getDistanceAway());
        milesOnMap.setText(miles);

    }

}
