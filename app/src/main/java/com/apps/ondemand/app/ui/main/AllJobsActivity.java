package com.apps.ondemand.app.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;

import com.apps.ondemand.R;
import com.apps.ondemand.app.ui.adapters.PackageViewPagerAdapter;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.base.BaseFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class AllJobsActivity extends BaseActivity {
    public static final int TOTAL_TABS = 2;

    PackageViewPagerAdapter adapter;
    ViewPager viewPager;
    TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_jobs);
        setActionBar(R.string.str_my_job);
        setSubTitle(R.string.str_scheduled_jobs);
        initViews();

    }
    private void initViews() {
        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.pager);
        List<String> titles = getTabsTitles();
        adapter = new PackageViewPagerAdapter(getSupportFragmentManager(), titles);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(0);
    }
    private List<String> getTabsTitles() {
        List<String> titles = new ArrayList<>();
        titles.add(getString(R.string.str_current_job));
        titles.add(getString(R.string.str_past));
        return titles;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onEventReceived(Intent intent) {
        super.onEventReceived(intent);

        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            if (fragment instanceof BaseFragment) {
                ((BaseFragment) fragment).onEventReceived(intent);
                break;
            }
        }
    }
}