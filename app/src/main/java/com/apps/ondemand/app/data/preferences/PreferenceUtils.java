package com.apps.ondemand.app.data.preferences;

public class PreferenceUtils {

    static final String PREFERENCE_NAME = "on_demand";
    static final int PRIVATE_MODE = 0;

    // User authentication
    public static final String COOKIE = "cookie";
    public static final String IS_LOGGED_IN = "is_log_in";
    // region fireBase
    public static final String FCM_TOKEN = "fcm_token";
    public static final String HAS_UPLOAD_TOKEN = "has_upload_token";
    //end region

    //region user
    public static final String USER_ID = "_id";
    public static final String NAME = "name";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String EMAIL = "email";
    public static final String ABOUT = "about";
    public static final String PHONE = "phoneNumber";
    public static final String PHONE_PREFIX = "phonePreFix";
    public static final String IS_BLOCKED = "isBlocked";
    public static final String IS_SIGN_UP_COMPLETED = "isSignUpCompleted";
    public static final String IMAGE = "profileImage";
    public static final String REFERRAL_CODE = "referralCode";
    public static final String AVERAGE_RATING = "avgRating";
    public static final String IS_VERIFIED_BY_ADMIN = "isVerifiedByAdmin";
    public static final String STEP_COMPLETED = "stepCompleted";
    public static final String IS_COMPANY = "isCompany";
    public static final String COMPANY_NAME = "companyName";
    public static final String IS_COMPANY_APPROVED = "isCompanyApproved";
    public static final String IS_COMPANY_REJECTED = "isCompanyRejected";
    public static final String IS_CERTIFICATE_APPROVED = "certificateApproved";
    public static final String IS_ONLINE = "isOnline";
    //end user

    public static final String BASE_URL = "url";

    //Language Preference Keys
    static final String LANGUAGE_PREFERENCE_NAME = "on_demand_language";
    public static final String PREFERRED_LANGUAGE = "preferred_language";
    public static final String IS_LANGUAGE_SELECTED = "is_language_added";
    public static final String CURRENCY = "currency";

    //Twilio Implementation
    public static final String SELECTED_INTERESTED_ID = "selectedInterestId";
    public static final String TOKEN = "twilio_token";
    public static final String IN_CALL = "in_call";
    public static final String BOARDING_SCREEN = "boarding_screens";

}
