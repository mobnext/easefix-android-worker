package com.apps.ondemand.app.ui.jobs;


import static com.apps.ondemand.common.utils.AppConstants.KEY_JOB_ID;
import static com.apps.ondemand.common.utils.AppConstants.STATUS_REJECTION;
import static com.apps.ondemand.common.utils.AppConstants.STATUS_TIME_OUT;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.app.data.models.Empty;
import com.apps.ondemand.app.data.models.JobDetailModel;
import com.apps.ondemand.app.data.models.Progress;
import com.apps.ondemand.app.data.models.ScheduleJobModel;
import com.apps.ondemand.app.ui.adapters.MyJobsAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewFragment;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyJobsFragments extends BaseRecyclerViewFragment implements OnRecyclerViewItemClickListener {
    private static final String LIST_TYPE_EXTRA_KEY = "list_type_extra_key";

    protected View rootView;
    private int pageNo = 0;
    private boolean isLoading = false;
    private boolean doLoadMore = false;
    private MyJobsAdapter adapter;
    private static final int LIMIT = 20;
    private String activePackageUrl = "";
    int listType = 2;
    private String fetchDetailsUrl = "";

    public static MyJobsFragments newInstance(int listType) {

        Bundle args = new Bundle();
        args.putInt(LIST_TYPE_EXTRA_KEY, listType);
        MyJobsFragments fragment = new MyJobsFragments();

        fragment.setArguments(args);
        return fragment;
    }

    public MyJobsFragments() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            listType = getArguments().getInt(LIST_TYPE_EXTRA_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_job, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initializeRecyclerView();
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter = null;
        setAdapter(null);
        pageNo = 0;
        getListData(true);
    }

    private void initializeRecyclerView() {
        RecyclerView recyclerView = getRecyclerView();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(false);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isLoading || !doLoadMore)       //Check if request is already called OR do need to get next page patients
                    return;
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int totalItemCount = linearLayoutManager.getItemCount();
                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (totalItemCount <= (lastVisibleItem + 1)) {
                    getListData(true);
                    doLoadMore = false;
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);

        try {
            isLoading = false;
            removeProgressItem();
            JSONObject responseJson = new JSONObject(response.getResponse());
            if (responseJson.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equalsIgnoreCase(activePackageUrl)) {
                    JSONArray offers = responseJson.getJSONObject("data").getJSONArray("jobList");
                    if (offers.length() != 0) {
                        List<BaseItem> offersItems = new Gson().fromJson(offers.toString(), new TypeToken<List<ScheduleJobModel>>() {
                        }.getType());
                        setPackageListingResponse(offersItems);
                    } else {
                        setEmptyLayout();
                    }
                }

            } else {
                showSnackBar(responseJson.getString("message"));
                setEmptyLayout();
            }
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    }

    @Override
    public void onNetworkError(HttpResponseItem response) {
        super.onNetworkError(response);
        removeProgressItem();
        setEmptyLayout();

    }

    private void setEmptyLayout() {
        if (getContext() == null)
            return;
        if (adapter == null || adapter.getItemCount() == 0) {
            List<BaseItem> itemList = new ArrayList<>();
            String message = listType == 1 ? getString(R.string.str_no_schedule_job) : getString(R.string.str_no_past_job);
            itemList.add(new Empty(message));
            populateList(itemList);
        }
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        if (UserManager.isUserLoggedIn()) {
            adapter = null;
            setAdapter(null);
            pageNo = 0;
            getListData(false);
        } else {
            stopRefresh();
        }

    }

    public void getListData(boolean addProgress) {
        isLoading = true;
        if (addProgress) {
            addProgressItem();
        }
        activePackageUrl = AppConstants.getServerUrl(String.format(AppConstants.FETCH_SCHEDULE_JOBS, listType));
        HttpRequestItem requestItem = new HttpRequestItem(activePackageUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(null, this);
        appNetworkTask.execute(requestItem);
    }

    private void addProgressItem() {
        List<BaseItem> items = new ArrayList<>();
        items.add(new Progress());
        populateList(items);
    }

    private void removeProgressItem() {
        if (adapter != null && adapter.getAdapterCount() > 0 &&
                adapter.getItemAt(adapter.getAdapterCount() - 1) instanceof Progress) {
            adapter.remove(adapter.getAdapterCount() - 1);
            adapter.notifyItemRemoved(adapter.getAdapterCount() - 1);
        }
        if (getSwipeLayout() != null) {
            getSwipeLayout().setRefreshing(false);
        }
    }

    private void setPackageListingResponse(List<BaseItem> data) throws JSONException {
        List<BaseItem> itemList = new ArrayList<>();
        if (data.size() == 0)
            doLoadMore = false;
        else {
            pageNo++;
            doLoadMore = data.size() == LIMIT;
            itemList.addAll(data);
        }

        populateList(itemList);

    }

    private void populateList(List<BaseItem> itemList) {
        if (adapter == null) {
            adapter = new MyJobsAdapter(getActivity(), itemList, this);
            setAdapter(adapter);
        } else
            adapter.addAll(itemList);

    }


    private void initView(View rootView) {

    }


    @Override
    public void onRecyclerViewItemClick(BaseRecyclerViewHolder holder) {
        super.onRecyclerViewItemClick(holder);
    }

    @Override
    public void onRecyclerViewChildItemClick(BaseRecyclerViewHolder holder, int resourceId) {
        super.onRecyclerViewChildItemClick(holder, resourceId);

        switch (resourceId) {
            case R.id.ll_job:
                ScheduleJobModel jobModel = (ScheduleJobModel) adapter.getItemAt(holder.getAdapterPosition());
                if (jobModel.getSpJobStatus() != STATUS_REJECTION && jobModel.getSpJobStatus() != STATUS_TIME_OUT) {
                    Intent intent = new Intent(getActivity(), JobDetailsActivity.class);
                    intent.putExtra(AppConstants.KEY_JOB_ID, jobModel.getId());
                    startActivity(intent);
                }
                break;
        }
    }

    @Override
    public void onEventReceived(Intent intent) {
        super.onEventReceived(intent);
        adapter = null;
        setAdapter(null);
        pageNo = 0;
        getListData(false);
    }

}
