package com.apps.ondemand.app.ui.adapters;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.ReasonModel;

import java.util.ArrayList;
import java.util.List;

public class CancellationReasonAdapter extends BaseAdapter {

    private List<ReasonModel> objects = new ArrayList<>();

    private Context context;
    private LayoutInflater layoutInflater;

    public CancellationReasonAdapter(Context context, List<ReasonModel> objects) {
        this.context = context;
        this.objects = objects;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public ReasonModel getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.row_cancelation, null);
            convertView.setTag(new ViewHolder(convertView));
        }
        initializeViews(getItem(position), (ViewHolder) convertView.getTag());
        return convertView;
    }

    private void initializeViews(ReasonModel object, ViewHolder holder) {
        if (object.isSelected()) {
            holder.ivReasonRelected.setVisibility(View.VISIBLE);
            holder.tvReason.setTextColor(Color.parseColor("#252325"));
        } else {
            holder.ivReasonRelected.setVisibility(View.GONE);
            holder.tvReason.setTextColor(Color.parseColor("#9f9e9f"));
        }
        holder.tvReason.setText(object.getReason());
    }

    protected class ViewHolder {
        private TextView tvReason;
        private ImageView ivReasonRelected;

        public ViewHolder(View view) {
            tvReason = (TextView) view.findViewById(R.id.tv_reason);
            ivReasonRelected = view.findViewById(R.id.iv_reason_selected);
        }
    }
}
