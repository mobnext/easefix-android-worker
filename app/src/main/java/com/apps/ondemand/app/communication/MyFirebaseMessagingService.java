package com.apps.ondemand.app.communication;

import android.content.Intent;
import android.util.Log;
import com.apps.ondemand.MyApplication;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.app.data.preferences.PreferenceUtils;
import com.apps.ondemand.app.data.preferences.SharedPreferenceManager;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.NotificationManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sendbird.calls.SendBirdCall;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    @Override
    public void onNewToken(String refreshedToken) {
        super.onNewToken(refreshedToken);
        SharedPreferenceManager.getInstance().save(PreferenceUtils.FCM_TOKEN, refreshedToken);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage == null)
            return;

        if (SendBirdCall.handleFirebaseMessageData(remoteMessage.getData())) {
            Log.i(MyApplication.TAG, "[MyFirebaseMessagingService] onMessageReceived() => " + remoteMessage.getData().toString());
        } else {
            // Check if message contains a notification payload.
            if (remoteMessage.getData() != null) {
                Map<String, String> data = remoteMessage.getData();
                String eventType = data.containsKey("type") ? data.get("type") : "";
                String title = data.containsKey("senderName") ? data.get("senderName") : "";
                String body = data.containsKey("alert") ? data.get("alert") : "";
                checkForApprovedRejected(eventType);
                try {
                    JSONObject gcmData = null;
                    String id = "";
                    if (data.containsKey("resource") && AppUtils.ifNotNullEmpty(data.get("resource"))) {
                        gcmData = new JSONObject(data.get("resource"));
                        id = gcmData.has("jobId") && gcmData.getString("jobId") != null ?
                                gcmData.getString("jobId") : "";
                    }

                    if (gcmData == null)
                        gcmData = new JSONObject();
                    NotificationManager.createNotification(getBaseContext(), id, eventType, title, body, gcmData.toString());
                    broadcastResults(eventType, id, title, body, gcmData.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void checkForApprovedRejected(String eventType) {
        if (eventType.equalsIgnoreCase(SocketService.KEY_ADMIN_APPROVED)) {
            UserManager.setVerified(true);
        } else if (eventType.equalsIgnoreCase(SocketService.KEY_ADMIN_REJECTED)) {
            UserManager.setVerified(false);
        }
    }

    /**
     * Method to broadcast job events from this Service to the Bind application component
     *
     * @param broadcast_type key of the broadcast message
     * @param data           value of the broadcast message
     */
    private void broadcastResults(String broadcast_type, String id, String title, String message, String data) {
        Intent intent = new Intent(SocketService.BROADCAST_NOTIFICATION);
        intent.putExtra(SocketService.BROADCAST_INTENT_TYPE, broadcast_type);
        intent.putExtra(SocketService.BROADCAST_JOB_ID, id);
        intent.putExtra(SocketService.BROADCAST_INTENT_DATA, data);
        intent.putExtra(AppConstants.FROM_WHICH_COMPONENT, AppConstants.COMPONENT_GCM);
        intent.putExtra(AppConstants.TITLE_GCM, title);
        intent.putExtra(AppConstants.MESSAGE_GCM, message);
        sendBroadcast(intent);
    }

}
