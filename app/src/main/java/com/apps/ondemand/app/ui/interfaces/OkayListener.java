package com.apps.ondemand.app.ui.interfaces;

public interface OkayListener {
    public void signUpToContinue();
}
