package com.apps.ondemand.app.ui.interfaces;

public interface ScheduleTimeListener {
    void onTimeSelected(boolean isCancelled, long selectedTime);
}
