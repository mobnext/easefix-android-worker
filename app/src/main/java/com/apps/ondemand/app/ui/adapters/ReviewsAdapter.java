package com.apps.ondemand.app.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.ReviewModel;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.VizImageView;

import java.util.List;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;


public class ReviewsAdapter extends BaseRecyclerViewAdapter {

    Context context;

    public ReviewsAdapter(Context context, List<BaseItem> items, OnRecyclerViewItemClickListener itemClickListener) {
        super(items, itemClickListener);
        this.context = context;
    }

    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        BaseRecyclerViewHolder holder;
        if (viewType == BaseItem.ITEM_REVIEWS) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_review, parent, false);
            holder = new ReviewHolder(view);
        } else if (viewType == BaseItem.ITEM_EMPTY) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
            holder = new EmptyViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_progress, parent, false);
            holder = new ProgressViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder holder, int position) {
        if (holder instanceof ReviewHolder) {
            ReviewHolder reviewHolder = (ReviewHolder) holder;
            ReviewModel reviewModel = (ReviewModel) getItemAt(position);

            reviewHolder.ivProfile.setImage(reviewModel.getProfileImage(),
                    (int) context.getResources().getDimension(R.dimen.dimen_60),
                    (int) context.getResources().getDimension(R.dimen.dimen_60),
                    R.color.colorPrimaryBlue);

            reviewHolder.tvName.setText(reviewModel.getName());
            reviewHolder.tvReview.setText(reviewModel.getReview());
            reviewHolder.rbRating.setRating(reviewModel.getAvgRating());
            reviewHolder.tvRating.setText(AppUtils.formatRating(reviewModel.getAvgRating()));

        }

    }

    private class ReviewHolder extends BaseRecyclerViewHolder {
        private RelativeLayout rlHeader;
        private VizImageView ivProfile;
        private TextView tvName;
        private MaterialRatingBar rbRating;
        private TextView tvRating;
        private TextView tvReview;


        public ReviewHolder(View view) {
            super(view);

            rlHeader =  view.findViewById(R.id.rl_header);
            ivProfile =  view.findViewById(R.id.iv_profile_image);
            tvName =  view.findViewById(R.id.tv_profile_name);
            rbRating = view.findViewById(R.id.rb_profile_rating);
            tvRating = view.findViewById(R.id.tv_profile_rating);
            tvReview = view.findViewById(R.id.tv_review);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return ReviewHolder.this;
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            if (getItemClickListener() != null)
                getItemClickListener().onRecyclerViewItemClick(ReviewHolder.this);
        }
    }

    public class ProgressViewHolder extends BaseRecyclerViewHolder {
        private ProgressViewHolder(View view) {
            super(view);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    public class EmptyViewHolder extends BaseRecyclerViewHolder {
        private TextView textViewEmpty;

        private EmptyViewHolder(View view) {
            super(view);
            textViewEmpty = view.findViewById(R.id.tv_message);
            textViewEmpty.setText(R.string.str_no_reviews_found);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getItemAt(position).getItemType();
    }
}
