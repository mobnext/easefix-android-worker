package com.apps.ondemand.app.ui.main;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.ondemand.BuildConfig;
import com.apps.ondemand.R;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.utils.AppConstants;


public class ShareDiscountActivity extends BaseActivity implements View.OnClickListener {

    protected TextView tvCopy;
    protected RelativeLayout rlShareCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_share_discount);
        setActionBar(R.string.str_share);
        setSubTitle(R.string.str_share_sub);
        initView();
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tv_copy) {
            copyToClipBoard(UserManager.getReferralCode());
        } else if (view.getId() == R.id.rl_share_code) {
//            String shareMessage = String.format(getString(R.string.str_share_message), UserManager.getReferralCode()) + "\n"
//                    + "Android: " + AppConstants.PLAY_STORE_URL_PREFIX + BuildConfig.APPLICATION_ID;
            String shareMessage = getString(R.string.str_share__referral)+String.format(getString(R.string.str_share_message), UserManager.getReferralCode()) + "\n\n"
                    +getString(R.string.str_download)+getString(R.string.str_android) + AppConstants.PLAY_STORE_URL_PREFIX + BuildConfig.APPLICATION_ID+getString(R.string.str_ios)+getString(R.string.str_end);
            share(shareMessage);
        }
    }

    private void initView() {
        findTextViewById(R.id.tv_referral_code).setText(UserManager.getReferralCode());
        tvCopy = (TextView) findViewById(R.id.tv_copy);
        tvCopy.setOnClickListener(ShareDiscountActivity.this);
        rlShareCode = (RelativeLayout) findViewById(R.id.rl_share_code);
        rlShareCode.setOnClickListener(ShareDiscountActivity.this);
    }

    private void copyToClipBoard(String referralCode) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("label", referralCode);
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }
        showSnackBar(R.string.str_copy_to_clipboard);
    }

    private void share(String shareMessage) {

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Share");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
        startActivity(Intent.createChooser(sharingIntent, "Share Using"));
    }
}
