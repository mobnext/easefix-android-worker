package com.apps.ondemand.app.ui.viewPager;

import android.content.Context;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import android.widget.MediaController;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.MediaModel;
import com.apps.ondemand.app.data.models.PortfolioModel;
import com.apps.ondemand.app.ui.videoUpload.VideoViewActivity;
import com.apps.ondemand.common.business.BaseItem;
import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Objects;

public class ViewPagerAdapter extends PagerAdapter {

    // Context object
    Context context;
    int type;
    // Array of images
    List<BaseItem> images;

    // Layout Inflater
    LayoutInflater mLayoutInflater;


    // Viewpager Constructor
    public ViewPagerAdapter(Context context, List<BaseItem> images, int type) {
        this.context = context;
        this.images = images;
        this.type = type;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // return the number of images
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((ConstraintLayout) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        // inflating the item.xml
        View itemView = mLayoutInflater.inflate(R.layout.layout_full_screen, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.iv_upload_image_port);
        ImageView videoView = (ImageView) itemView.findViewById(R.id.videoBtn);
        if (type == 1) {
            PortfolioModel portfolioModel = (PortfolioModel) images.get(position);
            Glide.with(itemView.getContext()).load(portfolioModel.getUrl()).placeholder(R.drawable.user_loader).into(imageView);
            if (portfolioModel.getType()==1){
                videoView.setVisibility(View.VISIBLE);
            }else {
                videoView.setVisibility(View.GONE);
            }
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (portfolioModel.getType()==1){
                        Intent intent= new Intent(itemView.getContext(), VideoViewActivity.class);
                        intent.putExtra("url",portfolioModel.getUrl());
                        itemView.getContext().startActivity(intent);

                }
                }
            });

        } else if (type == 2) {
            MediaModel portfolioModel = (MediaModel) images.get(position);
            Glide.with(itemView.getContext()).load(portfolioModel.getUrl()).placeholder(R.drawable.user_loader).into(imageView);
            if (portfolioModel.getType()==1){
                videoView.setVisibility(View.VISIBLE);
            }else {
                videoView.setVisibility(View.GONE);
            }
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (portfolioModel.getType()==1){
                        Intent intent= new Intent(itemView.getContext(), VideoViewActivity.class);
                        intent.putExtra("url",portfolioModel.getUrl());
                        itemView.getContext().startActivity(intent);
                }
                }
            });
        }


        // referencing the image view from the item.xml file
//        VizImageVideoView imageView = (VizImageVideoView) itemView.findViewById(R.id.iv_upload_image_port);


        // setting the image in the imageView
//        imageView.setImage(portfolioModel.getUrl(),100,100,R.color.colorPrimaryBlue);
//        GlideUtils.glideLoadImageOrPlaceHolderNoFade(mLayoutInflater.getContext(), imageView,portfolioModel.getUrl(),200,200);
//        Glide.with(itemView.getContext()).load(portfolioModel.getUrl()).placeholder(R.drawable.user_loader).into(imageView);
        // Adding the View
        Objects.requireNonNull(container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((ConstraintLayout) object);
    }

}