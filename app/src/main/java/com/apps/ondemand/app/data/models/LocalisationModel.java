package com.apps.ondemand.app.data.models;

/**
 * Created on 2016-12-03 15:01.
 *
 * @author Rana
 */
public class LocalisationModel {
    private String key;
    private String value;

    public LocalisationModel() {
    }

    public LocalisationModel(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
} // FeedModel
