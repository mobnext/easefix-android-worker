package com.apps.ondemand.app.ui.dialogs;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.ProficiencyModel;
import com.apps.ondemand.app.data.models.SpokenLanguageModel;
import com.apps.ondemand.app.ui.adapters.ProficiencyAdapter;
import com.apps.ondemand.app.ui.interfaces.ProficiencySelectionCallback;
import com.apps.ondemand.common.base.BaseActivity;

import java.util.List;


public class DialogLanguageProficiency extends BaseDialog implements AdapterView.OnItemClickListener, View.OnClickListener {
    private int SELECTED_INDEX = 0;

    BaseActivity activity;
    BottomSheetBehavior mBehavior;
    View contentView;
    List<ProficiencyModel> proficiencyModels;
    ProficiencyAdapter proficiencyAdapter;
    SpokenLanguageModel selectedLanguageModel;
    ProficiencyModel selectedProficiency = null;
    ProficiencySelectionCallback callback;

    private DialogLanguageProficiency(@NonNull BaseActivity activity, List<ProficiencyModel> models, SpokenLanguageModel selectedLanguageModel, ProficiencySelectionCallback callback) {
        super(activity);
        this.activity = activity;
        this.proficiencyModels = models;
        this.selectedLanguageModel = selectedLanguageModel;
        this.callback = callback;
        setSelectedProficiency();
    }

    private void setSelectedProficiency() {
        for (int i = 0; i < proficiencyModels.size(); i++) {
            ProficiencyModel proficiencyModel = proficiencyModels.get(i);
            if (proficiencyModel.getId().equals(selectedLanguageModel.getProficiencyId())) {
                SELECTED_INDEX = i;
            }
            proficiencyModel.setSelected(false);
        }
        proficiencyModels.get(SELECTED_INDEX).setSelected(true);
        selectedProficiency = proficiencyModels.get(SELECTED_INDEX);
    }

    public static DialogLanguageProficiency newInstance(BaseActivity activity, List<ProficiencyModel> models, SpokenLanguageModel selectedLanguageModel, ProficiencySelectionCallback callback) {
        return new DialogLanguageProficiency(activity, models, selectedLanguageModel, callback);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        contentView = View.inflate(getContext(), R.layout.proficiency_bottom_sheet, null);

        setContentView(contentView);

        mBehavior = BottomSheetBehavior.from((View) contentView.getParent());
        ((View) contentView.getParent()).setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));
    }

    @Override
    public void onStart() {
        super.onStart();
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        getWindow().setLayout(width, height);
        setCanceledOnTouchOutside(false);
        populateViews();
        setCancelable(false);
    }

    @Override
    public void onBackPressed() {
        dismiss();
    }

    private void populateViews() {
        mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING)
                    bottomSheet.post(new Runnable() {
                        @Override
                        public void run() {
                            mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    });
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
        mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        ListView lvProficiencies = findViewById(R.id.lv_proficiency);

        proficiencyAdapter = new ProficiencyAdapter(activity, proficiencyModels);
        lvProficiencies.setAdapter(proficiencyAdapter);
        lvProficiencies.setOnItemClickListener(this);

        findImageViewById(R.id.iv_cancel).setOnClickListener(this);
        findButtonById(R.id.btn_add_language).setOnClickListener(this);
        findTextViewById(R.id.tv_label).setText(selectedLanguageModel.getLabel());
        findTextViewById(R.id.tv_sub_label).setText(selectedLanguageModel.getSubLabel());
        findTextViewById(R.id.tv_proficiency).setText(selectedProficiency.getLabel());
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        for (ProficiencyModel model : proficiencyModels) {
            model.setSelected(false);
        }
        proficiencyModels.get(position).setSelected(true);
        proficiencyAdapter.notifyDataSetChanged();
        selectedProficiency = proficiencyModels.get(position);
        findTextViewById(R.id.tv_proficiency).setText(selectedProficiency.getLabel());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_language:
                callback.onAddLanguage(selectedProficiency);
                dismiss();
                break;

            case R.id.iv_cancel:

                dismiss();
                break;
        }
    }
}