package com.apps.ondemand.app.ui.interfaces;

public interface TermsPrivacyListener {
    void onAgree(boolean isAgreedToTerms, boolean
            isAgreedToPrivacy);
}
