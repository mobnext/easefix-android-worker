package com.apps.ondemand.app.ui.interfaces;

public interface DeleteListener {
     void onDelete(int deleteFlag);

}
