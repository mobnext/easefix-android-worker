package com.apps.ondemand.app.ui.earnings;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;

import android.view.View;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.WeekModel;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.renderer.XAxisRenderer;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import static com.apps.ondemand.common.utils.AppConstants.KEY_EXTRA_WEEK_NUMBER;
import static com.apps.ondemand.common.utils.AppConstants.KEY_EXTRA_YEAR;
import static com.apps.ondemand.common.utils.AppConstants.WEEK_CODE;
import static com.apps.ondemand.common.utils.DateUtils.getWeekDateArray;


public class EarningActivity extends BaseActivity implements View.OnClickListener {

    int weekNumber, year;
    private String urlGetEarnings;
    private Calendar curCalendar = Calendar.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earning);
        setActionBar(R.string.str_earnings);
        setSubTitle(R.string.str_earnings_sub);
        initViews();

        findTextViewById(R.id.tv_referral_bonus).setText(getString(R.string.referral_bonus)+" "+AppUtils.getAmount(0));
    }

    private void initViews() {
        findViewById(R.id.ll_week_title).setOnClickListener(this);
        findViewById(R.id.ll_past_jobs).setOnClickListener(this);

        curCalendar = Calendar.getInstance();
        weekNumber = curCalendar.get(Calendar.WEEK_OF_YEAR);
        year = curCalendar.get(Calendar.YEAR);
    }

    @Override
    public void onResume() {
        super.onResume();
        getEarningsApiCall();
    }

    private void getEarningsApiCall() {

        urlGetEarnings = AppConstants.getServerUrl(AppConstants.GET_EARNINGS);

        HttpRequestItem requestItem = new HttpRequestItem(urlGetEarnings);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());

        Map<String, Object> params = new HashMap<>();
        params.put("weekNumber", weekNumber-1);
        params.put("year", year);

        requestItem.setParams(params);

        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_week_title:
                Intent intent = new Intent(this, PastWeekActivity.class);
                startActivityForResult(intent, WEEK_CODE);
                break;

            case R.id.ll_past_jobs:
                intent = new Intent(this, PastJobsActivity.class);
                intent.putExtra(KEY_EXTRA_WEEK_NUMBER, weekNumber);
                intent.putExtra(KEY_EXTRA_YEAR, year);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == WEEK_CODE && resultCode == Activity.RESULT_OK) {
            final WeekModel weekModel = data.getParcelableExtra(AppConstants.KEY_EXTRA_WEEK_MODEL);
            weekNumber = weekModel.getWeekNumber();
            year = weekModel.getWeekYear();
            curCalendar.set(Calendar.WEEK_OF_YEAR, weekNumber);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    findTextViewById(R.id.tv_week_title).setText(weekModel.getTitle());
                    findTextViewById(R.id.tv_referral_bonus).setText(getString(R.string.referral_bonus)+" "+AppUtils.getAmount(0));
                    getEarningsApiCall();
                }
            }, 500);
        }
    }


    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            JSONObject responseJson = new JSONObject(response.getResponse());
            if (response.getHttpRequestUrl().equals(urlGetEarnings)) {
                if (responseJson.getInt("success") == 1) {
                    JSONObject data = responseJson.getJSONObject("data");

                    if (data.has("weekDayEarnings")) {
                        populateView(data);
                    }

                } else {
                    showSnackBar(responseJson.getString("message"));
                }
            }
        } catch (JSONException e) {
            Logger.error(false, e);
        }
    }

    private void populateView(JSONObject data) throws JSONException {
        animate();
        findViewById(R.id.layout_earnings).setVisibility(View.VISIBLE);

        String weekTitle = AppUtils.getStringFromJson(data, "weekTitle");
        String totalEarnings = AppUtils.getAmount(AppUtils.getDoubleFromJson(data, "totalSpEarning"));
        int totalJobs = AppUtils.getIntFromJson(data, "totalJobCount");

        TextView txtOverallEarned = findViewById(R.id.tv_total_earned);
        TextView txtDoneJobs = findViewById(R.id.tv_total_jobs);
        TextView txtWeekName = findViewById(R.id.tv_week_title);
        TextView txtReferral = findViewById(R.id.tv_referral_bonus);

        txtReferral.setText(getString(R.string.referral_bonus)+ " "+AppUtils.getAmount(
                AppUtils.getDoubleFromJson(data, "referralBonus")));

        txtOverallEarned.setText(totalEarnings);
        txtDoneJobs.setText(totalJobs + " " + getString(R.string.str_jobs_multi));
        txtWeekName.setText(weekTitle);
        JSONArray weekDaysEarning = data.getJSONArray("weekDayEarnings");

        float chartIndex = 0f;
        List<BarEntry> entries = new ArrayList<>();

        for (int i = 0; i < 7; i++) {
            BarEntry entry;
            if (weekDaysEarning.length() != 0) {
                entry = new BarEntry(chartIndex, (float) weekDaysEarning.getJSONObject(i).getDouble("spEarning"));
            } else {
                entry = new BarEntry(chartIndex, 0f);
            }
            entries.add(entry);
            chartIndex = chartIndex + 1f;
        }
        populateChart(entries);
        setHighlightedBar();

//        if (totalJobs > 0) {
//            findViewById(R.id.ll_past_jobs).setVisibility(View.VISIBLE);
//
//        } else {
            findViewById(R.id.ll_past_jobs).setVisibility(View.GONE);
//        }
    }


    private void populateChart(List<BarEntry> entries) {
        final BarChart chart = (BarChart) findViewById(R.id.chart);

        BarDataSet set = new BarDataSet(entries, "BarDataSet");
        set.setHighlightEnabled(true);
        set.setColor(ContextCompat.getColor(this, R.color.bar_normal));
        set.setValueTextColor(ContextCompat.getColor(this, R.color.text_primary_light));
        set.setHighLightColor(ContextCompat.getColor(this, R.color.bar_highlight));
        set.setBarBorderColor(ContextCompat.getColor(this, R.color.bar_border_color));
        set.setBarBorderWidth(1);
        set.setHighLightAlpha(1000);
        set.setValueTextSize(10);
        //set.setDrawValues(true);


        BarData data = new BarData(set);

        data.setBarWidth(0.99f); // set custom bar width
        chart.setScaleEnabled(false);
        chart.getDescription().setEnabled(false);
        chart.getLegend().setEnabled(false);
        chart.setFitBars(true); // make the x-axis fit exactly all bars
        chart.invalidate(); // refresh
        chart.setHighlightPerTapEnabled(false);
        chart.setDrawValueAboveBar(true);
        chart.setNoDataText(getString(R.string.str_no_earning_week));
        chart.setTouchEnabled(false);
        chart.setData(data);
        chart.animateY(3000, Easing.EasingOption.EaseInOutBounce);

        XAxis xAxis = chart.getXAxis();
        xAxis.setAxisMinimum(0f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setAxisMinimum(-0.5f);
        xAxis.setTextColor(ContextCompat.getColor(this, R.color.text_primary));
        xAxis.setTextSize(11);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(getWeekDateArray(curCalendar)));

//        xAxis.setValueFormatter(new IndexAxisValueFormatter(DateUtils.getWeekDaysArray()));

        YAxis leftAxis = chart.getAxisLeft();
        YAxis rightAxis = chart.getAxisRight();
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(false);
        leftAxis.setDrawAxisLine(false);
        leftAxis.setTextSize(11);
        leftAxis.setTextColor(ContextCompat.getColor(this, R.color.text_primary_light));
        rightAxis.setEnabled(false);
        leftAxis.setEnabled(false);
        chart.setViewPortOffsets(0, 0, 0, getResources().getDimensionPixelSize(R.dimen.dimen_40));
        chart.setXAxisRenderer(new CustomXAxisRenderer(chart.getViewPortHandler(), chart.getXAxis(), chart.getTransformer(YAxis.AxisDependency.LEFT)));
    }

    private void setHighlightedBar() {
        BarChart chart = (BarChart) findViewById(R.id.chart);
        if (chart.getData() != null && chart.getData().getDataSetCount() != 0) {

            Calendar calendar = Calendar.getInstance();
            int day = calendar.get(Calendar.DAY_OF_WEEK);

            int highlightIndex = 0;

            switch (day) {
                case Calendar.MONDAY:
                    highlightIndex = 0;
                    break;

                case Calendar.TUESDAY:
                    highlightIndex = 1;
                    break;

                case Calendar.WEDNESDAY:
                    highlightIndex = 2;
                    break;

                case Calendar.THURSDAY:
                    highlightIndex = 3;
                    break;

                case Calendar.FRIDAY:
                    highlightIndex = 4;
                    break;

                case Calendar.SATURDAY:
                    highlightIndex = 5;
                    break;

                case Calendar.SUNDAY:
                    highlightIndex = 6;
                    break;
            }

            chart.highlightValue(highlightIndex, 0);
        }
    }

    public class CustomXAxisRenderer extends XAxisRenderer {
        public CustomXAxisRenderer(ViewPortHandler viewPortHandler, XAxis xAxis, Transformer trans) {
            super(viewPortHandler, xAxis, trans);
        }

        @Override
        protected void drawLabel(Canvas c, String formattedLabel, float x, float y, MPPointF anchor, float angleDegrees) {
            String line[] = formattedLabel.split("\n");
            mAxisLabelPaint.setTypeface(Typeface.create("Arial", Typeface.BOLD));
            Utils.drawXAxisValue(c, line[0], x, y + 10, mAxisLabelPaint, anchor, angleDegrees);
            Utils.drawXAxisValue(c, line[1], x, y + mAxisLabelPaint.getTextSize() + 8, mAxisLabelPaint, anchor, angleDegrees);
        }
    }
}
