package com.apps.ondemand.app.ui.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.ProficiencyModel;
import com.apps.ondemand.app.data.models.SpokenLanguageModel;
import com.apps.ondemand.app.ui.adapters.AddedLanguageAdapter;
import com.apps.ondemand.app.ui.authentication.AddLanguageActivity;
import com.apps.ondemand.app.ui.authentication.SignUpCompletedActivity;
import com.apps.ondemand.app.ui.dialogs.DialogLanguageProficiency;
import com.apps.ondemand.app.ui.interfaces.ProficiencySelectionCallback;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.apps.ondemand.common.utils.AppConstants.GOOGLE_BASE_URL;
import static com.apps.ondemand.common.utils.AppConstants.KEY_PROFICIENCIES;
import static com.apps.ondemand.common.utils.AppConstants.KEY_SPOKEN_LANGUAGES;
import static com.apps.ondemand.common.utils.AppConstants.SPOKEN_LANGUAGE_CODE;

public class PersonalDetailsActivity extends BaseRecyclerViewActivity implements View.OnClickListener {


    private String fetchActiveLanguages = "";
    private String personalDetailsUrl = "";
    private String fetchLevelUrl = "";
    private String fetchProfileDataUrl = "";


    private ArrayList<SpokenLanguageModel> spokenLanguages;
    ArrayList<ProficiencyModel> proficiencyModels;
    ArrayList<SpokenLanguageModel> selectedLanguages = new ArrayList<>();
    private RecyclerView recyclerView;
    AddedLanguageAdapter addedLanguageAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_personal_details);
        setActionBar(R.string.str_personal_details);
        setSubTitle(R.string.str_personal_details_sub);
        initView();
        fetchActiveLanguages();
        fetchProficiencies();


    }

    private void initView() {
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        findViewById(R.id.ll_add_language).setOnClickListener(this);
        findViewById(R.id.btn_save).setOnClickListener(this);

        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
        ViewCompat.setNestedScrollingEnabled(findViewById(R.id.ns_wrapper), false);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.ll_add_language) {
            Intent intent = new Intent(this, AddLanguageActivity.class);
            intent.putParcelableArrayListExtra(KEY_SPOKEN_LANGUAGES, spokenLanguages);
            intent.putParcelableArrayListExtra(KEY_PROFICIENCIES, proficiencyModels);
            startActivityForResult(intent, SPOKEN_LANGUAGE_CODE);
        } else if (view.getId() == R.id.btn_save) {
            validateAndCallService();
        }
    }


    private void validateAndCallService() {
        AppUtils.hideSoftKeyboard(this);
        String accountName = getFieldText(R.id.et_account_name);
        String IBAN = getFieldText(R.id.et_iban);


        if (TextUtils.isEmpty(accountName)) {
            setError(R.id.et_account_name, R.string.error_empty_account_name);
            return;
        }
        if (TextUtils.isEmpty(IBAN)) {
            setError(R.id.et_iban, R.string.error_empty_iban);
            return;
        }

        if (selectedLanguages.size() == 0) {
            showSnackBar(R.string.error_add_languages);
            return;
        }

        personalDetailsUrl = AppConstants.getServerUrl(AppConstants.ADD_PERSONAL_DETAILS);

        JsonArray jsonArray = new JsonArray();
        for (SpokenLanguageModel spokenLanguageModel : selectedLanguages) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("languageId", spokenLanguageModel.getId());
            jsonObject.addProperty("proficiencyId", spokenLanguageModel.getProficiencyId());
            jsonArray.add(jsonObject);
        }

        Map<String, Object> map = new HashMap<>();
        map.put("languagesList", jsonArray);
        map.put("bankIbnNumber", IBAN);
        map.put("accountHolderName", accountName);

        HttpRequestItem requestItem = new HttpRequestItem(personalDetailsUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_POST);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);

    }

    private void fetchActiveLanguages() {
        fetchActiveLanguages = AppConstants.getServerUrl(AppConstants.GET_ACTIVE_LANGUAGES);
        HttpRequestItem requestItem = new HttpRequestItem(fetchActiveLanguages);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);

        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false),
                this);
        appNetworkTask.execute(requestItem);
    }

    private void fetchProficiencies() {
        fetchLevelUrl = AppConstants.getServerUrl(AppConstants.GET_LANGUAGES_LEVELS);
        HttpRequestItem requestItem = new HttpRequestItem(fetchLevelUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);

        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false),
                this);
        appNetworkTask.execute(requestItem);
    }

    private void fetchProfile() {
        fetchProfileDataUrl = AppConstants.getServerUrl(AppConstants.FETCH_PROFILE_DATA);

        HttpRequestItem requestItem = new HttpRequestItem(fetchProfileDataUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            if (response.getHttpRequestUrl().contains(GOOGLE_BASE_URL))
                return;
            JSONObject responseJson = new JSONObject(response.getResponse());
            if (responseJson.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equalsIgnoreCase(fetchActiveLanguages)) {
                    JSONArray languages = responseJson.getJSONObject("data").getJSONArray("languageList");
                    if (languages.length() != 0) {
                        spokenLanguages = new Gson().fromJson(languages.toString(),
                                new TypeToken<List<SpokenLanguageModel>>() {
                                }.getType());
                        fetchProfile();
                    }
                } else if (response.getHttpRequestUrl().equalsIgnoreCase(fetchLevelUrl)) {
                    JSONArray levels = responseJson.getJSONObject("data").getJSONArray("languageLevelList");
                    if (levels.length() != 0) {
                        proficiencyModels = new Gson().fromJson(levels.toString(),
                                new TypeToken<List<ProficiencyModel>>() {
                                }.getType());
                    }
                } else if (response.getHttpRequestUrl().equalsIgnoreCase(personalDetailsUrl)) {
                    showSnackBar(responseJson.getString("message"));
                }
                if (response.getHttpRequestUrl().equals(fetchProfileDataUrl)) {
                    JSONObject data = responseJson.getJSONObject("data");
                    setData(data);

                }
            } else
                showSnackBar(responseJson.getString("message"));
        } catch (JSONException e) {
            Logger.error(false, e);
        }
    }

    private void setData(JSONObject data) throws JSONException {
        findViewById(R.id.personal_info_wrapper).setVisibility(View.VISIBLE);
        findViewById(R.id.btn_save).setVisibility(View.VISIBLE);
        String accountHolderName = data.getString("accountHolderName");
        String bankIbnNumber = data.getString("bankIbnNumber");

        findErrorFieldById(R.id.et_account_name).setText(accountHolderName);
        findErrorFieldById(R.id.et_iban).setText(bankIbnNumber);
        JSONArray languages = data.getJSONArray("languages");
        for (int i = 0; i < languages.length(); i++) {
            String languageId = languages.getJSONObject(i).getString("languageId");
            String profName = languages.getJSONObject(i).getString("proficiencyLevel");
            String profId = languages.getJSONObject(i).getString("proficiencyId");

            for (SpokenLanguageModel spokenLanguage : spokenLanguages) {
                if (spokenLanguage.getId().equals(languageId)) {
                    spokenLanguage.setAdded(true);
                    spokenLanguage.setProficiencyName(profName);
                    spokenLanguage.setProficiencyId(profId);
                }
            }

        }
        setSelectedLanguages(spokenLanguages);
    }


    private void moveToNextActivity() {
        Intent intent = new Intent(this, SignUpCompletedActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SPOKEN_LANGUAGE_CODE) {
                spokenLanguages = data.getExtras().getParcelableArrayList(AppConstants.KEY_SPOKEN_LANGUAGES);
                setSelectedLanguages(spokenLanguages);
            }
        }
    }

    private void setSelectedLanguages(ArrayList<SpokenLanguageModel> spokenLanguages) {
        selectedLanguages = new ArrayList<>();
        for (SpokenLanguageModel spokenLanguage : spokenLanguages) {
            if (spokenLanguage.isAdded()) {
                selectedLanguages.add(spokenLanguage);
            }
        }
        addedLanguageAdapter = new AddedLanguageAdapter(this, new ArrayList<BaseItem>(selectedLanguages), this);
        recyclerView.setAdapter(addedLanguageAdapter);
    }

    int selectedPosition;

    @Override
    public void onRecyclerViewItemClick(BaseRecyclerViewHolder holder) {
        super.onRecyclerViewItemClick(holder);
        selectedPosition = holder.getAdapterPosition();
        SpokenLanguageModel spokenLanguageModel = selectedLanguages.get(selectedPosition);
        DialogLanguageProficiency.newInstance(this, proficiencyModels, spokenLanguageModel, callback).show();

    }

    ProficiencySelectionCallback callback = new ProficiencySelectionCallback() {
        @Override
        public void onAddLanguage(ProficiencyModel proficiencyModel) {
            SpokenLanguageModel selectedModel = selectedLanguages.get(selectedPosition);
            selectedModel.setAdded(true);
            selectedModel.setProficiencyName(proficiencyModel.getLabel());
            selectedModel.setProficiencyId(proficiencyModel.getId());
            addedLanguageAdapter.notifyItemChanged(selectedPosition);
        }
    };

}
