package com.apps.ondemand.app.ui.adapters;

import static com.apps.ondemand.common.utils.AppConstants.POND_CURRENCY;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.Empty;
import com.apps.ondemand.app.data.models.ScheduleJobModel;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.DateUtils;
import com.apps.ondemand.common.utils.VizImageView;

import java.util.List;


public class MyBidsJobAdapter extends BaseRecyclerViewAdapter {

    Context context;

    public MyBidsJobAdapter(Context context, List<BaseItem> items, OnRecyclerViewItemClickListener itemClickListener) {
        super(items, itemClickListener);
        this.context = context;
    }

    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        BaseRecyclerViewHolder holder;
        if (viewType == BaseItem.ITEM_JOB) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_bid_expired_job, parent, false);
            holder = new JobHolder(view);
        } else if (viewType == BaseItem.ITEM_EMPTY) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_job_empty, parent, false);
            holder = new EmptyViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_progress, parent, false);
            holder = new ProgressViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder holder, int position) {
        if (holder instanceof JobHolder) {
            JobHolder jobHolder = (JobHolder) holder;
            ScheduleJobModel scheduleJobModel = (ScheduleJobModel) getItemAt(position);
            jobHolder.tvJobId.setText(scheduleJobModel.getJobIdIdentifier());
            jobHolder.tvUserName.setText(scheduleJobModel.getName());
            jobHolder.ivUserImage.setImage(scheduleJobModel.getProfileImage(), false);
            jobHolder.rbUserRating.setRating(scheduleJobModel.getAvgRating());
            jobHolder.tvRating.setText(String.valueOf(AppUtils.formatRating(scheduleJobModel.getAvgRating())));
            jobHolder.tvJobName.setText(scheduleJobModel.getServiceName());
            jobHolder.tvTime.setText(DateUtils.convertDate(AppUtils.GetTimeForForApp(scheduleJobModel.getExpectedJobStartTime()), DateUtils.DATE_SCHEDULE_JOB));

            if (scheduleJobModel.getJobDurationKnown()){
                jobHolder.tvDuration.setText(scheduleJobModel.getJobDurationInMinutes() + "hr " + context.getString(R.string.str_job));
            }else {
                jobHolder.tvDuration.setText(context.getString(R.string.str_fixed_job));
            }
            jobHolder.tvLocation.setText(scheduleJobModel.getPrimaryAddress());
            jobHolder.setJobStatus(scheduleJobModel.getBidExpired(),scheduleJobModel.getBidAccepted(),scheduleJobModel.getBidAmount());
        } else if (holder instanceof EmptyViewHolder) {
            EmptyViewHolder emptyViewHolder = (EmptyViewHolder) holder;
            Empty empty = (Empty) getItemAt(position);
            emptyViewHolder.textViewEmpty.setText(empty.getMessage());

        }

    }


    private class JobHolder extends BaseRecyclerViewHolder {


        private LinearLayout llJob;
        private TextView tvStatus;
        private TextView bidAmount;
        private VizImageView ivUserImage;
        private TextView tvUserName;
        private TextView tvRating;
        private RatingBar rbUserRating;
        private TextView tvJobId;
        private TextView tvJobName;
        private TextView tvTime;
        private TextView tvDuration;
        private TextView tvLocation;

        public JobHolder(View view) {
            super(view);
            llJob = (LinearLayout) view.findViewById(R.id.ll_job);
            tvJobId = (TextView) view.findViewById(R.id.tv_job_id);
            ivUserImage = view.findViewById(R.id.iv_profile_image);
            tvUserName = view.findViewById(R.id.tv_profile_name);
            rbUserRating = view.findViewById(R.id.rb_profile_rating);
            tvRating = view.findViewById(R.id.tv_profile_rating);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            bidAmount = (TextView) view.findViewById(R.id.tv_bid_amount);
            tvJobName = (TextView) view.findViewById(R.id.tv_job_name);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
            tvDuration = (TextView) view.findViewById(R.id.tv_duration);
            tvLocation = (TextView) view.findViewById(R.id.tv_location);
            llJob.setOnClickListener(this);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return JobHolder.this;
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            if (getItemClickListener() != null)
                getItemClickListener().onRecyclerViewItemClick(JobHolder.this);
        }

        public void setJobStatus(boolean isBidExpired,boolean isBidAccepted,float spJobStatus) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_blue);
                if (isBidExpired && !isBidAccepted){
                    tvStatus.setBackgroundResource(R.drawable.bg_listing_status_red);
                    tvStatus.setText(R.string.bid_expired);
                }else if (isBidExpired && isBidAccepted){
                    tvStatus.setText(R.string.str_status_accepted);
                }else   if (!isBidExpired && !isBidAccepted){
                    tvStatus.setText(R.string.str_status_pending);
                }
                bidAmount.setText(POND_CURRENCY+""+spJobStatus);
        }
    }

    public class ProgressViewHolder extends BaseRecyclerViewHolder {
        private ProgressViewHolder(View view) {
            super(view);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    public class EmptyViewHolder extends BaseRecyclerViewHolder {
        private TextView textViewEmpty;
        private TextView tvPostJob;

        private EmptyViewHolder(View view) {
            super(view);
            textViewEmpty = view.findViewById(R.id.tv_message);
            tvPostJob = view.findViewById(R.id.tv_post_job);
            tvPostJob.setOnClickListener(this);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }


        @Override
        public void onClick(View v) {
            super.onClick(v);
            if (getItemClickListener() != null)
                getItemClickListener().onRecyclerViewChildItemClick(EmptyViewHolder.this, v.getId());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getItemAt(position).getItemType();
    }
}
