package com.apps.ondemand.app.ui.main;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.apps.ondemand.R;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.apps.ondemand.common.utils.AppConstants.KEY_CONFIGURATION;
import static com.apps.ondemand.common.utils.AppConstants.KEY_SHOW_TERMS;
import static com.apps.ondemand.common.utils.AppConstants.TYPE_SP;


public class TermsPrivacyActivity extends BaseActivity {
    boolean showTerms = false;
    private String getTermAndPrivacyUrl = "";
    private WebView wv1;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_terms_privacy);
        showTerms = getIntent().getBooleanExtra(KEY_SHOW_TERMS, false);
        if (showTerms) {
            url = "https://www.easefix.com/terms";
        } else {
            url = "https://www.easefix.com/privacy";
        }
        initView();
        fetchTermAndPrivacy();
//        loadUrl();
    }

//    private void loadUrl() {
//        wv1 = (WebView) findViewById(R.id.webview);
//        WebSettings webSettings = wv1.getSettings();
//        webSettings.setJavaScriptEnabled(true);
//        wv1.loadUrl(url);
//    }


    private void fetchTermAndPrivacy() {

        getTermAndPrivacyUrl = AppConstants.getServerUrl(AppConstants.GET_TERM_PRIVACY);
        getTermAndPrivacyUrl = KEY_CONFIGURATION;
//        Map<String, Object> map = new HashMap<>();
//        map.put("userType", TYPE_SP);
        HttpRequestItem requestItem = new HttpRequestItem(getTermAndPrivacyUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
//        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    private void initView() {
        if (showTerms) {
            setActionBar(R.string.str_terms_condition);
        } else {
            setActionBar(R.string.str_privacy_policy);
        }
        setSubTitle(R.string.str_privacy_term_sub);
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            JSONObject responseJson = new JSONObject(response.getResponse());
//            if (responseJson.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equals(getTermAndPrivacyUrl)) {
                    JSONObject data = responseJson.getJSONObject("data");
//                    String privacyDesc = data.getJSONObject("privacyPolicy").getString("description");
                    String termsDesc = data.getJSONObject("attributes").getString("terms_and_conditions");
                    String privacyDesc = data.getJSONObject("attributes").getString("privacy_policy");
                    setData(privacyDesc, termsDesc);
                }
//            } else
//                showSnackBar(responseJson.getString("message"));
        } catch (JSONException e) {
            Logger.error(false, e);
        }
    }

    private void setData(String privacyDesc, String termsDesc) {
        if (showTerms) {
            findTextViewById(R.id.tv_terms_privacy_detail).setText(Html.fromHtml(termsDesc));
        } else {
            findTextViewById(R.id.tv_terms_privacy_detail).setText(Html.fromHtml(privacyDesc));
        }
    }
}
