package com.apps.ondemand.app.communication;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.apps.ondemand.BuildConfig;
import com.apps.ondemand.R;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.app.data.models.LocData;
import com.apps.ondemand.app.data.preferences.PreferenceUtils;
import com.apps.ondemand.app.data.preferences.SharedPreferenceManager;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NotificationManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Manager;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.Transport;
import io.socket.engineio.client.transports.WebSocket;

/**
 * Created by T520 on 6/4/2015.
 */
public class SocketService extends Service implements LocationListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    private static final boolean IS_TIME_BASED = false;
    private static final int FORE_GROUND_NOTIFICATION_ID = 10000;
    public static final String TAG = "SocketService";
    private static final int DATA_LIST_SIZE = 1000;
    public static final String BROADCAST_NOTIFICATION = BuildConfig.APPLICATION_ID;
    public static final String BROADCAST_INTENT_TYPE = "Type";
    public static final String BROADCAST_INTENT_DATA = "data";
    public static final String BROADCAST_JOB_ID = "job_id";

    // Key of any event send from the server
    public static final String KEY_AMOUNT_TRANSFERRED = "amountTransferred";
    public static final String KEY_JOB_CHAT = "messageSendingToReceiverKey";
    public static final String KEY_JOB_OFFER = "userSendOffer";
    public static final String KEY_JOB_CANCEL = "userCancelJob";
    public static final String KEY_ADMIN_APPROVED = "adminApprovedAccount";
    public static final String KEY_ADMIN_REJECTED = "adminRejectedAccount";
    public static final String KEY_ADMIN_JOB_COMPLETE = "adminCompleteJob";

    //Twilio Implementation
    public static final String KEY_CALL_RECEIVED = "callReceivedAcknowledgement";

    //end Twilio

    /* keys for emitting data to server */
    public static final String EMIT_KEY_CHAT = "messageSendingKey";
    public static final String EMIT_KEY_RECEIVED = "markAsRead";
    public static final String KEY_LOCATION_UPDATE = "locationUpdate";
    public static final String KEY_READ_NOTIFICATION = "readNotification";
    public static final String KEY_RATING_REMAINDER_RECEIVED = "shownSpDialogue";
    public static final String KEY_OFFER_ACCEPTED = "spOfferAccepted";
    public static final String KEY_JOB_DISPUTED = "userJobDisputed";

    public int boundCount = 0;    // determine if any activity component is bind to the service (If the App is live or not)
    private final SocketBinder mBinder = new SocketBinder();
    private Socket mSocket = null;
    private boolean isSocketConnected = false;  // boolean to identify socket connection status
    public boolean isSocketConnecting = false;  // boolean to identify socket connecting status
    public boolean isLocationRequestOn = false; //boolean to identify location requests

    private final Emitter.Listener onJobDisputed = args -> {
        JSONObject data = (JSONObject) args[0];
        try {
            Log.e("Socket Received", KEY_JOB_DISPUTED + data.toString());
            String jobId = data.getJSONObject("socket").getJSONObject("resource").getString("jobId");
            String message = data.getJSONObject("socket").getJSONObject("resource").getString("message");
            broadcastResults(KEY_JOB_DISPUTED, jobId, message, data.getJSONObject("socket").getJSONObject("resource").toString());
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    };

    private final Emitter.Listener onJobChat = args -> {
        JSONObject data = (JSONObject) args[0];
        try {
            Log.e(TAG, KEY_JOB_CHAT + data.toString());
            JSONObject resource = data.getJSONObject("socket").getJSONObject("resource");
            String jobId = data.getJSONObject("socket").getJSONObject("resource").getString("jobId");
            String title = data.getJSONObject("socket").getJSONObject("resource").getString("title");
            broadcastResults(KEY_JOB_CHAT, jobId, title, resource.toString());
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    };

    private final Emitter.Listener onAmountTransferred = args -> {
        JSONObject data = (JSONObject) args[0];
        try {
            Log.e(TAG, KEY_AMOUNT_TRANSFERRED + data.toString());
            JSONObject resource = data.getJSONObject("socket").getJSONObject("resource");
            String jobId = data.getJSONObject("socket").getJSONObject("resource").getString("jobId");
            String title = data.getJSONObject("socket").getJSONObject("resource").getString("title");
            String message = data.getJSONObject("socket").getJSONObject("resource").getString("message");
            broadcastResults(KEY_JOB_DISPUTED, jobId, message, data.getJSONObject("socket").getJSONObject("resource").toString());
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    };

    private final Emitter.Listener onNotificationReceived = args -> {
        broadcastResult(KEY_CALL_RECEIVED, "", "Call Accepted");
    };
//    private Emitter.Listener onStartVideoSession = new Emitter.Listener() {
//        @Override
//        public void call(Object... args) {
//            JSONObject data = (JSONObject) args[0];
//            try {
//                Log.e("Socket Received",  KEY_START_SESSION+ data.toString());
////                String jobId = data.getJSONObject("socket").getJSONObject("resource").getString("jobId");
//                String message = data.getJSONObject("socket").getJSONObject("resource").getString("message");
//                broadcastResultsOne(KEY_START_SESSION,   data.getJSONObject("socket").getJSONObject("resource").toString());
//            } catch (JSONException e) {
//                Logger.caughtException(e);
//            }
//        }
//    };
//   private Emitter.Listener onRejectVideoSession = new Emitter.Listener() {
//        @Override
//        public void call(Object... args) {
//            JSONObject data = (JSONObject) args[0];
//            try {
//                Log.e("Socket Received",  KEY_START_SESSION+ data.toString());
////                String jobId = data.getJSONObject("socket").getJSONObject("resource").getString("jobId");
//                String message = data.getJSONObject("socket").getJSONObject("resource").getString("message");
//                broadcastResultsOne(KEY_REJECT_SESSION,   data.getJSONObject("socket").getJSONObject("resource").toString());
//            } catch (JSONException e) {
//                Logger.caughtException(e);
//            }
//        }
//    };
//   private Emitter.Listener onBusyVideoSession = new Emitter.Listener() {
//        @Override
//        public void call(Object... args) {
//            JSONObject data = (JSONObject) args[0];
//            try {
//                Log.e("Socket Received",  KEY_START_SESSION+ data.toString());
////                String jobId = data.getJSONObject("socket").getJSONObject("resource").getString("jobId");
//                String message = data.getJSONObject("socket").getJSONObject("resource").getString("message");
//                broadcastResultsOne(KEY_BUSY_SESSION,   data.getJSONObject("socket").getJSONObject("resource").toString());
//            } catch (JSONException e) {
//                Logger.caughtException(e);
//            }
//        }
//    };

    private void broadcastResult(String broadcast_type, String jobId, String data) {
        // we a component bounded with service
        if (boundCount > 0) {
            Log.e(TAG, "Broadcast: " + data);
            Intent intent = new Intent(BROADCAST_NOTIFICATION);
            intent.putExtra(BROADCAST_INTENT_TYPE, broadcast_type);
            intent.putExtra(BROADCAST_JOB_ID, jobId);
            intent.putExtra(BROADCAST_INTENT_DATA, data);
            intent.putExtra(AppConstants.FROM_WHICH_COMPONENT, AppConstants.COMPONENT_SOCKET);
            sendBroadcast(intent);
        }
    }
    private final Emitter.Listener onOfferAccepted = args -> {
        JSONObject data = (JSONObject) args[0];
        try {
            Log.e("Socket Received", KEY_OFFER_ACCEPTED + data.toString());
            String jobId = data.getJSONObject("socket").getJSONObject("resource").getString("jobId");
            String message = data.getJSONObject("socket").getJSONObject("resource").getString("message");
            broadcastResults(KEY_OFFER_ACCEPTED, jobId, message, data.getJSONObject("socket").getJSONObject("resource").toString());
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    };

    private final Emitter.Listener onJobOffer = args -> {
        JSONObject data = (JSONObject) args[0];
        try {
            Log.e(TAG, KEY_JOB_OFFER + data.toString());
            String jobId = data.getJSONObject("socket").getJSONObject("resource").getString("jobId");
            String message = data.getJSONObject("socket").getJSONObject("resource").getString("message");
            broadcastResults(KEY_JOB_OFFER, jobId, message, data.getJSONObject("socket").getJSONObject("resource").toString());
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    };
    private final Emitter.Listener onJobCancel = args -> {
        JSONObject data = (JSONObject) args[0];
        try {
            Log.e(TAG, KEY_JOB_CANCEL + data.toString());
            String jobId = data.getJSONObject("socket").getJSONObject("resource").getString("jobId");
            String message = data.getJSONObject("socket").getJSONObject("resource").getString("message");
            broadcastResults(KEY_JOB_CANCEL, jobId, message, data.getJSONObject("socket").getJSONObject("resource").toString());
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    };

    private final Emitter.Listener onAdminApproved = args -> {
        JSONObject data = (JSONObject) args[0];
        try {
            UserManager.setVerified(true);
            Log.e(TAG, KEY_ADMIN_APPROVED + data.toString());
            String message = data.getJSONObject("socket").getJSONObject("resource").getString("message");
            broadcastResults(KEY_ADMIN_APPROVED, "", message, data.getJSONObject("socket").getJSONObject("resource").toString());
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    };

    private final Emitter.Listener onAdminRejected = args -> {
        JSONObject data = (JSONObject) args[0];
        try {
            UserManager.setVerified(false);
            Log.e(TAG, KEY_ADMIN_REJECTED + data.toString());
            String message = data.getJSONObject("socket").getJSONObject("resource").getString("message");
            broadcastResults(KEY_ADMIN_REJECTED, "", message, data.getJSONObject("socket").getJSONObject("resource").toString());
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    };

    private final Emitter.Listener onAdminJobComplete = args -> {
        JSONObject data = (JSONObject) args[0];
        try {
            Log.e(TAG, KEY_ADMIN_JOB_COMPLETE + data.toString());
            String message = data.getJSONObject("socket").getJSONObject("resource").getString("message");
            broadcastResults(KEY_ADMIN_JOB_COMPLETE, "", message, data.getJSONObject("socket").getJSONObject("resource").toString());
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    };

    /**
     * Receiver class for socket disconnected event
     */
    private final Emitter.Listener onDisconnect = args -> {
        socketDisconnectListeners();
        isSocketConnected = false;
        isSocketConnecting = false;
        Log.e(TAG, "Disconnected Successfully");
    };

    /**
     * Receiver class for socket connected event
     */
    private final Emitter.Listener onConnect = args -> {
        if (!isSocketConnected)
            socketConnectListeners();
        isSocketConnected = true;
        isSocketConnecting = false;
        createGoogleApiClient();
    };

    /**
     * Receiver for socket failed events
     */
    private final Emitter.Listener onError = args -> {
        isSocketConnecting = false;
        Log.e(TAG, "Connection Failed " + args.toString());

    };

    private GoogleApiClient mGoogleApiClient;
    LocData data;
    private boolean isOnline = false;
    private boolean mustUpdate = false;

    public SocketService() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (mSocket != null)
            isSocketConnected = mSocket.connected();
        if (mSocket == null) {
            // Initialize and open the Socket connection
            initSocketConnection();
        } else if (!isSocketConnected() && !isSocketConnecting) {
            // Initialize and open the Socket connection
            mSocket.connect();
        }
        data = new LocData();
        {

            Notification.Builder builder = new Notification.Builder(this, BuildConfig.APPLICATION_ID)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(getString(R.string.foreground_service_desc))
                    .setAutoCancel(true);

            android.app.NotificationManager notificationManager =
                    (android.app.NotificationManager) getBaseContext().getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(getNotificationChannel(getBaseContext()));

            Notification notification = builder.build();
            startForeground(FORE_GROUND_NOTIFICATION_ID, notification);

        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.hasExtra(AppConstants.IS_ONLINE_EXTRA_KEY)) {
            isOnline = intent.getBooleanExtra(AppConstants.IS_ONLINE_EXTRA_KEY, false);
        } else {
            isOnline = false;
        }
        isOnline = UserManager.isOnline();
        if (mSocket != null) {
            isSocketConnected = mSocket.connected();
            if (!isSocketConnected() && !isSocketConnecting) {
                mSocket.connect();
            }
        } else {
            initSocketConnection();
        }

        if (isSocketConnected()) {
            isLocationRequestOn = false;
            createGoogleApiClient();
        }

        if (isOnline) {
            {

                Notification.Builder builder = new Notification.Builder(this, BuildConfig.APPLICATION_ID)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(getString(R.string.foreground_service_desc))
                        .setAutoCancel(true);

                android.app.NotificationManager notificationManager =
                        (android.app.NotificationManager) getBaseContext().getSystemService(NOTIFICATION_SERVICE);
                notificationManager.createNotificationChannel(getNotificationChannel(getBaseContext()));


                Notification notification = builder.build();
                startForeground(FORE_GROUND_NOTIFICATION_ID, notification);

            }
        } else {
            stopForeground(true);
        }
        return START_NOT_STICKY;
    }


    private static NotificationChannel getNotificationChannel(Context context) {
        CharSequence name = context.getString(R.string.app_name);// The user-visible name of the channel.
        int importance = android.app.NotificationManager.IMPORTANCE_HIGH;
        return new NotificationChannel(BuildConfig.APPLICATION_ID, name, importance);
    }

    @Override
    public IBinder onBind(Intent intent) {
        ++boundCount;
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        boundCount = 0;
        return true;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        ++boundCount;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopSocket();
        stopGoogleLocationUpdates();
        Log.e(TAG, "Stop Successfully");
    }

    /**
     * Initialize IO Socket Object
     */
    private void initSocketConnection() {
        try {
            IO.Options options = new IO.Options();
            options.timeout = 5000;
            options.secure = true;
            options.transports=new String[]{WebSocket.NAME};
            mSocket = IO.socket(BuildConfig.SOCKET_SERVER_ADDRESS, options);


            connectSocket();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Connect socket with the server
     */
    private void connectSocket() {
        try {
            mSocket.io().on(Manager.EVENT_TRANSPORT, args -> {
                Transport transport = (Transport) args[0];
                transport.on(Transport.EVENT_REQUEST_HEADERS, args1 -> {
                    @SuppressWarnings("unchecked")
                    Map<String, List<String>> headers = (Map<String, List<String>>) args1[0];
                    String cookieValue = SharedPreferenceManager.getInstance().read(PreferenceUtils.COOKIE, "");
                    if (AppUtils.ifNotNullEmpty(cookieValue)) {
                        headers.put("Cookie", Arrays.asList(cookieValue));
                        headers.put("appType", Arrays.asList("android"));
                        Log.d(TAG, "Cookie " + cookieValue);
                    }
                });
            });
            mSocket.connect();

            isSocketConnecting = true;
            mSocket.on(Socket.EVENT_CONNECT, onConnect);
            mSocket.on(Socket.EVENT_CONNECT_ERROR, onError);
//            mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onError);
//            mSocket.on(Socket.EVENT_ERROR, onError);
//            mSocket.on(Socket.EVENT_CONNECTING, new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    isSocketConnecting = true;
//
//                }
//            });
            Log.e(TAG, "Connection Success connectSocket()");
            mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        } catch (Exception e) {
            isSocketConnected = isSocketConnecting = false;
            e.printStackTrace();
        }
    }

    /**
     * Called to stop socket connection and all listeners associated with it
     */
    public void stopSocket() {
        if (mSocket != null) {
            mSocket.disconnect();
            socketDisconnectListeners();
            mSocket.off(Socket.EVENT_CONNECT, onConnect);
            mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
            mSocket.off(Socket.EVENT_CONNECT_ERROR, onError);
//            mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onError);
//            mSocket.off(Socket.EVENT_ERROR, onError);
//            mSocket.off(Socket.EVENT_CONNECTING);
            isSocketConnected = false;
            isSocketConnecting = false;
            Log.e(TAG, "Stop Successfully stopSocket()");

        }
    }

    /**
     * Method to send message through socket connection
     *
     * @param key     message key
     * @param message message value
     */
    public void emitResults(final String key, JSONObject message) {
        if (mSocket != null) {
            mSocket.emit(key, message, (Ack) args -> {
                if (args.length > 0) {

                    Log.e("LOCATION_UPDATE", "call: " + args.toString());
                }
            });
        }
    }

    /**
     * Method to send message through socket connection
     *
     * @param key message key
     * @param loc LatLng value
     */

    public void emitResults(final String key, final Map<String, Double> loc, boolean mustUpdate) {
        this.mustUpdate = mustUpdate;
        try {
            addToArrayList(loc);
            data.setSpProfileId(UserManager.getUserId());
            JSONObject message = new JSONObject(new Gson().toJson(data));
            if (isSocketActive()) {
                Log.e("SocketService", message.toString());
                mSocket.emit(key, message, new Ack() {
                    @Override
                    public void call(Object... args) {
                        if (args.length > 0) {
                            broadcastResults(KEY_LOCATION_UPDATE, "", new Gson().toJson(loc));
                            data.getCoordinates().clear();
                        } else {
                            initSocketConnection();
                        }
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void addToArrayList(Map<String, Double> loc) {
        Log.d(TAG, "addToArrayList: " + data.getCoordinates().size());
        if (loc.size() < DATA_LIST_SIZE) {
            data.getCoordinates().add(loc);
        } else {
            int index = new Random().nextInt(DATA_LIST_SIZE);
            data.getCoordinates().remove(index);
            data.getCoordinates().add(loc);
        }
    }


    /**
     * Method to send message through socket acknowledgement mechanism
     *
     * @param key     message key
     * @param message message value
     * @param ack     acknowledgement callback
     */
    public void emitResults(final String key, JSONObject message, Ack ack) {
        if (mSocket != null) {
            mSocket.emit(key, message, ack);
        }
    }

    /**
     * Method to send message through RideService connection
     *
     * @param key     message key
     * @param message message value
     */
    public void emitResults(String key, JSONArray message) {
        if (isSocketActive())
            mSocket.emit(key, message);
    }

    /**
     * Method to broadcast results from this Service to the Bind application component
     *
     * @param broadcast_type key of the broadcast message
     * @param data           value of the broadcast message
     */
    private void broadcastResults(String broadcast_type, String jobId, String data) {
        // we a component bounded with service
        if (boundCount > 0) {
            Log.e(TAG, "Broadcast: " + data);
            Intent intent = new Intent(BROADCAST_NOTIFICATION);
            intent.putExtra(BROADCAST_INTENT_TYPE, broadcast_type);
            intent.putExtra(BROADCAST_JOB_ID, jobId);
            intent.putExtra(BROADCAST_INTENT_DATA, data);
            intent.putExtra(AppConstants.FROM_WHICH_COMPONENT, AppConstants.COMPONENT_SOCKET);
            sendBroadcast(intent);
        }
    }

    /**
     * Method to broadcast job events from this Service to the Bind application component
     *
     * @param broadcast_type key of the broadcast message
     * @param data           value of the broadcast message
     */
    private void broadcastResults(String broadcast_type, String jobId, String message, String data) {
        // we a component bounded with service
        if (boundCount > 0) {
            Log.e(TAG, "Broadcast: " + data);
            Intent intent = new Intent(BROADCAST_NOTIFICATION);
            intent.putExtra(BROADCAST_INTENT_TYPE, broadcast_type);
            intent.putExtra(BROADCAST_JOB_ID, jobId);
            intent.putExtra(BROADCAST_INTENT_DATA, data);
            intent.putExtra(AppConstants.FROM_WHICH_COMPONENT, AppConstants.COMPONENT_SOCKET);
            sendBroadcast(intent);
            NotificationManager.createNotification(getBaseContext(), null, getString(R.string.app_name), message);
        } else {
            NotificationManager.createNotification(getBaseContext(), jobId, broadcast_type, getString(R.string.app_name), message, data);
        }
    }

    /**
     * Add all events listeners here
     */
    private void socketConnectListeners() {
        mSocket.on(KEY_JOB_CHAT, onJobChat);
        mSocket.on(KEY_JOB_DISPUTED, onJobDisputed);
        mSocket.on(KEY_JOB_OFFER, onJobOffer);
        mSocket.on(KEY_JOB_CANCEL, onJobCancel);
        mSocket.on(KEY_OFFER_ACCEPTED, onOfferAccepted);
        mSocket.on(KEY_ADMIN_APPROVED, onAdminApproved);
        mSocket.on(KEY_ADMIN_REJECTED, onAdminRejected);
        mSocket.on(KEY_ADMIN_JOB_COMPLETE, onAdminJobComplete);
        mSocket.on(KEY_CALL_RECEIVED, onNotificationReceived);
        mSocket.on(KEY_AMOUNT_TRANSFERRED, onAmountTransferred);

        Log.e(TAG, "Start Connection Listeners");
        if (mustUpdate && data.getCoordinates().size() > 0) {
            int lastIndex = data.getCoordinates().size() - 1;
            Map<String, Double> last = data.getCoordinates().get(lastIndex);
            data.getCoordinates().remove(lastIndex);
            emitResults(KEY_LOCATION_UPDATE, last, false);
        }

    }

    /**
     * Stop listening to events here
     */
    private void socketDisconnectListeners() {
        if (mSocket != null) {
            Log.e(TAG, "Stop Connection Listener");
            mSocket.off(KEY_JOB_CHAT, onJobChat);
            mSocket.off(KEY_JOB_OFFER, onJobOffer);
            mSocket.off(KEY_JOB_DISPUTED, onJobDisputed);
            mSocket.off(KEY_OFFER_ACCEPTED, onOfferAccepted);
            mSocket.off(KEY_JOB_CANCEL, onJobCancel);
            mSocket.off(KEY_ADMIN_APPROVED, onAdminApproved);
            mSocket.off(KEY_ADMIN_REJECTED, onAdminRejected);
            mSocket.off(KEY_ADMIN_JOB_COMPLETE, onAdminJobComplete);
            mSocket.off(KEY_AMOUNT_TRANSFERRED, onAmountTransferred);
            mSocket.off(KEY_CALL_RECEIVED, onNotificationReceived);

        }
    }


    public void createGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            mGoogleApiClient.connect();
        } else if (!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting())
            mGoogleApiClient.connect();
        else if (mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting())
            startGoogleLocationUpdates();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startGoogleLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Map<String, Double> map = new HashMap<>();
        map.put("latitude", location.getLatitude());
        map.put("longitude", location.getLongitude());
        if (isOnline) {
            emitResults(KEY_LOCATION_UPDATE, map, false);
        }
    }

    /**
     * when ever ride starts call this method to start location updates
     */
    public void startGoogleLocationUpdates() {
        if (!isLocationRequestOn && isOnline) {
            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            if (IS_TIME_BASED) {
                mLocationRequest.setInterval(10000);
                mLocationRequest.setFastestInterval(10000);
            } else {
                mLocationRequest.setSmallestDisplacement(50);
                mLocationRequest.setInterval(0);
            }
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this, Looper.getMainLooper());
                isLocationRequestOn = true;
            }
        }
    }

    /**
     * when ever ride completes call this method to stop location updates
     */
    public void stopGoogleLocationUpdates() {
        if (isLocationRequestOn) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            isLocationRequestOn = false;
        }

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();

        mGoogleApiClient = null;
    }


    public boolean isSocketConnected() {
        return mSocket != null && mSocket.connected();
    }

    /**
     * Socket IO service binder class
     */
    public class SocketBinder extends Binder {
        public SocketService getService() {
            return SocketService.this;
        }
    }

    private boolean isSocketActive() {
        if (mSocket == null) {
            initSocketConnection();
            return false;
        }
        if (!mSocket.connected() && !isSocketConnecting) {
            mSocket.connect();
            return false;
        }
        return true;
    }


}
