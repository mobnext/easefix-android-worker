package com.apps.ondemand.app.ui.interfaces;

import androidx.annotation.StringRes;

public interface ResourceSpanListener {
    public void onSpanClicked(@StringRes int clicked);
}
