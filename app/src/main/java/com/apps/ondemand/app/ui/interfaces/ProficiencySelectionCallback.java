package com.apps.ondemand.app.ui.interfaces;

import com.apps.ondemand.app.data.models.ProficiencyModel;
import com.apps.ondemand.app.data.models.ReasonModel;
import com.apps.ondemand.app.data.models.SpokenLanguageModel;

public interface ProficiencySelectionCallback {
    void onAddLanguage(ProficiencyModel proficiencyModel);
}
