package com.apps.ondemand.app.db.core;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.NonNull;

import com.apps.ondemand.app.db.DBConstants;
import com.apps.ondemand.app.db.Queries;


/**
 * Created on 2016-12-03 15:01.
 *
 * @author Rana
 */
public class SQliteHelper extends SQLiteOpenHelper {

    private SQliteHelper(Context ctx, String name, int version) {
        super(ctx, name, null, version);
    } // SQliteHelper

    @NonNull
    public static SQliteHelper newInstance(Context ctx, String name, int version) {
        return new SQliteHelper(ctx, name, version);
    }

    @Override
    public void onCreate(@NonNull SQLiteDatabase db) {
        db.execSQL(Queries.createLocalisation());

    } // onCreate

    /**
     * Drop all tables on upgrade and create a backup of user table.
     * Restore user table in (onCreate).
     */
    @Override
    public void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTables(db);
        onCreate(db);
    } // onUpgrade

    private void dropTables(@NonNull SQLiteDatabase db) {
        db.execSQL(Queries.drop(DBConstants.Localisation.TABLE_NAME));
    } // dropTables

} // SQliteHelper
