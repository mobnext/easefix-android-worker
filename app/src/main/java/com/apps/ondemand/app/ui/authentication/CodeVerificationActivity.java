package com.apps.ondemand.app.ui.authentication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.apps.ondemand.BuildConfig;
import com.apps.ondemand.R;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.app.ui.interfaces.ClickableSpanListener;
import com.apps.ondemand.app.ui.main.MainActivity;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.google.android.gms.tasks.OnSuccessListener;
//import com.google.firebase.iid.FirebaseInstanceId;
//import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.apps.ondemand.common.utils.AppConstants.KEY_EXTRA_COUNTRY_CODE;
import static com.apps.ondemand.common.utils.AppConstants.KEY_EXTRA_PHONE_NO;

public class CodeVerificationActivity extends BaseActivity implements View.OnClickListener {
    boolean fromSignUp;
    String countryCode;
    String phoneNo;
    String code1, code2, code3, code4;

    EditText edtVCode1;
    EditText edtVCode2;
    EditText edtVCode3;
    EditText edtVCode4;
    private String verifyCodeUrl = "";
    private String sendVerificationCodeUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_verification);
        fromSignUp = getIntent().getBooleanExtra(AppConstants.KEY_EXTRA_IS_SIGN_UP, false);
        countryCode = getIntent().getStringExtra(KEY_EXTRA_COUNTRY_CODE);
        phoneNo = getIntent().getStringExtra(KEY_EXTRA_PHONE_NO);
        initView();
        setData(fromSignUp);
        setTextChangeListener();
    }

    public void setData(boolean fromSignUp) {
        this.fromSignUp = fromSignUp;
        findTextViewById(R.id.tv_phone_no_desc).setText(format(R.string.str_verify_no_desc, getFullPhoneNo()));
        if (fromSignUp) {
            setActionBar(R.string.str_sign_up);
        } else {
            setActionBar(R.string.str_sign_in);
        }
        AppUtils.setSpannableTextView(this,
                getString(R.string.str_change_no_desc) + " " + getString(R.string.str_change_number),
                getString(R.string.str_change_number), R.color.colorPrimaryBlue, R.id.tv_already_register, spanListener);

        setSubTitle(R.string.str_verify_mobile_no);

    }

    ClickableSpanListener spanListener = new ClickableSpanListener() {
        @Override
        public void onSpanClicked() {
            finish();
        }
    };

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_continue) {
            checkCode();
        } else if (view.getId() == R.id.tv_resend_code) {
            sendVerificationCode();
        }
    }

    private void initView() {
        findViewById(R.id.btn_continue).setOnClickListener(this);
        findViewById(R.id.tv_resend_code).setOnClickListener(this);

    }

    public String getFullPhoneNo() {
        return countryCode + "" + phoneNo;
    }


    private void setTextChangeListener() {

        edtVCode1 = findViewById(R.id.edt_code1);
        edtVCode2 = findViewById(R.id.edt_code2);
        edtVCode3 = findViewById(R.id.edt_code3);
        edtVCode4 = findViewById(R.id.edt_code4);


        edtVCode4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DEL) {
                    edtVCode4.setText("");
                    edtVCode3.requestFocus();
                }
                return false;
            }
        });
        edtVCode3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DEL) {
                    edtVCode3.setText("");
                    edtVCode2.requestFocus();
                }
                return false;
            }
        });
        edtVCode2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DEL) {
                    edtVCode2.setText("");
                    edtVCode1.requestFocus();
                }
                return false;
            }
        });


        edtVCode1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                int textLength1 = edtVCode1.getText().length();

                if (textLength1 >= 1) {
                    edtVCode2.requestFocus();
                    code1 = String.valueOf(s);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }
        });
        edtVCode2.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                int textLength1 = edtVCode2.getText().length();

                if (textLength1 >= 1) {
                    edtVCode3.requestFocus();
                    code2 = String.valueOf(s);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //edtVCode2.setText(".");
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }
        });
        edtVCode3.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                int textLength1 = edtVCode3.getText().length();

                if (textLength1 >= 1) {
                    edtVCode4.requestFocus();
                    code3 = String.valueOf(s);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }
        });
        edtVCode4.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

                code4 = String.valueOf(s);
                AppUtils.hideSoftKeyboard(CodeVerificationActivity.this);
                setContinueButton();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }
        });


        edtVCode4.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // Your piece of code on keyboard search click
                    //AppUtils.hideSoftKeyboard(context);
                    InputMethodManager imm =
                            (InputMethodManager) edtVCode4.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null && imm.isActive())
                        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                    return true;
                }
                return false;
            }
        });
    }

    private void setContinueButton() {
        AppUtils.hideSoftKeyboard(this);
        String code = findFieldById(R.id.edt_code1).getText().toString()
                + findFieldById(R.id.edt_code2).getText().toString() +
                findFieldById(R.id.edt_code3).getText().toString() +
                findFieldById(R.id.edt_code4).getText().toString();

        if (AppUtils.ifNotNullEmpty(code) && code.length() == 4) {
            findButtonById(R.id.btn_continue).setEnabled(true);
        } else {
            findButtonById(R.id.btn_continue).setEnabled(false);
        }
    }

    private void checkCode() {
        AppUtils.hideSoftKeyboard(this);
        String code = code1 + code2 + code3 + code4;

        if (AppUtils.ifNotNullEmpty(code) && code.length() == 4) {

            sendLoginCode(code);

        } else {
            showSnackBar(R.string.error_verification_code);
        }
    }

    private void sendLoginCode(final String code) {
        String verificationNode = "";
        if (fromSignUp) {
            verificationNode = AppConstants.SIGN_UP_VERIFY_CODE;
        } else {
            verificationNode = AppConstants.SIGN_IN_VERIFY_CODE;
        }
        verifyCodeUrl = AppConstants.getServerUrl(verificationNode);
        FirebaseMessaging.getInstance().getToken().addOnSuccessListener(new OnSuccessListener<String>() {
            @Override
            public void onSuccess(String s) {
                Map<String, Object> params = new HashMap<>();
                params.put("countryCode", countryCode);
                params.put("phoneNumber", phoneNo);
                params.put("userType", AppConstants.TYPE_SP);
                params.put("deviceType", AppConstants.DEVICE_TYPE);
                params.put("deviceToken", s);
                params.put("code", code);

                HttpRequestItem requestItem = new HttpRequestItem(verifyCodeUrl);
                requestItem.setHttpRequestType(NetworkUtils.HTTP_POST);
                requestItem.setParams(params);
                requestItem.setHeaderParams(AppUtils.getHeaderParams());
                AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false),
                        CodeVerificationActivity.this);
                appNetworkTask.execute(requestItem);
            }
        });
//        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
//            @Override
//            public void onSuccess(InstanceIdResult instanceIdResult) {
//                Map<String, Object> params = new HashMap<>();
//                params.put("countryCode", countryCode);
//                params.put("phoneNumber", phoneNo);
//                params.put("userType", AppConstants.TYPE_SP);
//                params.put("deviceType", AppConstants.DEVICE_TYPE);
//                params.put("deviceToken", instanceIdResult.getToken());
//                params.put("code", code);
//
//                HttpRequestItem requestItem = new HttpRequestItem(verifyCodeUrl);
//                requestItem.setHttpRequestType(NetworkUtils.HTTP_POST);
//                requestItem.setParams(params);
//                requestItem.setHeaderParams(AppUtils.getHeaderParams());
//                AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false),
//                        CodeVerificationActivity.this);
//                appNetworkTask.execute(requestItem);
//            }
//        });
    }

    private void sendVerificationCode() {
        String verificationNode = "";
        if (fromSignUp) {
            verificationNode = AppConstants.SIGN_UP_CODE;
        } else {
            verificationNode = AppConstants.SIGN_IN_CODE;
        }
        sendVerificationCodeUrl = AppConstants.getServerUrl(verificationNode);

        Map<String, Object> map = new HashMap<>();
        map.put("countryCode", countryCode);
        map.put("phoneNumber", phoneNo);
        map.put("userType", AppConstants.TYPE_SP);

        HttpRequestItem requestItem = new HttpRequestItem(sendVerificationCodeUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_POST);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            JSONObject jsonObjectResponse = new JSONObject(response.getResponse());
            int success = jsonObjectResponse.getInt("success");
            if (success == 1) {
                JSONObject data = jsonObjectResponse.getJSONObject("data");
                if (response.getHttpRequestUrl().equals(sendVerificationCodeUrl)) {
                    String code = data.getString("verificationCode");
                        showToast("Verification code is " + code);
                } else if (response.getHttpRequestUrl().equals(verifyCodeUrl)) {
                    boolean isSignUpCompleted = data.getBoolean("isSignUpCompleted");
                    int stepCompleted = data.getInt("stepCompleted");
                    UserManager.saveUserData(data);
                    UserManager.saveSession(isSignUpCompleted);
                    moveToNextActivity(isSignUpCompleted, stepCompleted);
                }
            } else {
                showSnackBar(jsonObjectResponse.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void moveToNextActivity(boolean isSignUpCompleted, int stepCompleted) {
        Intent intent = new Intent(this, SignUpDetailsActivity.class);
        if (isSignUpCompleted) {
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        } else {
            if (stepCompleted == 0) {
                intent = new Intent(this, SignUpDetailsActivity.class);
            } else if (stepCompleted == 1) {
                intent = new Intent(this, SelectServiceActivity.class);
            } else if (stepCompleted == 2) {
//                intent = new Intent(this, AddIdentityDocumentActivity.class);
//            } else if (stepCompleted == 3) {
                intent = new Intent(this, AddServiceCertificateActivity.class);
            }
//            else if (stepCompleted == 4) {
//                intent = new Intent(this, AddPersonalDetailsActivity.class);
//            }
        }
        startActivity(intent);
    }
}
