package com.apps.ondemand.app.data.models;

import com.apps.ondemand.common.business.BaseItem;

public class LanguageModel implements BaseItem {

    String languageName;
    String languageCode;
    boolean isSelected;

    public LanguageModel() {
    }

    public LanguageModel(String languageName, String languageCode, boolean isSelected) {
        this.languageName = languageName;
        this.languageCode = languageCode;
        this.isSelected = isSelected;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public int getItemType() {
        return ITEM_LANGUAGE;
    }
}
