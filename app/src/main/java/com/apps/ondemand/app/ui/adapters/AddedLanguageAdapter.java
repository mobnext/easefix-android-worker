package com.apps.ondemand.app.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.LanguageModel;
import com.apps.ondemand.app.data.models.SpokenLanguageModel;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;

import java.util.List;


public class AddedLanguageAdapter extends BaseRecyclerViewAdapter {

    Context context;

    public AddedLanguageAdapter(Context context, List<BaseItem> items, OnRecyclerViewItemClickListener itemClickListener) {
        super(items, itemClickListener);
        this.context = context;
    }

    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        BaseRecyclerViewHolder holder;
        if (viewType == BaseItem.ITEM_SPOKEN_LANGUAGE) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_added_language, parent, false);
            holder = new LanguageHolder(view);
        } else if (viewType == BaseItem.ITEM_EMPTY) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
            holder = new EmptyViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_progress, parent, false);
            holder = new ProgressViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder holder, int position) {
        if (holder instanceof LanguageHolder) {
            LanguageHolder languageHolder = (LanguageHolder) holder;
            SpokenLanguageModel languageModel = (SpokenLanguageModel) getItemAt(position);
            languageHolder.tvLanguageName.setText(languageModel.getLabel());
            languageHolder.tvLanguageProficiency.setText(languageModel.getProficiencyName());
        }

    }

    private class LanguageHolder extends BaseRecyclerViewHolder {

        private TextView tvLanguageName;
        private TextView tvLanguageProficiency;
        private ImageView ivEditLanguage;

        public LanguageHolder(View view) {
            super(view);
            tvLanguageName = (TextView) view.findViewById(R.id.tv_language_name);
            tvLanguageProficiency = (TextView) view.findViewById(R.id.tv_language_proficiency);
            ivEditLanguage = (ImageView) view.findViewById(R.id.iv_edit_language);
            ivEditLanguage.setOnClickListener(this);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return LanguageHolder.this;
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            if (getItemClickListener() != null)
                getItemClickListener().onRecyclerViewItemClick(LanguageHolder.this);
        }
    }

    public class ProgressViewHolder extends BaseRecyclerViewHolder {
        private ProgressViewHolder(View view) {
            super(view);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    public class EmptyViewHolder extends BaseRecyclerViewHolder {
        private TextView textViewEmpty;

        private EmptyViewHolder(View view) {
            super(view);
            textViewEmpty = view.findViewById(R.id.tv_message);
            textViewEmpty.setText("");
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getItemAt(position).getItemType();
    }
}
