package com.apps.ondemand.app.ui.myAllBids;

import static com.apps.ondemand.common.utils.AppConstants.KEY_JOB_ID;

import static java.security.AccessController.getContext;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.content.Intent;
import android.os.Bundle;


import com.apps.ondemand.R;
import com.apps.ondemand.app.communication.SocketService;
import com.apps.ondemand.app.data.models.Empty;
import com.apps.ondemand.app.data.models.ScheduleJobModel;
import com.apps.ondemand.app.ui.adapters.MyBidsJobAdapter;
import com.apps.ondemand.app.ui.jobs.JobDetailsActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AllBidsActivity extends BaseRecyclerViewActivity implements OnRecyclerViewItemClickListener {

    protected RecyclerView recyclerView;
    private String jobsUrl = "";
    List<BaseItem> scheduleJobs = new ArrayList<>();
    MyBidsJobAdapter myBidsJobAdapter;
    private SocketService mService;
    int listType = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_jobs);
        setActionBar(R.string.str_my_bids);
        setSubTitle(R.string.str_scheduled_jobs);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchScheduleJobsData();
    }

    private void initView() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    public void fetchScheduleJobsData() {
        jobsUrl = AppConstants.getServerUrl(String.format(AppConstants.FETCH_AVAILABLE_JOBS, 2));
        HttpRequestItem requestItem = new HttpRequestItem(jobsUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            JSONObject responseJson = new JSONObject(response.getResponse());
            if (responseJson.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equalsIgnoreCase(jobsUrl)) {
                    JSONArray jobs = responseJson.getJSONObject("data").getJSONArray("jobList");
                    if (jobs.length() != 0) {
                        scheduleJobs = new Gson().fromJson(jobs.toString(), new TypeToken<List<ScheduleJobModel>>() {
                        }.getType());
                        setScheduleAdapter(scheduleJobs);
                    } else {
                        setEmptyLayout();
                    }
                }
            } else {
                showSnackBar(responseJson.getString("message"));
            }
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    }


    private void setScheduleAdapter(List<BaseItem> scheduleJobItems) {
        myBidsJobAdapter = new MyBidsJobAdapter(this, scheduleJobItems, this);
        recyclerView.setAdapter(myBidsJobAdapter);
    }

    @Override
    public void onRecyclerViewItemClick(BaseRecyclerViewHolder holder) {
        int position = holder.getAdapterPosition();
        ScheduleJobModel scheduleJobModel = (ScheduleJobModel) myBidsJobAdapter.getItemAt(position);
        if (!scheduleJobModel.getBidAccepted() && !scheduleJobModel.getBidExpired()) {
            openJobDetails(scheduleJobModel.getId());
        }
    }

    @Override
    public void onRecyclerViewChildItemClick(BaseRecyclerViewHolder holder, int resourceId) {

    }

    public void openJobDetails(String jobId) {
        Intent intent = new Intent(this, JobDetailsActivity.class);
        intent.putExtra(KEY_JOB_ID, jobId);
        startActivity(intent);
    }

    private void setEmptyLayout() {
        if (getContext() == null)
            return;
        if (myBidsJobAdapter == null || myBidsJobAdapter.getItemCount() == 0) {
            List<BaseItem> itemList = new ArrayList<>();
            String message = listType == 1 ? getString(R.string.str_no_bid) : getString(R.string.str_no_past_job);
            itemList.add(new Empty(message));
            setScheduleAdapter(itemList);
        }
    }

}