package com.apps.ondemand.app.ui.dialogs;


import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RatingBar;

import com.apps.ondemand.R;
import com.apps.ondemand.app.ui.interfaces.RatingListener;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.BorderErrorEditText;
import com.apps.ondemand.common.utils.VizImageView;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

import static com.apps.ondemand.app.communication.SocketService.KEY_RATING_REMAINDER_RECEIVED;


public class DialogJobCompleted extends BaseDialog implements View.OnClickListener {

    private final String image;
    private final String name;
    private final float avgRating;
    BaseActivity activity;
    BottomSheetBehavior mBehavior;
    View contentView;
    RatingListener ratingListener;
    private RatingBar rbWorkerRating;
    BorderErrorEditText etWorkerReview;

    private DialogJobCompleted(@NonNull BaseActivity activity, String image, String name, float avgRating, RatingListener ratingListener) {
        super(activity);
        this.activity = activity;
        this.image = image;
        this.name = name;
        this.avgRating = avgRating;
        this.ratingListener = ratingListener;
    }

    public static DialogJobCompleted newInstance(BaseActivity activity, String image, String name, float avgRating, RatingListener ratingListener) {
        return new DialogJobCompleted(activity, image, name, avgRating, ratingListener);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        contentView = View.inflate(getContext(), R.layout.job_completed_bottom_sheet, null);

        setContentView(contentView);

        mBehavior = BottomSheetBehavior.from((View) contentView.getParent());
        ((View) contentView.getParent()).setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent, null));
    }

    @Override
    public void onStart() {
        super.onStart();
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        getWindow().setLayout(width, height);
        setCanceledOnTouchOutside(false);
        populateViews();
        setCancelable(false);
    }

    @Override
    public void onBackPressed() {
        activity.onBackPressed();
    }

    @SuppressLint("FindViewByIdCast")
    private void populateViews() {
        mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING)
                    bottomSheet.post(() -> mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED));
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
        mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        findTextViewById(R.id.tv_profile_name).setText(name);
        VizImageView imageView = findViewById(R.id.iv_profile_image);
        assert imageView != null;
        imageView.setImage(image, false);
        ((MaterialRatingBar) Objects.requireNonNull(findViewById(R.id.rb_profile_rating))).setRating(avgRating);
        findTextViewById(R.id.tv_profile_rating).setText(AppUtils.formatRating(avgRating));

        rbWorkerRating = findViewById(R.id.rb_worker_rating);
        etWorkerReview = findViewById(R.id.et_worker_review);

        findViewById(R.id.btn_submit).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_submit) {
            if (ratingListener != null) {
                ratingListener.onSubmitPressed(rbWorkerRating.getRating(), Objects.requireNonNull(etWorkerReview.getText()).toString());
            }
            dismiss();
        }
    }

    public void show(String jobId) {
        Map<String, Object> map = new HashMap<>();
        map.put("jobId", jobId);
        activity.emitFromSocket(KEY_RATING_REMAINDER_RECEIVED, new JSONObject(map));
        show();
    }
}