package com.apps.ondemand.app.ui.earnings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.Empty;
import com.apps.ondemand.app.data.models.Progress;
import com.apps.ondemand.app.data.models.WeekModel;
import com.apps.ondemand.app.ui.adapters.PastWeekAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PastWeekActivity extends BaseRecyclerViewActivity {

    private PastWeekAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_week);
        setActionBar(R.string.str_weeks);
        setSubTitle(R.string.str_weeks_sub);
        initializeRecyclerView();
        populateList(null);

    }


    private void callWeekListApi(boolean addProgress) {

        if (addProgress)
            addProgressItem();

        HttpRequestItem requestItem = new HttpRequestItem(
                AppConstants.getServerUrl(AppConstants.GET_ALL_WEEK_LIST));
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());

        Map<String, Object> params = new HashMap<>();
        params.put("year", Calendar.getInstance().get(Calendar.YEAR));

        requestItem.setParams(params);

        AppNetworkTask appNetworkTask = new AppNetworkTask(null, this);
        appNetworkTask.execute(requestItem);
    }

    private void addProgressItem() {
        List<BaseItem> items = new ArrayList<>();
        items.add(new Progress());
        populateList(items);
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            removeProgressItem();
            JSONObject responseJson = new JSONObject(response.getResponse());
            if (response.getHttpRequestUrl().equals(AppConstants.getServerUrl(AppConstants.GET_ALL_WEEK_LIST))) {
                if (responseJson.getInt("success") == 1) {
                    JSONObject data = responseJson.getJSONObject("data");
                    JSONArray weeksFound = data.getJSONArray("weekList");
                    if (weeksFound.length() != 0) {
                        List<BaseItem> jobsItem = new Gson().fromJson(weeksFound.toString(),
                                new TypeToken<List<WeekModel>>() {
                                }.getType());
                        setWeekListingResponse(jobsItem);
                    } else {
                        setEmptyLayout();
                    }
                } else {
                    setEmptyLayout();
                    showSnackBar(responseJson.getString("message"));

                }
            }

        } catch (JSONException e) {
            setEmptyLayout();
            Logger.error(false, e);
        }
    }

    @Override
    public void onNetworkError(HttpResponseItem response) {
        super.onNetworkError(response);
        removeProgressItem();
        setEmptyLayout();
    }

    private void removeProgressItem() {
        if (adapter != null && adapter.getAdapterCount() > 0 &&
                adapter.getItemAt(adapter.getAdapterCount() - 1) instanceof Progress) {
            adapter.remove(adapter.getAdapterCount() - 1);
            adapter.notifyItemRemoved(adapter.getAdapterCount() - 1);
        }
        if (getSwipeLayout() != null) {
            getSwipeLayout().setRefreshing(false);
        }
    }

    private void setEmptyLayout() {
        if (adapter == null || adapter.getItemCount() == 0) {
            List<BaseItem> itemList = new ArrayList<>();
            itemList.add(new Empty());
            populateList(itemList);
        }
    }

    private void setWeekListingResponse(List<BaseItem> data) {
        List<BaseItem> itemList = new ArrayList<>();

        if (data.size() != 0)
            itemList.addAll(data);

        populateList(itemList);
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        adapter = null;
        setAdapter(null);
        callWeekListApi(false);

    }

    @Override
    public void onResume() {
        super.onResume();
        adapter = null;
        setAdapter(null);
        callWeekListApi(true);
    }

    private void initializeRecyclerView() {
        RecyclerView recyclerView = getRecyclerView();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(false);

    }

    private void populateList(List<BaseItem> weeks) {
        if (adapter == null) {
            adapter = new PastWeekAdapter(weeks, this);
            setAdapter(adapter);
        } else
            adapter.addAll(weeks);

        runLayoutAnimation(getRecyclerView());
    }


    private void runLayoutAnimation(final RecyclerView recyclerView) {
        if (recyclerView == null)
            return;

        if (recyclerView.getAdapter() == null)
            return;

        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

    @Override
    public void onRecyclerViewItemClick(BaseRecyclerViewHolder holder) {
        super.onRecyclerViewItemClick(holder);
        int position = holder.getAdapterPosition();
        WeekModel weekModel = (WeekModel) adapter.getItemAt(position);
        Intent data = new Intent();
        data.putExtra(AppConstants.KEY_EXTRA_WEEK_MODEL, weekModel);
        setResult(RESULT_OK, data);
        finish();
    }
}
