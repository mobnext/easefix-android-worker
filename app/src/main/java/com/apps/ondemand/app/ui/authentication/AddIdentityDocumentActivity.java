package com.apps.ondemand.app.ui.authentication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.IdentityTypeModel;
import com.apps.ondemand.common.base.TakePhotoActivity;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.BitmapUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.apps.ondemand.common.utils.VizImageView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddIdentityDocumentActivity extends TakePhotoActivity implements View.OnClickListener, VizImageView.OnImageUploadingListener {
    private String profileImage = "";
    private String profileImageBack = "";


    private String fetchDocTypeUrl = "";
    private String selectIdentityDocUrl = "";
    private List<IdentityTypeModel> identityTypeModels = new ArrayList<>();
    IdentityTypeModel selectedType = null;

    boolean front = true;
    private boolean isCommingFromActivityResult = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_add_identity_document);
        setActionBar(R.string.str_identity_documents);
        setSubTitle(R.string.str_add_documents);
        initView();
        fetchDocumentTypes();
    }

    private void fetchDocumentTypes() {
        fetchDocTypeUrl = AppConstants.getServerUrl(AppConstants.FETCH_IDENTITY_TYPES);
        HttpRequestItem requestItem = new HttpRequestItem(fetchDocTypeUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);

        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false),
                this);
        appNetworkTask.execute(requestItem);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.cv_identity_type) {
            populateIdentityTypeData(identityTypeModels);
        } else if (view.getId() == R.id.iv_upload_image_port) {
            front = true;
            isCommingFromActivityResult = true;
            showChoosePhotoDialog();
        } else if (view.getId() == R.id.iv_upload_image_back) {
            front = false;
            isCommingFromActivityResult = true;
            showChoosePhotoDialog();
        } else if (view.getId() == R.id.btn_continue) {
            validateAndCallService();
        }
    }

    private void validateAndCallService() {
        AppUtils.hideSoftKeyboard(this);

        if (selectedType == null) {
            showSnackBar(R.string.error_select_identity_type);
            return;
        }

        if (TextUtils.isEmpty(profileImage)) {
            showSnackBar(R.string.error_empty_image);
            return;
        }
        if (TextUtils.isEmpty(profileImageBack)) {
            showSnackBar("Upload Back Side Image");
            return;
        }

        selectIdentityDocUrl = AppConstants.getServerUrl(AppConstants.UPLOAD_IDENTITY_DOCUMENT);

        Map<String, Object> map = new HashMap<>();

        map.put("identityDocId", selectedType.getId());
        map.put("identityFrontUrl", profileImage);
        map.put("identityBackUrl", profileImageBack);

        HttpRequestItem requestItem = new HttpRequestItem(selectIdentityDocUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_POST);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);

    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {

            JSONObject responseJson = new JSONObject(response.getResponse());
            if (responseJson.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equalsIgnoreCase(fetchDocTypeUrl)) {
                    JSONArray docTypes = responseJson.getJSONObject("data").getJSONArray("identityDoc");
                    if (docTypes.length() != 0) {
                        identityTypeModels = new Gson().fromJson(docTypes.toString(),
                                new TypeToken<List<IdentityTypeModel>>() {
                                }.getType());
                    }
                } else if (response.getHttpRequestUrl().equalsIgnoreCase(selectIdentityDocUrl)) {
                    moveToNextActivity();
                }
            } else
                showSnackBar(responseJson.getString("message"));
        } catch (JSONException e) {
            Logger.error(false, e);
        }
    }

    private void populateIdentityTypeData(final List<IdentityTypeModel> identityTypeModels) {
        Context wrapper = new ContextThemeWrapper(this, R.style.PopupStyle);

        PopupMenu popup = new PopupMenu(wrapper, findViewById(R.id.cv_identity_type));
        for (int i = 0; i < identityTypeModels.size(); i++) {
            popup.getMenu().add(i, i, i, identityTypeModels.get(i).getName());

        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                selectedType = identityTypeModels.get(item.getItemId());
                findTextViewById(R.id.tv_identity_type).setText(selectedType.getName());
                return true;
            }
        });
        popup.show();
    }

    private void moveToNextActivity() {
        Intent intent = new Intent(this, AddServiceCertificateActivity.class);
        startActivity(intent);
    }

    private void initView() {
        findViewById(R.id.cv_identity_type).setOnClickListener(this);
        findViewById(R.id.iv_upload_image_port).setOnClickListener(AddIdentityDocumentActivity.this);
        findViewById(R.id.iv_upload_image_back).setOnClickListener(AddIdentityDocumentActivity.this);
        findViewById(R.id.btn_continue).setOnClickListener(AddIdentityDocumentActivity.this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CHOOSE_PHOTO_RESULT_CODE) {
                if (data.getData() != null) {
                    selectedPhotoUri = data.getData();
                } else
                    selectedPhotoUri = null;
            } else {
                if (requestCode == TAKE_PHOTO_RESULT_CODE)
                    selectedPhotoUri = BitmapUtils.getImageUri(this, (Bitmap) data.getExtras().get("data"));
                else selectedPhotoUri = null;
            }
//            filePath = FileUtils.getPath(this, selectedPhotoUri);
            loadImageInView();
        } else {
            selectedPhotoUri = null;
        }

        isCommingFromActivityResult = true;

    }

    private void loadImageInView() {
        if (selectedPhotoUri == null)
            return;

        VizImageView imageView = findViewById(R.id.iv_upload_image_port);
        VizImageView imageViewBack = findViewById(R.id.iv_upload_image_back);
        if (front)
            imageView.loadImageInView(imageView.getId(), AppConstants.UPLOAD_IDENTITY_IMAGE, selectedPhotoUri, this);
        else
            imageViewBack.loadImageInView(imageViewBack.getId(), AppConstants.UPLOAD_IDENTITY_IMAGE, selectedPhotoUri, this);

    }

    @Override
    public void onUploadingResponse(boolean isSuccessful, String imageURL, int imageViewID, String message) {
        if (isSuccessful) {
            if (front) {
                profileImage = imageURL;
            }else{
                profileImageBack = imageURL;
            }
        } else {
            profileImage = "";
        }
    }
}
