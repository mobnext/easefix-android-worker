package com.apps.ondemand.app.ui.adapters;

import static android.provider.MediaStore.Video.Thumbnails.MINI_KIND;
import static com.apps.ondemand.common.utils.AppConstants.IMAGE_PORT;
import static com.apps.ondemand.common.utils.AppConstants.VIDEO_PORT;

import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.PortfolioModel;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.utils.VizImageVideoView;

import java.util.List;


public class PortfolioFullScreenAdapter extends BaseRecyclerViewAdapter {

    BaseActivity context;

    public PortfolioFullScreenAdapter(BaseActivity context, List<BaseItem> items, OnRecyclerViewItemClickListener itemClickListener) {
        super(items, itemClickListener);
        this.context = context;
    }

    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        BaseRecyclerViewHolder holder;
        if (viewType == BaseItem.ITEM_PORTFOLIO) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_full_screen, parent, false);
            holder = new PortfolioHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
            holder = new EmptyViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder holder, int position) {
        if (holder instanceof PortfolioHolder) {
            PortfolioHolder portfolioHolder = (PortfolioHolder) holder;
            PortfolioModel portfolioModel = (PortfolioModel) getItemAt(position);

            if (portfolioModel.getType() == IMAGE_PORT) {
                portfolioHolder.progress_circular.setVisibility(View.GONE);
                portfolioHolder.vizImageVideoView.setImage(portfolioModel.getUrl(), (int) context.getResources().getDimension(R.dimen.dimen_100),
                        (int) context.getResources().getDimension(R.dimen.dimen_100),
                        R.color.colorPrimaryBlue);
            } else if (portfolioModel.getType() == VIDEO_PORT) {
               portfolioHolder.iv_upload_image_Video.setVisibility(View.VISIBLE);
                portfolioHolder.vizImageVideoView.setImage(portfolioModel.getUrl(), (int) context.getResources().getDimension(R.dimen.dimen_100),
                        (int) context.getResources().getDimension(R.dimen.dimen_100),
                        R.color.colorPrimaryBlue);
                portfolioHolder.iv_upload_image_Video.setVideoURI(Uri.parse(portfolioModel.getUrl()));
                portfolioHolder.iv_upload_image_Video.requestFocus();
                portfolioHolder.iv_upload_image_Video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        portfolioHolder.vizImageVideoView.setVisibility(View.GONE);
                        portfolioHolder.progress_circular.setVisibility(View.GONE);
                        portfolioHolder.iv_upload_image_Video.start();
                    }
                });
                portfolioHolder.iv_upload_image_Video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        portfolioHolder.iv_upload_image_Video.start();
                    }
                });
            }
        }

    }

    public static Bitmap getThumblineImage(String videoPath) {
        return ThumbnailUtils.createVideoThumbnail(videoPath, MINI_KIND);
    }

    private class PortfolioHolder extends BaseRecyclerViewHolder {
        VizImageVideoView vizImageVideoView;
        VideoView iv_upload_image_Video;
        ProgressBar progress_circular;
        public PortfolioHolder(View view) {
            super(view, true);
            view.setOnClickListener(this);
            vizImageVideoView = view.findViewById(R.id.iv_upload_image_port);
            iv_upload_image_Video = view.findViewById(R.id.iv_upload_image_Video);
            progress_circular = view.findViewById(R.id.progress_circular);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return PortfolioHolder.this;
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            if (getItemClickListener() != null)
//                getItemClickListener().onRecyclerViewItemClick(PortfolioHolder.this);
                getItemClickListener().onRecyclerViewChildItemClick(PortfolioHolder.this, v.getId());
        }
    }

    public class ProgressViewHolder extends BaseRecyclerViewHolder {
        private ProgressViewHolder(View view) {
            super(view);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    public class EmptyViewHolder extends BaseRecyclerViewHolder {
        private TextView textViewEmpty;

        private EmptyViewHolder(View view) {
            super(view);
            textViewEmpty = view.findViewById(R.id.tv_message);
            textViewEmpty.setText("");
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getItemAt(position).getItemType();
    }
}
