package com.apps.ondemand.app.ui.interfaces;

public interface DraftListener {
     void onSaveDraft(boolean save);

}
