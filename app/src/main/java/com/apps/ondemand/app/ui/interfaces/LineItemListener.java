package com.apps.ondemand.app.ui.interfaces;

import com.apps.ondemand.app.data.models.LineItemModel;

public interface LineItemListener {

    public static final int SAVE = 1;
    public static final int DELETE = 2;
    public static final int DISCARD = 3;
    public static final int EDIT = 4;

    void onButtonClicked(LineItemModel lineItemModel, int position,int clickType);


}
