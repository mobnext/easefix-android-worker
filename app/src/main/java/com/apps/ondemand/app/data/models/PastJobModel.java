package com.apps.ondemand.app.data.models;

import com.apps.ondemand.common.business.BaseItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PastJobModel implements BaseItem {


    @SerializedName("jobId")
    @Expose
    private String id;
    @SerializedName("spJobStatus")
    @Expose
    private int spJobStatus;
    @SerializedName("jobEndTime")
    @Expose
    private long jobEndTime;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("jobAddress")
    @Expose
    private JobAddressModel jobAddressModel;
    @SerializedName("serviceName")
    @Expose
    private String serviceName;

    @SerializedName("spProfileId")
    @Expose
    private String spProfileId;
    @SerializedName("avgRating")
    @Expose
    private float avgRating;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("spEarnedAmount")
    @Expose
    private float spEarnedAmount;

    @SerializedName("JobIdIdentifier")
    @Expose
    private String jobIdIdentifier;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSpJobStatus() {
        return spJobStatus;
    }

    public void setSpJobStatus(int spJobStatus) {
        this.spJobStatus = spJobStatus;
    }

    public long getJobEndTime() {
        return jobEndTime;
    }

    public void setJobEndTime(long jobEndTime) {
        this.jobEndTime = jobEndTime;
    }



    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public JobAddressModel getJobAddressModel() {
        return jobAddressModel;
    }

    public void setJobAddressModel(JobAddressModel jobAddressModel) {
        this.jobAddressModel = jobAddressModel;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getSpProfileId() {
        return spProfileId;
    }

    public void setSpProfileId(String spProfileId) {
        this.spProfileId = spProfileId;
    }

    public float getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(float avgRating) {
        this.avgRating = avgRating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSpEarnedAmount() {
        return spEarnedAmount;
    }

    public void setSpEarnedAmount(float spEarnedAmount) {
        this.spEarnedAmount = spEarnedAmount;
    }

    public PastJobModel() {

    }

    public String getJobIdIdentifier() {
        return jobIdIdentifier;
    }

    public void setJobIdIdentifier(String jobIdIdentifier) {
        this.jobIdIdentifier = jobIdIdentifier;
    }

    @Override
    public int getItemType() {
        return ITEM_JOB;
    }
}
