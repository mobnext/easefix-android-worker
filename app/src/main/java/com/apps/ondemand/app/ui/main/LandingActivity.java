package com.apps.ondemand.app.ui.main;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import androidx.annotation.RequiresApi;

import com.apps.ondemand.R;
import com.apps.ondemand.app.ui.authentication.AuthenticationActivity;
import com.apps.ondemand.app.ui.dialogs.DialogLanguageProficiency;
import com.apps.ondemand.app.ui.interfaces.ClickableSpanListener;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;

@RequiresApi(api = Build.VERSION_CODES.S)
public class LandingActivity extends BaseActivity implements View.OnClickListener {

    protected Button btnSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_landing);
        initView();
    }

    private void initView() {
        btnSignUp =  findViewById(R.id.btn_sign_up);
        btnSignUp.setOnClickListener(LandingActivity.this);
        Button btnLogin = findButtonById(R.id.btn_sign_in);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_sign_up) {
            Intent intent = new Intent(LandingActivity.this, AuthenticationActivity.class);
            intent.putExtra(AppConstants.KEY_EXTRA_IS_SIGN_UP,true);
            startActivity(intent);
        }
        if (view.getId() == R.id.btn_sign_in) {
            Intent intent = new Intent(LandingActivity.this, AuthenticationActivity.class);
            intent.putExtra(AppConstants.KEY_EXTRA_IS_SIGN_UP,false);
            startActivity(intent);
        }
    }


}
