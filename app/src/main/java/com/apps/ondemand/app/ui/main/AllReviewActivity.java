package com.apps.ondemand.app.ui.main;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.Empty;
import com.apps.ondemand.app.data.models.Progress;
import com.apps.ondemand.app.data.models.ReviewModel;
import com.apps.ondemand.app.ui.adapters.ReviewsAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.GridSpacingItemDecorator;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.apps.ondemand.common.utils.AppConstants.KEY_USER_ID;
import static com.apps.ondemand.common.utils.AppConstants.KEY_USER_TYPE;


public class AllReviewActivity extends BaseRecyclerViewActivity implements OnRecyclerViewItemClickListener {

    private int pageNo = 0;
    private boolean isLoading = false;
    private boolean doLoadMore = false;
    private ReviewsAdapter adapter;
    private static final int LIMIT = 20;
    private String reviewsUrl = "";
    private String userType = "";
    private String id = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_review);
        userType = getIntent().getStringExtra(KEY_USER_TYPE);
        id = getIntent().getStringExtra(KEY_USER_ID);
        setActionBar(R.string.str_rating_review);
        setSubTitle(R.string.str_rating_review_sub);
        initializeRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter = null;
        setAdapter(null);
        pageNo = 0;
        getListData(true);
    }


    private void initializeRecyclerView() {
        RecyclerView recyclerView = getRecyclerView();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(new GridSpacingItemDecorator(5, AppUtils.dpToPx(5, this)));
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isLoading || !doLoadMore)       //Check if request is already called OR do need to get next page patients
                    return;
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int totalItemCount = linearLayoutManager.getItemCount();
                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (totalItemCount <= (lastVisibleItem + 1)) {
                    getListData(true);
                    doLoadMore = false;
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);

        try {
            isLoading = false;
            removeProgressItem();
            JSONObject responseJson = new JSONObject(response.getResponse());
            if (responseJson.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equalsIgnoreCase(reviewsUrl)) {
                    JSONArray reports = responseJson.getJSONObject("data").getJSONArray("overAllRating");
                    if (reports.length() != 0) {
                        List<BaseItem> reportItems = new Gson().fromJson(reports.toString(),
                                new TypeToken<List<ReviewModel>>() {
                                }.getType());
                        setListingResponse(reportItems);
                    } else {
                        setEmptyLayout();
                        doLoadMore = false;
                    }
                }
            } else {
                showSnackBar(responseJson.getString("message"));
                setEmptyLayout();
                doLoadMore = false;
            }
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    }

    @Override
    public void onNetworkError(HttpResponseItem response) {
        super.onNetworkError(response);
        removeProgressItem();
        setEmptyLayout();
        isLoading = false;
    }

    private void getListData(boolean addProgress) {
        isLoading = true;
        if (addProgress) {
            addProgressItem();
        }
        reviewsUrl = AppConstants.getServerUrl(String.format(AppConstants.GET_ALL_REVIEWS, userType, id, (pageNo * LIMIT), LIMIT));
        HttpRequestItem requestItem = new HttpRequestItem(reviewsUrl);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());// Add header params (Cookies)
        AppNetworkTask appNetworkTask = new AppNetworkTask(null,
                this);
        appNetworkTask.execute(requestItem);
    }

    private void setListingResponse(List<BaseItem> data) throws JSONException {

        List<BaseItem> itemList = new ArrayList<>();


        if (data.size() == 0)
            doLoadMore = false;
        else {
            pageNo++;
            doLoadMore = data.size() == LIMIT;
            itemList.addAll(data);
        }

        populateList(itemList);

    }

    private void populateList(List<BaseItem> itemList) {
        if (adapter == null) {
            adapter = new ReviewsAdapter(this, itemList, this);
            setAdapter(adapter);
        } else
            adapter.addAll(itemList);

    }

    private void addProgressItem() {
        List<BaseItem> items = new ArrayList<>();
        items.add(new Progress());
        populateList(items);
    }

    private void removeProgressItem() {
        if (adapter != null && adapter.getAdapterCount() > 0 &&
                adapter.getItemAt(adapter.getAdapterCount() - 1) instanceof Progress) {
            adapter.remove(adapter.getAdapterCount() - 1);
            adapter.notifyItemRemoved(adapter.getAdapterCount() - 1);
        }
        if (getSwipeLayout() != null) {
            getSwipeLayout().setRefreshing(false);
        }
    }

    private void setEmptyLayout() {
        if (adapter == null || adapter.getItemCount() == 0) {
            List<BaseItem> itemList = new ArrayList<>();
            itemList.add(new Empty());
            populateList(itemList);
        }
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        adapter = null;
        setAdapter(null);
        pageNo = 0;
        getListData(false);

    }

    @Override
    public void onRecyclerViewItemClick(BaseRecyclerViewHolder holder) {
        super.onRecyclerViewItemClick(holder);
        int position = holder.getAdapterPosition();

    }
}
