package com.apps.ondemand.app.data.models;

import com.apps.ondemand.common.business.BaseItem;

import java.util.ArrayList;
import java.util.List;

public class TimeSlotModel implements BaseItem {
    int hours;
    boolean isSelected;

    public TimeSlotModel() {
    }

    public TimeSlotModel(int hours, boolean isSelected) {
        this.hours = hours;
        this.isSelected = isSelected;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public int getItemType() {
        return ITEM_TIME_SLOT;
    }

    public static ArrayList<BaseItem> getTimeSlot(int maxHours) {
        ArrayList<BaseItem> timeSlots = new ArrayList<>();
        for (int i = 0; i < maxHours; i++) {
            timeSlots.add(new TimeSlotModel(i + 1, false));
        }
        ((TimeSlotModel) timeSlots.get(0)).setSelected(true);
        return timeSlots;
    }
}
