package com.apps.ondemand.app.ui.main;

import android.content.Intent;

import androidx.appcompat.app.AlertDialog;
import androidx.core.view.ViewCompat;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.app.data.models.ReviewModel;
import com.apps.ondemand.app.data.preferences.SharedPreferenceManager;
import com.apps.ondemand.app.ui.SplashActivity;
import com.apps.ondemand.app.ui.adapters.ReviewsAdapter;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.apps.ondemand.common.utils.VizImageView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

import static com.apps.ondemand.common.utils.AppConstants.KEY_USER_ID;
import static com.apps.ondemand.common.utils.AppConstants.KEY_USER_TYPE;
import static com.apps.ondemand.common.utils.AppConstants.TYPE_SP;
import static com.apps.ondemand.common.utils.AppConstants.TYPE_USER;

public class MyProfileActivity extends BaseActivity implements View.OnClickListener {

    private String fetchProfileUrl = "";
    private String deleteAccount = "";
    protected RecyclerView rvReviews;
    ReviewsAdapter reviewsAdapter;
    List<ReviewModel> reviewModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        setActionBar(R.string.str_my_profile);
        setSubTitle(R.string.str_my_profile_sub);
        initViews();
        fetchProfile();
    }

    private void initViews() {
        rvReviews = (RecyclerView) findViewById(R.id.rv_reviews);
        rvReviews.setLayoutManager(new LinearLayoutManager(this));
        rvReviews.setHasFixedSize(false);


        ViewCompat.setNestedScrollingEnabled(rvReviews, false);
        ViewCompat.setNestedScrollingEnabled(findViewById(R.id.ns_wrapper), false);

        findViewById(R.id.tv_view_all).setOnClickListener(this);
        findViewById(R.id.deleteAccount).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_view_all:
                Intent intent = new Intent(this, AllReviewActivity.class);
                intent.putExtra(KEY_USER_TYPE, TYPE_SP);
                intent.putExtra(KEY_USER_ID, UserManager.getUserId());
                startActivity(intent);
                break;
            case R.id.deleteAccount:
                deleteDialog();
                break;
        }
    }

    private void deleteDialog() {
        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(R.layout.dialog_delete_account, null);
        final androidx.appcompat.app.AlertDialog deleteDialog = new AlertDialog.Builder(this)
                .setView(deleteDialogView)
                .setCancelable(true)
                .create();
        TextView tvDelete = deleteDialogView.findViewById(R.id.tv_delete);
        TextView cancel = deleteDialogView.findViewById(R.id.tv_cancel);


        tvDelete.setOnClickListener(v -> {
            deleteAccount();
            deleteDialog.dismiss();
        });
        cancel.setOnClickListener(v -> {
            deleteDialog.dismiss();
        });

        deleteDialog.show();
    }
    private void deleteAccount()
    {
        deleteAccount = AppConstants.getServerUrl(AppConstants.DELETE_ACCOUNT);
        HttpRequestItem requestItem = new HttpRequestItem(deleteAccount);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        Map<String, Object> map = new HashMap<>();
        map.put("userType", TYPE_SP);
        requestItem.setParams(map);
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    private void fetchProfile() {
        fetchProfileUrl = AppConstants.getServerUrl(String.format(AppConstants.FETCH_SP_PROFILE, UserManager.getUserId()));

        HttpRequestItem requestItem = new HttpRequestItem(fetchProfileUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {

            JSONObject jsonObjectResponse = new JSONObject(response.getResponse());
            int success = jsonObjectResponse.getInt("success");
            if (success == 1) {
                if (response.getHttpRequestUrl().equals(fetchProfileUrl)) {
                    JSONObject data = jsonObjectResponse.getJSONObject("data");
                    UserManager.saveUserData(data);
                    JSONArray reviews = data.getJSONArray("ratingAndReview");
                    if (reviews.length() != 0) {
                        reviewModels = new Gson().fromJson(reviews.toString(),
                                new TypeToken<List<ReviewModel>>() {
                                }.getType());
                    }
                    setProfileData();
                }else if (response.getHttpRequestUrl().equals(deleteAccount)){
                    SharedPreferenceManager.getInstance().clearPreferences();
                    Intent intent = new Intent(this, SplashActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            } else {
                showSnackBar(jsonObjectResponse.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setProfileData() {
        animate();
        findViewById(R.id.profile_info_wrapper).setVisibility(View.VISIBLE);


        findTextViewById(R.id.tv_profile_name).setText(UserManager.getUserName());
        VizImageView imageView = findViewById(R.id.iv_profile_image);
        imageView.setImage(UserManager.getImage(), false);
        float rating = 0.0f;
        try {
            rating = Float.parseFloat(UserManager.getAverageRating());
        } catch (Exception e) {
            e.printStackTrace();
        }
        ((MaterialRatingBar) findViewById(R.id.rb_profile_rating)).setRating(rating);
        findTextViewById(R.id.tv_profile_rating).setText(String.valueOf(AppUtils.formatRating(rating)));

        if (UserManager.getIsCompany()) {
            findViewById(R.id.cv_company_name).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.cv_company_name).setVisibility(View.GONE);
        }

        findFieldById(R.id.et_company_name).setText(UserManager.getCompanyName());
        findFieldById(R.id.et_first_name).setText(UserManager.getFirstName());
        findFieldById(R.id.et_last_name).setText(UserManager.getLastName());
        findFieldById(R.id.et_email).setText(UserManager.getEmail());
        findFieldById(R.id.et_phone).setText(UserManager.getFullNumber());
        findFieldById(R.id.et_about).setText(UserManager.getBio());


        if (reviewModels.size() == 0) {
            findTextViewById(R.id.tv_view_all).setVisibility(View.GONE);
            findTextViewById(R.id.tv_no_reviews).setVisibility(View.VISIBLE);
        } else if (reviewModels.size() > 2) {
            findTextViewById(R.id.tv_view_all).setVisibility(View.VISIBLE);
            findTextViewById(R.id.tv_no_reviews).setVisibility(View.GONE);
            reviewsAdapter = new ReviewsAdapter(this, new ArrayList<BaseItem>(reviewModels).subList(0, 2), null);
            rvReviews.setAdapter(reviewsAdapter);
        } else {
            findTextViewById(R.id.tv_view_all).setVisibility(View.GONE);
            findTextViewById(R.id.tv_no_reviews).setVisibility(View.GONE);
            reviewsAdapter = new ReviewsAdapter(this, new ArrayList<BaseItem>(reviewModels), null);
            rvReviews.setAdapter(reviewsAdapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.m_edit:
                startActivityForResult(new Intent(this, EditProfileActivity.class), AppConstants.EDIT_PROFILE_REQUEST_CODE);
                break;
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fetchProfile();
    }
}
