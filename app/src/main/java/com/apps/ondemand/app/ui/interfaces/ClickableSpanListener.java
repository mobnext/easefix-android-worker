package com.apps.ondemand.app.ui.interfaces;

public interface ClickableSpanListener {
    public void onSpanClicked();
}
