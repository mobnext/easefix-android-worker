package com.apps.ondemand.app.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.CountryModel;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.utils.GlideUtils;

import java.util.List;


public class CountryAdapter extends BaseRecyclerViewAdapter {

    BaseActivity context;

    public CountryAdapter(BaseActivity context, List<BaseItem> items, OnRecyclerViewItemClickListener itemClickListener) {
        super(items, itemClickListener);
        this.context = context;
    }

    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        BaseRecyclerViewHolder holder;
        if (viewType == BaseItem.ITEM_COUNTRY) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_country, parent, false);
            holder = new CountryHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
            holder = new EmptyViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder holder, int position) {
        if (holder instanceof CountryHolder) {
            CountryHolder countryHolder = (CountryHolder) holder;
            CountryModel countryModel = (CountryModel) getItemAt(position);
            countryHolder.tvCountryName.setText(countryModel.getName());
            countryHolder.tvCountryCode.setText(countryModel.getCountryCode());
        }

    }

    private class CountryHolder extends BaseRecyclerViewHolder {
        ImageView ivCheck;
        TextView tvCountryName;
        TextView tvCountryCode;

        public CountryHolder(View view) {
            super(view, true);
            view.setOnClickListener(this);
            ivCheck = view.findViewById(R.id.iv_check_mark);
            tvCountryName = view.findViewById(R.id.tv_country_name);
            tvCountryCode = view.findViewById(R.id.tv_country_code);

        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return CountryHolder.this;
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            if (getItemClickListener() != null)
                getItemClickListener().onRecyclerViewItemClick(CountryHolder.this);
        }
    }

    public class ProgressViewHolder extends BaseRecyclerViewHolder {
        private ProgressViewHolder(View view) {
            super(view);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    public class EmptyViewHolder extends BaseRecyclerViewHolder {
        private TextView textViewEmpty;

        private EmptyViewHolder(View view) {
            super(view);
            textViewEmpty = view.findViewById(R.id.tv_message);
            textViewEmpty.setText("");
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getItemAt(position).getItemType();
    }
}
