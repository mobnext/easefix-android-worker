package com.apps.ondemand.app.ui.adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.ServiceCertificateModel;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.utils.BorderErrorEditText;
import com.apps.ondemand.common.utils.VizImageView;

import java.util.List;


public class AddServiceCertificatesAdapter extends BaseRecyclerViewAdapter {

    Context context;

    public AddServiceCertificatesAdapter(Context context, List<BaseItem> items, OnRecyclerViewItemClickListener itemClickListener) {
        super(items, itemClickListener);
        this.context = context;
    }

    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        BaseRecyclerViewHolder holder;
        if (viewType == BaseItem.ITEM_SERVICE_CERTIFICATES) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_service_certificates, parent, false);
            holder = new ServiceCertificateHolder(view);
        } else if (viewType == BaseItem.ITEM_EMPTY) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
            holder = new EmptyViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_progress, parent, false);
            holder = new ProgressViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder holder, int position) {
        if (holder instanceof ServiceCertificateHolder) {
            ServiceCertificateHolder serviceCertificateHolder = (ServiceCertificateHolder) holder;
            ServiceCertificateModel serviceCertificateModel = (ServiceCertificateModel) getItemAt(position);

            if (!URLUtil.isValidUrl(serviceCertificateModel.getProfessionalDocsFront())) {
                serviceCertificateHolder.tvDocumentTitle.setText(context.getString(R.string.str_please_add) + " " + (position + 1));
                serviceCertificateHolder.ivUploadImage.removeImage(R.drawable.upload_front_side, false);
            } else {
                serviceCertificateHolder.ivUploadImage
                        .setImage(serviceCertificateModel.getProfessionalDocsFront(), false);
                serviceCertificateHolder.tvDocumentTitle.setText(context.getString(R.string.str_document) + " " + (position + 1));
                serviceCertificateHolder.etDocumentTitle.setText(serviceCertificateModel.getProfessionalDocsTitle());
            }
            if (getAdapterCount() > 1) {
                serviceCertificateHolder.ivDeleteImage.setVisibility(View.VISIBLE);
            } else {
                serviceCertificateHolder.ivDeleteImage.setVisibility(View.GONE);
            }
            if (serviceCertificateModel.getProfessionalDocsTitle().isEmpty()){
                serviceCertificateHolder.etDocumentTitle.setError("Enter Title");
            }
//            else
//                serviceCertificateHolder.etDocumentTitle.setText(serviceCertificateModel.getProfessionalDocsTitle());

        }

    }

    private class ServiceCertificateHolder extends BaseRecyclerViewHolder {
        private TextView tvDocumentTitle;
        BorderErrorEditText etDocumentTitle;
        private VizImageView ivUploadImage;
        private ImageView ivDeleteImage;


        public ServiceCertificateHolder(View view) {
            super(view);
            tvDocumentTitle = view.findViewById(R.id.tv_document_title);
            etDocumentTitle = view.findViewById(R.id.et_document_title);
            ivUploadImage = view.findViewById(R.id.iv_upload_image_port);
            ivDeleteImage = view.findViewById(R.id.iv_delete_image);
            ivUploadImage.setOnClickListener(this);
            ivDeleteImage.setOnClickListener(this);
            etDocumentTitle.addTextChangedListener(titleChange);
        }

        TextWatcher titleChange = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int position = getAdapterPosition();
                ServiceCertificateModel model = (ServiceCertificateModel) getItemAt(position);
                model.setProfessionalDocsTitle(s.toString());
            }
        };

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return ServiceCertificateHolder.this;
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            if (getItemClickListener() != null)
                getItemClickListener().onRecyclerViewChildItemClick(ServiceCertificateHolder.this, v.getId());
        }
    }

    public class ProgressViewHolder extends BaseRecyclerViewHolder {
        private ProgressViewHolder(View view) {
            super(view);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    public class EmptyViewHolder extends BaseRecyclerViewHolder {
        private TextView textViewEmpty;

        private EmptyViewHolder(View view) {
            super(view);
            textViewEmpty = view.findViewById(R.id.tv_message);
            textViewEmpty.setText("");
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getItemAt(position).getItemType();
    }
}
