package com.apps.ondemand.app.ui.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.ReasonModel;
import com.apps.ondemand.app.ui.adapters.CancellationReasonAdapter;
import com.apps.ondemand.app.ui.interfaces.ReasonSelectionCallback;
import com.apps.ondemand.common.utils.BorderErrorEditText;

import java.util.List;

import static com.apps.ondemand.common.utils.AppConstants.CANCELLATION_REASON;

public class DialogCancellationReason extends BaseDialog implements AdapterView.OnItemClickListener, View.OnClickListener {


    Context context;
    List<ReasonModel> reasonModels;
    BottomSheetBehavior mBehavior;
    ReasonSelectionCallback selectionCallback;
    CancellationReasonAdapter reasonAdapter;
    BorderErrorEditText etOtherReason;

    public DialogCancellationReason(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    private DialogCancellationReason(Context context, List<ReasonModel> reasonModels, ReasonSelectionCallback selectionCallback) {
        super(context);
        this.context = context;
        this.reasonModels = reasonModels;
        this.selectionCallback = selectionCallback;

    }

    public static DialogCancellationReason newInstance(Context context, List<ReasonModel> reasonModels, ReasonSelectionCallback selectionCallback) {
        return new DialogCancellationReason(context, reasonModels, selectionCallback);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        View contentView = View.inflate(getContext(), R.layout.cancellation_reason_bottom_sheet, null);
        setContentView(contentView);

        mBehavior = BottomSheetBehavior.from((View) contentView.getParent());
        ((View) contentView.getParent()).setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));

    }


    @Override
    public void onStart() {
        super.onStart();
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        this.getWindow().setLayout(width, height);
        this.setCanceledOnTouchOutside(false);
        populateViews();
    }

    private void populateViews() {
        mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING)
                    bottomSheet.post(new Runnable() {
                        @Override
                        public void run() {
                            mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    });
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
        mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        ListView lvReasons = findViewById(R.id.lv_reasons);

        reasonAdapter = new CancellationReasonAdapter(context, reasonModels);
        lvReasons.setAdapter(reasonAdapter);
        lvReasons.setOnItemClickListener(this);
        findButtonById(R.id.btn_submit).setOnClickListener(this);
        etOtherReason = findViewById(R.id.et_other_reason);

    }

    ReasonModel selectedReason = null;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ReasonModel currentReason = reasonModels.get(position);

        if (selectedReason != null && selectedReason.getId() == currentReason.getId()) {
            for (ReasonModel model : reasonModels) {
                model.setSelected(false);
            }
            reasonAdapter.notifyDataSetChanged();
            selectedReason = null;
        } else {
            for (ReasonModel model : reasonModels) {
                model.setSelected(false);
            }
            selectedReason = reasonModels.get(position);
            selectedReason.setSelected(true);
            reasonAdapter.notifyDataSetChanged();
            selectedReason = currentReason;
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                if (selectedReason == null) {
                    showSnackBar(getContext().getString(R.string.error_select_reason));
                    return;
                }
                if (TextUtils.isEmpty(selectedReason.getId()) && TextUtils.isEmpty(etOtherReason.getText().toString())) {
                    etOtherReason.setError("");
                    showSnackBar(getContext().getString(R.string.error_add_reason));
                    return;
                }
                selectionCallback.onSelectReason(selectedReason, etOtherReason.getText().toString(), CANCELLATION_REASON);
                dismiss();
        }
    }


    @Override
    public void onBackPressed() {
        dismiss();
    }
}
