package com.apps.ondemand.app.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.WeekModel;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;

import java.util.List;


public class PastWeekAdapter extends BaseRecyclerViewAdapter {

    Context context;

    public PastWeekAdapter(List<BaseItem> items, OnRecyclerViewItemClickListener onRecyclerViewItemClickListener) {
        super(items, onRecyclerViewItemClickListener);
        this.context = context;
    }

    @NonNull
    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        BaseRecyclerViewHolder holder;
        if (viewType == BaseItem.ITEM_WEEK) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_past_weeks, parent, false);
            holder = new PastWeekAdapter.WeekHolder(view, true);
        } else if (viewType == BaseItem.ITEM_EMPTY) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
            holder = new PastWeekAdapter.EmptyViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_progress, parent, false);
            holder = new PastWeekAdapter.ProgressViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseRecyclerViewHolder holder, int position) {
        if (holder instanceof WeekHolder) {
            WeekHolder weekHolder = (WeekHolder) holder;
            WeekModel weeks = (WeekModel) getItemAt(position);
            String weekName = weeks.getTitle();
            weekHolder.tvWeekTitle.setText(weekName);
        }
    }

    private class WeekHolder extends BaseRecyclerViewHolder {
        TextView tvWeekTitle, tvWeekEarning;

        WeekHolder(View view, boolean isViewClickable) {
            super(view, isViewClickable);
            view.setOnClickListener(this);
            tvWeekTitle = view.findViewById(R.id.tv_week_title);
            tvWeekEarning = view.findViewById(R.id.tv_week_earning);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return WeekHolder.this;
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            if (getItemClickListener() != null)
                getItemClickListener().onRecyclerViewItemClick(WeekHolder.this);
        }

    }

    public class ProgressViewHolder extends BaseRecyclerViewHolder {
        private ProgressViewHolder(View view) {
            super(view);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }


    public class EmptyViewHolder extends BaseRecyclerViewHolder {
        private TextView textViewEmpty;

        private EmptyViewHolder(View view) {
            super(view);
            textViewEmpty = view.findViewById(R.id.tv_message);
            textViewEmpty.setText(R.string.str_no_weeks_found);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getItemAt(position).getItemType();
    }
}
