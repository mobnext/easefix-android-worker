
package com.apps.ondemand.app.data.models;


import com.apps.ondemand.common.business.BaseItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AttachmentsModel implements BaseItem, Serializable {

    @SerializedName("type")
    @Expose
    private int type;
    @SerializedName("url")
    @Expose
    private String url;

    public AttachmentsModel(){

    }
    public AttachmentsModel(int i, String imageURL) {
        this.type=i;
        this.url=imageURL;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int getItemType() {
        return ITEM_ATTACHMENTS;
    }
}