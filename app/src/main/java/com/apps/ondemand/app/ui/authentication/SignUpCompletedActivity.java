package com.apps.ondemand.app.ui.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.app.ui.main.MainActivity;
import com.apps.ondemand.common.base.BaseActivity;

public class SignUpCompletedActivity extends BaseActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_sign_up_completed);
        UserManager.saveSession(true);
        initView();
    }

    private void initView() {
        findViewById(R.id.btn_continue).setOnClickListener(SignUpCompletedActivity.this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_continue) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }
}
