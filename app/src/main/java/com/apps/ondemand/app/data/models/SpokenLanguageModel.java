package com.apps.ondemand.app.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.apps.ondemand.common.business.BaseItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpokenLanguageModel implements BaseItem, Parcelable {

    @SerializedName("label")
    @Expose
    private String label;

    @SerializedName("subLabel")
    @Expose
    private String subLabel;
    @SerializedName("_id")
    @Expose
    private String id;

    String proficiencyId = "";
    String proficiencyName = "";

    boolean isAdded = false;

    public SpokenLanguageModel() {
    }

    protected SpokenLanguageModel(Parcel in) {
        label = in.readString();
        subLabel = in.readString();
        id = in.readString();
        proficiencyId = in.readString();
        proficiencyName = in.readString();
        isAdded = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(label);
        dest.writeString(subLabel);
        dest.writeString(id);
        dest.writeString(proficiencyId);
        dest.writeString(proficiencyName);
        dest.writeByte((byte) (isAdded ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SpokenLanguageModel> CREATOR = new Creator<SpokenLanguageModel>() {
        @Override
        public SpokenLanguageModel createFromParcel(Parcel in) {
            return new SpokenLanguageModel(in);
        }

        @Override
        public SpokenLanguageModel[] newArray(int size) {
            return new SpokenLanguageModel[size];
        }
    };

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int getItemType() {
        return ITEM_SPOKEN_LANGUAGE;
    }

    public String getSubLabel() {
        return subLabel;
    }

    public void setSubLabel(String subLabel) {
        this.subLabel = subLabel;
    }

    public String getProficiencyId() {
        return proficiencyId;
    }

    public void setProficiencyId(String proficiencyId) {
        this.proficiencyId = proficiencyId;
    }

    public String getProficiencyName() {
        return proficiencyName;
    }

    public void setProficiencyName(String proficiencyName) {
        this.proficiencyName = proficiencyName;
    }

    public boolean isAdded() {
        return isAdded;
    }

    public void setAdded(boolean added) {
        isAdded = added;
    }
}
