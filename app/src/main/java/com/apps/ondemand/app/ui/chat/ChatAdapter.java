package com.apps.ondemand.app.ui.chat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.Chat;
import com.apps.ondemand.app.data.models.Progress;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.utils.AppConstants;

import java.util.List;

/**
 * Created by bilal on 31/01/2018.
 */

public class ChatAdapter extends BaseRecyclerViewAdapter {

    public ChatAdapter(List<BaseItem> items, OnRecyclerViewItemClickListener itemClickListener) {
        super(items, itemClickListener);
    }

    @NonNull
    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        BaseRecyclerViewHolder holder = null;
        if (viewType == BaseItem.ITEM_PROGRESS) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_progress, parent, false);
            holder = new ProgressHolder(view);
        }else if (viewType == BaseItem.ITEM_OUTGOING){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user_chat, parent, false);
            holder = new OutgoingHolder(view);
        }else if (viewType == BaseItem.ITEM_INCOMING){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_driver_chat, parent, false);
            holder = new IncomingHolder(view);
        }
        assert holder != null;
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseRecyclerViewHolder holder, int position) {
        if (holder instanceof ProgressHolder)
            return;
        Chat chat = (Chat) getItemAt(position);
        if (chat == null)
            return;
        if (holder instanceof IncomingHolder)
            populateIncomingChat((IncomingHolder) holder, chat);
        else if (holder instanceof OutgoingHolder)
            populateOutgoingChat((OutgoingHolder) holder, chat);
    }

    private void populateIncomingChat(IncomingHolder holder, Chat chat){
        holder.tvChat.setText(chat.getMessage());
        holder.tvTime.setText(chat.getTimePassed());
    }

    private void populateOutgoingChat(OutgoingHolder holder, Chat chat){
        holder.tvChat.setText(chat.getMessage());
        holder.tvTime.setText(chat.getTimePassed());
    }

    public void addToStart(List<BaseItem> newItems){
        for (int i = newItems.size()- 1; i >= 0 ; i--){
            getAdapterItems().add(0, newItems.get(i));
            notifyItemInserted(0);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (getItemAt(position) instanceof Progress)
            return BaseItem.ITEM_PROGRESS;
        Chat chat = (Chat) getItemAt(position);
        if (chat == null)
            return super.getItemViewType(position);
        if (chat.getSenderType() == AppConstants.INCOMING_MESSAGE)
            return BaseItem.ITEM_INCOMING;
        else
            return BaseItem.ITEM_OUTGOING;
    }

    private static class IncomingHolder extends BaseRecyclerViewHolder {

        TextView tvChat, tvTime;
        @Override
        protected BaseRecyclerViewHolder populateView() {
            return IncomingHolder.this;
        }

        public IncomingHolder(View view) {
            super(view, false);
            tvChat = view.findViewById(R.id.tv_chat);
            tvTime = view.findViewById(R.id.tv_time);
        }
    }


    private static class OutgoingHolder extends BaseRecyclerViewHolder {

        TextView tvChat, tvTime;
        @Override
        protected BaseRecyclerViewHolder populateView() {
            return OutgoingHolder.this;
        }

        public OutgoingHolder(View view) {
            super(view, false);
            tvChat = view.findViewById(R.id.tv_chat);
            tvTime = view.findViewById(R.id.tv_time);
        }
    }

    private static class ProgressHolder extends BaseRecyclerViewHolder {

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return ProgressHolder.this;
        }

        public ProgressHolder(View view) {
            super(view, false);
        }
    }

}
