package com.apps.ondemand.app.ui.dialogs;


import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.JobRequestModel;
import com.apps.ondemand.app.ui.interfaces.JobRequestListener;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.DateUtils;
import com.apps.ondemand.common.utils.VizImageView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.Objects;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class DialogJobRequest extends BaseDialog implements View.OnClickListener {

    BaseActivity activity;
    BottomSheetBehavior mBehavior;
    View contentView;
    JobRequestModel jobRequestModel;
    JobRequestListener jobRequestListener;

    private DialogJobRequest(@NonNull BaseActivity activity, JobRequestModel jobRequestModel, JobRequestListener jobRequestListener) {
        super(activity);
        this.activity = activity;
        this.jobRequestModel = jobRequestModel;
        this.jobRequestListener = jobRequestListener;
    }

    public static DialogJobRequest newInstance(BaseActivity activity, JobRequestModel jobRequestModel, JobRequestListener jobRequestListener) {
        return new DialogJobRequest(activity, jobRequestModel, jobRequestListener);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        contentView = View.inflate(getContext(), R.layout.job_request_bottom_sheet, null);

        setContentView(contentView);

        mBehavior = BottomSheetBehavior.from((View) contentView.getParent());
        ((View) contentView.getParent()).setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent, null));
    }

    @Override
    public void onStart() {
        super.onStart();
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        getWindow().setLayout(width, height);
        setCanceledOnTouchOutside(false);
        populateViews();
        setCancelable(false);
    }

    @Override
    public void onBackPressed() {
        dismiss();
    }

    @SuppressLint({"SetTextI18n", "FindViewByIdCast"})
    private void populateViews() {
        mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING)
                    bottomSheet.post(() -> mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED));
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
        mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        findViewById(R.id.btn_view_details).setOnClickListener(this);
//        findViewById(R.id.btn_accept).setOnClickListener(this);
//        findViewById(R.id.btn_reject).setOnClickListener(this);

        findTextViewById(R.id.tv_job_id).setText(jobRequestModel.getJobIdIdentifier());
        findTextViewById(R.id.tv_profile_name).setText(jobRequestModel.getName());
        VizImageView imageView = findViewById(R.id.iv_profile_image);
        Log.e("job", jobRequestModel.toString());
        try {
            assert jobRequestModel.getProfileImage() != null;
            if (!jobRequestModel.getProfileImage().equalsIgnoreCase("") && !jobRequestModel.getProfileImage().isEmpty()){
                assert imageView != null;
                imageView.setImage(jobRequestModel.getProfileImage(), false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(jobRequestModel.getIsJobFixed()){
            TextView tv_price = findTextViewById(R.id.price);
            tv_price.setVisibility(View.VISIBLE);
            tv_price.setText("Fixed Job ("+AppConstants.POND_CURRENCY+jobRequestModel.getJobFixedPrice()+")");
        }
        ((MaterialRatingBar) Objects.requireNonNull(findViewById(R.id.rb_profile_rating))).setRating(jobRequestModel.getAvgRating());
        findTextViewById(R.id.tv_profile_rating).setText(AppUtils.formatRating(jobRequestModel.getAvgRating()));

        findTextViewById(R.id.tv_job_name).setText(jobRequestModel.getServiceName()+" ("+jobRequestModel.getSubService()+")");
        if(jobRequestModel.getSubService().isEmpty()){
            findTextViewById(R.id.tv_job_name).setText(jobRequestModel.getServiceName());
        }
        findTextViewById(R.id.tv_time).setText(DateUtils.convertDate(AppUtils.GetTimeForForApp(
                jobRequestModel.getJobScheduleTime()), DateUtils.DATE_SCHEDULE_JOB));

          if (jobRequestModel.getJobDurationKnown()){
            findTextViewById(R.id.tv_duration).setText(jobRequestModel.getJobScheduleTime() + "hr " + activity.getString(R.string.str_job));
        } else {
            findTextViewById(R.id.tv_duration).setText(activity.getString(R.string.str_fixed_job));
        }
        findTextViewById(R.id.tv_location).setText(jobRequestModel.getJobAddress());
//        findTextViewById(R.id.tv_location).setText(jobRequestModel.getJobAddress().getPrimaryAddress());
        findTextViewById(R.id.tv_description).setText(jobRequestModel.getWorkDescription());

    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_view_details) {
            jobRequestListener.onButtonPressed(jobRequestModel.getJobId(), AppConstants.ACCEPT_JOB);
            dismiss();
            //            case R.id.btn_accept:
//                jobRequestListener.onButtonPressed(jobRequestModel.getJobId(), AppConstants.ACCEPT_JOB);
//                dismiss();
//                break;
//            case R.id.btn_reject:
//                jobRequestListener.onButtonPressed(jobRequestModel.getJobId(), AppConstants.REJECT_JOB);
//                dismiss();
//                break;
        }
    }
}