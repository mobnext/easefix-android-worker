package com.apps.ondemand.app.ui.earnings;

import android.os.Bundle;

import com.apps.ondemand.R;
import com.apps.ondemand.app.ui.jobs.JobFragment;
import com.apps.ondemand.common.base.BaseActivity;

import static com.apps.ondemand.common.utils.AppConstants.KEY_EXTRA_WEEK_NUMBER;
import static com.apps.ondemand.common.utils.AppConstants.KEY_EXTRA_YEAR;

public class PastJobsActivity extends BaseActivity {
    int year;
    int weekNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_jobs);
        setActionBar(R.string.str_jobs);
        setSubTitle(R.string.str_past_jobs_sub);
        year = getIntent().getIntExtra(KEY_EXTRA_YEAR, 0);
        weekNumber = getIntent().getIntExtra(KEY_EXTRA_WEEK_NUMBER, 0);
        // using fragment to reuse user side code for listing as it is
        onAddFragment(JobFragment.newInstance(weekNumber, year), R.id.content_frame);
    }
}
