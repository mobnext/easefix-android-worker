package com.apps.ondemand.app.ui.authentication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.CountryModel;
import com.apps.ondemand.app.data.models.Empty;
import com.apps.ondemand.app.ui.adapters.CountryAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.GridSpacingItemDecorator;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SelectCountryActivity extends BaseRecyclerViewActivity {

    /*  Pagination relevant variables */
    private int pageNo = 0;
    private boolean isLoading = false;
    private boolean doLoadMore = false;
    private CountryAdapter adapter;
    private static final int LIMIT = 20;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_country);
        setActionBar(R.string.str_select_country);
        setSubTitle(R.string.str_select_country_code);
        initializeRecyclerView();
    }


    @Override
    public void onResume() {
        super.onResume();
        adapter = null;
        setAdapter(null);
        pageNo = 0;
        getListData(true);
    }


    private void initializeRecyclerView() {
        RecyclerView recyclerView = getRecyclerView();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(new GridSpacingItemDecorator(5, AppUtils.dpToPx(5, this)));
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isLoading || !doLoadMore)       //Check if request is already called OR do need to get next page patients
                    return;
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int totalItemCount = linearLayoutManager.getItemCount();
                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (totalItemCount <= (lastVisibleItem + 1)) {
                    getListData(true);
                    doLoadMore = false;
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);

        try {
            isLoading = false;
            removeProgressItem();
            JSONObject responseJson = new JSONObject(response.getResponse());
            if (responseJson.getInt("success") == 1) {
                JSONArray countries = responseJson.getJSONObject("data").getJSONArray("countryCodes");
                if (countries.length() != 0) {
                    List<BaseItem> countryItems = new Gson().fromJson(countries.toString(),
                            new TypeToken<List<CountryModel>>() {
                            }.getType());
                    setCountryListingResponse(countryItems);
                } else {
                    setEmptyLayout();
                    doLoadMore = false;
                }
            } else {
                showSnackBar(responseJson.getString("message"));
                setEmptyLayout();
                doLoadMore = false;
            }
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    }

    @Override
    public void onNetworkError(HttpResponseItem response) {
        super.onNetworkError(response);
        removeProgressItem();
        setEmptyLayout();
        isLoading = false;
    }

    private void getListData(boolean addProgress) {
        isLoading = true;
        HttpRequestItem requestItem = new HttpRequestItem(
                AppConstants.getServerUrl(AppConstants.GET_COUNTRIES));
        requestItem.setHeaderParams(AppUtils.getHeaderParams());    // Add header params (Cookies)
        Map<String, Object> params = new HashMap<>();
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false),
                this);
        appNetworkTask.execute(requestItem);
    }


    private void removeProgressItem() {
        if (getSwipeLayout() != null) {
            getSwipeLayout().setRefreshing(false);
        }
    }

    private void setCountryListingResponse(List<BaseItem> data) throws JSONException {

        List<BaseItem> itemList = new ArrayList<>();


        if (data.size() == 0)
            doLoadMore = false;
        else {
            pageNo++;
            doLoadMore = data.size() == LIMIT;
            itemList.addAll(data);
        }

        populateList(itemList);

    }

    private void populateList(List<BaseItem> itemList) {
        if (adapter == null) {
            adapter = new CountryAdapter(this, itemList, this);
            setAdapter(adapter);
        } else
            adapter.addAll(itemList);

    }

    @Override
    public void onRecyclerViewItemClick(BaseRecyclerViewHolder holder) {
        super.onRecyclerViewItemClick(holder);
        CountryModel countryModel = (CountryModel) adapter.getItemAt(holder.getAdapterPosition());
        Intent resultIntent = new Intent();
        resultIntent.putExtra(AppConstants.KEY_EXTRA_COUNTRY, countryModel);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    private void setEmptyLayout() {
        if (adapter == null || adapter.getItemCount() == 0) {
            List<BaseItem> itemList = new ArrayList<>();
            itemList.add(new Empty(""));
            populateList(itemList);
        }
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        adapter = null;
        setAdapter(null);
        pageNo = 0;
        getListData(false);

    }
}
