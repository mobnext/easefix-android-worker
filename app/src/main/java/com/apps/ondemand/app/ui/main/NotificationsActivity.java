package com.apps.ondemand.app.ui.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.Empty;
import com.apps.ondemand.app.data.models.NotificationModel;
import com.apps.ondemand.app.data.models.Progress;
import com.apps.ondemand.app.ui.adapters.NotificationsAdapter;
import com.apps.ondemand.app.ui.jobs.JobDetailsActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.GridSpacingItemDecorator;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import static com.apps.ondemand.app.communication.SocketService.KEY_READ_NOTIFICATION;
import static com.apps.ondemand.common.utils.AppConstants.KEY_JOB_ID;
import static com.apps.ondemand.common.utils.NetworkUtils.HTTP_POST;

public class NotificationsActivity extends BaseRecyclerViewActivity {

    private int pageNo = 0;
    private boolean isLoading = false;
    private boolean doLoadMore = false;
    private NotificationsAdapter adapter;
    private static final int LIMIT = 20;
    private String notificationUrl = "";
    private int type;
    private String notificationsActionURL = "";
    private List<BaseItem> notificationItems = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        setActionBar(R.string.str_notifications);
        setSubTitle(R.string.str_notifications_sub);
        initializeRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter = null;
        setAdapter(null);
        pageNo = 0;
        getListData(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notifications_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_read_all: {
                type = AppConstants.MARKS_AS_READ;
                performNotificationAction();
                break;
            }
            case R.id.mi_delete_all: {
                showDeleteConfirmationDialog();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void showDeleteConfirmationDialog() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(getString(R.string.app_name))
                .setMessage(R.string.str_delete_notification)
                .setPositiveButton(R.string.str_okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        type = AppConstants.CLEAR_ALL;
                        performNotificationAction();
                    }
                })
                .setNegativeButton(R.string.str_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }

    public void performNotificationAction() {
        notificationsActionURL = AppConstants.getServerUrl(AppConstants.NOTIFICATION_ACTION);
        HttpRequestItem requestItem = new HttpRequestItem(notificationsActionURL);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        requestItem.setHttpRequestType(HTTP_POST);
        requestItem.setParams(getParam());
        AppNetworkTask mAppNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        mAppNetworkTask.execute(requestItem);
    }

    private Map<String, Object> getParam() {
        Map<String, Object> map = new HashMap<>();
        map.put("type", type);
        map.put("userType", AppConstants.TYPE_SP);
        return map;
    }

    private void initializeRecyclerView() {
        RecyclerView recyclerView = getRecyclerView();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(new GridSpacingItemDecorator(5, AppUtils.dpToPx(5, this)));
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isLoading || !doLoadMore)       //Check if request is already called OR do need to get next page patients
                    return;
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int totalItemCount = linearLayoutManager.getItemCount();
                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (totalItemCount <= (lastVisibleItem + 1)) {
                    getListData(true);
                    doLoadMore = false;
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);

        try {
            isLoading = false;
            removeProgressItem();
            JSONObject responseJson = new JSONObject(response.getResponse());
            if (responseJson.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equalsIgnoreCase(notificationUrl)) {
                    JSONArray notifications = responseJson.getJSONObject("data").getJSONArray("notificationData");
                    if (notifications.length() != 0) {
                        notificationItems = new Gson().fromJson(notifications.toString(),
                                new TypeToken<List<NotificationModel>>() {
                                }.getType());
                        setListingResponse(notificationItems);
                    } else {
                        hideMenuItems();
                        setEmptyLayout();
                        doLoadMore = false;
                    }
                } else if (notificationsActionURL.contains(response.getHttpRequestUrl())) {
                    if (type == AppConstants.MARKS_AS_READ) {
                        for (BaseItem baseItem : adapter.getAdapterItems()) {
                            NotificationModel notificationModel = (NotificationModel) baseItem;
                            if (!notificationModel.getIsRead())
                                notificationModel.setIsRead(true);
                        }
                        adapter.notifyDataSetChanged();
                    } else if (type == AppConstants.CLEAR_ALL) {
                        adapter = null;
                        hideMenuItems();
                        getListData(true);
                    }
                }
            } else {
                showSnackBar(responseJson.getString("message"));
                setEmptyLayout();
                doLoadMore = false;
            }
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    }

    private void hideMenuItems() {
        if (getToolbar() != null) {
            getToolbar().getMenu().findItem(R.id.mi_delete_all).setVisible(false);
            getToolbar().getMenu().findItem(R.id.mi_read_all).setVisible(false);
        }
    }

    @Override
    public void onNetworkError(HttpResponseItem response) {
        super.onNetworkError(response);
        removeProgressItem();
        setEmptyLayout();
        isLoading = false;
    }

    private void getListData(boolean addProgress) {
        isLoading = true;
        if (addProgress) {
            addProgressItem();
        }
        notificationUrl = AppConstants.getServerUrl(String.format(AppConstants.GET_NOTIFICATIONS, pageNo * LIMIT, LIMIT));
        HttpRequestItem requestItem = new HttpRequestItem(notificationUrl);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());// Add header params (Cookies)
        AppNetworkTask appNetworkTask = new AppNetworkTask(null,
                this);
        appNetworkTask.execute(requestItem);
    }

    private void setListingResponse(List<BaseItem> data) throws JSONException {

        List<BaseItem> itemList = new ArrayList<>();


        if (data.size() == 0)
            doLoadMore = false;
        else {
            pageNo++;
            doLoadMore = data.size() == LIMIT;
            itemList.addAll(data);
        }

        populateList(itemList);

    }

    private void populateList(List<BaseItem> itemList) {
        if (adapter == null) {
            adapter = new NotificationsAdapter(this, itemList, this);
            setAdapter(adapter);
        } else
            adapter.addAll(itemList);

    }

    private void addProgressItem() {
        List<BaseItem> items = new ArrayList<>();
        items.add(new Progress());
        populateList(items);
    }

    private void removeProgressItem() {
        if (adapter != null && adapter.getAdapterCount() > 0 &&
                adapter.getItemAt(adapter.getAdapterCount() - 1) instanceof Progress) {
            adapter.remove(adapter.getAdapterCount() - 1);
            adapter.notifyItemRemoved(adapter.getAdapterCount() - 1);
        }
        if (getSwipeLayout() != null) {
            getSwipeLayout().setRefreshing(false);
        }
    }

    private void setEmptyLayout() {
        if (adapter == null || adapter.getItemCount() == 0) {
            List<BaseItem> itemList = new ArrayList<>();
            itemList.add(new Empty());
            populateList(itemList);
        }
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        adapter = null;
        setAdapter(null);
        pageNo = 0;
        getListData(false);

    }

    @Override
    public void onRecyclerViewItemClick(BaseRecyclerViewHolder holder) {
        super.onRecyclerViewItemClick(holder);
        int position = holder.getAdapterPosition();
        NotificationModel model = (NotificationModel) adapter.getItemAt(position);
        emitData(model.getId());
        model.setIsRead(true);
        adapter.notifyItemChanged(position);

        Intent intent = new Intent(this, JobDetailsActivity.class);
        intent.putExtra(KEY_JOB_ID, model.getJobId());
        startActivity(intent);
    }


    private void emitData(String notificationId) {
        try {
            JSONObject data = new JSONObject();
            data.put("notificationId", notificationId);
            emitFromSocket(KEY_READ_NOTIFICATION, data);
            Log.d("TAG", "emitData: " + new Gson().toJson(data));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
