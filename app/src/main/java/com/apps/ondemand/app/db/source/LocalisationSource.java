package com.apps.ondemand.app.db.source;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;

import com.apps.ondemand.app.data.models.LocalisationModel;
import com.apps.ondemand.app.db.core.DatabaseConnection;
import com.apps.ondemand.app.db.source.core.BaseDataSource;
import com.apps.ondemand.common.utils.Logger;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static com.apps.ondemand.app.db.DBConstants.Localisation.ID;
import static com.apps.ondemand.app.db.DBConstants.Localisation.KEY;
import static com.apps.ondemand.app.db.DBConstants.Localisation.TABLE_NAME;
import static com.apps.ondemand.app.db.DBConstants.Localisation.VALUE;


/**
 *
 *
 * @author Rana
 */
public class LocalisationSource extends BaseDataSource<LocalisationModel> {

    private LocalisationSource() {
    }

    @NonNull
    public static LocalisationSource newInstance() {
        return new LocalisationSource();
    }

    @Override
    protected void fillValues(@NonNull LocalisationModel model, @NonNull ContentValues values) {
        values.put(KEY, model.getKey());
        values.put(VALUE, model.getValue());
    }

    @NonNull
    @Override
    protected LocalisationModel getModelFromCursor(@NonNull Cursor cursor) {
        LocalisationModel model = new LocalisationModel();
        model.setKey(cursor.getString(cursor.getColumnIndex(KEY)));
        model.setValue(cursor.getString(cursor.getColumnIndex(VALUE)));
        return model;
    }


    public String get(String key) {

        String ret = "";
        List<LocalisationModel> feeds = getAllWhere(KEY, key);

        if (feeds.size() > 0) {
            ret = feeds.get(0).getValue();
        }
        return ret;
    }

    public HashMap<String, String> getAllByKeySpecifier(String key) {
        HashMap<String, String> returner = new HashMap<>();

        String rawQuery = "SELECT * FROM " + getTableName()
                + " WHERE " + KEY + " LIKE '%" + key + "%'";

        SQLiteDatabase db = DatabaseConnection.getAndOpenConnection();
        Cursor cursor = db.rawQuery(rawQuery, null);

        if (cursor.moveToFirst()) {
            do {
                LocalisationModel feedModel = getModelFromCursor(cursor);
                returner.put(feedModel.getKey(), feedModel.getValue());
            } while (cursor.moveToNext());
        }

        cursor.close();
        DatabaseConnection.closeConnection();
        return returner;
    }

    public void delete(String key) {
        deleteWhere(KEY, key);
    }

    public void add(String key, String value) {
        insertOrUpdate(new LocalisationModel(key, value));
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected String getFilterKey() {
        return ID;
    }

    public void addTranslations(JSONObject translations) {
        LocalisationSource.newInstance().deleteAll();
        SQLiteDatabase db = DatabaseConnection.getAndOpenConnection();
        db.beginTransaction();
        try {
            for (Iterator<String> iter = translations.keys(); iter.hasNext(); ) {
                String key = iter.next();
                String value = translations.getString(key);
                ContentValues contentValues = new ContentValues();
                fillValues(new LocalisationModel(key, value), contentValues);
                long insertId = db.insertWithOnConflict(getTableName(), null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
                Logger.debug(LocalisationSource.class.getName(), String.valueOf(insertId));
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            DatabaseConnection.closeConnection();
        }
    }
} // LocalisationSource
