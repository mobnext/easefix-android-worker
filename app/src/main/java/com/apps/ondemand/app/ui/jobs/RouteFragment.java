package com.apps.ondemand.app.ui.jobs;

import static com.apps.ondemand.app.ui.jobs.JobDetailsActivity.latitudeCurrent;
import static com.apps.ondemand.app.ui.jobs.JobDetailsActivity.longitudeCurrent;
import static com.apps.ondemand.common.utils.AppConstants.GOOGLE_BASE_URL;
import static com.apps.ondemand.common.utils.AppConstants.KEY_JOB_DETAILS;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.core.content.ContextCompat;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.JobDetailModel;
import com.apps.ondemand.app.data.models.RouteModel;
import com.apps.ondemand.common.base.GoogleMapFragment;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RouteFragment extends GoogleMapFragment {

    protected View rootView;
    private boolean isFirstLocation = true;
    JobDetailModel jobDetailsModel;
    Location routeEndLocation;
    int routePadding = 100;
    private boolean isZoomEnabled = true;
    private Marker navigator;
    private PolylineOptions polyline;
    ProgressBar progressBar;

    public RouteFragment() {
        // Required empty public constructor
    }

    public static RouteFragment newInstance(JobDetailModel jobDetailModel) {
        RouteFragment fragment = new RouteFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_JOB_DETAILS, jobDetailModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            jobDetailsModel = getArguments().getParcelable(KEY_JOB_DETAILS);
        }
        routeEndLocation = new Location("");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_route, container, false);
        progressBar = rootView.findViewById(R.id.progress);
        return rootView;
    }


    @SuppressLint("MissingPermission")
    @Override
    public void moveToMyLocation(Location location) {
        super.moveToMyLocation(location);
        googleMap.setMyLocationEnabled(true);
        if (location == null) {
            Log.e("fragment", "anjum2");
            return;
        }
        if (isFirstLocation) {

            Log.e("fragment", "anjum1"+latitudeCurrent+"/"+longitudeCurrent);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitudeCurrent,
                    longitudeCurrent)).zoom(AppConstants.ZOOM_LEVEL).build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            isFirstLocation = false;
            googleMap.addMarker(new MarkerOptions().position(new LatLng(latitudeCurrent, longitudeCurrent)).anchor(0.5f, 0.5f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.radius)));
        }
    }


    @Override
    public void setMapProperties() {
        super.setMapProperties();
        googleMap.getUiSettings().setAllGesturesEnabled(true);
        googleMap.setOnMarkerClickListener(marker -> {

            // Event was handled by our code do not launch default behaviour.
            return true;
        });
        googleMap.setOnMapLoadedCallback(() -> updateJobModel(jobDetailsModel));
    }

    public void updateJobModel(JobDetailModel detailModel) {
        this.jobDetailsModel = detailModel;

        if (jobDetailsModel.getSpJobStatus() == AppConstants.STATUS_OPEN) {
            //findViewById(R.id.radius).setVisibility(View.VISIBLE);
            getDirection(jobDetailsModel.getPickupLatitude(),
                    jobDetailsModel.getPickupLongitude(),
                    jobDetailsModel.getJobAddress().getLatitude(),
                    jobDetailsModel.getJobAddress().getLongitude(), false);
            findTextViewById(R.id.tv_status).setText(R.string.str_status_open);
            findViewById(R.id.ll_status).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_distance_time).setVisibility(View.GONE);
        } else if (jobDetailsModel.getSpJobStatus() == AppConstants.STATUS_ACCEPTED) {
            findViewById(R.id.radius).setVisibility(View.GONE);
            getDirection(jobDetailsModel.getPickupLatitude(),
                    jobDetailsModel.getPickupLongitude(),
                    jobDetailsModel.getJobAddress().getLatitude(),
                    jobDetailsModel.getJobAddress().getLongitude(), false);
            findTextViewById(R.id.tv_status).setText(R.string.str_status_accepted);
            findViewById(R.id.ll_status).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_distance_time).setVisibility(View.GONE);
        } else if (jobDetailsModel.getSpJobStatus() == AppConstants.STATUS_ON_THE_WAY) {
            findViewById(R.id.radius).setVisibility(View.GONE);
            isZoomEnabled = true;
            getDirection(jobDetailsModel.getPickupLatitude(),
                    jobDetailsModel.getPickupLongitude(),
                    jobDetailsModel.getJobAddress().getLatitude(),
                    jobDetailsModel.getJobAddress().getLongitude(), true);
            findTextViewById(R.id.tv_status).setText(R.string.str_status_on_way);
            findViewById(R.id.ll_status).setVisibility(View.GONE);
            findViewById(R.id.ll_distance_time).setVisibility(View.GONE);
            routeMap(true);
        } else if (jobDetailsModel.getSpJobStatus() == AppConstants.STATUS_ARRIVED) {
            findViewById(R.id.radius).setVisibility(View.GONE);
            isZoomEnabled = true;
            populateAfterCompletion(detailModel.getRouteModel());
            findTextViewById(R.id.tv_status).setText(R.string.str_status_arrived);
            findViewById(R.id.ll_status).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_distance_time).setVisibility(View.GONE);
            routeMap(true);
        } else if (jobDetailsModel.getSpJobStatus() == AppConstants.STATUS_STARTED) {
            findViewById(R.id.radius).setVisibility(View.GONE);
            isZoomEnabled = true;
            populateAfterCompletion(detailModel.getRouteModel());
            findTextViewById(R.id.tv_status).setText(R.string.str_status_on_going);
            findViewById(R.id.ll_status).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_distance_time).setVisibility(View.GONE);
        } else if (jobDetailsModel.getSpJobStatus() == AppConstants.STATUS_COMPLETED) {
            findViewById(R.id.radius).setVisibility(View.GONE);
            isZoomEnabled = true;
            populateAfterCompletion(detailModel.getRouteModel());
            findTextViewById(R.id.tv_status).setText(R.string.str_status_completed);
            findViewById(R.id.ll_status).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_distance_time).setVisibility(View.GONE);
        } else if (jobDetailsModel.getSpJobStatus() == AppConstants.STATUS_CANCELLED) {
            findViewById(R.id.radius).setVisibility(View.VISIBLE);
            findTextViewById(R.id.tv_status).setText(R.string.str_status_cancelled);
            findViewById(R.id.ll_status).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_distance_time).setVisibility(View.GONE);
        } else if (jobDetailsModel.getSpJobStatus() == AppConstants.STATUS_REJECTION) {
            findViewById(R.id.radius).setVisibility(View.VISIBLE);
            findTextViewById(R.id.tv_status).setText(R.string.str_status_rejected);
            findViewById(R.id.ll_status).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_distance_time).setVisibility(View.GONE);
        }
    }

    public void routeMap(boolean show) {
        if (show) {
            findViewById(R.id.routeDraw).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.routeDraw).setVisibility(View.GONE);
        }
        findViewById(R.id.routeDraw).setOnClickListener(v -> {
            Uri gmmIntentUri = Uri.parse("google.navigation:q=" + jobDetailsModel.getDropOffLatitude() + "," + jobDetailsModel.getDropOffLongitude());
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            try {
                startActivity(mapIntent);
            } catch (ActivityNotFoundException innerEx) {
                showToast("Please install a maps application");
            }
//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr="+jobDetailsModel.getDropOffLatitude()+"&daddr="+jobDetailsModel.getDropOffLongitude()));
//                startActivity(intent);
        });

    }

    private void getDirection(double startLat, double startLong,
                              double endLat, double endLong, boolean isInBackground) {
        if (getContext() == null) {
            return;
        }
        if (!isInBackground) {
            showProgress();
        }
        routeEndLocation.setLatitude(endLat);
        routeEndLocation.setLongitude(endLong);
        HttpRequestItem requestItem = new HttpRequestItem(GOOGLE_BASE_URL
                + AppConstants.GET_DIRECTIONS);
        requestItem.setInBackGround(isInBackground);
        Map<String, Object> param = new HashMap<>();
        param.put("origin", "" + startLat + "," + startLong);
        param.put("destination", endLat + "," + endLong);
        param.put("mode", "driving");
        param.put("sensor", false);
        param.put("key", getString(R.string.google_key));
        requestItem.setParams(param);
        AppNetworkTask mAppNetworkTask = new AppNetworkTask(null, this);
        mAppNetworkTask.execute(requestItem);
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            hideProgress();
            JSONObject responseJson = new JSONObject(response.getResponse());
            String points = "";
            if (responseJson.has("geocoded_waypoints")) {
                JSONArray routes = responseJson.getJSONArray("routes");
                if (response.isInBackGround()) {
                    if (routes.length() > 0) {
                        JSONObject route = routes.getJSONObject(0);
                        JSONObject polyLine = route.getJSONObject("overview_polyline");
                        points = polyLine.getString("points");
                        if (route.getJSONArray("legs").length() > 0) {
                            JSONObject leg = route.getJSONArray("legs").getJSONObject(0);
                            updateEstimatedTime(leg);
                        }
                    }
                    populateWhileNavigation(decodePoly(points));
                } else {
                    if (routes.length() > 0) {
                        JSONObject route = routes.getJSONObject(0);
                        JSONObject polyLine = route.getJSONObject("overview_polyline");
                        points = polyLine.getString("points");
                        route.getJSONArray("legs").length();
                        setDataOnMap(points);
                    }
                }
            }
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    }

    @Override
    public void onNetworkError(HttpResponseItem response) {
        super.onNetworkError(response);
        hideProgress();
    }

    public void setDataOnMap(final String points) {
        new Handler().postDelayed(() -> populateOnMap(decodePoly(points)), 500);
    }

    private void updateEstimatedTime(JSONObject leg) throws JSONException {
        findViewById(R.id.ll_status).setVisibility(View.GONE);
        findViewById(R.id.ll_distance_time).setVisibility(View.VISIBLE);
        JSONObject duration = leg.getJSONObject("duration");
        JSONObject distance = leg.getJSONObject("distance");
        String durationStr = AppUtils.getStringFromJson(duration, "text");
        String distanceStr = AppUtils.getStringFromJson(distance, "text");
        findTextViewById(R.id.tv_distance).setText(distanceStr);
        findTextViewById(R.id.tv_duration).setText(durationStr);
    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<>();
        if (!AppUtils.ifNotNullEmpty(encoded))
            return poly;
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }


    private void populateWhileNavigation(final List<LatLng> latLngs) {
        if (latLngs.size() > 0 && googleMap != null) {
            googleMap.clear();
            LatLng start = latLngs.get(0);
            LatLng end = latLngs.get(latLngs.size() - 1);

            navigator = googleMap.addMarker(new MarkerOptions().position(start).anchor(0.5f, 0.5f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));

            googleMap.addMarker(new MarkerOptions().position(end).anchor(0.5f, 1.0f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_dropoff)));

            polyline = new PolylineOptions().addAll(latLngs).color(ContextCompat.getColor(requireContext(), R.color.purple));

            googleMap.addPolyline(polyline);

            if (isZoomEnabled) {
                rotateMarker(navigator, (float) bearingBetweenLocations(start, end));
                zoomRoute(latLngs);
            } else {
                navigator.setRotation((float) angleToRotate);
            }
        }

    }

    private void populateOnMap(final List<LatLng> latLngs) {

        if (latLngs.size() > 0 && googleMap != null) {
            googleMap.clear();

            LatLng start = latLngs.get(0);
            LatLng end = latLngs.get(latLngs.size() - 1);

            googleMap.addMarker(new MarkerOptions().position(start).anchor(0.5f, 0.5f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pickup)));

            googleMap.addMarker(new MarkerOptions().position(end).anchor(0.5f, 1.0f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_dropoff)));
            googleMap.addPolyline(new PolylineOptions().addAll(latLngs).color(ContextCompat.getColor(requireContext(), R.color.purple)));
            zoomRoute(latLngs);
        }

    }

    private void populateAfterCompletion(final List<RouteModel> spRoute) {

        googleMap.clear();
        List<LatLng> spRouteLatLngs = new ArrayList<>();

        for (RouteModel routeModel : spRoute) {
            spRouteLatLngs.add(new LatLng(routeModel.getLatitude(), routeModel.getLongitude()));
        }


        if (spRouteLatLngs.size() > 0) {

            LatLng start = spRouteLatLngs.get(0);
            LatLng end = spRouteLatLngs.get(spRouteLatLngs.size() - 1);

            googleMap.addMarker(new MarkerOptions().position(start).anchor(0.5f, 0.5f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pickup)));


            googleMap.addMarker(new MarkerOptions().position(end).anchor(0.5f, 1.0f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_dropoff)));
            polyline = new PolylineOptions().addAll(spRouteLatLngs).color(ContextCompat.getColor(requireContext(), R.color.purple));
            googleMap.addPolyline(polyline);
            zoomRoute(spRouteLatLngs);
        }
    }

    /**
     * Zooms a Route (given a List of LalLng) at the greatest possible zoom level.
     *
     * @param lstLatLngRoute: list of LatLng forming Route
     */
    public void zoomRoute(List<LatLng> lstLatLngRoute) {

        if (googleMap == null || lstLatLngRoute == null || lstLatLngRoute.isEmpty())
            return;

        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        for (LatLng latLngPoint : lstLatLngRoute)
            boundsBuilder.include(latLngPoint);


        LatLngBounds latLngBounds = boundsBuilder.build();

        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, routePadding));

    }

    public void updateLocations(final double latitude, final double longitude) {

        if (googleMap != null && navigator != null) {
            animateMarker(googleMap, navigator, new LatLng(latitude, longitude), () -> {
                isZoomEnabled = false;
                getDirection(latitude, longitude, routeEndLocation.getLatitude(), routeEndLocation.getLongitude(), true);

            });

            double bearing = bearingBetweenLocations(new LatLng(jobDetailsModel.getJobAddress().getLatitude(), jobDetailsModel.getJobAddress().getLongitude()),
                    new LatLng(latitude, longitude));
            rotateMarker(navigator, (float) bearing);
            jobDetailsModel.setPickupLatitude(latitude);
            jobDetailsModel.setPickupLongitude(longitude);
        }
    }

    private void showProgress() {
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    private void hideProgress() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
    }
}
