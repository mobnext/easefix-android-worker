package com.apps.ondemand.app.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.ondemand.R;
import com.apps.ondemand.common.base.BaseActivity;

import java.util.ArrayList;

public class DepositAdapter extends RecyclerView.Adapter {


    private BaseActivity context;
    int maxTimeSlots;
    private onDepositAmount onHourSelect;
    ArrayList<Integer> arrayList;
    int depositSlot;

    public interface onDepositAmount {
        void onDepositSelected(int selectedHourSlot);
    }

    public DepositAdapter(BaseActivity context, int maxTimeSlots, int selectedTimeSlot, onDepositAmount onHourSelect) {
        this.context = context;
        this.onHourSelect = onHourSelect;
        this.maxTimeSlots = maxTimeSlots;
        this.depositSlot = selectedTimeSlot;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_deposit_slot, parent, false);
        return new DepositHolder(view);
    }

    public void setDepositAmount(int depositAmount) {
        this.depositSlot = depositAmount;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof DepositHolder) {
            DepositHolder depositHolder = (DepositHolder) holder;
            if (position == depositSlot) {
                depositHolder.rlTimeSlot.setSelected(true);
                depositHolder.tvTimeSlot.setSelected(true);
            } else {
                depositHolder.rlTimeSlot.setSelected(false);
                depositHolder.tvTimeSlot.setSelected(false);
            }
            if (position == 0) {
                depositHolder.tvTimeSlot.setText("100%");
            }else if (position==1){
                depositHolder.tvTimeSlot.setText("50%");
            }else if (position==2){
                depositHolder.tvTimeSlot.setText("25%");
            }
        }
    }


    @Override
    public int getItemCount() {
        return maxTimeSlots;
    }


    private class DepositHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RelativeLayout rlTimeSlot;
        private TextView tvTimeSlot;

        public DepositHolder(View view) {
            super(view);
            rlTimeSlot = (RelativeLayout) view.findViewById(R.id.rl_time_slot);
            tvTimeSlot = (TextView) view.findViewById(R.id.tv_time_slot);
            rlTimeSlot.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onHourSelect != null) {
                onHourSelect.onDepositSelected(getAdapterPosition());
            }

        }
    }

}
