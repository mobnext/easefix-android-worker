package com.apps.ondemand.app.data.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LocData {

    String spProfileId;

    public LocData() {

    }

    public String getSpProfileId() {
        return spProfileId;
    }

    public void setSpProfileId(String spProfileId) {
        this.spProfileId = spProfileId;
    }

    List<Map<String, Double>> coordinates = new ArrayList<>();

    public List<Map<String, Double>> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Map<String, Double>> coordinates) {
        this.coordinates = coordinates;
    }
}
