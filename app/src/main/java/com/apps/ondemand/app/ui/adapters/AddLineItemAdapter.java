package com.apps.ondemand.app.ui.adapters;

import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.LineItemModel;
import com.apps.ondemand.app.data.models.ServiceModel;
import com.apps.ondemand.app.ui.interfaces.LineItemListener;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.BorderErrorEditText;

import java.util.List;

import static com.apps.ondemand.common.utils.AppConstants.REGISTER_AS_COMPANY;


public class AddLineItemAdapter extends BaseRecyclerViewAdapter {

    BaseActivity context;
    LineItemListener lineItemListener;

    public AddLineItemAdapter(BaseActivity context, List<BaseItem> items, LineItemListener lineItemListener) {
        super(items, null);
        this.context = context;
        this.lineItemListener = lineItemListener;
    }

    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_add_line_item, parent, false);
        return new AddLineItemHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder holder, int position) {
        if (holder instanceof AddLineItemHolder) {
            AddLineItemHolder addLineItemHolder = (AddLineItemHolder) holder;
            LineItemModel lineItemModel = (LineItemModel) getItemAt(position);
            addLineItemHolder.tvLineItemLabel.setText(Html.fromHtml(lineItemModel.getItemName() + " <sup>x" + lineItemModel.getItemQuantity() + "</sup>"));
            addLineItemHolder.tvLineItemCost.setText((int) (lineItemModel.getItemPrice()*lineItemModel.getItemQuantity())+"");
            addLineItemHolder.etItemName.setText(lineItemModel.getItemName());
            addLineItemHolder.etItemName.setSelection(addLineItemHolder.etItemName.getText().length());

            if (lineItemModel.getItemQuantity() > 0) {
                addLineItemHolder.etItemQuantity.setText(String.valueOf(lineItemModel.getItemQuantity()));
            } else {
                addLineItemHolder.etItemQuantity.setText("");
            }
            if (lineItemModel.getItemPrice() > 0) {
                addLineItemHolder.etItemPrice.setText(String.valueOf(lineItemModel.getItemPrice()));
            } else {
                addLineItemHolder.etItemPrice.setText("");

            }
            if (!lineItemModel.isAdded() && !lineItemModel.isEditMode()) {
                // new one
                addLineItemHolder.llEditLineItem.setVisibility(View.VISIBLE);
                addLineItemHolder.rlAddedLineItem.setVisibility(View.GONE);
                addLineItemHolder.btnDiscard.setText(R.string.str_discard);
            } else if (lineItemModel.isAdded() && lineItemModel.isEditMode()) {
                // added and edit mode
                addLineItemHolder.rlAddedLineItem.setVisibility(View.VISIBLE);
                addLineItemHolder.llEditLineItem.setVisibility(View.VISIBLE);
                addLineItemHolder.btnDiscard.setText(R.string.str_delete);
            } else if (lineItemModel.isAdded() && !lineItemModel.isEditMode()) {
                // added
                addLineItemHolder.rlAddedLineItem.setVisibility(View.VISIBLE);
                addLineItemHolder.llEditLineItem.setVisibility(View.GONE);
                addLineItemHolder.btnDiscard.setText(R.string.str_delete);
            }
        }

    }


    private class AddLineItemHolder extends BaseRecyclerViewHolder {
        private RelativeLayout rlAddedLineItem;
        private TextView tvLineItemLabel;
        private TextView tvLineItemCost;
        private ImageView ivEditLineItem;
        private LinearLayout llEditLineItem;
        private BorderErrorEditText etItemName;
        private BorderErrorEditText etItemQuantity;
        private BorderErrorEditText etItemPrice;
        private Button btnDiscard;
        private Button btnSave;

        public AddLineItemHolder(View view) {
            super(view);
            rlAddedLineItem = (RelativeLayout) view.findViewById(R.id.rl_added_line_item);
            tvLineItemLabel = (TextView) view.findViewById(R.id.tv_line_item_label);
            tvLineItemCost = (TextView) view.findViewById(R.id.tv_line_item_cost);
            ivEditLineItem = (ImageView) view.findViewById(R.id.iv_edit_line_item);
            llEditLineItem = (LinearLayout) view.findViewById(R.id.ll_edit_line_item);
            etItemName = (BorderErrorEditText) view.findViewById(R.id.et_item_name);
            etItemQuantity = (BorderErrorEditText) view.findViewById(R.id.et_item_quantity);
            etItemPrice = (BorderErrorEditText) view.findViewById(R.id.et_item_price);
            btnDiscard = (Button) view.findViewById(R.id.btn_discard);
            btnSave = (Button) view.findViewById(R.id.btn_save);
            btnDiscard.setOnClickListener(this);
            btnSave.setOnClickListener(this);
            ivEditLineItem.setOnClickListener(this);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return AddLineItemHolder.this;
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            AppUtils.hideSoftKeyboard(context);
            int position = getAdapterPosition();
            LineItemModel lineItemModel = (LineItemModel) getItemAt(position);
            switch (v.getId()) {
                case R.id.btn_discard:
                    onDelete(lineItemModel, position);

                    break;

                case R.id.btn_save:
                    onSave(lineItemModel, position);
                    break;

                case R.id.iv_edit_line_item:
                    lineItemModel.setEditMode(!lineItemModel.isEditMode());
                    notifyItemChanged(position);
                    break;
            }
        }

        private void onDelete(LineItemModel lineItemModel, int position) {
            AppConstants.line_Visible=0;
            if (lineItemModel.isAdded()) {
                lineItemListener.onButtonClicked(lineItemModel, position, LineItemListener.DELETE);
            } else {
                lineItemListener.onButtonClicked(lineItemModel, position, LineItemListener.DISCARD);
            }
        }

        private void onSave(LineItemModel lineItemModel, int position) {
            AppUtils.hideSoftKeyboard(context);
            String itemName = etItemName.getText().toString();
            String itemQuantity = etItemQuantity.getText().toString();
            String itemPrice = etItemPrice.getText().toString();
            AppConstants.line_Visible=0;

            if (TextUtils.isEmpty(itemName)) {
                etItemName.setError("");
                context.showSnackBar(R.string.error_empty_item_name);
                return;
            }
            if (TextUtils.isEmpty(itemQuantity)) {
                etItemQuantity.setError("");
                context.showSnackBar(R.string.error_empty_item_quantity);
                return;
            }
            if (TextUtils.isEmpty(itemPrice)) {
                etItemPrice.setError("");
                context.showSnackBar(R.string.error_empty_item_price);
                return;
            }
            if (Integer.parseInt(itemQuantity) == 0) {
                etItemQuantity.setError("");
                context.showSnackBar(R.string.error_greater_than_zero);
                return;
            }
            if (Float.parseFloat(itemPrice) == 0.0f) {
                etItemPrice.setError("");
                context.showSnackBar(R.string.error_price_greater_zero);
                return;
            }
            lineItemModel.setItemName(itemName);
            lineItemModel.setItemQuantity(Integer.parseInt(itemQuantity));
            lineItemModel.setItemPrice(Float.parseFloat(itemPrice));
            if (lineItemModel.isEditMode()) {
                lineItemListener.onButtonClicked(lineItemModel, position, LineItemListener.EDIT);
            } else {
                lineItemListener.onButtonClicked(lineItemModel, position, LineItemListener.SAVE);
            }
        }
    }

    public class ProgressViewHolder extends BaseRecyclerViewHolder {
        private ProgressViewHolder(View view) {
            super(view);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    public class EmptyViewHolder extends BaseRecyclerViewHolder {
        private TextView textViewEmpty;

        private EmptyViewHolder(View view) {
            super(view);
            textViewEmpty = view.findViewById(R.id.tv_message);
            textViewEmpty.setText("");
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getItemAt(position).getItemType();
    }
}
