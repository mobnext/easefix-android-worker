package com.apps.ondemand.app.ui.authentication;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.Button;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.LanguageManager;
import com.apps.ondemand.app.data.models.LanguageModel;
import com.apps.ondemand.app.ui.adapters.SelectLanguageAdapter;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.utils.LocaleUtils;

import java.util.List;

import static com.apps.ondemand.app.data.LanguageManager.getLanguageList;

public class SelectLanguageActivity extends BaseActivity implements OnRecyclerViewItemClickListener, View.OnClickListener {
    protected Button btnContinue;
    RecyclerView recyclerView;
    SelectLanguageAdapter adapter;
    private List<BaseItem> languageItems;
    LanguageModel selectLanguageModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_select_language);
        setActionBar(R.string.str_sign_up);
        setSubTitle(R.string.str_select_language);
        languageItems = getLanguageList();
        setSelectedLanguage();
        initView();
        setAdapter();
    }

    private void initView() {
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(false);
        btnContinue = (Button) findViewById(R.id.btn_continue);
        btnContinue.setOnClickListener(SelectLanguageActivity.this);
    }

    private void setAdapter() {
        adapter = new SelectLanguageAdapter(this, languageItems, this);
        recyclerView.setAdapter(adapter);
    }

    private void setSelectedLanguage() {
        String code = LanguageManager.getUserPreferredLanguage();

        for (BaseItem languageItem : languageItems) {
            LanguageModel languageModel = (LanguageModel) languageItem;
            if (languageModel.getLanguageCode().equals(code)) {
                selectLanguageModel = languageModel;
                languageModel.setSelected(true);
            } else {
                languageModel.setSelected(false);
            }

        }
    }


    @Override
    public void onRecyclerViewItemClick(BaseRecyclerViewHolder holder) {
        selectLanguageModel = (LanguageModel) adapter.getItemAt(holder.getAdapterPosition());
        for (BaseItem languageItem : languageItems) {
            ((LanguageModel) languageItem).setSelected(false);
        }
        selectLanguageModel.setSelected(true);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onRecyclerViewChildItemClick(BaseRecyclerViewHolder holder, int resourceId) {

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_continue) {
            if (selectLanguageModel == null) {
                showSnackBar(R.string.err_select_language);
                return;
            }
            LanguageManager.setUserPreferredLanguage(selectLanguageModel.getLanguageCode());
            LocaleUtils.changeAppLocale(this);
        }
    }


}
