package com.apps.ondemand.app.data.models;

import com.apps.ondemand.common.business.BaseItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceCertificateModel implements BaseItem {

    @SerializedName("_id")
    @Expose
    private String id;

    private String professionalDocsTitle;
    private String professionalDocsFront;
    private String professionalDocsBack;


    public ServiceCertificateModel() {
        this.id = "";
        this.professionalDocsTitle = "";
        this.professionalDocsFront = "";
        this.professionalDocsBack = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfessionalDocsTitle() {
        return professionalDocsTitle;
    }

    public void setProfessionalDocsTitle(String professionalDocsTitle) {
        this.professionalDocsTitle = professionalDocsTitle;
    }

    public String getProfessionalDocsFront() {
        return professionalDocsFront;
    }

    public void setProfessionalDocsFront(String professionalDocsFront) {
        this.professionalDocsFront = professionalDocsFront;
    }

    public String getProfessionalDocsBack() {
        return professionalDocsBack;
    }

    public void setProfessionalDocsBack(String professionalDocsBack) {
        this.professionalDocsBack = professionalDocsBack;
    }

    @Override
    public int getItemType() {
        return ITEM_SERVICE_CERTIFICATES;
    }
}

