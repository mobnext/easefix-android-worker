package com.apps.ondemand.app.ui.interfaces;

import com.apps.ondemand.app.data.models.ReasonModel;

public interface ReasonSelectionCallback {
    void onSelectReason(ReasonModel reasonModel, String otherReason, int type);
}
