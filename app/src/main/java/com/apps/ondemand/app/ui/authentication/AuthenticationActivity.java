package com.apps.ondemand.app.ui.authentication;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.apps.ondemand.BuildConfig;
import com.apps.ondemand.R;
import com.apps.ondemand.app.calling.sendBird.utils.ToastUtils;
import com.apps.ondemand.app.data.models.CountryModel;
import com.apps.ondemand.app.ui.interfaces.ClickableSpanListener;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.ErrorEditText;
import com.apps.ondemand.common.utils.GlideUtils;
import com.apps.ondemand.common.utils.NetworkUtils;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apps.ondemand.common.utils.AppConstants.KEY_EXTRA_COUNTRY_CODE;
import static com.apps.ondemand.common.utils.AppConstants.KEY_EXTRA_IS_SIGN_UP;
import static com.apps.ondemand.common.utils.AppConstants.KEY_EXTRA_PHONE_NO;

import androidx.annotation.RequiresApi;

@RequiresApi(api = Build.VERSION_CODES.S)
public class AuthenticationActivity extends BaseActivity implements View.OnClickListener {
    private static final int SELECT_COUNTRY_RESULT_CODE = 100;
    boolean fromSignUp;
    private String sendVerificationCodeUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_authentication);
        fromSignUp = getIntent().getBooleanExtra(KEY_EXTRA_IS_SIGN_UP, false);
        initView();
        setData(fromSignUp);
        checkPermissionsOne();
    }

    public void setData(boolean fromSignUp) {
        this.fromSignUp = fromSignUp;
        if (fromSignUp) {
            setActionBar(R.string.str_sign_up);
            setSubTitle(R.string.str_enter_mobile_no);
            AppUtils.setSpannableTextView(this,
                    getString(R.string.str_already_have_account) + " " + getString(R.string.str_sign_in),
                    getString(R.string.str_sign_in), R.color.colorPrimaryBlue, R.id.tv_already_register, spanListener);
            findTextViewById(R.id.btn_authenticate).setText(R.string.str_sign_up_now);
        } else {
            setActionBar(R.string.str_sign_in);
            setSubTitle(R.string.str_sign_in_book_service);
            AppUtils.setSpannableTextView(this,
                    getString(R.string.str_don_t_have_an_account) + " " + getString(R.string.str_sign_up),
                    getString(R.string.str_sign_up), R.color.colorPrimaryBlue, R.id.tv_already_register, spanListener);
            findTextViewById(R.id.btn_authenticate).setText(R.string.str_sign_in);
        }

        listenerEdit();

    }
    private void listenerEdit(){
        ErrorEditText edit = (ErrorEditText)findViewById(R.id.et_phone);
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit.getText().toString().startsWith("0")){
                    edit.setText(edit.getText().toString().replace("0", ""));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    ClickableSpanListener spanListener = new ClickableSpanListener() {
        @Override
        public void onSpanClicked() {
            Intent intent = new Intent(AuthenticationActivity.this, AuthenticationActivity.class);
            intent.putExtra(KEY_EXTRA_IS_SIGN_UP, !fromSignUp);
            startActivity(intent);
            finish();
        }
    };

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_authenticate) {
            if (isValidated()) {
                sendVerificationCode();
            }
        } else if (view.getId() == R.id.fl_country) {
            Intent intent = new Intent(this, SelectCountryActivity.class);
            startActivityForResult(intent, SELECT_COUNTRY_RESULT_CODE);
        }
    }


    private boolean isValidated() {
        String phoneNo = findFieldById(R.id.et_phone).getText().toString();
        if (!phoneNo.startsWith("7")){
            findErrorFieldById(R.id.et_phone).setError("Mobile Number Should Start with 7");
            return false;
        }
        if (TextUtils.isEmpty(phoneNo)) {
            findErrorFieldById(R.id.et_phone).setError(R.string.error_empty_phone);
            return false;
        }else if (findFieldById(R.id.et_phone).getText().toString().trim().length()<7){
            findErrorFieldById(R.id.et_phone).setError(R.string.error_correct_num);
            return false;
        }
        return true;
    }

    private void initView() {
        findViewById(R.id.btn_authenticate).setOnClickListener(this);
       findViewById(R.id.fl_country).setOnClickListener(this);
    }

    public void setCountryData(CountryModel countryModel) {
      /*  ImageView imgFlag = findViewById(R.id.img_flag_tag);
        if (!TextUtils.isEmpty(countryModel.getFlag())) {
            GlideUtils.glideLoadImageOrPlaceHolder(this, imgFlag, countryModel.getFlag());
        }*/
        findTextViewById(R.id.tv_country_code).setText(countryModel.getCountryCode());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case (SELECT_COUNTRY_RESULT_CODE): {
                    CountryModel countryModel = data.getParcelableExtra(AppConstants.KEY_EXTRA_COUNTRY);
                    setCountryData(countryModel);
                    break;
                }
            }
        }
    }

    private void sendVerificationCode() {
        String verificationNode = "";
        if (fromSignUp) {
            verificationNode = AppConstants.SIGN_UP_CODE;
        } else {
            verificationNode = AppConstants.SIGN_IN_CODE;
        }
        sendVerificationCodeUrl = AppConstants.getServerUrl(verificationNode);
//      String one=  CharMatcher.is('0').trimLeadingFrom(findFieldById(R.id.et_phone).getText().toString());
       String one=  findFieldById(R.id.et_phone).getText().toString();
        Log.d("sss", "sendVerificationCode: "+one);
        Map<String, Object> map = new HashMap<>();
        map.put("countryCode", findTextViewById(R.id.tv_country_code).getText().toString());
        map.put("phoneNumber", findFieldById(R.id.et_phone).getText().toString());
        map.put("userType", AppConstants.TYPE_SP);

        HttpRequestItem requestItem = new HttpRequestItem(sendVerificationCodeUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_POST);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            JSONObject jsonObjectResponse = new JSONObject(response.getResponse());
            int success = jsonObjectResponse.getInt("success");
            if (success == 1) {
                JSONObject data = jsonObjectResponse.getJSONObject("data");
                if (response.getHttpRequestUrl().equals(sendVerificationCodeUrl)) {
                    String code = data.getString("verificationCode");
                    String countryCode = data.getString("phonePreFix");
                    String phoneNo = data.getString("phoneNumber");
                        showToast("Verification code is " + code);
                    moveToNextActivity(countryCode,phoneNo);

                }
            } else {
                showSnackBar(jsonObjectResponse.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void moveToNextActivity(String countryCode, String phoneNo) {
        Intent intent = new Intent(this, CodeVerificationActivity.class);
        intent.putExtra(KEY_EXTRA_IS_SIGN_UP,fromSignUp);
        intent.putExtra(KEY_EXTRA_COUNTRY_CODE,countryCode);
        intent.putExtra(KEY_EXTRA_PHONE_NO,phoneNo);
        startActivity(intent);

    }
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;
    private static final String[] MANDATORY_PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,   // for VoiceCall and VideoCall
            Manifest.permission.CAMERA          // for VideoCall
    };
    protected void checkPermissionsOne() {
        ArrayList<String> deniedPermissions = new ArrayList<>();
        for (String permission : MANDATORY_PERMISSIONS) {
            if (checkCallingOrSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                deniedPermissions.add(permission);
            }
        }

        if (deniedPermissions.size() > 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(deniedPermissions.toArray(new String[0]), REQUEST_PERMISSIONS_REQUEST_CODE);
            } else {
                ToastUtils.showToast(this, "Permission denied.");
            }
        }
    }
}
