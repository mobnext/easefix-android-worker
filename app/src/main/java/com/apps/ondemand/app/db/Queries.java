package com.apps.ondemand.app.db;


import com.apps.ondemand.app.db.core.QueryGenerator;

/**
 * <p>Contains create queries for every table in the app.</p>
 * <p>
 * Created on 2016-12-03 15:01.
 *
 * @author Rana
 */
public class Queries {

    public static String createLocalisation() {
        return QueryGenerator.getInstance()
                .addIntegerPrimaryKeyAutoIncrement(DBConstants.Localisation.ID)
                .addTextUniqueField(DBConstants.Localisation.KEY).addTextField(DBConstants.Localisation.VALUE)
                .generate(DBConstants.Localisation.TABLE_NAME);
    } // createLocalisation

    public static String drop(String tableName) {
        return String.format("DROP TABLE IF EXISTS %s", tableName);
    } // drop
} // Queries
