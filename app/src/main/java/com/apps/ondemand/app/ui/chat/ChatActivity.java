package com.apps.ondemand.app.ui.chat;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.EditText;

import com.apps.ondemand.R;
import com.apps.ondemand.app.communication.SocketService;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.app.data.models.Chat;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewActivity;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.business.MyJSONObject;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.apps.ondemand.common.utils.AppConstants.TYPE_SP;
import static com.apps.ondemand.common.utils.BitmapUtils.getCroppedBitmap;


public class ChatActivity extends BaseRecyclerViewActivity implements View.OnClickListener {

    private ChatAdapter adapter;
    private String jobId = "";
    private String bidId = "";
    private int statusCode = 0;
    private int userId = 0;
    private int driverId = 0;
    private int pageNo = 0;
    private int limit = 20;
    private boolean doLoadMore = false;
    private int jobStatus = 0;
    private String spImage = "";
    private boolean isPastChat = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        setActionBar(R.string.str_chat);
        getIntentExtras();
        initializeRecyclerView();
        populateViews();
        findViewById(R.id.btn_send).setOnClickListener(this);
        findViewById(R.id.btn_send).requestFocus();
        AppUtils.hideSoftKeyboard(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chat_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final MenuItem image = menu.findItem(R.id.menu_image);
        if (URLUtil.isValidUrl(spImage)) {
            Glide.with(this)
                    .asBitmap()
                    .load(AppUtils.imageLowerSizeReplace(spImage))
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            image.setIcon(new BitmapDrawable(getResources(), getCroppedBitmap(ChatActivity.this, resource)));
                        }
                    });
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        pageNo = 0;
        limit = 20;
        getChatHistory();
        adapter = null;
        setAdapter(null);
    }

    private void getIntentExtras() {
        if (getIntent().getExtras() != null) {
            jobId = getIntent().getExtras().getString(AppConstants.KEY_JOB_ID);
            bidId = getIntent().getExtras().getString(AppConstants.KEY_BID_ID);
            jobStatus = getIntent().getIntExtra(AppConstants.KEY_JOB_STATUS, 0);
        }
    }

    private void populateViews() {
        findViewById(R.id.btn_send).setOnClickListener(this);
        findFieldById(R.id.et_chat).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    findViewById(R.id.btn_send).setEnabled(false);
                } else {
                    findViewById(R.id.btn_send).setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (jobStatus == AppConstants.STATUS_COMPLETED || jobStatus == AppConstants.STATUS_CANCELLED) {
            findFieldById(R.id.et_chat).setEnabled(false);
            findFieldById(R.id.et_chat).setFocusable(false);
            findFieldById(R.id.et_chat).setFocusableInTouchMode(false);
            findViewById(R.id.btn_send).setEnabled(false);
        }
    }

    private void initializeRecyclerView() {
        RecyclerView recyclerView = getRecyclerView();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        getSwipeLayout().setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (doLoadMore) {
                    getChatHistory();
                } else {
                    getSwipeLayout().setRefreshing(false);
                }
            }
        });
    }

    private void getChatHistory() {
        String chatThreadsURL = String.format(AppConstants.GET_THREAD_CHATS, bidId, TYPE_SP, pageNo * limit, limit);
        HttpRequestItem item = new HttpRequestItem(AppConstants.getServerUrl(chatThreadsURL));
        item.setHttpRequestType(NetworkUtils.HTTP_GET);
        item.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(item);
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            getSwipeLayout().setRefreshing(false);
            MyJSONObject jsonObject = new MyJSONObject(response.getResponse());
            if (jsonObject.getInt("success") == 1) {
                JSONObject data = jsonObject.getJSONObject("data");
                String userName = data.getJSONObject("userData").getString("name");
                String userProfile = data.getJSONObject("userData").getString("profileImage");
                isPastChat = AppUtils.getBooleanFromJson(data, "isPastChat");

                if (isPastChat) {
                    findFieldById(R.id.et_chat).setEnabled(false);
                    findFieldById(R.id.et_chat).setFocusable(false);
                    findFieldById(R.id.et_chat).setFocusableInTouchMode(false);
                    findViewById(R.id.btn_send).setEnabled(false);
                }

                setSubTitle(getString(R.string.str_chat_sub) + " " + userName);
                loadUserPicture(userProfile);

                JSONArray messages = data.getJSONArray("messages");
                if (messages.length() > 0) {
                    List<BaseItem> chats = new Gson().fromJson(messages.toString(), new TypeToken<List<Chat>>() {
                    }.getType());
                    if (chats == null)
                        chats = new ArrayList<>();
                    populateList(chats);
                    if (limit == messages.length()) {
                        doLoadMore = true;
                        pageNo++;
                    } else {
                        doLoadMore = false;
                    }
                } else {
                    if (pageNo == 0)
                        checkLoadedItems();
                    else
                        doLoadMore = false;
                }

            } else {
                checkLoadedItems();
                showToast(jsonObject.getString("message"));
            }
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    }

    private void loadUserPicture(String spProfile) {
        this.spImage = spProfile;
        invalidateOptionsMenu();
    }

    @Override
    public void onNetworkError(HttpResponseItem response) {
        super.onNetworkError(response);
        checkLoadedItems();
    }

    private void populateList(List<BaseItem> chats) {
        if (adapter == null) {
            adapter = new ChatAdapter(chats, this);
            setAdapter(adapter);
            getRecyclerView().scrollToPosition(adapter.getItemCount() - 1);
        } else {

            for (int i = 0; i < adapter.getItemCount(); i++) {
                BaseItem chat = adapter.getItemAt(i);
                int indexToRemove = contains(chats, chat);
                if (indexToRemove != -1) {
                    chats.remove(indexToRemove);
                }
            }
            adapter.addToStart(chats);
            getRecyclerView().scrollToPosition(chats.size() - 1);
        }
    }

    public int contains(List<BaseItem> chats, BaseItem chat) {
        Chat toCompared = (Chat) chat;
        for (int i = 0; i < chats.size(); i++) {
            BaseItem baseItem = chats.get(i);
            Chat comparedWith = (Chat) baseItem;
            if (comparedWith.getId().equalsIgnoreCase(toCompared.getId())) {
                return i;
            }
        }

        return -1;
    }


    private void checkLoadedItems() {
        findViewById(R.id.tv_info).setVisibility(isListEmpty() ? View.VISIBLE : View.GONE);
        if ((jobStatus == AppConstants.STATUS_COMPLETED
                || jobStatus == AppConstants.STATUS_CANCELLED) || isPastChat) {
            findTextViewById(R.id.tv_info).setText(R.string.str_no_chat_found);
        } else {
            findTextViewById(R.id.tv_info).setText(R.string.str_start_chatting);
        }

    }

    private boolean isListEmpty() {
        if (adapter == null)
            return true;
        if (adapter.getItemCount() == 0)
            return true;
        return false;
    }

    @Override
    public void onClick(View v) {
        EditText etMessage = findViewById(R.id.et_chat);
        String message = etMessage.getText().toString();
        if (!message.isEmpty()) {
            sendMessage(message);
            etMessage.setText("");
        }
    }

    @Override
    public void onEventReceived(Intent intent) {
        super.onEventReceived(intent);
        String jobEvent = intent.getExtras().getString(SocketService.BROADCAST_INTENT_TYPE);
        if (jobEvent.equalsIgnoreCase(SocketService.KEY_JOB_CHAT)) {
            try {
                MyJSONObject jsonObject = new MyJSONObject(intent.getStringExtra(SocketService.BROADCAST_INTENT_DATA));
                onMessageReceived(jsonObject);
                try {
                    sendConfirmation(jsonObject.getString("_id"));
                } catch (JSONException e) {
                    Logger.caughtException(e);
                }
            } catch (JSONException e) {
                Logger.caughtException(e);
            }
        } else if (jobEvent.equalsIgnoreCase(SocketService.KEY_JOB_CANCEL)) {
            finish();
        }
    }

    private void onMessageReceived(JSONObject message) {
        Chat chat = new Gson().fromJson(message.toString(), Chat.class);
        if (chat == null)
            return;
        addToList(chat);
    }

    private void sendConfirmation(String messageId) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", messageId);
            emitFromSocket(SocketService.EMIT_KEY_RECEIVED, jsonObject);
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    }

    private void addToList(Chat chat) {
        if (adapter == null) {
            adapter = new ChatAdapter(new ArrayList<>(), this);
            setAdapter(adapter);
        }

        boolean isChatExist = checkChatDuplication(chat);
        if (isChatExist) {
            Log.d("Duplication", "Removed");
            return;
        }

        adapter.add(chat);
        adapter.notifyDataSetChanged();
        getRecyclerView().scrollToPosition(adapter.getItemCount() - 1);
        checkLoadedItems();
    }

    private boolean checkChatDuplication(Chat chat) {
        boolean isChatExist = false;
        if (adapter.getAdapterItems() != null && adapter.getAdapterItems().size() > 0) {
            for (BaseItem baseItem : adapter.getAdapterItems()) {
                Chat chatItem = (Chat) baseItem;
                if (chatItem.getId().equalsIgnoreCase(chat.getId())) {
                    isChatExist = true;
                }
            }
        }
        return isChatExist;
    }

    private void sendMessage(String message) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("jobId", jobId);
            jsonObject.put("bidId", bidId);
            jsonObject.put("messageText", message);
            jsonObject.put("senderUserType", TYPE_SP);
            jsonObject.put("receiverUserType", AppConstants.TYPE_USER);
            jsonObject.put("senderUserId", UserManager.getUserId());
            emitFromSocket(SocketService.EMIT_KEY_CHAT, jsonObject, args -> {
                if (args.length > 0) {
                    runOnUiThread(() -> {
                        try {
                            JSONObject jsonObject1 = new JSONObject(args[0].toString());
                            JSONObject jsonChat = jsonObject1.getJSONObject("resource");
                            onMessageReceived(jsonChat);
                        } catch (JSONException e) {
                            Logger.caughtException(e);
                        }
                    });
                }
            });
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    }
}
