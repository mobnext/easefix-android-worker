package com.apps.ondemand.app.ui.jobs;

import static com.apps.ondemand.common.utils.AppConstants.BID_AMOUNT_;
import static com.apps.ondemand.common.utils.AppConstants.CANCELLATION_REASON;
import static com.apps.ondemand.common.utils.AppConstants.KEY_BID_ID;
import static com.apps.ondemand.common.utils.AppConstants.KEY_JOB_ID;
import static com.apps.ondemand.common.utils.AppConstants.KEY_JOB_STATUS;
import static com.apps.ondemand.common.utils.AppConstants.POND_CURRENCY;
import static com.apps.ondemand.common.utils.AppConstants.REJECT_BID;
import static com.apps.ondemand.common.utils.AppConstants.SHOW_PRICE_BREAKDOWN_CODE;
import static com.apps.ondemand.common.utils.AppConstants.STATUS_ACCEPTED;
import static com.apps.ondemand.common.utils.AppConstants.STATUS_ARRIVED;
import static com.apps.ondemand.common.utils.AppConstants.STATUS_ON_THE_WAY;
import static com.apps.ondemand.common.utils.AppConstants.STATUS_STARTED;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.calling.sendBird.call.CallService;
import com.apps.ondemand.app.calling.sendBird.utils.PrefUtils;
import com.apps.ondemand.app.communication.SocketService;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.app.data.models.JobDetailModel;
import com.apps.ondemand.app.data.models.LineItemModel;
import com.apps.ondemand.app.data.models.PriceBreakDownModel;
import com.apps.ondemand.app.data.models.ReasonModel;
import com.apps.ondemand.app.ui.adapters.AddLineItemAdapter;
import com.apps.ondemand.app.ui.adapters.AttachmentAdapter;
import com.apps.ondemand.app.ui.adapters.DepositAdapter;
import com.apps.ondemand.app.ui.adapters.HourSlotAdapter;
import com.apps.ondemand.app.ui.chat.ChatActivity;
import com.apps.ondemand.app.ui.dialogs.DialogCancellationReason;
import com.apps.ondemand.app.ui.dialogs.DialogCaution;
import com.apps.ondemand.app.ui.dialogs.DialogJobCompleted;
import com.apps.ondemand.app.ui.interfaces.LineItemListener;
import com.apps.ondemand.app.ui.interfaces.ReasonSelectionCallback;
import com.apps.ondemand.app.ui.main.ImageActivity;
import com.apps.ondemand.app.ui.main.PaymentActivity;
import com.apps.ondemand.common.base.GoogleMapActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.BorderErrorEditText;
import com.apps.ondemand.common.utils.DateUtils;
import com.apps.ondemand.common.utils.GlideUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.apps.ondemand.common.utils.VizImageView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

@SuppressLint("NewApi")
public class JobDetailsActivity extends GoogleMapActivity implements View.OnClickListener, OnRecyclerViewItemClickListener {

    RecyclerView hoursRv;
    TextView depositedAmount;
    RecyclerView advancePercentage;
    HourSlotAdapter hourSlotAdapter;
    DepositAdapter depositAdapter;
    private static final int MAX_HOURS = 23;
    private static final int MAX_PAYMENTS = 3;
    private static final int MIN_SELECTED_VALUE = 1;
    private int selectedTimeSlot = MIN_SELECTED_VALUE;
    private int selectDepositAmount = 0;
    private String fixedMessage = "";
    private String bidMessage = "Please give approximate price and later you can readjust and add extra cost in the line item section";
    public static Double latitudeCurrent;
    public static Double longitudeCurrent;
    private String fetchDetailsUrl = "";
    private String cancellationReasonUrl = "";
    private String cancelPackageUrl = "";
    private String rateUserUrl = "";
    private String updateJobStatusUrl = "";
    private String completeJobUrl = "";
    private String pauseStartJobUrl = "";
    private String jobId;
    JobDetailModel detailModel;
    private Location myLocation;
    String disputeCreated;
    Handler handler;
    Runnable runner;
    RecyclerView rvLineItem;
    AddLineItemAdapter lineItemAdapter;
    List<BaseItem> lineItemModels;
    List<BaseItem> mediaModelList = new ArrayList<>();
    private String addLineItemUrl = "";
    private String editLineItemUrl = "";
    private String deleteLineItemUrl = "";
    private boolean isFixed;
    RecyclerView recyclerView;
    AttachmentAdapter attachmentAdapter;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_details);
        try {
            jobId = getIntent().getStringExtra(KEY_JOB_ID);
            isFixed = getIntent().getBooleanExtra("isFixed", false);
            latitudeCurrent = getIntent().getDoubleExtra("lat", 0.0);
            longitudeCurrent = getIntent().getDoubleExtra("lng", 0.0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setActionBar(R.string.str_job_detail);
        initView();
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchJobDetails();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runner);
    }

    @Override
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);
        this.myLocation = location;
    }

    private void initView() {
        handler = new Handler();
        findViewById(R.id.iv_timer_play).setOnClickListener(this);
        findViewById(R.id.tv_accept).setOnClickListener(this);
        findViewById(R.id.tv_reject).setOnClickListener(this);
        findViewById(R.id.iv_timer_pause).setOnClickListener(this);
        findViewById(R.id.tv_view_job_instruction).setOnClickListener(this);
        findViewById(R.id.tv_cancel).setOnClickListener(this);
        findViewById(R.id.tv_call_icon).setOnClickListener(this);
        findViewById(R.id.tv_video_icon).setOnClickListener(this);
        findViewById(R.id.tv_message_icon).setOnClickListener(this);
        findViewById(R.id.rl_price_break).setOnClickListener(this);
        findViewById(R.id.tv_rate_user).setOnClickListener(this);
        findViewById(R.id.btn_job_flow).setOnClickListener(this);
        findViewById(R.id.ll_add_line_item).setOnClickListener(this);
        findViewById(R.id.btn_bid_now).setOnClickListener(this);
        findViewById(R.id.dispute).setOnClickListener(this);
        rvLineItem = findViewById(R.id.rv_line_item);
        rvLineItem.setLayoutManager(new LinearLayoutManager(this));
        rvLineItem.setHasFixedSize(false);
        ViewCompat.setNestedScrollingEnabled(rvLineItem, false);
        ViewCompat.setNestedScrollingEnabled(findViewById(R.id.ns_wrapper), false);
        recyclerView = findViewById(R.id.recycler_view_media);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.S)
    public void populateData() {
        animate();
        onAddFragment(RouteFragment.newInstance(detailModel), R.id.content_frame);
        findViewById(R.id.job_details_wrapper).setVisibility(View.VISIBLE);
        findTextViewById(R.id.tv_profile_name).setText(detailModel.getName());
        VizImageView imageView = findViewById(R.id.iv_profile_image);
        imageView.setImage(detailModel.getProfileImage(), false);
        ((MaterialRatingBar) findViewById(R.id.rb_profile_rating)).setRating(detailModel.getAvgRating());
        findTextViewById(R.id.tv_profile_rating).setText(AppUtils.formatRating(detailModel.getAvgRating()));
        findTextViewById(R.id.tv_address_label).setText(detailModel.getJobAddress().getCity() + ", " + detailModel.getJobAddress().getCountry());
        findTextViewById(R.id.tv_primary_address).setText(detailModel.getJobAddress().getPrimaryAddress());
        GlideUtils.glideLoadImage(this,
                findImageViewById(R.id.iv_service_icon),
                detailModel.getServiceImage(),
                (int) getResources().getDimension(R.dimen.dimen_50));
        findTextViewById(R.id.tv_service_name).setText(detailModel.getServiceName() + "\n" + detailModel.getSubService());
        findTextViewById(R.id.tv_service_desc).setText(detailModel.getServiceDescription());
        if (detailModel.getSpJobStatus() < STATUS_STARTED) {
            findTextViewById(R.id.tv_schedule_date_time).setText(DateUtils.convertDate(AppUtils.GetTimeForForApp(detailModel.getJobScheduleTime()), DateUtils.DATE_JOB_SUMMARY));
        } else {
            findTextViewById(R.id.tv_schedule_date_time).setText(DateUtils.convertDate(AppUtils.GetTimeForForApp(detailModel.getJobStartTime()), DateUtils.DATE_JOB_SUMMARY));
        }
        if (!detailModel.isEnableCommunication()) {
            if (!detailModel.isJobFixed()) {
                findTextViewById(R.id.callMessage).setVisibility(View.VISIBLE);
            }
            findTextViewById(R.id.tv_message_icon).setEnabled(false);
            findTextViewById(R.id.tv_call_icon).setEnabled(false);
            findTextViewById(R.id.tv_video_icon).setEnabled(false);
            findViewById(R.id.visibleCommunication).setVisibility(View.VISIBLE);
            findTextViewById(R.id.tv_message_icon).setTextColor(getColor(R.color.disable));
            findTextViewById(R.id.tv_message_icon).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_message_24, 0, 0, 0);
        } else {
            findTextViewById(R.id.tv_message_icon).setTextColor(getColor(R.color.text_primary));
            findTextViewById(R.id.tv_message_icon).setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_chat_icon, 0, 0, 0);
            findViewById(R.id.visibleCommunication).setVisibility(View.VISIBLE);
            findTextViewById(R.id.tv_message_icon).setEnabled(true);
            findTextViewById(R.id.tv_call_icon).setEnabled(false);
            findTextViewById(R.id.tv_video_icon).setEnabled(false);
        }
        findTextViewById(R.id.tv_call_icon).setTextColor(getColor(R.color.disable));
        findTextViewById(R.id.tv_video_icon).setTextColor(getColor(R.color.disable));
        findTextViewById(R.id.tv_call_icon).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_call_blue_disable, 0, 0, 0);
        findTextViewById(R.id.tv_video_icon).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_video_icon_disable, 0, 0, 0);

        if (detailModel.isJobDurationKnown()) {
            findTextViewById(R.id.tv_schedule_desc).setText(String.format(getString(R.string.str_for_hours), detailModel.getTotalJobDuration() + ""));
        } else {
            findTextViewById(R.id.tv_schedule_desc).setText(getString(R.string.str_fixed_job));
        }
        findTextViewById(R.id.tv_work_desc).setText(detailModel.getWorkDescription());
        if (TextUtils.isEmpty(detailModel.getSpecialInstruction())) {
            findTextViewById(R.id.tv_view_job_instruction).setVisibility(View.GONE);
        } else {
            findTextViewById(R.id.tv_view_job_instruction).setVisibility(View.VISIBLE);
        }
        setMessageCounter(detailModel.getMessageCount());
        setStatusData();

        mediaModelList = new ArrayList<>(detailModel.getMediaModel());
        if (numCheck == 0) {
            numCheck = 1;
            populateList(mediaModelList);
        }
    }


    @SuppressLint("NewApi")
    private void setMessageCounter(int messageCount) {
        if (messageCount == 0) {
            findTextViewById(R.id.tv_message_count).setVisibility(View.GONE);
        } else {
            findTextViewById(R.id.tv_message_count).setText(String.valueOf(messageCount));
            findTextViewById(R.id.tv_message_count).setVisibility(View.VISIBLE);
        }
    }


    @SuppressLint("SetTextI18n")
    private void setStatusData() {
        if (detailModel.isIsbidAdded()) {
            findTextViewById(R.id.callMessage).setVisibility(View.GONE);
            if (isFixed) {
                findViewById(R.id.fixed).setVisibility(View.VISIBLE);
                findViewById(R.id.tv_accept).setVisibility(View.GONE);
                findViewById(R.id.tv_reject).setVisibility(View.GONE);
                findViewById(R.id.tv_fixed).setVisibility(View.VISIBLE);
                findTextViewById(R.id.tv_fixed).setText("Fixed Job (" + POND_CURRENCY + detailModel.getFixedJobAmount() + ")");
            } else {
                findViewById(R.id.fixed).setVisibility(View.GONE);
                findTextViewById(R.id.btn_bid_now).setVisibility(View.VISIBLE);
                findTextViewById(R.id.btn_bid_now).setText(getString(R.string.update_bid));
                iconEnabled(false);
            }
        } else if (isFixed) {
            findTextViewById(R.id.btn_bid_now).setVisibility(View.GONE);
            findViewById(R.id.fixed).setVisibility(View.VISIBLE);
        } else {
            findTextViewById(R.id.btn_bid_now).setVisibility(View.VISIBLE);
        }
        findViewById(R.id.rl_time_wrapper).setVisibility(View.GONE);
        setSubTitle(String.format(getString(R.string.str_job_identifier), detailModel.getJobIdIdentifier()));

        if (detailModel.getSpJobStatus() == AppConstants.STATUS_ACCEPTED) {
            findViewById(R.id.tv_cancel).setVisibility(View.VISIBLE);
            findViewById(R.id.rl_price_break).setVisibility(View.GONE);
            findButtonById(R.id.btn_job_flow).setText(R.string.str_start_driving);
            findViewById(R.id.ll_line_item_wrapper).setVisibility(View.GONE);
            findViewById(R.id.btn_job_flow).setVisibility(View.VISIBLE);
            findViewById(R.id.tv_call_icon).setVisibility(View.VISIBLE);
            findViewById(R.id.tv_video_icon).setVisibility(View.VISIBLE);
            iconEnabled(true);
            findTextViewById(R.id.btn_bid_now).setVisibility(View.GONE);
            findViewById(R.id.contact_icon_divider).setVisibility(View.VISIBLE);
        } else if (detailModel.getSpJobStatus() == AppConstants.STATUS_DISPUTE) {
            findViewById(R.id.dispute).setVisibility(View.VISIBLE);
            findTextViewById(R.id.btn_bid_now).setVisibility(View.GONE);
            TextView tv = findViewById(R.id.dispute);
            tv.setText(R.string.disputed);
        } else if (detailModel.getSpJobStatus() == AppConstants.STATUS_ON_THE_WAY) {
            findViewById(R.id.tv_cancel).setVisibility(View.VISIBLE);
            findTextViewById(R.id.btn_bid_now).setVisibility(View.GONE);
            findViewById(R.id.rl_price_break).setVisibility(View.GONE);
            findButtonById(R.id.btn_job_flow).setText(R.string.str_tap_when_arrived);
            findViewById(R.id.ll_line_item_wrapper).setVisibility(View.GONE);
            findViewById(R.id.btn_job_flow).setVisibility(View.VISIBLE);
            findViewById(R.id.tv_call_icon).setVisibility(View.VISIBLE);
            findViewById(R.id.tv_video_icon).setVisibility(View.VISIBLE);
            findViewById(R.id.contact_icon_divider).setVisibility(View.VISIBLE);
            iconEnabled(true);
        } else if (detailModel.getSpJobStatus() == AppConstants.STATUS_ARRIVED) {
            findViewById(R.id.tv_cancel).setVisibility(View.VISIBLE);
            findViewById(R.id.rl_price_break).setVisibility(View.GONE);
            findButtonById(R.id.btn_job_flow).setText(R.string.str_start_job);
            findTextViewById(R.id.btn_bid_now).setVisibility(View.GONE);
            findViewById(R.id.ll_line_item_wrapper).setVisibility(View.GONE);
            findViewById(R.id.btn_job_flow).setVisibility(View.VISIBLE);
            findViewById(R.id.tv_call_icon).setVisibility(View.VISIBLE);
            findViewById(R.id.tv_video_icon).setVisibility(View.VISIBLE);
            findViewById(R.id.contact_icon_divider).setVisibility(View.VISIBLE);
            iconEnabled(true);
        } else if (detailModel.getSpJobStatus() == AppConstants.STATUS_STARTED) {
            findViewById(R.id.tv_cancel).setVisibility(View.GONE);
            findViewById(R.id.dispute).setVisibility(View.VISIBLE);
            findViewById(R.id.rl_price_break).setVisibility(View.GONE);
            findTextViewById(R.id.btn_bid_now).setVisibility(View.GONE);
            findButtonById(R.id.btn_job_flow).setText(R.string.str_tap_when_finished);
            findViewById(R.id.btn_job_flow).setVisibility(View.VISIBLE);
            findViewById(R.id.tv_call_icon).setVisibility(View.VISIBLE);
            findViewById(R.id.tv_video_icon).setVisibility(View.VISIBLE);
            findViewById(R.id.contact_icon_divider).setVisibility(View.VISIBLE);
            iconEnabled(true);
//            setTimer();
            setLineItemsFlow();
        } else if (detailModel.getSpJobStatus() == AppConstants.STATUS_COMPLETED) {
            if (AppConstants.line_Visible == 1) {
                showSnackBar(getString(R.string.error_line));
            } else {
                setCompletedStatus();
            }
            findViewById(R.id.dispute).setVisibility(View.GONE);
            findTextViewById(R.id.btn_bid_now).setVisibility(View.GONE);
        } else if (detailModel.getSpJobStatus() == AppConstants.STATUS_REJECTION) {
            findViewById(R.id.tv_cancel).setVisibility(View.GONE);
            findViewById(R.id.rl_price_break).setVisibility(View.GONE);
            findViewById(R.id.ll_line_item_wrapper).setVisibility(View.GONE);
            findViewById(R.id.btn_job_flow).setVisibility(View.GONE);
            findViewById(R.id.tv_call_icon).setVisibility(View.GONE);
            findTextViewById(R.id.btn_bid_now).setVisibility(View.GONE);
            findViewById(R.id.tv_video_icon).setVisibility(View.GONE);
            findViewById(R.id.contact_icon_divider).setVisibility(View.GONE);
            findViewById(R.id.dispute).setVisibility(View.GONE);
        } else if (detailModel.getSpJobStatus() == AppConstants.STATUS_CANCELLED) {
            findViewById(R.id.tv_cancel).setVisibility(View.GONE);
            findViewById(R.id.dispute).setVisibility(View.GONE);
            findViewById(R.id.rl_price_break).setVisibility(View.GONE);
            findViewById(R.id.ll_line_item_wrapper).setVisibility(View.GONE);
            findViewById(R.id.btn_job_flow).setVisibility(View.GONE);
            findTextViewById(R.id.btn_bid_now).setVisibility(View.GONE);
            findViewById(R.id.tv_call_icon).setVisibility(View.GONE);
            findViewById(R.id.tv_video_icon).setVisibility(View.GONE);
            findViewById(R.id.contact_icon_divider).setVisibility(View.GONE);
        }
    }

    private void setLineItemsFlow() {
        findViewById(R.id.ll_line_item_wrapper).setVisibility(View.VISIBLE);
        findViewById(R.id.ll_add_line_item).setVisibility(View.VISIBLE);
        lineItemModels = new ArrayList<>(detailModel.getPriceBreakDownModel().getLineItems());
        lineItemAdapter = new AddLineItemAdapter(this, lineItemModels, lineItemListener);
        rvLineItem.setAdapter(lineItemAdapter);
    }

    private void toggleTimerButton(boolean jobStart) {
        findImageViewById(R.id.iv_timer_pause).setEnabled(jobStart);
        findImageViewById(R.id.iv_timer_play).setEnabled(!jobStart);
    }

    private void setCompletedStatus() {
        findViewById(R.id.btn_job_flow).setVisibility(View.GONE);
        findViewById(R.id.ll_line_item_wrapper).setVisibility(View.GONE);
        findViewById(R.id.tv_call_icon).setVisibility(View.GONE);
        findViewById(R.id.tv_video_icon).setVisibility(View.GONE);
        findViewById(R.id.contact_icon_divider).setVisibility(View.GONE);
        setPriceBreakDown();
        if (detailModel.isSpRated()) {
            findViewById(R.id.tv_rate_user).setVisibility(View.GONE);
        } else if (!detailModel.isSpRated() && !detailModel.isSpRatingScreenShown()) {
            showRatingDialog();
        } else if (!detailModel.isSpRated() && detailModel.isSpRatingScreenShown()) {
            findViewById(R.id.tv_rate_user).setVisibility(View.VISIBLE);
        }
        isAmountPaid();
    }

    private void iconEnabled(boolean check) {
        if (check) {
            findViewById(R.id.tv_call_icon).setEnabled(true);
            findViewById(R.id.tv_video_icon).setEnabled(true);
            findTextViewById(R.id.tv_call_icon).setTextColor(getColor(R.color.text_primary));
            findTextViewById(R.id.tv_video_icon).setTextColor(getColor(R.color.text_primary));
            findTextViewById(R.id.tv_call_icon).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_call_blue, 0, 0, 0);
            findTextViewById(R.id.tv_video_icon).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_video_icon, 0, 0, 0);
        } else {
            findViewById(R.id.tv_call_icon).setEnabled(false);
            findViewById(R.id.tv_video_icon).setEnabled(false);
            findTextViewById(R.id.tv_call_icon).setTextColor(getColor(R.color.disable));
            findTextViewById(R.id.tv_video_icon).setTextColor(getColor(R.color.disable));
            findTextViewById(R.id.tv_call_icon).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_call_blue_disable, 0, 0, 0);
            findTextViewById(R.id.tv_video_icon).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_video_icon_disable, 0, 0, 0);
        }
    }

    private void showRatingDialog() {
        DialogJobCompleted.newInstance(this,
                detailModel.getProfileImage(),
                detailModel.getName(),
                detailModel.getAvgRating(),
                this::callRateUser).show();
    }


    @SuppressLint("UseCompatLoadingForDrawables")
    private void isAmountPaid() {
        findViewById(R.id.amountPaidNot).setVisibility(View.VISIBLE);
        if (detailModel.isTotalAmountPaid()) {
            findViewById(R.id.amountPaidNot).setBackground(getDrawable(R.drawable.ic_paid));
        } else {
            findViewById(R.id.amountPaidNot).setBackground(getDrawable(R.drawable.ic_unpaid));
        }
    }

    private void setPriceBreakDown() {
        findViewById(R.id.rl_price_break).setVisibility(View.VISIBLE);
        findTextViewById(R.id.tv_price_break).setText(AppUtils.getAmount(detailModel.getTotalAmount()));
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dispute:
                conflictFun();
                break;
            case R.id.iv_timer_play:
                detailModel.setJobStart(true);
                toggleTimerButton(detailModel.isJobStart());
                pauseStartJob();
                break;

            case R.id.iv_timer_pause:
                detailModel.setJobStart(false);
                toggleTimerButton(detailModel.isJobStart());
                pauseStartJob();
                break;
            case R.id.tv_view_job_instruction:
                DialogCaution.CautionBuilder.newInstance(this)
                        .setTitle(R.string.str_job_instruction)
                        .setMessage(detailModel.getSpecialInstruction())
                        .setRightButton(R.string.str_okay)
                        .build()
                        .show();
                break;
            case R.id.tv_accept:
                sendFixedBidPost();
                break;

            case R.id.tv_reject:
                //to do
                sendRejectPost();
                break;
            case R.id.tv_cancel:
                fetchCancellationReason();
                break;
            case R.id.tv_call_icon:
                callToDriver(false);
                break;
            case R.id.tv_video_icon:
                callToDriver(true);
                break;
            case R.id.tv_message_icon:
                openChatActivity();
                break;
            case R.id.rl_price_break:
                goToPriceBreakDownActivity(detailModel.getPriceBreakDownModel());
                break;

            case R.id.tv_rate_user:
                showRatingDialog();
                break;

            case R.id.btn_job_flow:
                updateJobFlow();
                break;
            case R.id.ll_add_line_item:
                addNewLineItem();
                break;
            case R.id.btn_bid_now:
                if (!UserManager.isVerifiedByAdmin()) {
                    showSnackBar(getString(R.string.str_not_approved_));
                }  else {
                    bidDialog();
                }
                break;
        }
    }

    private void googleRouteDraw(double latitude, double longitude) {
        new Handler().postDelayed(() -> {
            Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            try {
                startActivity(mapIntent);
            } catch (ActivityNotFoundException innerEx) {
                showToast("Please install a maps application");
            }
        }, 2000);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void addNewLineItem() {
        AppConstants.line_Visible = 1;
        lineItemModels.add(new LineItemModel(false, false));
        lineItemAdapter.notifyDataSetChanged();
    }


    private void updateJobFlow() {
        numCheck = 1;
        if (mLocation == null) {
            showSnackBar(R.string.error_location_fetching);
            return;
        }
        if (detailModel.getSpJobStatus() == STATUS_ACCEPTED) {
            updateJobStatus(STATUS_ON_THE_WAY, myLocation.getLatitude(), myLocation.getLongitude());
            googleRouteDraw(myLocation.getLatitude(), myLocation.getLongitude());
        } else if (detailModel.getSpJobStatus() == STATUS_ON_THE_WAY) {
            updateJobStatus(STATUS_ARRIVED, myLocation.getLatitude(), myLocation.getLongitude());
        } else if (detailModel.getSpJobStatus() == STATUS_ARRIVED) {
            if (!detailModel.isConnectAccountExists()) {
                moveToPaymentActivity();
            } else {
                updateJobStatus(STATUS_STARTED, myLocation.getLatitude(), myLocation.getLongitude());
            }
        } else if (detailModel.getSpJobStatus() == STATUS_STARTED) {
            if (AppConstants.line_Visible == 1) {
                showSnackBar(getString(R.string.error_line));
            } else {
                completeJob();
            }
        }
    }


    private void openChatActivity() {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(KEY_JOB_ID, jobId);
        intent.putExtra(KEY_BID_ID, detailModel.getBidId());
        intent.putExtra(KEY_JOB_STATUS, detailModel.getSpJobStatus());
        startActivity(intent);
    }

    private void goToPriceBreakDownActivity(PriceBreakDownModel priceBreakDown) {
        if (priceBreakDown == null) {
            return;
        }
        Intent intent = new Intent(this, PriceBreakdownActivity.class);
        intent.putExtra(AppConstants.KEY_PRICE_BREAKDOWN, priceBreakDown);
        intent.putExtra("isFixed", isFixed);
        intent.putExtra(AppConstants.KEY_CAN_APPLY_PROMO, false);
        intent.putExtra("JobIdentifier", detailModel.getJobIdIdentifier());
        startActivityForResult(intent, SHOW_PRICE_BREAKDOWN_CODE);
    }


    private void callRateUser(float rating, String review) {
        rateUserUrl = AppConstants.getServerUrl(AppConstants.RATE_USER);
        Map<String, Object> map = new HashMap<>();
        map.put("review", review);
        map.put("rating", rating);
        map.put("jobId", detailModel.getJobId());
        map.put("userProfileId", detailModel.getUserProfileId());
        HttpRequestItem requestItem = new HttpRequestItem(rateUserUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_PUT);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }


    private void updateJobStatus(int status, double latitude, double longitude) {
        updateJobStatusUrl = AppConstants.getServerUrl(AppConstants.UPDATE_JOB_STATUS);
        Map<String, Object> map = new HashMap<>();
        map.put("spJobStatus", status);
        map.put("latitude", latitude);
        map.put("longitude", longitude);
        map.put("jobId", detailModel.getJobId());
        HttpRequestItem requestItem = new HttpRequestItem(updateJobStatusUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_PUT);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);

    }

    private void completeJob() {
        completeJobUrl = AppConstants.getServerUrl(String.format(AppConstants.COMPLETE_JOB, jobId));
        HttpRequestItem requestItem = new HttpRequestItem(completeJobUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    private void pauseStartJob() {
        pauseStartJobUrl = AppConstants.getServerUrl(String.format(AppConstants.PAUSE_START_JOB, jobId));
        HttpRequestItem requestItem = new HttpRequestItem(pauseStartJobUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    private void fetchJobDetails() {
        fetchDetailsUrl = AppConstants.getServerUrl(String.format(AppConstants.GET_JOB_DETAILS, jobId));
        HttpRequestItem requestItem = new HttpRequestItem(fetchDetailsUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }


    private void fetchCancellationReason() {
        cancellationReasonUrl = AppConstants.getServerUrl(String.format(AppConstants.FETCH_CANCELLATION_REASONS, AppConstants.TYPE_SP));
        HttpRequestItem requestItem = new HttpRequestItem(cancellationReasonUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false),
                this);
        appNetworkTask.execute(requestItem);
    }

    private void cancelPackage(ReasonModel reasonModel, String otherReason) {
        cancelPackageUrl = AppConstants.getServerUrl(AppConstants.JOB_CANCEL);
        HttpRequestItem requestItem = new HttpRequestItem(cancelPackageUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_PUT);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        Map<String, Object> map = new HashMap<>();
        map.put("jobId", jobId);
        if (TextUtils.isEmpty(reasonModel.getId())) {
            map.put("reasonText", otherReason);
        } else {
            map.put("reasonId", reasonModel.getId());
        }
        requestItem.setParams(map);
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false),
                this);
        appNetworkTask.execute(requestItem);
    }

    private void addLineItem(LineItemModel lineItemModel) {
        addLineItemUrl = AppConstants.getServerUrl(AppConstants.ADD_LINE_ITEM);
        Map<String, Object> map = new HashMap<>();
        map.put("jobId", detailModel.getJobId());
        map.put("itemName", lineItemModel.getItemName());
        map.put("itemQuantity", lineItemModel.getItemQuantity());
        map.put("itemPrice", lineItemModel.getItemPrice());
        HttpRequestItem requestItem = new HttpRequestItem(addLineItemUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_PUT);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    private void editLineItem(LineItemModel lineItemModel) {
        editLineItemUrl = AppConstants.getServerUrl(AppConstants.EDIT_LINE_ITEM);
        Map<String, Object> map = new HashMap<>();
        map.put("jobId", detailModel.getJobId());
        map.put("lineItemId", lineItemModel.getId());
        map.put("itemName", lineItemModel.getItemName());
        map.put("itemQuantity", lineItemModel.getItemQuantity());
        map.put("itemPrice", lineItemModel.getItemPrice());
        HttpRequestItem requestItem = new HttpRequestItem(editLineItemUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_PUT);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    private void deleteLineItem(LineItemModel lineItemModel) {
        deleteLineItemUrl = AppConstants.getServerUrl(AppConstants.DELETE_LINE_ITEM);

        Map<String, Object> map = new HashMap<>();
        map.put("jobId", detailModel.getJobId());
        map.put("lineItemId", lineItemModel.getId());

        HttpRequestItem requestItem = new HttpRequestItem(deleteLineItemUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_PUT);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);

    }

    @SuppressLint("NotifyDataSetChanged")
    private void discardLineItem(int position) {
        lineItemModels.remove(position);
        lineItemAdapter.notifyDataSetChanged();
    }

    int numCheck = 0;

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            JSONObject responseJson = new JSONObject(response.getResponse());
            if (responseJson.getInt("success") == 1) {

                if (response.getHttpRequestUrl().equals(fetchDetailsUrl)) {
                    JSONObject data = responseJson.getJSONObject("data");
                    Log.e("anjum: job detail", data.toString());
                    detailModel = new Gson().fromJson(data.toString(), JobDetailModel.class);
                    isFixed = detailModel.isJobFixed();
                    latitudeCurrent = detailModel.getJobAddress().getLatitude();
                    longitudeCurrent = detailModel.getJobAddress().getLongitude();
//                    secondCheck=1;
                    populateData();
                } else if (response.getHttpRequestUrl().equalsIgnoreCase(disputeCreated)) {
                    //showSnackBar(responseJson.getString("message"));
                    showConflict();
                } else if (response.getHttpRequestUrl().equalsIgnoreCase(sendRejectPost)) {
                    showSnackBar("Rejected Successfully");
                    new Handler().postDelayed(this::onBackPressed, 1500);
                } else if (response.getHttpRequestUrl().equalsIgnoreCase(cancellationReasonUrl)) {
                    JSONArray reasons = responseJson.getJSONObject("data").getJSONArray("reasonList");
                    if (reasons.length() != 0) {
                        List<ReasonModel> reasonModelList = new Gson().fromJson(reasons.toString(),
                                new TypeToken<List<ReasonModel>>() {
                                }.getType());

                        ReasonModel otherReason = new ReasonModel();
                        otherReason.setSelected(false);
                        otherReason.setId("");
                        otherReason.setReason(getString(R.string.str_other));
                        reasonModelList.add(otherReason);

                        DialogCancellationReason.newInstance(this, reasonModelList, selectionCallback).show();
                    }
                } else if (response.getHttpRequestUrl().equalsIgnoreCase(cancelPackageUrl)) {
                    showSnackBar(responseJson.getString("message"));
                    AppUtils.setResultAndFinish(this);
                } else if (response.getHttpRequestUrl().equalsIgnoreCase(rateUserUrl)) {
                    showSnackBar(responseJson.getString("message"));
                    findViewById(R.id.tv_rate_user).setVisibility(View.GONE);
                    AppUtils.setResultAndFinish(this);
                } else if (response.getHttpRequestUrl().equalsIgnoreCase(updateJobStatusUrl)
                        || response.getHttpRequestUrl().equalsIgnoreCase(completeJobUrl)
                        || response.getHttpRequestUrl().equalsIgnoreCase(pauseStartJobUrl)
                        || response.getHttpRequestUrl().equalsIgnoreCase(addLineItemUrl)
                        || response.getHttpRequestUrl().equalsIgnoreCase(editLineItemUrl)
                        || response.getHttpRequestUrl().equalsIgnoreCase(deleteLineItemUrl)) {
                    if (response.getHttpRequestUrl().equalsIgnoreCase(completeJobUrl)) {
                        JSONObject data = responseJson.getJSONObject("data");
                        showCashReceivedPopUp(data);
                        return;

                    }
                    numCheck = 1;
                    fetchJobDetails();
                } else if (response.getHttpRequestUrl().equalsIgnoreCase(sendBidPost)) {
                    fetchJobDetails();
                }
            } else if (response.getHttpRequestUrl().equalsIgnoreCase(sendBidPost)) {
                JSONObject data = responseJson.getJSONObject("data");
                if (!data.getBoolean("isConnectedAccountExists")) {
                    moveToPaymentActivity();
                }
            } else
                showSnackBar(responseJson.getString("message"));
        } catch (JSONException e) {
            Logger.error(false, e);
        }
    }

    @SuppressLint("SetTextI18n")
    private void showConflict() {
        findViewById(R.id.tv_cancel).setVisibility(View.GONE);
        findViewById(R.id.dispute).setVisibility(View.VISIBLE);
        TextView tv = findViewById(R.id.dispute);
        tv.setText("Disputed");
        findViewById(R.id.rl_price_break).setVisibility(View.GONE);
        findViewById(R.id.ll_line_item_wrapper).setVisibility(View.GONE);
        findViewById(R.id.btn_job_flow).setVisibility(View.GONE);
        findViewById(R.id.tv_call_icon).setVisibility(View.GONE);
        findViewById(R.id.tv_video_icon).setVisibility(View.GONE);
        findViewById(R.id.contact_icon_divider).setVisibility(View.GONE);
    }

    private void moveToPaymentActivity() {
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra("first", 2);
        startActivity(intent);
    }

    private void showCashReceivedPopUp(JSONObject fare) {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        dialog.setContentView(R.layout.dialog_cash);

        TextView tvAmount = dialog.findViewById(R.id.tv_amount);
        TextView tvRemainingAmount = dialog.findViewById(R.id.tv_amount_remaining);
        TextView tvTotalAmount = dialog.findViewById(R.id.tv_amount_total);
        tvAmount.setText(AppUtils.getAmount(AppUtils.getDoubleFromJson(fare, "spEarnedAmount")));
        tvRemainingAmount.setText(AppUtils.getAmount(AppUtils.getDoubleFromJson(fare, "spRemainingAmount")));
        tvTotalAmount.setText(AppUtils.getAmount(AppUtils.getDoubleFromJson(fare, "totalAmount")));

        dialog.findViewById(R.id.btn_cash).setOnClickListener(v -> {
            dialog.dismiss();
            fetchJobDetails();
        });
        dialog.show();
    }

    @Override
    public void onEventReceived(Intent intent) {
        super.onEventReceived(intent);
        try {
            String action = intent.getStringExtra(SocketService.BROADCAST_INTENT_TYPE);
            String data = intent.getStringExtra(SocketService.BROADCAST_INTENT_DATA);

            JSONObject dataJson = new JSONObject(data);

            if (action.equalsIgnoreCase(SocketService.KEY_LOCATION_UPDATE) && detailModel.getSpJobStatus() == STATUS_ON_THE_WAY) {
                double latitude = dataJson.getDouble("latitude");
                double longitude = dataJson.getDouble("longitude");
                if (getCurrentFragment() != null && getCurrentFragment() instanceof RouteFragment) {
                    ((RouteFragment) getCurrentFragment()).updateLocations(latitude, longitude);
                }
                Log.d("Location", dataJson.toString());
            } else if (action.equalsIgnoreCase(SocketService.KEY_JOB_CANCEL)) {
                AppUtils.setResultAndFinish(this);
            } else if (action.equalsIgnoreCase(SocketService.KEY_JOB_CHAT)) {
                detailModel.setMessageCount(detailModel.getMessageCount() + 1);
                setMessageCounter(detailModel.getMessageCount());
            } else if (action.equalsIgnoreCase(SocketService.KEY_ADMIN_JOB_COMPLETE)) {
                fetchJobDetails();
            } else if (action.equalsIgnoreCase(SocketService.KEY_OFFER_ACCEPTED)) {
                fetchJobDetails();
            }
        } catch (Exception e) {
            Logger.caughtException(e);
        }
    }

    ReasonSelectionCallback selectionCallback = (reasonModel, otherReason, type) -> {
        if (type == CANCELLATION_REASON) {
            cancelPackage(reasonModel, otherReason);
        }
    };

    LineItemListener lineItemListener = (lineItemModel, position, clickType) -> {
        switch (clickType) {
            case LineItemListener.SAVE:
                addLineItem(lineItemModel);
                break;
            case LineItemListener.DELETE:
                deleteLineItem(lineItemModel);
                break;
            case LineItemListener.DISCARD:
                discardLineItem(position);
                break;
            case LineItemListener.EDIT:
                editLineItem(lineItemModel);
                break;
        }
    };
    BorderErrorEditText enterBidAmount;

    @SuppressLint("SetTextI18n")
    private void bidDialog() {
        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(R.layout.layout_bid_job, null);
        final androidx.appcompat.app.AlertDialog deleteDialog = new AlertDialog.Builder(this)
                .setView(deleteDialogView)
                .setCancelable(true)
                .create();
        enterBidAmount = deleteDialogView.findViewById(R.id.bidAmount);
        TextView etBidType = deleteDialogView.findViewById(R.id.et_bid_type);
        advancePercentage = deleteDialogView.findViewById(R.id.advance_payment_rv);
        hoursRv = deleteDialogView.findViewById(R.id.hours_rv);
        depositedAmount = deleteDialogView.findViewById(R.id.depositedAmount);
        TextView ab = deleteDialogView.findViewById(R.id.ab);
        setDepositViewData();
        if (detailModel.getTotalHours() != 0) {
            depositedAmount.setText("Deposit Amount (" + POND_CURRENCY + detailModel.getBidAmount() * detailModel.getTotalHours() + ")");
        } else {
            depositedAmount.setText("Deposit Amount (" + POND_CURRENCY + detailModel.getBidAmount() + ")");
        }
        enterBidAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                amountPercent(selectDepositAmount);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        if (detailModel.isJobDurationKnown()) {
            etBidType.setText(getString(R.string.hourly));
            setRecyclerViewData();
        } else {
            hoursRv.setVisibility(View.GONE);
            ab.setVisibility(View.GONE);
            etBidType.setText(getString(R.string.fixed));
        }
        enterBidAmount.setText(detailModel.getBidAmount() + "");
        Button bidBtnPost = deleteDialogView.findViewById(R.id.bidBtnPost);

        if (detailModel.isIsbidAdded()) {
            bidBtnPost.setText(getString(R.string.update_bid));
        }

        bidBtnPost.setOnClickListener(v -> {
            String amount = Objects.requireNonNull(enterBidAmount.getText()).toString().trim();
            if (TextUtils.isEmpty(amount)) {
                showSnackBar(R.string.please_enter_bid_amount);
//                setError(R.id.bidAmount, R.string.please_enter_bid_amount);
                return;
            }
            float newAmount = Float.parseFloat(amount);
            if (newAmount < 1) {
                showSnackBar(R.string.error_zero);
//                setError(R.id.bidAmount,R.string.error_zero);
                return;
            }
            findTextViewById(R.id.callMessage).setVisibility(View.GONE);
            sendBidPost(amount);
            numCheck = 1;
            deleteDialog.dismiss();
        });

        deleteDialog.show();
    }

    String sendBidPost = "";

    private void sendBidPost(String amount) {
        sendBidPost = AppConstants.getServerUrl(BID_AMOUNT_);

        Map<String, Object> map = new HashMap<>();
        map.put("jobId", jobId);
        map.put("bidAmount", amount);
        map.put("totalHours", selectedTimeSlot);
        map.put("firstPaymentInPercentage", selectDepositAmount);
        HttpRequestItem requestItem = new HttpRequestItem(sendBidPost);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_POST);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);

    }

    private void sendFixedBidPost() {
        sendBidPost = AppConstants.getServerUrl(BID_AMOUNT_);

        Map<String, Object> map = new HashMap<>();
        map.put("jobId", jobId);
        map.put("bidAmount", detailModel.getFixedJobAmount());
        map.put("totalHours", selectedTimeSlot);
        map.put("firstPaymentInPercentage", 0);
        HttpRequestItem requestItem = new HttpRequestItem(sendBidPost);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_POST);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);

    }

    String sendRejectPost;

    private void sendRejectPost() {
        sendRejectPost = AppConstants.getServerUrl(REJECT_BID);

        Map<String, Object> map = new HashMap<>();
        map.put("jobId", jobId);
        HttpRequestItem requestItem = new HttpRequestItem(sendRejectPost);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_POST);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);

    }

    @Override
    public void onRecyclerViewItemClick(BaseRecyclerViewHolder holder) {

    }

    @Override
    public void onRecyclerViewChildItemClick(BaseRecyclerViewHolder holder, int resourceId) {
        if (resourceId == R.id.iv_upload_image_port) {
            moveToFullScreen(holder);
        }
    }

    private void moveToFullScreen(BaseRecyclerViewHolder holder) {
        int num = holder.getAdapterPosition();
        Intent intent = new Intent(this, ImageActivity.class);
        intent.putExtra("type", 2);
        intent.putExtra("holderPosition", num);
        intent.putExtra("ImageVideo", (Serializable) mediaModelList);
        startActivity(intent);
    }

    private void populateList(List<BaseItem> itemList) {
        if (attachmentAdapter == null) {
            attachmentAdapter = new AttachmentAdapter(this, itemList, this);
            recyclerView.setAdapter(attachmentAdapter);
        } else
            attachmentAdapter.addAll(itemList);
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
    }

    HourSlotAdapter.OnHourSelect onHourSelect = new HourSlotAdapter.OnHourSelect() {
        @Override
        public void onHourSelected(int selectedValue) {
            selectedTimeSlot = selectedValue;
            hourSlotAdapter.setSelectedTimeSlot(selectedTimeSlot);
            if (detailModel.isJobDurationKnown()) {
                amountPercent(selectDepositAmount);
            }
        }
    };

    DepositAdapter.onDepositAmount onDepositSelect = new DepositAdapter.onDepositAmount() {
        @Override
        public void onDepositSelected(int selectedDeposit) {
            selectDepositAmount = selectedDeposit;
            depositAdapter.setDepositAmount(selectDepositAmount);
            amountPercent(selectDepositAmount);
        }
    };

    @SuppressLint("SetTextI18n")
    public void amountPercent(int per) {
        int time = 1;
        if (selectedTimeSlot != 0) {
            time = selectedTimeSlot;
        }
        String amount1 = Objects.requireNonNull(enterBidAmount.getText()).toString();
        if (!TextUtils.isEmpty(amount1)) {
            float amount11 = Float.parseFloat(amount1);
            if (per == 0) {
                depositedAmount.setText("Deposit Amount (" + POND_CURRENCY + amount11 * time + ")");
            } else if (per == 1) {
                depositedAmount.setText("Deposit Amount (" + POND_CURRENCY + (amount11 / 2) * time + ")");
            } else if (per == 2) {
                depositedAmount.setText("Deposit Amount (" + POND_CURRENCY + (amount11 / 4) * time + ")");
            }
        } else {
            depositedAmount.setText("Deposited Amount (" + POND_CURRENCY + "0)");
        }
    }

    private void setRecyclerViewData() {
        hoursRv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        hoursRv.setHasFixedSize(false);
        hoursRv.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        hourSlotAdapter = new HourSlotAdapter(this, MAX_HOURS, selectedTimeSlot, onHourSelect);
        hoursRv.setAdapter(hourSlotAdapter);
        ViewCompat.setNestedScrollingEnabled(hoursRv, false);
        ViewCompat.setNestedScrollingEnabled(findViewById(R.id.ns_wrapper), false);
        if (detailModel.getTotalHours() != 0) {
            selectedTimeSlot = detailModel.getTotalHours();
            hourSlotAdapter.setSelectedTimeSlot(detailModel.getTotalHours());
        }
    }

    private void setDepositViewData() {
        advancePercentage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        advancePercentage.setHasFixedSize(false);
        advancePercentage.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        depositAdapter = new DepositAdapter(this, MAX_PAYMENTS, selectDepositAmount, onDepositSelect);
        advancePercentage.setAdapter(depositAdapter);
        ViewCompat.setNestedScrollingEnabled(advancePercentage, false);
        ViewCompat.setNestedScrollingEnabled(findViewById(R.id.ns_wrapper), false);
        if (detailModel.getFirstPaymentInPercentage() != 0) {
            selectDepositAmount = detailModel.getFirstPaymentInPercentage();
            depositAdapter.setDepositAmount(detailModel.getFirstPaymentInPercentage());
        }
    }

    private void conflictFun() {
        disputeCreated = AppConstants.getServerUrl(String.format(AppConstants.CONFLICT, jobId));
        HttpRequestItem requestItem = new HttpRequestItem(disputeCreated);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_POST);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false),
                this);
        appNetworkTask.execute(requestItem);
    }

    private void callToDriver(boolean videoCall) {
        if (!TextUtils.isEmpty(detailModel.getUserProfileId())) {
            CallService.dial(this, detailModel.getUserProfileId(), videoCall);
            PrefUtils.setCalleeId(this, detailModel.getUserProfileId());
            PrefUtils.setCalleeName(this, detailModel.getName());
        } else {
            showToast("id Empty");
        }
    }
}
