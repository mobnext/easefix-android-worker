package com.apps.ondemand.app.ui.adapters;

import android.os.Build;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.ondemand.R;

import com.apps.ondemand.app.data.models.NotificationModel;
import com.apps.ondemand.app.ui.views.RoundedImageView;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.utils.GlideUtils;

import java.util.List;



public class NotificationsAdapter extends BaseRecyclerViewAdapter {

    BaseActivity context;

    public NotificationsAdapter(BaseActivity context, List<BaseItem> items, OnRecyclerViewItemClickListener itemClickListener) {
        super(items, itemClickListener);
        this.context = context;
    }

    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        BaseRecyclerViewHolder holder;
        if (viewType == BaseItem.ITEM_NOTIFICATION) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notification, parent, false);
            holder = new NotificationHolder(view);
        } else if (viewType == BaseItem.ITEM_EMPTY) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
            holder = new EmptyViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_progress, parent, false);
            holder = new ProgressViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder holder, int position) {
        if (holder instanceof NotificationHolder) {
            NotificationHolder notificationHolder = (NotificationHolder) holder;
            NotificationModel notificationModel = (NotificationModel) getItemAt(position);
            if (notificationModel.getIsRead()) {
                notificationHolder.rlNotificationWrapper.setBackgroundColor(ContextCompat.getColor(context, R.color.background));
            } else {
                notificationHolder.rlNotificationWrapper.setBackgroundColor(ContextCompat.getColor(context, R.color.unread_notification));
            }
            GlideUtils.glideLoadImageOrPlaceHolder(context, notificationHolder.ivNotificiationImage,
                    notificationModel.getProfileImage(), R.drawable.user_profile,
                    (int) context.getResources().getDimension(R.dimen.dimen_45),
                    (int) context.getResources().getDimension(R.dimen.dimen_45));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                notificationHolder.tvNotificationMessage.setText(Html.fromHtml(notificationModel.getMessage(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                notificationHolder.tvNotificationMessage.setText(Html.fromHtml(notificationModel.getMessage()));
            }
            notificationHolder.tvNotificationAgoTime.setText(notificationModel.getTimeAgo());
        }

    }

    private class NotificationHolder extends BaseRecyclerViewHolder {
        private RelativeLayout rlNotificationWrapper;
        private RoundedImageView ivNotificiationImage;
        private TextView tvNotificationMessage;
        private TextView tvNotificationAgoTime;

        public NotificationHolder(View view) {
            super(view);
            rlNotificationWrapper = (RelativeLayout) view.findViewById(R.id.rl_notification_wrapper);
            ivNotificiationImage = (RoundedImageView) view.findViewById(R.id.iv_notification_image);
            tvNotificationMessage = (TextView) view.findViewById(R.id.tv_notification_message);
            tvNotificationAgoTime = (TextView) view.findViewById(R.id.tv_notification_ago_time);
            rlNotificationWrapper.setOnClickListener(this);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return NotificationHolder.this;
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            if (getItemClickListener() != null)
                getItemClickListener().onRecyclerViewItemClick(NotificationHolder.this);
        }
    }

    public class ProgressViewHolder extends BaseRecyclerViewHolder {
        private ProgressViewHolder(View view) {
            super(view);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    public class EmptyViewHolder extends BaseRecyclerViewHolder {
        private TextView textViewEmpty;

        private EmptyViewHolder(View view) {
            super(view);
            textViewEmpty = view.findViewById(R.id.tv_message);
            textViewEmpty.setText(R.string.str_no_notifications);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getItemAt(position).getItemType();
    }
}
