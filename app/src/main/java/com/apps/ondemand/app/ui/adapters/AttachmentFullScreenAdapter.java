package com.apps.ondemand.app.ui.adapters;

import static com.apps.ondemand.common.utils.AppConstants.IMAGE_PORT;
import static com.apps.ondemand.common.utils.AppConstants.VIDEO_PORT;

import android.media.MediaPlayer;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.MediaModel;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.utils.VizImageVideoView;

import java.util.List;


public class AttachmentFullScreenAdapter extends BaseRecyclerViewAdapter {

    BaseActivity context;

    public AttachmentFullScreenAdapter(BaseActivity context, List<BaseItem> items, OnRecyclerViewItemClickListener itemClickListener) {
        super(items, itemClickListener);
        this.context = context;
    }

    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        BaseRecyclerViewHolder holder;
        if (viewType == BaseItem.ITEM_ATTACHMENTS) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_full_screen, parent, false);
            holder = new AttachmentHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
            holder = new EmptyViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder holder, int position) {
        if (holder instanceof AttachmentHolder) {
            AttachmentHolder attachmentHolder = (AttachmentHolder) holder;
            MediaModel portfolioModel = (MediaModel) getItemAt(position);

            if (portfolioModel.getType()== IMAGE_PORT){
                attachmentHolder.progress_circular.setVisibility(View.GONE);
                attachmentHolder.vizImageVideoView.setImage(portfolioModel.getUrl(), (int) context.getResources().getDimension(R.dimen.dimen_100), (int) context.getResources().getDimension(R.dimen.dimen_100), R.color.colorPrimaryBlue);
            }else if (portfolioModel.getType() == VIDEO_PORT){
                attachmentHolder.vizImageVideoView.setImage(portfolioModel.getUrl(), (int) context.getResources().getDimension(R.dimen.dimen_100), (int) context.getResources().getDimension(R.dimen.dimen_100), R.color.colorPrimaryBlue);
                attachmentHolder.iv_upload_image_Video.setVisibility(View.VISIBLE);
                attachmentHolder.iv_upload_image_Video.setVideoURI(Uri.parse(portfolioModel.getUrl()));
                attachmentHolder.iv_upload_image_Video.requestFocus();
                attachmentHolder.iv_upload_image_Video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        attachmentHolder.vizImageVideoView.setVisibility(View.GONE);
                        attachmentHolder.progress_circular.setVisibility(View.GONE);
                        attachmentHolder.iv_upload_image_Video.start();
                    }
                });
                attachmentHolder.iv_upload_image_Video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        attachmentHolder.iv_upload_image_Video.start();
                    }
                });
            }
        }

    }

    private class AttachmentHolder extends BaseRecyclerViewHolder {
        VizImageVideoView vizImageVideoView;
        VideoView iv_upload_image_Video;
        ProgressBar progress_circular;

        public AttachmentHolder(View view) {
            super(view, true);
            view.setOnClickListener(this);
            vizImageVideoView = view.findViewById(R.id.iv_upload_image_port);
            iv_upload_image_Video = view.findViewById(R.id.iv_upload_image_Video);
            progress_circular = view.findViewById(R.id.progress_circular);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return AttachmentHolder.this;
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            if (getItemClickListener() != null)
                getItemClickListener().onRecyclerViewChildItemClick(AttachmentHolder.this,v.getId());
        }
    }

    public class ProgressViewHolder extends BaseRecyclerViewHolder {
        private ProgressViewHolder(View view) {
            super(view);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    public class EmptyViewHolder extends BaseRecyclerViewHolder {
        private TextView textViewEmpty;

        private EmptyViewHolder(View view) {
            super(view);
            textViewEmpty = view.findViewById(R.id.tv_message);
            textViewEmpty.setText("");
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getItemAt(position).getItemType();
    }
}
