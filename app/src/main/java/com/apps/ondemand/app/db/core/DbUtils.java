package com.apps.ondemand.app.db.core;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created on 2016-12-03 15:01.
 *
 * @author Rana
 */
class DbUtils {

    private static ExecutorService executorService = Executors.newFixedThreadPool(2);

    public static void run(Runnable runnable) {
        executorService.submit(runnable);
    }

} // DbUtils
