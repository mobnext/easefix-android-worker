package com.apps.ondemand.app.ui.authentication;

import static com.apps.ondemand.BuildConfig.stripe_key;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.apps.ondemand.R;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.BankAccount;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@RequiresApi(api = Build.VERSION_CODES.S)
public class AddPaymentActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment);
        setActionBar(R.string.str_bank_account);
        setSubTitle(R.string.str_add_bank_account);

        findViewById(R.id.btn_proceed).setOnClickListener(this);
//        getBanksApiCall();
    }

    private void getBanksApiCall() {
        HttpRequestItem requestItem = new HttpRequestItem(AppConstants.getServerUrl(AppConstants.FETCH_BANK_ACCOUNT));
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    private boolean validateData() {

        String accountNumber = ((EditText) findViewById(R.id.et_account_number)).getText().toString();
        String sortNumber = ((EditText) findViewById(R.id.et_sortNumber)).getText().toString();

        if (!AppUtils.ifNotNullEmpty(accountNumber)) {
            showSnackBar("Please enter account Number");
            return false;
        } else if (!AppUtils.ifNotNullEmpty(sortNumber)) {
            showSnackBar("Please enter Sort Code");
            return false;
        } else
            return true;
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            JSONObject jsonObject = new JSONObject(response.getResponse());
            if (jsonObject.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equalsIgnoreCase(AppConstants.getServerUrl(AppConstants.FETCH_BANK_ACCOUNT))) {

                    if (jsonObject.getJSONObject("data").has("bankList") && !jsonObject.getJSONObject("data").isNull("bankList")) {
                        JSONArray bankList = jsonObject.getJSONObject("data").getJSONArray("bankList");
//                        populateBanksList(bankList);
                    }
                    findViewById(R.id.btn_proceed).setEnabled(true);
                    moveToNextActivity();
                }
            }
        } catch (Exception e) {

        }
    }

    private void updateBankInfoApiCall(String token) {
        HttpRequestItem requestItem = new HttpRequestItem(
                AppConstants.getServerUrl(AppConstants.FETCH_BANK_ACCOUNT));
        requestItem.setHttpRequestType(NetworkUtils.HTTP_POST);     //by default its GET
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        requestItem.setParams(getServerParams(token));
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    private Map<String, Object> getServerParams(String token) {
        Map<String, Object> params = new HashMap<>();

        params.put("bankToken", "" + token);

        return params;
    }

    @Override
    public void onClick(View v) {
        if (validateData()) {
            findViewById(R.id.btn_proceed).setEnabled(false);
            bankInf0();
        }
    }

    private void bankInf0() {

        String sortNum = ((EditText) findViewById(R.id.et_sortNumber)).getText().toString();
        String accountNumber = ((EditText) findViewById(R.id.et_account_number)).getText().toString();

        Stripe stripe = new Stripe(this);
        stripe.setDefaultPublishableKey(stripe_key);
        BankAccount bankAccount = new BankAccount(accountNumber, "GB", "GBP", sortNum);
        stripe.createBankAccountToken(bankAccount, new TokenCallback() {
            @Override
            public void onError(Exception error) {
                Log.e("Stripe Error", error.getMessage());
                showSnackBar(error.getMessage());
                findViewById(R.id.btn_proceed).setEnabled(true);
            }

            @Override
            public void onSuccess(com.stripe.android.model.Token token) {
                Log.e("Bank Token", token.getId());
                updateBankInfoApiCall(token.getId());
            }
        });
    }

    private void moveToNextActivity() {
        Intent intent = new Intent(this, SignUpCompletedActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}