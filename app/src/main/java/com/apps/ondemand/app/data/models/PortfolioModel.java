
package com.apps.ondemand.app.data.models;


import com.apps.ondemand.common.business.BaseItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PortfolioModel implements BaseItem, Serializable {


    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("type")
    @Expose
    private int type;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("isArchived")
    @Expose
    private boolean isArchived;

    public PortfolioModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }

    @Override
    public int getItemType() {
        return ITEM_PORTFOLIO;
    }
}