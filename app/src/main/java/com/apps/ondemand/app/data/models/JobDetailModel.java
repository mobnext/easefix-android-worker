package com.apps.ondemand.app.data.models;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class JobDetailModel implements Parcelable {

    @SerializedName("jobId")
    @Expose
    private String jobId;

    @SerializedName("isJobStart")
    @Expose
    private boolean isJobStart;
    @SerializedName("isJobDurationKnown")
    @Expose
    private boolean isJobDurationKnown;
    @SerializedName("isbidAdded")
    @Expose
    private boolean isbidAdded;

    @SerializedName("isTotalAmountPaid")
    @Expose
    private boolean isTotalAmountPaid;

    @SerializedName("serviceName")
    @Expose
    private String serviceName;
    @SerializedName("subService")
    @Expose
    private String subService;
    @SerializedName("serviceImage")
    @Expose
    private String serviceImage;
    @SerializedName("serviceDescription")
    @Expose
    private String serviceDescription;
    @SerializedName("specialInstruction")
    @Expose
    private String specialInstruction;
    @SerializedName("jobAddress")
    @Expose
    private JobAddressModel jobAddress;
    @SerializedName("workDescription")
    @Expose
    private String workDescription;
    @SerializedName("userProfileId")
    @Expose
    private String userProfileId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("profileImage")
    @Expose
    private String profileImage;
    @SerializedName("avgRating")
    @Expose
    private float avgRating;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("jobScheduleTime")
    @Expose
    private long jobScheduleTime;
    @SerializedName("jobStartTime")
    @Expose
    private long jobStartTime;
    @SerializedName("bidId")
    @Expose
    private String bidId;
    @SerializedName("isEnableCommunication")
    @Expose
    private boolean isEnableCommunication;
    @SerializedName("jobElapsedTime")
    @Expose
    private long jobElapsedTime;
    @SerializedName("spJobStatus")
    @Expose
    private int spJobStatus;
    @SerializedName("pickupLatitude")
    @Expose
    private double pickupLatitude;
    @SerializedName("pickupLongitude")
    @Expose
    private double pickupLongitude;
    @SerializedName("dropOffLatitude")
    @Expose
    private double dropOffLatitude;
    @SerializedName("dropOffLongitude")
    @Expose
    private double dropOffLongitude;
    @SerializedName("isSpRatingScreenShown")
    @Expose
    private boolean isSpRatingScreenShown;
    @SerializedName("isConnectAccountExists")
    @Expose
    private boolean isConnectAccountExists;
    @SerializedName("isSpRated")
    @Expose
    private boolean isSpRated;
    @SerializedName("messageCount")
    @Expose
    private int messageCount;
    @SerializedName("jobPauseStartTiming")
    @Expose
    private long jobPauseStartTiming;
    @SerializedName("costBreakDown")
    @Expose
    private PriceBreakDownModel priceBreakDownModel;
    @SerializedName("totalAmount")
    @Expose
    private double totalAmount;

    @SerializedName("totalJobDuration")
    @Expose
    private float totalJobDuration;
    @SerializedName("routeLocation")
    @Expose
    private List<RouteModel> routeModel = new ArrayList<>();
    @SerializedName("media")
    @Expose
    private List<MediaModel> mediaModel = new ArrayList<>();

    @SerializedName("JobIdIdentifier")
    @Expose
    private String jobIdIdentifier;
    @SerializedName("bidAmount")
    @Expose
    private float bidAmount;

    @SerializedName("fixedJobAmount")
    @Expose
    private float fixedJobAmount;

    @SerializedName("isJobFixed")
    @Expose
    private boolean isJobFixed;

    @SerializedName("totalHours")
    @Expose
    private int totalHours;
   @SerializedName("firstPaymentInPercentage")
    @Expose
    private int firstPaymentInPercentage;

    @RequiresApi(api = Build.VERSION_CODES.Q)
    protected JobDetailModel(Parcel in) {
        jobId = in.readString();
        isJobStart = in.readByte() != 0;
        isJobDurationKnown = in.readByte() != 0;
        isbidAdded = in.readByte() != 0;
        isTotalAmountPaid = in.readByte() != 0;
        serviceName = in.readString();
        subService = in.readString();
        serviceImage = in.readString();
        serviceDescription = in.readString();
        specialInstruction = in.readString();
        jobAddress = in.readParcelable(JobAddressModel.class.getClassLoader());
        workDescription = in.readString();
        userProfileId = in.readString();
        name = in.readString();
        profileImage = in.readString();
        avgRating = in.readFloat();
        phoneNumber = in.readString();
        jobScheduleTime = in.readLong();
        jobStartTime = in.readLong();
        jobElapsedTime = in.readLong();
        spJobStatus = in.readInt();
        pickupLatitude = in.readDouble();
        pickupLongitude = in.readDouble();
        dropOffLatitude = in.readDouble();
        dropOffLongitude = in.readDouble();
        isSpRatingScreenShown = in.readByte() != 0;
        isSpRated = in.readByte() != 0;
        isConnectAccountExists = in.readByte() != 0;
        messageCount = in.readInt();
        jobPauseStartTiming = in.readLong();
        priceBreakDownModel = in.readParcelable(PriceBreakDownModel.class.getClassLoader());
        totalAmount = in.readDouble();
        totalJobDuration = in.readFloat();
        routeModel = in.createTypedArrayList(RouteModel.CREATOR);
        mediaModel= in.createTypedArrayList(MediaModel.CREATOR);
        jobIdIdentifier = in.readString();
        bidAmount = in.readFloat();
        totalHours = in.readInt();
        firstPaymentInPercentage = in.readInt();
        bidId=in.readString();
        isEnableCommunication=in.readBoolean();
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(jobId);
        dest.writeByte((byte) (isJobStart ? 1 : 0));
        dest.writeByte((byte) (isJobDurationKnown ? 1 : 0));
        dest.writeByte((byte) (isbidAdded ? 1 : 0));
        dest.writeByte((byte) (isTotalAmountPaid ? 1 : 0));
        dest.writeString(serviceName);
        dest.writeString(subService);
        dest.writeString(serviceImage);
        dest.writeString(serviceDescription);
        dest.writeString(specialInstruction);
        dest.writeParcelable(jobAddress, flags);
        dest.writeString(workDescription);
        dest.writeString(userProfileId);
        dest.writeString(name);
        dest.writeString(profileImage);
        dest.writeFloat(avgRating);
        dest.writeString(phoneNumber);
        dest.writeLong(jobScheduleTime);
        dest.writeLong(jobStartTime);
        dest.writeLong(jobElapsedTime);
        dest.writeInt(spJobStatus);
        dest.writeDouble(pickupLatitude);
        dest.writeDouble(pickupLongitude);
        dest.writeDouble(dropOffLatitude);
        dest.writeDouble(dropOffLongitude);
        dest.writeByte((byte) (isSpRatingScreenShown ? 1 : 0));
        dest.writeByte((byte) (isSpRated ? 1 : 0));
        dest.writeByte((byte) (isConnectAccountExists ? 1 : 0));
        dest.writeInt(messageCount);
        dest.writeLong(jobPauseStartTiming);
        dest.writeParcelable(priceBreakDownModel, flags);
        dest.writeDouble(totalAmount);
        dest.writeFloat(totalJobDuration);
        dest.writeTypedList(routeModel);
        dest.writeTypedList(mediaModel);
        dest.writeString(jobIdIdentifier);
        dest.writeFloat(bidAmount);
        dest.writeInt(totalHours);
        dest.writeInt(firstPaymentInPercentage);
        dest.writeString(bidId);
        dest.writeBoolean(isEnableCommunication);
    }

    public int getFirstPaymentInPercentage() {
        return firstPaymentInPercentage;
    }

    public void setFirstPaymentInPercentage(int firstPaymentInPercentage) {
        this.firstPaymentInPercentage = firstPaymentInPercentage;
    }

    public boolean isIsbidAdded() {
        return isbidAdded;
    }

    public void setIsbidAdded(boolean isbidAdded) {
        this.isbidAdded = isbidAdded;
    }

    public boolean isTotalAmountPaid() {
        return isTotalAmountPaid;
    }

    public void setTotalAmountPaid(boolean totalAmountPaid) {
        isTotalAmountPaid = totalAmountPaid;
    }

    public float getBidAmount() {
        return bidAmount;
    }

    public int getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(int totalHours) {
        this.totalHours = totalHours;
    }

    public void setBidAmount(float bidAmount) {
        this.bidAmount = bidAmount;
    }

    public String getBidId() {
        return bidId;
    }

    public boolean isJobDurationKnown() {
        return isJobDurationKnown;
    }

    public void setJobDurationKnown(boolean jobDurationKnown) {
        isJobDurationKnown = jobDurationKnown;
    }

    public void setBidId(String bidId) {
        this.bidId = bidId;
    }

    public boolean isEnableCommunication() {
        return isEnableCommunication;
    }

    public void setEnableCommunication(boolean enableCommunication) {
        isEnableCommunication = enableCommunication;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JobDetailModel> CREATOR = new Creator<JobDetailModel>() {
        @RequiresApi(api = Build.VERSION_CODES.Q)
        @Override
        public JobDetailModel createFromParcel(Parcel in) {
            return new JobDetailModel(in);
        }

        @Override
        public JobDetailModel[] newArray(int size) {
            return new JobDetailModel[size];
        }
    };

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public boolean isJobStart() {
        return isJobStart;
    }

    public void setJobStart(boolean jobStart) {
        isJobStart = jobStart;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getSubService() {
        return subService;
    }

    public void setSubService(String subService) {
        this.subService = subService;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceImage() {
        return serviceImage;
    }

    public void setServiceImage(String serviceImage) {
        this.serviceImage = serviceImage;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getSpecialInstruction() {
        return specialInstruction;
    }

    public void setSpecialInstruction(String specialInstruction) {
        this.specialInstruction = specialInstruction;
    }

    public JobAddressModel getJobAddress() {
        return jobAddress;
    }

    public void setJobAddress(JobAddressModel jobAddress) {
        this.jobAddress = jobAddress;
    }

    public String getWorkDescription() {
        return workDescription;
    }

    public void setWorkDescription(String workDescription) {
        this.workDescription = workDescription;
    }

    public String getUserProfileId() {
        return userProfileId;
    }

    public void setUserProfileId(String userProfileId) {
        this.userProfileId = userProfileId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public float getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(float avgRating) {
        this.avgRating = avgRating;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public long getJobScheduleTime() {
        return jobScheduleTime;
    }

    public void setJobScheduleTime(long jobScheduleTime) {
        this.jobScheduleTime = jobScheduleTime;
    }

    public long getJobStartTime() {
        return jobStartTime;
    }

    public void setJobStartTime(long jobStartTime) {
        this.jobStartTime = jobStartTime;
    }

    public int getSpJobStatus() {
        return spJobStatus;
    }

    public void setSpJobStatus(int spJobStatus) {
        this.spJobStatus = spJobStatus;
    }

    public double getPickupLatitude() {
        return pickupLatitude;
    }

    public void setPickupLatitude(double pickupLatitude) {
        this.pickupLatitude = pickupLatitude;
    }

    public double getPickupLongitude() {
        return pickupLongitude;
    }

    public void setPickupLongitude(double pickupLongitude) {
        this.pickupLongitude = pickupLongitude;
    }

    public double getDropOffLatitude() {
        return dropOffLatitude;
    }

    public void setDropOffLatitude(double dropOffLatitude) {
        this.dropOffLatitude = dropOffLatitude;
    }

    public double getDropOffLongitude() {
        return dropOffLongitude;
    }

    public void setDropOffLongitude(double dropOffLongitude) {
        this.dropOffLongitude = dropOffLongitude;
    }

    public boolean isSpRatingScreenShown() {
        return isSpRatingScreenShown;
    }

    public void setSpRatingScreenShown(boolean spRatingScreenShown) {
        isSpRatingScreenShown = spRatingScreenShown;
    }

    public boolean isConnectAccountExists() {
        return isConnectAccountExists;
    }

    public void setConnectAccountExists(boolean connectAccountExists) {
        isConnectAccountExists = connectAccountExists;
    }

    public boolean isSpRated() {
        return isSpRated;
    }

    public void setSpRated(boolean spRated) {
        isSpRated = spRated;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    public long getJobPauseStartTiming() {
        return jobPauseStartTiming;
    }

    public void setJobPauseStartTiming(long jobPauseStartTiming) {
        this.jobPauseStartTiming = jobPauseStartTiming;
    }

    public PriceBreakDownModel getPriceBreakDownModel() {
        return priceBreakDownModel;
    }

    public void setPriceBreakDownModel(PriceBreakDownModel priceBreakDownModel) {
        this.priceBreakDownModel = priceBreakDownModel;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<RouteModel> getRouteModel() {
        return routeModel;
    }

    public void setRouteModel(List<RouteModel> routeModel) {
        this.routeModel = routeModel;
    }

    public List<MediaModel> getMediaModel() {
        return mediaModel;
    }

    public void setMediaModel(List<MediaModel> mediaModel) {
        this.mediaModel = mediaModel;
    }

    public float getTotalJobDuration() {
        //this will remove fraction when fractional part is zero
        return totalJobDuration;
    }

    public void setTotalJobDuration(float totalJobDuration) {
        this.totalJobDuration = totalJobDuration;
    }

    public long getJobElapsedTime() {
        return jobElapsedTime;
    }

    public void setJobElapsedTime(long jobElapsedTime) {
        this.jobElapsedTime = jobElapsedTime;
    }

    public String getJobIdIdentifier() {
        return jobIdIdentifier;
    }

    public void setJobIdIdentifier(String jobIdIdentifier) {
        this.jobIdIdentifier = jobIdIdentifier;
    }

    public float getFixedJobAmount() {
        return fixedJobAmount;
    }

    public void setFixedJobAmount(float fixedJobAmount) {
        this.fixedJobAmount = fixedJobAmount;
    }

    public boolean isJobFixed() {
        return isJobFixed;
    }

    public void setJobFixed(boolean jobFixed) {
        isJobFixed = jobFixed;
    }
}
