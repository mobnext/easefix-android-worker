package com.apps.ondemand.app.ui.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.ProficiencyModel;

import java.util.ArrayList;
import java.util.List;

public class ProficiencyAdapter extends BaseAdapter {

    private List<ProficiencyModel> objects = new ArrayList<>();

    private Context context;
    private LayoutInflater layoutInflater;

    public ProficiencyAdapter(Context context, List<ProficiencyModel> objects) {
        this.context = context;
        this.objects = objects;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public ProficiencyModel getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.row_proficiency, null);
            convertView.setTag(new ViewHolder(convertView));
        }
        initializeViews(getItem(position), (ViewHolder) convertView.getTag());
        return convertView;
    }

    private void initializeViews(ProficiencyModel object, ViewHolder holder) {
        holder.ivProficiencySelected.setSelected(object.isSelected());
        holder.tvProficiencyLabel.setText(object.getLabel());
    }

    protected class ViewHolder {
        private TextView tvProficiencyLabel;
        private ImageView ivProficiencySelected;

        public ViewHolder(View view) {
            tvProficiencyLabel = (TextView) view.findViewById(R.id.tv_proficiency_label);
            ivProficiencySelected = view.findViewById(R.id.iv_proficiency_selected);
        }
    }
}
