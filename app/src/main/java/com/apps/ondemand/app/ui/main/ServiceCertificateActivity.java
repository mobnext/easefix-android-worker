package com.apps.ondemand.app.ui.main;

import android.app.Activity;
import android.content.Intent;
import androidx.core.view.ViewCompat;

import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.webkit.URLUtil;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.ServiceCertificateModel;
import com.apps.ondemand.app.ui.adapters.UpdateServiceCertificatesAdapter;
import com.apps.ondemand.common.base.TakePhotoActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.BitmapUtils;
import com.apps.ondemand.common.utils.FileUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServiceCertificateActivity extends TakePhotoActivity implements OnRecyclerViewItemClickListener, View.OnClickListener {
    private String fetchProfileDataUrl = "";
    protected RecyclerView recyclerView;
    UpdateServiceCertificatesAdapter addServiceCertificatesAdapter;
    List<BaseItem> certificateModelList = new ArrayList<>();
    private String imageUploadUrl = "";
    private String addServiceCertificates = "";
    private int selectedPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_service_certificate);
        setActionBar(R.string.str_service_certificates);
        setSubTitle(R.string.str_service_certificates_sub);
        initView();
        fetchProfile();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.ll_add_certificate) {
            animate();
            certificateModelList.add(new ServiceCertificateModel());
            addServiceCertificatesAdapter.notifyDataSetChanged();
        } else if (view.getId() == R.id.btn_save) {
            validateCertificatesCallApi();
        }
    }

    private void validateCertificatesCallApi() {
        List<ServiceCertificateModel> selectedModels = new ArrayList<>();


        for (int i = 0; i < certificateModelList.size(); i++) {

            ServiceCertificateModel serviceCertificateModel = (ServiceCertificateModel) certificateModelList.get(i);
            if (TextUtils.isEmpty(serviceCertificateModel.getProfessionalDocsTitle())) {
                String error = String.format(getString(R.string.error_service_certificate_title), String.valueOf(i + 1));
                showSnackBar(error);
                setTitleError(String.valueOf(i+1));
                return;
            }
            if (!URLUtil.isValidUrl(serviceCertificateModel.getProfessionalDocsFront())) {
                showSnackBar(getString(R.string.error_service_certificate) + " " + (i + 1));
                return;
            }
            selectedModels.add(serviceCertificateModel);

        }


        addServiceCertificates = AppConstants.getServerUrl(AppConstants.UPDATE_SERVICE_CERTIFICATES);

        Map<String, Object> map = new HashMap<>();
        map.put("serviceImages", new Gson().toJson(selectedModels));

        HttpRequestItem requestItem = new HttpRequestItem(addServiceCertificates);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_PUT);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);

    }
    private void setTitleError(String title) {
        if (selectedPosition != -1) {
            ((ServiceCertificateModel) certificateModelList.get(selectedPosition)).setProfessionalDocsTitle("");
            addServiceCertificatesAdapter.notifyDataSetChanged();
        }
    }
    private void initView() {
        findButtonById(R.id.btn_save).setText(R.string.str_save);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        addServiceCertificatesAdapter = new UpdateServiceCertificatesAdapter(this, certificateModelList, this);
        recyclerView.setAdapter(addServiceCertificatesAdapter);
        findViewById(R.id.ll_add_certificate).setOnClickListener(this);
        findViewById(R.id.btn_save).setOnClickListener(this);

        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
        ViewCompat.setNestedScrollingEnabled(findViewById(R.id.ns_wrapper), false);
    }


    @Override
    public void onRecyclerViewItemClick(BaseRecyclerViewHolder holder) {

    }

    @Override
    public void onRecyclerViewChildItemClick(BaseRecyclerViewHolder holder, int resourceId) {
        int position = holder.getAdapterPosition();
        switch (resourceId) {
            case R.id.iv_delete_image:
                animate();
                certificateModelList.remove(position);
                addServiceCertificatesAdapter.notifyDataSetChanged();
                break;

            case R.id.iv_upload_image_port:
                this.selectedPosition = position;
                showChoosePhotoDialog();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == CHOOSE_PHOTO_RESULT_CODE) {
                    if (data.getData() != null) {
                        selectedPhotoUri = data.getData();
                    } else {
                        selectedPhotoUri = null;
                    }
                } else {
                    if (requestCode == TAKE_PHOTO_RESULT_CODE) {
                        selectedPhotoUri = BitmapUtils.getImageUri(this, (Bitmap) data.getExtras().get("data"));
                    } else {
                        selectedPhotoUri = null;
                    }
                }
//            filePath = FileUtils.getPath(this, selectedPhotoUri);
                String filePath = FileUtils.getPath(this, selectedPhotoUri);
                uploadImageToServer(filePath);
            } else {
                selectedPhotoUri = null;
            }
//            switch (requestCode) {
//                case CHOOSE_PHOTO_RESULT_CODE:
//                case TAKE_PHOTO_RESULT_CODE: {
//                    if (requestCode == CHOOSE_PHOTO_RESULT_CODE)
//                        selectedPhotoUri = data.getData();
//
//                    if (selectedPhotoUri == null)
//                        return;
//
//                    String filePath = FileUtils.getPath(this, selectedPhotoUri);
//                    uploadImageToServer(filePath);
//                    break;
//                }
//
//            }
        }
    }

    public void uploadImageToServer(String filePath) {

        imageUploadUrl = AppConstants.getServerUrl(AppConstants.UPLOAD_LICENCE_IMAGE);
        HttpRequestItem requestItem = new HttpRequestItem(imageUploadUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_MULTIPART);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        Map<String, Object> data = new HashMap<>();
        data.put("path", filePath);
        requestItem.setParams(data);
        requestItem.setHttpRequestTimeout(NetworkUtils.HTTP_MULTIPART_TIMEOUT);

        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    private void fetchProfile() {
        fetchProfileDataUrl = AppConstants.getServerUrl(AppConstants.FETCH_PROFILE_DATA);

        HttpRequestItem requestItem = new HttpRequestItem(fetchProfileDataUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            JSONObject responseJson = new JSONObject(response.getResponse());
            if (responseJson.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equalsIgnoreCase(imageUploadUrl)) {
                    JSONObject data = responseJson.getJSONObject("data");
                    String imageUrl = data.getString("url");
                    setImageInList(imageUrl);
                } else if (response.getHttpRequestUrl().equalsIgnoreCase(addServiceCertificates)) {
                    showSnackBar(responseJson.getString("message"));
                    fetchProfile();
                } else if (response.getHttpRequestUrl().equals(fetchProfileDataUrl)) {
                    JSONObject data = responseJson.getJSONObject("data");
                    JSONArray professionalDocuments = data.getJSONArray("professionalLicenseDocuments");
                    if (professionalDocuments.length() != 0) {
                        certificateModelList = new Gson().fromJson(professionalDocuments.toString(),
                                new TypeToken<List<ServiceCertificateModel>>() {
                                }.getType());
                        addServiceCertificatesAdapter = new UpdateServiceCertificatesAdapter (this, certificateModelList, this);
                        recyclerView.setAdapter(addServiceCertificatesAdapter);
                    }

                }
            } else
                showSnackBar(responseJson.getString("message"));
        } catch (JSONException e) {
            Logger.error(false, e);
        }
    }

    private void setImageInList(String imageUrl) {
        if (selectedPosition != -1) {
            ((ServiceCertificateModel) certificateModelList.get(selectedPosition)).setProfessionalDocsFront(imageUrl);
            addServiceCertificatesAdapter.notifyDataSetChanged();
        }
    }


}
