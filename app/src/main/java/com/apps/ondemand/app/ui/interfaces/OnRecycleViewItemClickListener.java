package com.apps.ondemand.app.ui.interfaces;

import androidx.recyclerview.widget.RecyclerView;

public interface OnRecycleViewItemClickListener {
    void onRecyclerViewItemClick(RecyclerView.ViewHolder holder);
}
