package com.apps.ondemand.app.data;

import com.apps.ondemand.app.data.preferences.PreferenceUtils;
import com.apps.ondemand.app.data.preferences.SharedPreferenceManager;
import com.apps.ondemand.common.utils.Logger;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by bilal on 11/01/2018.
 */

public class UserManager {

    public static void saveUserData(JSONObject data) {
        parseStringAndSave(data, PreferenceUtils.USER_ID);
        parseStringAndSave(data, PreferenceUtils.NAME);
        parseStringAndSave(data, PreferenceUtils.FIRST_NAME);
        parseStringAndSave(data, PreferenceUtils.LAST_NAME);
        parseStringAndSave(data, PreferenceUtils.PHONE_PREFIX);
        parseStringAndSave(data, PreferenceUtils.REFERRAL_CODE);
        parseStringAndSave(data, PreferenceUtils.PHONE);
        parseStringAndSave(data, PreferenceUtils.EMAIL);
        parseStringAndSave(data, PreferenceUtils.ABOUT);
        parseStringAndSave(data, PreferenceUtils.IMAGE);
        parseBooleanAndSave(data, PreferenceUtils.IS_BLOCKED);
        parseBooleanAndSave(data, PreferenceUtils.IS_SIGN_UP_COMPLETED);
        parseStringAndSave(data, PreferenceUtils.AVERAGE_RATING);
        parseBooleanAndSave(data, PreferenceUtils.IS_VERIFIED_BY_ADMIN);
        parseIntAndSave(data, PreferenceUtils.STEP_COMPLETED);
        parseBooleanAndSave(data, PreferenceUtils.IS_COMPANY);
        parseStringAndSave(data, PreferenceUtils.COMPANY_NAME);
        parseBooleanAndSave(data, PreferenceUtils.IS_COMPANY_APPROVED);
        parseBooleanAndSave(data, PreferenceUtils.IS_COMPANY_REJECTED);
        parseBooleanAndSave(data, PreferenceUtils.IS_CERTIFICATE_APPROVED);
    }

    public static String getUserId() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.USER_ID, "");
    }

    public static String getUserName() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.NAME, "");
    }

    public static String getFirstName() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.FIRST_NAME, "");
    }

    public static String getLastName() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.LAST_NAME, "");
    }

    public static String getReferralCode() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.REFERRAL_CODE, "");
    }

    public static String getEmail() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.EMAIL, "");
    }

    public static String getFullNumber() {
        return getCountryCode().concat(getPhone());
    }

    public static String getCountryCode() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.PHONE_PREFIX, "");
    }

    public static String getPhone() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.PHONE, "");
    }

    public static boolean getIsCompany() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.IS_COMPANY, false);
    }
    public static boolean getIsCertificateVerified() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.IS_CERTIFICATE_APPROVED, false);
    }

    public static String getCompanyName() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.COMPANY_NAME, "");
    }

    public static boolean getIsBlocked() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.IS_BLOCKED, false);
    }

    public static boolean getIsSignUpCompleted() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.IS_SIGN_UP_COMPLETED, false);
    }

    public static void setVerified(boolean verified) {
        SharedPreferenceManager.getInstance().save(PreferenceUtils.IS_VERIFIED_BY_ADMIN, verified);
    }

    public static boolean isVerifiedByAdmin() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.IS_VERIFIED_BY_ADMIN, false);
    }

    public static String getImage() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.IMAGE, "");
    }

    public static String getAverageRating() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.AVERAGE_RATING, "0.0");
    }

    public static String getBio() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.ABOUT, "");
    }

    private static void parseStringAndSave(JSONObject data, String key) {
        try {
            SharedPreferenceManager.getInstance().save(key, data.has(key)
                    ? data.getString(key) : "");
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    }

    private static void parseIntAndSave(JSONObject data, String key) {
        try {
            SharedPreferenceManager.getInstance().save(key, data.has(key)
                    ? data.getInt(key) : 0);
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    }

    private static void parseBooleanAndSave(JSONObject data, String key) {
        try {
            SharedPreferenceManager.getInstance().save(key, data.has(key) && data.getBoolean(key));
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    }


    public static boolean isUserLoggedIn() {
        if (!SharedPreferenceManager.getInstance().
                read(PreferenceUtils.COOKIE, "").equalsIgnoreCase("") &&
                SharedPreferenceManager.getInstance().read(PreferenceUtils.IS_LOGGED_IN, false))
            return true;
        return false;
    }

    public static void saveSession(boolean isLoggedIn) {
        SharedPreferenceManager.getInstance().save(PreferenceUtils.IS_LOGGED_IN, isLoggedIn);
    }

    public static void setOnline(boolean isOnline) {
        SharedPreferenceManager.getInstance().save(PreferenceUtils.IS_ONLINE, isOnline);
    }

    public static boolean isOnline() {
        return SharedPreferenceManager.getInstance()
                .read(PreferenceUtils.IS_ONLINE, false);

    }
    //Twilio Implementation
    public static void setSelectedInterestId(String selectedInterestId) {
        SharedPreferenceManager.getInstance().save(PreferenceUtils.SELECTED_INTERESTED_ID, selectedInterestId);
    }

    public static String getSelectedInterestId() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.SELECTED_INTERESTED_ID, "");
    }

    public static String getAccessToken() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.TOKEN, "");
    }

    public static void setAccessToken(String accessToken) {
        SharedPreferenceManager.getInstance().save(PreferenceUtils.TOKEN, accessToken);
    }

    public static boolean isInCall() {
        return SharedPreferenceManager.getInstance().read(PreferenceUtils.IN_CALL, false);
    }

    public static void setInCall(boolean inCall) {
        SharedPreferenceManager.getInstance().save(PreferenceUtils.IN_CALL, inCall);
    }
}
