package com.apps.ondemand.app.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.SpokenLanguageModel;
import com.apps.ondemand.app.ui.interfaces.OnRecycleViewItemClickListener;

import java.util.ArrayList;
import java.util.List;


public class SpokenLanguageAdapter extends RecyclerView.Adapter<SpokenLanguageAdapter.SpokenLanguageHolder> implements Filterable {

    Context context;
    OnRecycleViewItemClickListener itemClickListener;
    private List<SpokenLanguageModel> chapterList;
    private List<SpokenLanguageModel> chapterListFiltered;

    public SpokenLanguageAdapter(Context context, List<SpokenLanguageModel> items, OnRecycleViewItemClickListener itemClickListener) {
        this.context = context;
        this.itemClickListener = itemClickListener;
        this.chapterList = new ArrayList<>(items);
        this.chapterListFiltered = items;
    }

    @NonNull
    @Override
    public SpokenLanguageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_spoken_language, parent, false);

        return new SpokenLanguageHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SpokenLanguageHolder holder, int position) {
        SpokenLanguageModel spokenLanguageModel = getItemAt(position);
        holder.tvLabel.setText(spokenLanguageModel.getLabel());
        holder.tvSubLabel.setText(spokenLanguageModel.getSubLabel());
        if (spokenLanguageModel.isAdded()) {
            holder.ivLanguageAction.setImageResource(R.drawable.delete_language);

        } else {
            holder.ivLanguageAction.setImageResource(R.drawable.add_language);
        }


    }

    @Override
    public int getItemCount() {
        return chapterListFiltered.size();
    }

    public SpokenLanguageModel getItemAt(int position) {
        return chapterListFiltered.get(position);
    }


    public class SpokenLanguageHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvLabel;
        private TextView tvSubLabel;
        private ImageView ivLanguageAction;


        public SpokenLanguageHolder(View view) {
            super(view);
            tvLabel = view.findViewById(R.id.tv_label);
            tvSubLabel = view.findViewById(R.id.tv_sub_label);
            ivLanguageAction = view.findViewById(R.id.iv_language_action);
            ivLanguageAction.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onRecyclerViewItemClick(this);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    chapterListFiltered = chapterList;
                } else {
                    List<SpokenLanguageModel> filteredList = new ArrayList<>();
                    for (SpokenLanguageModel spokenLanguageModel : chapterList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (spokenLanguageModel.getLabel().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(spokenLanguageModel);
                        }
                    }

                    chapterListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = chapterListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                chapterListFiltered = (ArrayList<SpokenLanguageModel>) filterResults.values;
                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}
