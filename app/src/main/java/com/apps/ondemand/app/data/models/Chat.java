
package com.apps.ondemand.app.data.models;


import com.apps.ondemand.common.business.BaseItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Chat implements BaseItem {


    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("jobId")
    @Expose
    private String jobId;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("senderType")
    @Expose
    private int senderType;
    @SerializedName("receiverType")
    @Expose
    private int receiverType;
    @SerializedName("driverAccountId")
    @Expose
    private String driverAccountId;
    @SerializedName("userAccountId")
    @Expose
    private String userAccountId;
    @SerializedName("timePassed")
    @Expose
    private String timePassed;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("unReadCount")
    @Expose
    private Integer unReadCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getSenderType() {
        return senderType;
    }

    public void setSenderType(int senderType) {
        this.senderType = senderType;
    }

    public int getReceiverType() {
        return receiverType;
    }

    public void setReceiverType(int receiverType) {
        this.receiverType = receiverType;
    }

    public String getDriverAccountId() {
        return driverAccountId;
    }

    public void setDriverAccountId(String driverAccountId) {
        this.driverAccountId = driverAccountId;
    }

    public String getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(String userAccountId) {
        this.userAccountId = userAccountId;
    }

    public String getTimePassed() {
        return timePassed;
    }

    public void setTimePassed(String timePassed) {
        this.timePassed = timePassed;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getUnReadCount() {
        return unReadCount;
    }

    public void setUnReadCount(Integer unReadCount) {
        this.unReadCount = unReadCount;
    }

    @Override
    public int getItemType() {
        return 0;
    }
}