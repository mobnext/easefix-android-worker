package com.apps.ondemand.app.db.core;

/**
 * <p>Sort order for fetching records from database.</p>
 * <p>
 * Created on 2016-12-03 15:01.
 *
 * @author Rana
 */
public enum SortOrder {
    ASC, DESC
} // SortOrder
