package com.apps.ondemand.app.ui.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.Empty;
import com.apps.ondemand.app.data.models.ScheduleJobModel;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.DateUtils;
import com.apps.ondemand.common.utils.VizImageView;

import java.util.List;

public class MyJobsAdapter extends BaseRecyclerViewAdapter {

    Context context;

    public MyJobsAdapter(Context context, List<BaseItem> items, OnRecyclerViewItemClickListener itemClickListener) {
        super(items, itemClickListener);
        this.context = context;
    }

    @NonNull
    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        BaseRecyclerViewHolder holder;
        if (viewType == BaseItem.ITEM_JOB) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_schedule_job, parent, false);
            holder = new JobHolder(view);
        } else if (viewType == BaseItem.ITEM_EMPTY) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_job_empty, parent, false);
            holder = new EmptyViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_progress, parent, false);
            holder = new ProgressViewHolder(view);
        }
        return holder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull BaseRecyclerViewHolder holder, int position) {
        if (holder instanceof JobHolder) {
            JobHolder jobHolder = (JobHolder) holder;
            ScheduleJobModel scheduleJobModel = (ScheduleJobModel) getItemAt(position);
            jobHolder.tvJobId.setText(scheduleJobModel.getJobIdIdentifier());
            jobHolder.tvUserName.setText(scheduleJobModel.getName());
            jobHolder.ivUserImage.setImage(scheduleJobModel.getProfileImage(), false);
            jobHolder.rbUserRating.setRating(scheduleJobModel.getAvgRating());
            jobHolder.tvRating.setText(AppUtils.formatRating(scheduleJobModel.getAvgRating()));
            jobHolder.tvJobName.setText(scheduleJobModel.getServiceName());
            jobHolder.tvTime.setText(DateUtils.convertDate(AppUtils.GetTimeForForApp(scheduleJobModel.getExpectedJobStartTime()), DateUtils.DATE_SCHEDULE_JOB));

            if (scheduleJobModel.getIsFixed()) {
                if (!scheduleJobModel.getIsbidAdded()) {
                    jobHolder.tvStatus.setVisibility(View.GONE);
                }
                jobHolder.fixed.setVisibility(View.VISIBLE);
            }
            else{
                jobHolder.tvStatus.setVisibility(View.VISIBLE);
                jobHolder.fixed.setVisibility(View.GONE);
            }
            if (scheduleJobModel.getJobDurationKnown()) {
                jobHolder.tvDuration.setText(scheduleJobModel.getJobDurationInMinutes() + "hr " + context.getString(R.string.str_job));
            } else {
                jobHolder.tvDuration.setText(context.getString(R.string.str_fixed_job));
            }
            jobHolder.tvLocation.setText(scheduleJobModel.getPrimaryAddress());
            jobHolder.setJobStatus(scheduleJobModel.getSpJobStatus());
//            jobHolder.setJobStatus(scheduleJobModel.getIsbidAdded(),scheduleJobModel.getBidAmount());
        } else if (holder instanceof EmptyViewHolder) {
            EmptyViewHolder emptyViewHolder = (EmptyViewHolder) holder;
            Empty empty = (Empty) getItemAt(position);
            emptyViewHolder.textViewEmpty.setText(empty.getMessage());

        }

    }


    private class JobHolder extends BaseRecyclerViewHolder {

        private final LinearLayout fixed;
        private final TextView tvStatus;
        private final VizImageView ivUserImage;
        private final TextView tvUserName;
        private final TextView tvRating;
        private final RatingBar rbUserRating;
        private final TextView tvJobId;
        private final TextView tvJobName;
        private final TextView tvTime;
        private final TextView tvDuration;
        private final TextView tvLocation;

        public JobHolder(View view) {
            super(view);
            fixed = view.findViewById(R.id.fixed);
            LinearLayout llJob = view.findViewById(R.id.ll_job);
            tvJobId = view.findViewById(R.id.tv_job_id);
            ivUserImage = view.findViewById(R.id.iv_profile_image);
            tvUserName = view.findViewById(R.id.tv_profile_name);
            rbUserRating = view.findViewById(R.id.rb_profile_rating);
            tvRating = view.findViewById(R.id.tv_profile_rating);
            tvStatus = view.findViewById(R.id.tv_status);
            tvJobName = view.findViewById(R.id.tv_job_name);
            tvTime = view.findViewById(R.id.tv_time);
            tvDuration = view.findViewById(R.id.tv_duration);
            tvLocation = view.findViewById(R.id.tv_location);
            llJob.setOnClickListener(this);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return JobHolder.this;
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            if (getItemClickListener() != null)
                getItemClickListener().onRecyclerViewChildItemClick(JobHolder.this,v.getId());
        }

        public void setJobStatus(int spJobStatus) {
            if (spJobStatus == AppConstants.STATUS_OPEN) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_blue);
                tvStatus.setText(R.string.bid_now);
            }
            else if (spJobStatus == AppConstants.STATUS_ACCEPTED) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_blue);
                tvStatus.setText(R.string.str_status_accepted);
            }
            else if (spJobStatus == AppConstants.STATUS_DISPUTE) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_blue);
                tvStatus.setText(R.string.disputed);
            }
            else if (spJobStatus == AppConstants.STATUS_ON_THE_WAY) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_blue);
                tvStatus.setText(R.string.str_status_on_way);
            }
            else if (spJobStatus == AppConstants.STATUS_ARRIVED) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_blue);
                tvStatus.setText(R.string.str_status_arrived);
            }
            else if (spJobStatus == AppConstants.STATUS_STARTED) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_blue);
                tvStatus.setText(R.string.str_status_on_going);
            }
            else if (spJobStatus == AppConstants.STATUS_COMPLETED) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_blue);
                tvStatus.setText(R.string.str_status_completed);
            }
            else if (spJobStatus == AppConstants.STATUS_REJECTION) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_red);
                tvStatus.setText(R.string.str_status_rejected);
            }
            else if (spJobStatus == AppConstants.STATUS_CANCELLED) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_red);
                tvStatus.setText(R.string.str_status_cancelled);
            }
        }
    }

    public static class ProgressViewHolder extends BaseRecyclerViewHolder {
        private ProgressViewHolder(View view) {
            super(view);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    public class EmptyViewHolder extends BaseRecyclerViewHolder {
        private final TextView textViewEmpty;

        private EmptyViewHolder(View view) {
            super(view);
            textViewEmpty = view.findViewById(R.id.tv_message);
            TextView tvPostJob = view.findViewById(R.id.tv_post_job);
            tvPostJob.setOnClickListener(this);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }


        @Override
        public void onClick(View v) {
            super.onClick(v);
            if (getItemClickListener() != null)
                getItemClickListener().onRecyclerViewChildItemClick(EmptyViewHolder.this, v.getId());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getItemAt(position).getItemType();
    }
}
