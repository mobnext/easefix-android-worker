package com.apps.ondemand.app.ui.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.PopupMenu;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.IdentityTypeModel;
import com.apps.ondemand.common.base.TakePhotoActivity;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.apps.ondemand.common.utils.VizImageView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IdentityDocActivity extends TakePhotoActivity implements View.OnClickListener, VizImageView.OnImageUploadingListener {
    private String profileImage = "";
    private String profileImageBack = "";
    private String fetchProfileDataUrl = "";
    private String fetchProfileDataBackUrl = "";


    private String otherName = "";
    private String fetchDocTypeUrl = "";
    private String selectIdentityDocUrl = "";
    private List<IdentityTypeModel> identityTypeModels = new ArrayList<>();
    IdentityTypeModel selectedType = null;
    EditText otherText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_identity_doc);
        setActionBar(R.string.str_identity_documents);
        setSubTitle(R.string.str_identity_documents_sub);
        initView();
        fetchProfile();
        fetchDocumentTypes();
        otherText = findViewById(R.id.otherText);
    }

    private void fetchDocumentTypes() {
        fetchDocTypeUrl = AppConstants.getServerUrl(AppConstants.FETCH_IDENTITY_TYPES);
        HttpRequestItem requestItem = new HttpRequestItem(fetchDocTypeUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);

        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    private void fetchProfile() {
        fetchProfileDataUrl = AppConstants.getServerUrl(AppConstants.FETCH_PROFILE_DATA);

        HttpRequestItem requestItem = new HttpRequestItem(fetchProfileDataUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    boolean front = true;

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.cv_identity_type) {
            populateIdentityTypeData(identityTypeModels);
        } else if (view.getId() == R.id.iv_upload_image_port) {
            front = true;
            showChoosePhotoDialog();
        } else if (view.getId() == R.id.iv_upload_image_back) {
            front = false;
            showChoosePhotoDialog();
        } else if (view.getId() == R.id.btn_save) {
            validateAndCallService();
        }
    }

    private void validateAndCallService() {
        AppUtils.hideSoftKeyboard(this);

        if (selectedType == null) {
            showSnackBar(R.string.error_select_identity_type);
            return;
        }

        if (TextUtils.isEmpty(profileImage)) {
            showSnackBar(R.string.error_empty_image);
            return;
        }
        if (TextUtils.isEmpty(profileImageBack)) {
            showSnackBar("Upload Back Side Image");
            return;
        }

        if (selectedType.equals("Others") && TextUtils.isEmpty(otherText.getText().toString())) {
            setError(R.id.tv_other_text_error, R.string.error_other);
            return;
        }

        selectIdentityDocUrl = AppConstants.getServerUrl(AppConstants.UPLOAD_IDENTITY_DOCUMENT);

        Map<String, Object> map = new HashMap<>();

        map.put("identityDocId", selectedType.getId());
        map.put("identityFrontUrl", profileImage);
        map.put("identityBackUrl", profileImageBack);
        map.put("otherName", otherText.getText().toString());

        HttpRequestItem requestItem = new HttpRequestItem(selectIdentityDocUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_POST);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);

    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {

            JSONObject responseJson = new JSONObject(response.getResponse());
            if (responseJson.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equalsIgnoreCase(fetchDocTypeUrl)) {
                    JSONArray docTypes = responseJson.getJSONObject("data").getJSONArray("identityDoc");
                    if (docTypes.length() != 0) {
                        identityTypeModels = new Gson().fromJson(docTypes.toString(),
                                new TypeToken<List<IdentityTypeModel>>() {
                                }.getType());
                    }
                } else if (response.getHttpRequestUrl().equalsIgnoreCase(selectIdentityDocUrl)) {
                    //  moveToNextActivity();
                    showSnackBar(responseJson.getString("message"));
                } else if (response.getHttpRequestUrl().equals(fetchProfileDataUrl)) {
                    JSONObject data = responseJson.getJSONObject("data");
                    JSONObject identityDocuments = data.getJSONObject("identityVerificationDocuments");
                    // TODO: 12/03/2019 fetch identity title
                    String id = identityDocuments.getString("identityDocId");
                    String name = identityDocuments.getString("documentName");
                    selectedType = new IdentityTypeModel(id, name);
                    profileImage = identityDocuments.getString("identityFront");
                    profileImageBack = identityDocuments.getString("identityBack");
                    if (data.getString("identityVerificationDocuments").contains("otherName")){
                    otherName = identityDocuments.getString("otherName");
                    }
                    setIdentityDocData();

                }
            } else
                showSnackBar(responseJson.getString("message"));
        } catch (JSONException e) {
            Logger.error(false, e);
        }
    }

    private void populateIdentityTypeData(final List<IdentityTypeModel> identityTypeModels) {
        Context wrapper = new ContextThemeWrapper(this, R.style.PopupStyle);

        PopupMenu popup = new PopupMenu(wrapper, findViewById(R.id.cv_identity_type));
        for (int i = 0; i < identityTypeModels.size(); i++) {
            popup.getMenu().add(i, i, i, identityTypeModels.get(i).getName());
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                selectedType = identityTypeModels.get(item.getItemId());
                findTextViewById(R.id.tv_identity_type).setText(selectedType.getName());
                if (selectedType.getName().equals("Others")) {
                    showTextView(true);
                } else {
                    showTextView(false);
                }
                return true;
            }
        });
        popup.show();
    }

    private void showTextView(boolean type) {
        if (type) {
            findViewById(R.id.otherTextCard).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.otherTextCard).setVisibility(View.GONE);
            otherText.setText("");
        }
    }

    private void initView() {
        findViewById(R.id.cv_identity_type).setOnClickListener(this);
        findViewById(R.id.iv_upload_image_port).setOnClickListener(this);
        findViewById(R.id.iv_upload_image_back).setOnClickListener(this);
        findViewById(R.id.btn_save).setOnClickListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CHOOSE_PHOTO_RESULT_CODE:
                case TAKE_PHOTO_RESULT_CODE: {
                    if (requestCode == CHOOSE_PHOTO_RESULT_CODE)
                        selectedPhotoUri = data.getData();
                    loadImageInView();
                    break;
                }

            }
        }
    }

    private void loadImageInView() {
        if (selectedPhotoUri == null)
            return;

        VizImageView imageView = findViewById(R.id.iv_upload_image_port);
        VizImageView imageViewBack = findViewById(R.id.iv_upload_image_back);
        if (front)
            imageView.loadImageInView(imageView.getId(), AppConstants.UPLOAD_IDENTITY_IMAGE, selectedPhotoUri, this);
        else
            imageViewBack.loadImageInView(imageView.getId(), AppConstants.UPLOAD_IDENTITY_IMAGE, selectedPhotoUri, this);
    }

    @Override
    public void onUploadingResponse(boolean isSuccessful, String imageURL, int imageViewID, String message) {
        if (isSuccessful) {
            if (front)
                profileImage = imageURL;
            else
                profileImageBack = imageURL;
        } else {
            profileImage = "";
        }
    }

    private void setIdentityDocData() {
        findTextViewById(R.id.tv_identity_type).setText(selectedType.getName());
        VizImageView imageView = findViewById(R.id.iv_upload_image_port);
        VizImageView imageViewBack = findViewById(R.id.iv_upload_image_back);
        imageView.setImage(profileImage, false);
        imageViewBack.setImage(profileImageBack, false);
        if (selectedType.getName().equals("Others")) {
            showTextView(true);
            otherText.setText(otherName + "");
        }
    }
}
