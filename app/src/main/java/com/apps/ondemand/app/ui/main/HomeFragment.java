package com.apps.ondemand.app.ui.main;


import static com.apps.ondemand.app.communication.SocketService.KEY_LOCATION_UPDATE;
import static com.apps.ondemand.common.utils.AppConstants.KEY_JOB_ID;
import static com.apps.ondemand.common.utils.AppConstants.MAP_ZOOM_LEVEL;
import static com.google.android.gms.maps.model.BitmapDescriptorFactory.fromBitmap;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.communication.SocketService;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.app.data.models.ScheduleJobModel;
import com.apps.ondemand.app.ui.adapters.ScheduleJobAdapter;
import com.apps.ondemand.app.ui.jobs.JobDetailsActivity;
import com.apps.ondemand.common.base.GoogleMapFragment;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends GoogleMapFragment implements OnRecyclerViewItemClickListener {


    private boolean isFirstLocation = true;

    protected View rootView;
    protected RecyclerView recyclerView;
    private String jobsUrl = "";
    List<BaseItem> scheduleJobs = new ArrayList<>();
    ScheduleJobAdapter scheduleJobAdapter;
    private boolean emitLocation = false;
    private SocketService mService;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        initView(rootView);
        jobFetcher10Sec();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        fetchScheduleJobsData();
        if (!UserManager.isVerifiedByAdmin()) {
            setHomeBanner(R.string.str_not_approved_messages, true,0);
        }

    }

    @Override
    public void onStop() {
        clearData();
        super.onStop();
    }

    public void clearData() {
        if (googleMap != null) {
            googleMap.clear();
        }
        scheduleJobs = new ArrayList<>();
        recyclerView.setAdapter(null);
        scheduleJobAdapter = null;
    }

    private void initView(View rootView) {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        rootView.findViewById(R.id.fab_my_loc).setOnClickListener(v -> onMyLocationClicked());
    }

    private void onMyLocationClicked() {
        if (mLocation == null)
            return;
        double latitude = mLocation.getLatitude();
        double longitude = mLocation.getLongitude();

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), MAP_ZOOM_LEVEL));
    }


    @SuppressLint("MissingPermission")
    @Override
    public void moveToMyLocation(Location location) {
        super.moveToMyLocation(location);
        googleMap.setMyLocationEnabled(false);
        if (location == null)
            return;
        mLocation = location;
        if (isFirstLocation) {

            googleMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).anchor(0.5f, 0.5f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pickup)));
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(location.getLatitude(),
                    location.getLongitude())).zoom(MAP_ZOOM_LEVEL).build();

            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            isFirstLocation = false;
        }
        if (emitLocation) {
            Map<String, Double> map = new HashMap<>();
            map.put("latitude", location.getLatitude());
            map.put("longitude", location.getLongitude());
            mService.emitResults(KEY_LOCATION_UPDATE, map, true);
            emitLocation = false;
        }

    }

    public void emitLocation(SocketService mService) {
        this.mService = mService;
        emitLocation = true;
    }


    @Override
    public void setMapProperties() {
        super.setMapProperties();
        googleMap.setOnMarkerClickListener(marker -> {
            String jobId = (String) marker.getTag();
            // to check on marker click
            if (!TextUtils.isEmpty(jobId)) {
                try {
                    for(int i=0; i<scheduleJobs.size(); i++) {
                        ScheduleJobModel model = (ScheduleJobModel) scheduleJobs.get(i);
                        if(model.getId().equals(jobId)) {
                            openJobDetails(jobId, marker.getPosition().latitude, marker.getPosition().longitude, model.getIsFixed());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return true;
        });
        googleMap.setOnMapLoadedCallback(() -> {

        });
    }
    Handler handler = new Handler();
    Runnable runnable;
    int delay = 60000;

    private void jobFetcher10Sec() {
        handler.postDelayed(runnable = () -> {
            //do something
            fetchScheduleJobsData();
            handler.postDelayed(runnable, delay);
        }, delay);
    }

    public void fetchScheduleJobsData() {

        jobsUrl = AppConstants.getServerUrl(String.format(AppConstants.FETCH_AVAILABLE_JOBS,1));
        HttpRequestItem requestItem = new HttpRequestItem(jobsUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);

        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(null, this);
        appNetworkTask.execute(requestItem);
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            JSONObject responseJson = new JSONObject(response.getResponse());
            if (responseJson.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equalsIgnoreCase(jobsUrl)) {
                    JSONArray jobs = responseJson.getJSONObject("data").getJSONArray("jobList");
                    if (jobs.length() != 0) {
                        scheduleJobs = new Gson().fromJson(jobs.toString(), new TypeToken<List<ScheduleJobModel>>() {}.getType());
//                        ScheduleJobModel jm = (ScheduleJobModel) scheduleJobs.get(0);
//                        jm.setJobFixed();
//                        scheduleJobs.add(1, jm);
//                        jm = (ScheduleJobModel) scheduleJobs.get(3);
//                        jm.setJobFixed();
//                        scheduleJobs.add(5, jm);
                        setScheduleAdapter(scheduleJobs);
                        if (!UserManager.isVerifiedByAdmin()) {
                            setHomeBanner(R.string.str_not_approved_messages, true,0);
                            return;
                        }
                        setHomeBanner(R.string.str_no_schedule_jobs, false,1);
                    } else {
                        if (!UserManager.isVerifiedByAdmin()) {
                            setHomeBanner(R.string.str_not_approved_messages, true,0);
                            return;
                        }
                        setHomeBanner(R.string.str_no_schedule_jobs, true,1);
                    }
                    setMyLocationMarker();

                }
            } else {
                showSnackBar(responseJson.getString("message"));

            }
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    }


    private void setScheduleAdapter(List<BaseItem> scheduleJobItems) {
        scheduleJobAdapter = new ScheduleJobAdapter(getContext(), scheduleJobItems, this);
        recyclerView.setAdapter(scheduleJobAdapter);
        addCustomMarkers(scheduleJobItems);
    }


    private void addCustomMarkers(List<BaseItem> jobList) {
        if (getActivity() == null || getView() == null || googleMap == null)
            return;
        try {
            googleMap.clear();

            for (int i = 0; i < jobList.size(); i++) {
                ScheduleJobModel scheduleJobModel = (ScheduleJobModel) jobList.get(i);
                final LatLng position = new LatLng(scheduleJobModel.getLatitude(), scheduleJobModel.getLongitude());

                final JobMapMarker jobMapMarker = new JobMapMarker(getActivity(), scheduleJobModel);
                Marker marker = googleMap.addMarker(new MarkerOptions().position(position).icon(fromBitmap(createDrawableFromView(jobMapMarker))));
                assert marker != null;
                marker.setTag(scheduleJobModel.getId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setMyLocationMarker() {
        if (getActivity() == null || getView() == null || googleMap == null || mLocation == null)
            return;

        googleMap.addMarker(new MarkerOptions().position(new LatLng(mLocation.getLatitude(), mLocation.getLongitude())).anchor(0.5f, 0.5f)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pickup)));
    }

    // Convert a view to bitmap
    public Bitmap createDrawableFromView(View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        (requireActivity()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));
        //   view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;

        Bitmap bitmap;
        bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_4444);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;

    }

    @Override
    public void onRecyclerViewItemClick(BaseRecyclerViewHolder holder) {
        int position = holder.getAdapterPosition();
        ScheduleJobModel scheduleJobModel = (ScheduleJobModel) scheduleJobAdapter.getItemAt(position);
        openJobDetails(scheduleJobModel.getId(), scheduleJobModel.getLatitude(), scheduleJobModel.getLongitude(), scheduleJobModel.getIsFixed());
    }

    @Override
    public void onRecyclerViewChildItemClick(BaseRecyclerViewHolder holder, int resourceId) {

    }

    public void openJobDetails(String jobId, Double jobLat, Double jobLng, boolean fixed) {
        handler.removeCallbacks(runnable);
        Intent intent = new Intent(getActivity(), JobDetailsActivity.class);
        intent.putExtra(KEY_JOB_ID, jobId);
        intent.putExtra("isFixed", fixed);
        intent.putExtra("lat", jobLat);
        intent.putExtra("lng", jobLng);
        startActivity(intent);
    }

    public void setHomeBanner(@StringRes int id, boolean isVisible,int unapproved) {
        findTextViewById(R.id.tv_home_message).setVisibility(isVisible ? View.VISIBLE : View.GONE);
        findTextViewById(R.id.tv_home_message).setText(id);
        if (unapproved==0){
            findTextViewById(R.id.tv_home_message).setBackgroundColor(requireActivity().getColor(R.color.red));
            findTextViewById(R.id.tv_home_message).setTextColor(requireActivity().getColor(R.color.white));
        }else {
            findTextViewById(R.id.tv_home_message).setBackgroundColor(requireActivity().getColor(R.color.white));
            findTextViewById(R.id.tv_home_message).setTextColor(requireActivity().getColor(R.color.text_primary_light));
        }
    }
    public void setApproved(){
        setHomeBanner(R.string.str_no_schedule_jobs, false,1);
    }

    @Override
    public void rejectLocationServicesRequest() {
        super.rejectLocationServicesRequest();
    }


}
