package com.apps.ondemand.app.ui.authentication;

import android.content.Intent;
import androidx.core.view.ViewCompat;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.ServiceModel;
import com.apps.ondemand.app.data.models.ServiceStateEnum;
import com.apps.ondemand.app.ui.adapters.ServiceAdapter;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.apps.ondemand.common.utils.AppConstants.GOOGLE_BASE_URL;

public class SelectServiceActivity extends BaseActivity implements View.OnClickListener, OnRecyclerViewItemClickListener {

    private RecyclerView recyclerView;
    private ServiceAdapter serviceAdapter;

    List<ServiceModel> serviceModels = new ArrayList<>();
    private String fetchServicesUrl = "";
    private String selectServicesUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_service);
        setActionBar(R.string.str_select_services);
        setSubTitle(R.string.str_select_service_you_offer);
        initializeRecycleView();
        fetchAllServices();
    }

    private void fetchAllServices() {
        fetchServicesUrl = AppConstants.getServerUrl(AppConstants.FETCH_SERVICES);
        HttpRequestItem requestItem = new HttpRequestItem(fetchServicesUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);

        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false),
                this);
        appNetworkTask.execute(requestItem);
    }

    private void initializeRecycleView() {
        findViewById(R.id.btn_continue).setOnClickListener(this);
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(false);
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_continue:
                validateServicesCallApi();
                break;
        }
    }

    private void validateServicesCallApi() {
        JsonArray jsonArray = new JsonArray();

        for (BaseItem baseItem : serviceAdapter.getAdapterItems()) {
            ServiceModel serviceModel = (ServiceModel) baseItem;
            if (serviceModel.getState() != ServiceStateEnum.SAVED && serviceModel.getState() != ServiceStateEnum.UNSELECTED) {
                showSnackBar(format(R.string.error_service_selection, serviceModel.getServiceName()));
                return;
            }
            if (serviceModel.getState() == ServiceStateEnum.SAVED) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("serviceId",serviceModel.getId());
                jsonObject.addProperty("serviceHourlyRate",serviceModel.getServiceHourlyRate());
                jsonArray.add(jsonObject);
            }
        }
        if (jsonArray.size() == 0) {
            showSnackBar(R.string.error_no_service_selection);
            return;
        }

        selectServicesUrl = AppConstants.getServerUrl(AppConstants.SELECT_SERVICES);

        Map<String, Object> map = new HashMap<>();
        map.put("selectedServices", jsonArray);

        HttpRequestItem requestItem = new HttpRequestItem(selectServicesUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_POST);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);

    }


    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            if (response.getHttpRequestUrl().contains(GOOGLE_BASE_URL))
                return;
            JSONObject responseJson = new JSONObject(response.getResponse());
            if (responseJson.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equalsIgnoreCase(fetchServicesUrl)) {
                    JSONArray services = responseJson.getJSONObject("data").getJSONArray("servicesList");
                    if (services.length() != 0) {
                        serviceModels = new Gson().fromJson(services.toString(),
                                new TypeToken<List<ServiceModel>>() {
                                }.getType());
                        populateServiceData(serviceModels);
                    }

                } else if (response.getHttpRequestUrl().equalsIgnoreCase(selectServicesUrl)) {
                    moveToNextActivity();
                }
            } else
                showSnackBar(responseJson.getString("message"));
        } catch (JSONException e) {
            Logger.error(false, e);
        }
    }
    private void moveToNextActivity() {
        Intent intent = new Intent(this, AddServiceCertificateActivity.class);
        startActivity(intent);
    }
    private void populateServiceData(List<ServiceModel> serviceModels) {
        serviceAdapter = new ServiceAdapter(this, new ArrayList<BaseItem>(serviceModels), this);
        recyclerView.setAdapter(serviceAdapter);
    }


    @Override
    public void onRecyclerViewItemClick(BaseRecyclerViewHolder holder) {

    }

    @Override
    public void onRecyclerViewChildItemClick(BaseRecyclerViewHolder holder, int resourceId) {
        int position = holder.getAdapterPosition();
        switch (resourceId) {


        }
    }

}

