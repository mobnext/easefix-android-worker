package com.apps.ondemand.app.data.models;

import com.apps.ondemand.common.business.BaseItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ScheduleJobModel implements BaseItem {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("spJobStatus")
    @Expose
    private int spJobStatus;
    @SerializedName("isJobFixed")
    @Expose
    private boolean isJobFixed = false;
    @SerializedName("expectedJobStartTime")
    @Expose
    private int expectedJobStartTime;
    @SerializedName("jobDurationInMinutes")
    @Expose
    private float jobDurationInMinutes;
    @SerializedName("distanceAway")
    @Expose
    private double distanceAway;
    @SerializedName("distanceUnit")
    @Expose
    private String distanceUnit;
    @SerializedName("latitude")
    @Expose
    private double latitude;
    @SerializedName("longitude")
    @Expose
    private double longitude;
    @SerializedName("serviceName")
    @Expose
    private String serviceName;
    @SerializedName("serviceImage")
    @Expose
    private String serviceImage;
    @SerializedName("avgRating")
    @Expose
    private float avgRating;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("profileImage")
    @Expose
    private String profileImage;
    @SerializedName("primaryAddress")
    @Expose
    private String primaryAddress;
    @SerializedName("JobIdIdentifier")
    @Expose
    private String jobIdIdentifier;
    @SerializedName("isbidAdded")
    @Expose
    private Boolean isbidAdded;
    @SerializedName("isBidExpired")
    @Expose
    private Boolean isBidExpired;
   @SerializedName("isBidAccepted")
    @Expose
    private Boolean isBidAccepted;

    @SerializedName("isJobDurationKnown")
    @Expose
    private Boolean isJobDurationKnown;
     @SerializedName("bidAmount")
    @Expose
    private float bidAmount;

     public Boolean getIsFixed(){
         return isJobFixed;
     }

     public void setJobFixed(){
         isJobFixed = true;
     }

    public Boolean getBidExpired() {
        return isBidExpired;
    }

    public void setBidExpired(Boolean bidExpired) {
        isBidExpired = bidExpired;
    }

    public Boolean getBidAccepted() {
        return isBidAccepted;
    }

    public void setBidAccepted(Boolean bidAccepted) {
        isBidAccepted = bidAccepted;
    }

    public Boolean getJobDurationKnown() {
        return isJobDurationKnown;
    }

    public void setJobDurationKnown(Boolean jobDurationKnown) {
        isJobDurationKnown = jobDurationKnown;
    }

    public float getBidAmount() {
        return bidAmount;
    }

    public void setBidAmount(float bidAmount) {
        this.bidAmount = bidAmount;
    }

    public Boolean getIsbidAdded() {
        return isbidAdded;
    }

    public void setIsbidAdded(Boolean isbidAdded) {
        this.isbidAdded = isbidAdded;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSpJobStatus() {
        return spJobStatus;
    }

    public void setSpJobStatus(int spJobStatus) {
        this.spJobStatus = spJobStatus;
    }

    public int getExpectedJobStartTime() {
        return expectedJobStartTime;
    }

    public void setExpectedJobStartTime(int expectedJobStartTime) {
        this.expectedJobStartTime = expectedJobStartTime;
    }

    public float getJobDurationInMinutes() {
        return jobDurationInMinutes;
    }

    public void setJobDurationInMinutes(float jobDurationInMinutes) {
        this.jobDurationInMinutes = jobDurationInMinutes;
    }

    public double getDistanceAway() {
        return distanceAway;
    }

    public void setDistanceAway(double distanceAway) {
        this.distanceAway = distanceAway;
    }

    public String getDistanceUnit() {
        return distanceUnit;
    }

    public void setDistanceUnit(String distanceUnit) {
        this.distanceUnit = distanceUnit;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceImage() {
        return serviceImage;
    }

    public void setServiceImage(String serviceImage) {
        this.serviceImage = serviceImage;
    }

    public float getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(float avgRating) {
        this.avgRating = avgRating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getPrimaryAddress() {
        return primaryAddress;
    }

    public void setPrimaryAddress(String primaryAddress) {
        this.primaryAddress = primaryAddress;
    }

    public String getJobIdIdentifier() {
        return jobIdIdentifier;
    }

    public void setJobIdIdentifier(String jobIdIdentifier) {
        this.jobIdIdentifier = jobIdIdentifier;
    }

    @Override
    public int getItemType() {
        return ITEM_JOB;
    }
}
