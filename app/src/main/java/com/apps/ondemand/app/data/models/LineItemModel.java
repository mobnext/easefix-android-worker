package com.apps.ondemand.app.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.apps.ondemand.common.business.BaseItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LineItemModel implements Parcelable, BaseItem {

    @SerializedName("itemName")
    @Expose
    private String itemName;
    @SerializedName("itemQuantity")
    @Expose
    private int itemQuantity;
    @SerializedName("itemPrice")
    @Expose
    private float itemPrice;
    @SerializedName("_id")
    @Expose
    private String id;

    boolean isAdded = true;
    boolean isEditMode = false;

    public LineItemModel() {

    }

    public LineItemModel(boolean isAdded, boolean isEditMode) {
        this.isAdded = isAdded;
        this.isEditMode = isEditMode;
    }


    protected LineItemModel(Parcel in) {
        itemName = in.readString();
        itemQuantity = in.readInt();
        itemPrice = in.readFloat();
        id = in.readString();
        isAdded = in.readByte() != 0;
        isEditMode = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(itemName);
        dest.writeInt(itemQuantity);
        dest.writeFloat(itemPrice);
        dest.writeString(id);
        dest.writeByte((byte) (isAdded ? 1 : 0));
        dest.writeByte((byte) (isEditMode ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LineItemModel> CREATOR = new Creator<LineItemModel>() {
        @Override
        public LineItemModel createFromParcel(Parcel in) {
            return new LineItemModel(in);
        }

        @Override
        public LineItemModel[] newArray(int size) {
            return new LineItemModel[size];
        }
    };

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(int itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public float getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(float itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isAdded() {
        return isAdded;
    }

    public void setAdded(boolean added) {
        isAdded = added;
    }

    public boolean isEditMode() {
        return isEditMode;
    }

    public void setEditMode(boolean editMode) {
        isEditMode = editMode;
    }

    @Override
    public int getItemType() {
        return ITEM_LINE_ITEM;
    }
}
