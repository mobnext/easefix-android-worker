package com.apps.ondemand.app.ui.adapters;

import android.content.Context;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.JobStatusModel;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.utils.AppUtils;

import java.util.List;

import static com.apps.ondemand.common.utils.AppConstants.*;


public class JobStatusAdapter extends BaseRecyclerViewAdapter {

    Context context;

    public JobStatusAdapter(Context context, List<BaseItem> items, OnRecyclerViewItemClickListener itemClickListener) {
        super(items, itemClickListener);
        this.context = context;
    }

    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        BaseRecyclerViewHolder holder;
        if (viewType == BaseItem.ITEM_JOB_STATUS) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_status, parent, false);
            holder = new JobStatusHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
            holder = new EmptyViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder holder, int position) {
        if (holder instanceof JobStatusHolder) {
            JobStatusHolder jobStatusHolder = (JobStatusHolder) holder;
            JobStatusModel jobStatusModel = (JobStatusModel) getItemAt(position);
            jobStatusHolder.setStatus(jobStatusModel, position);

            if (position == getItemCount() - 1) {
                jobStatusHolder.ivDots.setVisibility(View.GONE);
            } else {
                jobStatusHolder.ivDots.setVisibility(View.VISIBLE);
            }
        }

    }

    private class JobStatusHolder extends BaseRecyclerViewHolder {
        private ImageView ivStart;
        private ImageView ivDots;
        private TextView tvLocationLabel;
        private TextView tvStatus;
        private TextView tvDateTime;
        private ImageView ivDone;
        private TextView tvStatusDesc;

        public JobStatusHolder(View view) {
            super(view);
            ivStart = (ImageView) view.findViewById(R.id.iv_start);
            ivDots = (ImageView) view.findViewById(R.id.iv_dots);
            tvLocationLabel = (TextView) view.findViewById(R.id.tv_location_label);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            tvDateTime = (TextView) view.findViewById(R.id.tv_date_time);
            ivDone = (ImageView) view.findViewById(R.id.iv_done);
            tvStatusDesc = (TextView) view.findViewById(R.id.tv_status_desc);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return JobStatusHolder.this;
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            if (getItemClickListener() != null)
                getItemClickListener().onRecyclerViewItemClick(JobStatusHolder.this);
        }

        public void setStatus(JobStatusModel jobStatusModel, int position) {
            tvLocationLabel.setText(AppUtils.getLabel(position));
            if (jobStatusModel.getStatus() == STATUS_ON_THE_WAY) {
                setStatusData(true, R.string.str_status_on_way,
                        R.drawable.bg_circle_purple,
                        R.drawable.bg_details_status_purple);
            } else if (jobStatusModel.getStatus() == STATUS_ARRIVED) {
                setStatusData(false, R.string.str_status_arrived,
                        R.drawable.bg_circle_blue,
                        R.drawable.bg_details_status_blue);
            } else if (jobStatusModel.getStatus() == STATUS_STARTED) {

                setStatusData(false, R.string.str_status_on_going,
                        R.drawable.bg_circle_blue,
                        R.drawable.bg_details_status_blue);
            } else if (jobStatusModel.getStatus() == STATUS_COMPLETED) {
                setStatusData(false, R.string.str_status_completed,
                        R.drawable.bg_circle_blue,
                        R.drawable.bg_details_status_blue);
            } else if (jobStatusModel.getStatus() == STATUS_CANCELLED) {
                setStatusData(false, R.string.str_status_cancelled,
                        R.drawable.bg_circle_red,
                        R.drawable.bg_details_status_red);
            } else if (jobStatusModel.getStatus() == STATUS_REJECTION) {

                setStatusData(false, R.string.str_status_rejected,
                        R.drawable.bg_circle_red,
                        R.drawable.bg_details_status_red);
            }
        }

        public void setStatusData(boolean showIcon, @StringRes int status,
                                  @DrawableRes int labelDrawable, @DrawableRes int statusDrawable) {
            if (showIcon) {
                ivStart.setVisibility(View.VISIBLE);
                tvLocationLabel.setVisibility(View.GONE);
            } else {
                ivStart.setVisibility(View.GONE);
                tvLocationLabel.setVisibility(View.VISIBLE);
            }


            tvStatus.setText(status);
            tvLocationLabel.setBackgroundResource(labelDrawable);
            tvStatus.setBackgroundResource(statusDrawable);
        }
    }

    public class EmptyViewHolder extends BaseRecyclerViewHolder {
        private TextView textViewEmpty;

        private EmptyViewHolder(View view) {
            super(view);
            textViewEmpty = view.findViewById(R.id.tv_message);
            textViewEmpty.setText("");
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getItemAt(position).getItemType();
    }
}
