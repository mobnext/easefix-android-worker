package com.apps.ondemand.app.ui.jobs;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.app.data.models.Empty;
import com.apps.ondemand.app.data.models.PastJobModel;
import com.apps.ondemand.app.data.models.Progress;
import com.apps.ondemand.app.ui.adapters.JobAdapter;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewFragment;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.apps.ondemand.common.utils.AppConstants.KEY_EXTRA_WEEK_NUMBER;
import static com.apps.ondemand.common.utils.AppConstants.KEY_EXTRA_YEAR;


/**
 * A simple {@link Fragment} subclass.
 */
public class JobFragment extends BaseRecyclerViewFragment implements OnRecyclerViewItemClickListener {

    protected View rootView;
    private int pageNo = 0;
    private boolean isLoading = false;
    private boolean doLoadMore = false;
    private JobAdapter adapter;
    private static final int LIMIT = 20;
    private String activePackageUrl = "";
    int listType = 1;
    private int year;
    private int weekNumber;

    public static JobFragment newInstance(int weekNumber, int year) {
        Bundle args = new Bundle();
        args.putInt(KEY_EXTRA_WEEK_NUMBER, weekNumber);
        args.putInt(KEY_EXTRA_YEAR, year);
        JobFragment fragment = new JobFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public JobFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            weekNumber = getArguments().getInt(KEY_EXTRA_WEEK_NUMBER);
            year = getArguments().getInt(KEY_EXTRA_YEAR);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_job, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initializeRecyclerView();
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter = null;
        setAdapter(null);
        pageNo = 0;
        getListData(true);
    }

    private void initializeRecyclerView() {
        RecyclerView recyclerView = getRecyclerView();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(false);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isLoading || !doLoadMore)       //Check if request is already called OR do need to get next page patients
                    return;
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int totalItemCount = linearLayoutManager.getItemCount();
                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (totalItemCount <= (lastVisibleItem + 1)) {
                    getListData(true);
                    doLoadMore = false;
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);

        try {
            isLoading = false;
            removeProgressItem();
            JSONObject responseJson = new JSONObject(response.getResponse());
            if (responseJson.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equalsIgnoreCase(activePackageUrl)) {
                    JSONArray offers = responseJson.getJSONObject("data").getJSONArray("spJobsFound");
                    if (offers.length() != 0) {
                        List<BaseItem> offersItems = new Gson().fromJson(offers.toString(),
                                new TypeToken<List<PastJobModel>>() {
                                }.getType());
                        setPackageListingResponse(offersItems);
                    } else {
                        setEmptyLayout();
                        doLoadMore = false;
                    }
                }

            } else {
                showSnackBar(responseJson.getString("message"));
                setEmptyLayout();
                doLoadMore = false;
            }
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
    }

    @Override
    public void onNetworkError(HttpResponseItem response) {
        super.onNetworkError(response);
        removeProgressItem();
        setEmptyLayout();
        isLoading = false;
    }

    private void setEmptyLayout() {
        if (adapter == null || adapter.getItemCount() == 0) {
            List<BaseItem> itemList = new ArrayList<>();
            itemList.add(new Empty());
            populateList(itemList);
        }
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        if (UserManager.isUserLoggedIn()) {
            adapter = null;
            setAdapter(null);
            pageNo = 0;
            getListData(false);
        } else {
            stopRefresh();
        }

    }

    public void getListData(boolean addProgress) {
        isLoading = true;
        if (addProgress) {
            addProgressItem();
        }
        activePackageUrl = AppConstants.getServerUrl(AppConstants.GET_EARNING_LISTING);

        HttpRequestItem requestItem = new HttpRequestItem(activePackageUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);

        Map<String, Object> params = new HashMap<>();
        params.put("weekNumber", weekNumber);
        params.put("year", year);
        params.put("offset", pageNo * LIMIT);
        params.put("limit", LIMIT);

        requestItem.setParams(params);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(null,
                this);
        appNetworkTask.execute(requestItem);
    }

    private void addProgressItem() {
        List<BaseItem> items = new ArrayList<>();
        items.add(new Progress());
        populateList(items);
    }

    private void removeProgressItem() {
        if (adapter != null && adapter.getAdapterCount() > 0 &&
                adapter.getItemAt(adapter.getAdapterCount() - 1) instanceof Progress) {
            adapter.remove(adapter.getAdapterCount() - 1);
            adapter.notifyItemRemoved(adapter.getAdapterCount() - 1);
        }
        if (getSwipeLayout() != null) {
            getSwipeLayout().setRefreshing(false);
        }
    }

    private void setPackageListingResponse(List<BaseItem> data) throws JSONException {
        List<BaseItem> itemList = new ArrayList<>();
        if (data.size() == 0)
            doLoadMore = false;
        else {
            pageNo++;
            doLoadMore = data.size() == LIMIT;
            itemList.addAll(data);
        }

        populateList(itemList);

    }

    private void populateList(List<BaseItem> itemList) {
        if (adapter == null) {
            adapter = new JobAdapter((BaseActivity) getActivity(), itemList, this);
            setAdapter(adapter);
        } else
            adapter.addAll(itemList);

    }


    private void initView(View rootView) {

    }


    @Override
    public void onRecyclerViewItemClick(BaseRecyclerViewHolder holder) {
        super.onRecyclerViewItemClick(holder);
        PastJobModel pastJobModel = (PastJobModel) adapter.getItemAt(holder.getAdapterPosition());
        Intent intent = new Intent(getActivity(), JobDetailsActivity.class);
        intent.putExtra(AppConstants.KEY_JOB_ID, pastJobModel.getId());
        startActivity(intent);
    }

    @Override
    public void onRecyclerViewChildItemClick(BaseRecyclerViewHolder holder, int resourceId) {
        super.onRecyclerViewChildItemClick(holder, resourceId);
        switch (resourceId) {
            case R.id.tv_post_job:
                getActivity().finish();
                break;
        }
    }

    @Override
    public void onEventReceived(Intent intent) {
        super.onEventReceived(intent);
        getListData(false);
    }
}
