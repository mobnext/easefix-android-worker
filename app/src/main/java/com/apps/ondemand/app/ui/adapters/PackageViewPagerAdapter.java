package com.apps.ondemand.app.ui.adapters;

import static com.apps.ondemand.app.ui.main.AllJobsActivity.TOTAL_TABS;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.apps.ondemand.app.ui.jobs.MyJobsFragments;
import com.apps.ondemand.common.utils.AppConstants;

import java.util.List;

public class PackageViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<String> titles;

    public PackageViewPagerAdapter(FragmentManager fm, List<String> titles) {
        super(fm);
        this.titles = titles;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return MyJobsFragments.newInstance(1);
            case 1:
                return MyJobsFragments.newInstance(2);
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return titles.get(AppConstants.ACTIVE_INDEX);
            case 1:
                return titles.get(AppConstants.PAST_INDEX);
        }
        return super.getPageTitle(AppConstants.ACTIVE_INDEX);
    }

    @Override
    public int getCount() {
        return TOTAL_TABS;
    }
}