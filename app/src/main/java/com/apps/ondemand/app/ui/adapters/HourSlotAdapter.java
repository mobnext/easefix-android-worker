package com.apps.ondemand.app.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.ondemand.R;
import com.apps.ondemand.common.base.BaseActivity;

import java.util.ArrayList;

public class HourSlotAdapter extends RecyclerView.Adapter {


    private BaseActivity context;
    int maxTimeSlots;
    private OnHourSelect onHourSelect;
    ArrayList<Integer> arrayList;
    int selectedTimeSlot;

    public interface OnHourSelect {
        void onHourSelected(int selectedHourSlot);
    }

    public HourSlotAdapter(BaseActivity context, int maxTimeSlots, int selectedTimeSlot, OnHourSelect onHourSelect) {
        this.context = context;
        this.onHourSelect = onHourSelect;
        this.maxTimeSlots = maxTimeSlots;
        this.selectedTimeSlot = selectedTimeSlot;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_time_slot, parent, false);
        return new HourSlotHolder(view);
    }

    public void setSelectedTimeSlot(int selectedTimeSlot) {
        this.selectedTimeSlot = selectedTimeSlot;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HourSlotHolder) {
            HourSlotHolder hourSlotHolder = (HourSlotHolder) holder;
            if ((position + 1) == selectedTimeSlot) {
                hourSlotHolder.rlTimeSlot.setSelected(true);
                hourSlotHolder.tvTimeSlot.setSelected(true);
            } else {
                hourSlotHolder.rlTimeSlot.setSelected(false);
                hourSlotHolder.tvTimeSlot.setSelected(false);
            }
            hourSlotHolder.tvTimeSlot.setText(String.valueOf(position + 1));
        }
    }


    @Override
    public int getItemCount() {
        return maxTimeSlots;
    }


    private class HourSlotHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RelativeLayout rlTimeSlot;
        private TextView tvTimeSlot;

        public HourSlotHolder(View view) {
            super(view);
            rlTimeSlot = (RelativeLayout) view.findViewById(R.id.rl_time_slot);
            tvTimeSlot = (TextView) view.findViewById(R.id.tv_time_slot);
            rlTimeSlot.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onHourSelect != null) {
                onHourSelect.onHourSelected(getAdapterPosition() + 1);
            }

        }
    }

}
