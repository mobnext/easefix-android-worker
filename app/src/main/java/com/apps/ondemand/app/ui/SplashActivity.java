package com.apps.ondemand.app.ui;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.LanguageManager;
import com.apps.ondemand.app.data.preferences.PreferenceUtils;
import com.apps.ondemand.app.data.preferences.SharedPreferenceManager;
import com.apps.ondemand.app.ui.authentication.SelectLanguageActivity;
import com.apps.ondemand.app.ui.main.LandingActivity;
import com.apps.ondemand.app.ui.main.MainActivity;
import com.uxcam.UXCam;
import com.uxcam.datamodel.UXConfig;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        UXConfig config = new UXConfig.Builder("ouqvcc0jw8ng81h")
                .enableAutomaticScreenNameTagging(true)
                .enableImprovedScreenCapture(true)
                .build();
        UXCam.startWithConfiguration(config);

        new Handler().postDelayed(() -> {
            if (!SharedPreferenceManager.getInstance().
                    read(PreferenceUtils.COOKIE, "").equalsIgnoreCase("") &&
                    SharedPreferenceManager.getInstance().read(PreferenceUtils.IS_LOGGED_IN, false))
                moveToNextActivity(MainActivity.class);
            else
                moveToNextActivity(LandingActivity.class);
            finish();
        }, 1000);
    }

    public void moveToNextActivity(Class<?> clas) {
        boolean isLanguageSelected = LanguageManager.isLanguageSelected();
        if (isLanguageSelected) {
         //   startActivity(new Intent(this, SignUpCompletedActivity.class));

          startActivity(new Intent(this, clas));
        } else {
            LanguageManager.setLanguageSelected();
            startActivity(new Intent(this, SelectLanguageActivity.class));
        }
    }
}
