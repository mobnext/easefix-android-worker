package com.apps.ondemand.app.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.apps.ondemand.common.business.BaseItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeekModel implements BaseItem , Parcelable {

    @SerializedName("weekTitle")
    @Expose
    private String title;
    @SerializedName("weekNumber")
    @Expose
    private int weekNumber;
    @SerializedName("weekYear")
    @Expose
    private int weekYear;

    protected WeekModel(Parcel in) {
        title = in.readString();
        weekNumber = in.readInt();
        weekYear = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeInt(weekNumber);
        dest.writeInt(weekYear);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WeekModel> CREATOR = new Creator<WeekModel>() {
        @Override
        public WeekModel createFromParcel(Parcel in) {
            return new WeekModel(in);
        }

        @Override
        public WeekModel[] newArray(int size) {
            return new WeekModel[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getWeekNumber() {
        return weekNumber;
    }

    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public int getWeekYear() {
        return weekYear;
    }

    public void setWeekYear(int weekYear) {
        this.weekYear = weekYear;
    }

    @Override
    public int getItemType() {
        return BaseItem.ITEM_WEEK;
    }

}