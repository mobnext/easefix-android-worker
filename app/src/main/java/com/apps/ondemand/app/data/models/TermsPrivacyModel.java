package com.apps.ondemand.app.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TermsPrivacyModel implements Parcelable {

    @SerializedName("isPrivacyPolicyUpdated")
    @Expose
    private boolean isPrivacyPolicyUpdated;
    @SerializedName("isTermAndConditionUpdated")
    @Expose
    private boolean isTermAndConditionUpdated;
    @SerializedName("termConditionDescription")
    @Expose
    private String termConditionDescription;
    @SerializedName("privacyPolicyDescription")
    @Expose
    private String privacyPolicyDescription;

    protected TermsPrivacyModel(Parcel in) {
        isPrivacyPolicyUpdated = in.readByte() != 0;
        isTermAndConditionUpdated = in.readByte() != 0;
        termConditionDescription = in.readString();
        privacyPolicyDescription = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (isPrivacyPolicyUpdated ? 1 : 0));
        dest.writeByte((byte) (isTermAndConditionUpdated ? 1 : 0));
        dest.writeString(termConditionDescription);
        dest.writeString(privacyPolicyDescription);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TermsPrivacyModel> CREATOR = new Creator<TermsPrivacyModel>() {
        @Override
        public TermsPrivacyModel createFromParcel(Parcel in) {
            return new TermsPrivacyModel(in);
        }

        @Override
        public TermsPrivacyModel[] newArray(int size) {
            return new TermsPrivacyModel[size];
        }
    };

    public boolean getIsPrivacyPolicyUpdated() {
        return isPrivacyPolicyUpdated;
    }

    public void setIsPrivacyPolicyUpdated(boolean isPrivacyPolicyUpdated) {
        this.isPrivacyPolicyUpdated = isPrivacyPolicyUpdated;
    }

    public boolean getIsTermAndConditionUpdated() {
        return isTermAndConditionUpdated;
    }

    public void setIsTermAndConditionUpdated(boolean isTermAndConditionUpdated) {
        this.isTermAndConditionUpdated = isTermAndConditionUpdated;
    }

    public String getTermConditionDescription() {
        return termConditionDescription;
    }

    public void setTermConditionDescription(String termConditionDescription) {
        this.termConditionDescription = termConditionDescription;
    }

    public String getPrivacyPolicyDescription() {
        return privacyPolicyDescription;
    }

    public void setPrivacyPolicyDescription(String privacyPolicyDescription) {
        this.privacyPolicyDescription = privacyPolicyDescription;
    }
}
