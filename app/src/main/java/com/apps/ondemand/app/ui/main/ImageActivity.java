package com.apps.ondemand.app.ui.main;


import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.ui.viewPager.ViewPagerAdapter;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.business.BaseItem;


import java.util.ArrayList;
import java.util.List;

public class ImageActivity extends BaseActivity {
    int type = 0;
    int itemNum = 0;
    List<BaseItem> itemList = new ArrayList<>();

    ViewPagerAdapter mViewPagerAdapter;
    ViewPager mViewPager;
    ImageView cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        type = getIntent().getIntExtra("type", 0);
        itemNum = getIntent().getIntExtra("holderPosition", 0);
        itemList = (List<BaseItem>) getIntent().getExtras().getSerializable("ImageVideo");
        initRecyclerView();
        if (type == 1) {
            setAdapterImage(itemList, 1);
        } else if (type == 2) {
            setAdapterImage(itemList, 2);
        }
    }

    private void initRecyclerView() {

        mViewPager = (ViewPager) findViewById(R.id.viewPagerMain);
        cancelButton = (ImageView) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setAdapterImage(List<BaseItem> itemList, int type) {
        mViewPagerAdapter = new ViewPagerAdapter(ImageActivity.this, itemList, type);
        mViewPager.setAdapter(mViewPagerAdapter);
        mViewPager.setCurrentItem(itemNum);
    }


}