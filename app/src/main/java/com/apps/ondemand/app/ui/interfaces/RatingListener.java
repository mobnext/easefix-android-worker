package com.apps.ondemand.app.ui.interfaces;

public interface RatingListener {
     void onSubmitPressed(float rating, String reviewStr);

}
