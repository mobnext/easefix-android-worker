package com.apps.ondemand.app.ui.interfaces;

public interface JobRequestListener {
    void onButtonPressed(String jobId, int status);

}
