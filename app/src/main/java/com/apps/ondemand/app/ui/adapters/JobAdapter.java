package com.apps.ondemand.app.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.LanguageManager;
import com.apps.ondemand.app.data.models.PastJobModel;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.DateUtils;

import java.util.List;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;


public class JobAdapter extends BaseRecyclerViewAdapter {

    Context context;

    public JobAdapter(Context context, List<BaseItem> items, OnRecyclerViewItemClickListener itemClickListener) {
        super(items, itemClickListener);
        this.context = context;
    }

    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        BaseRecyclerViewHolder holder;
        if (viewType == BaseItem.ITEM_JOB) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_job, parent, false);
            holder = new JobHolder(view);
        } else if (viewType == BaseItem.ITEM_EMPTY) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
            holder = new EmptyViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_progress, parent, false);
            holder = new ProgressViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder holder, int position) {
        if (holder instanceof JobHolder) {
            JobHolder jobHolder = (JobHolder) holder;
            PastJobModel pastJobModel = (PastJobModel) getItemAt(position);
            jobHolder.tvJobId.setText(pastJobModel.getJobIdIdentifier());
            jobHolder.tvName.setText(pastJobModel.getName());
            jobHolder.rbRating.setRating(pastJobModel.getAvgRating());
            jobHolder.tvRating.setText(AppUtils.formatRating(pastJobModel.getAvgRating()));
            jobHolder.tvServiceName.setText(pastJobModel.getServiceName());
            jobHolder.tvTime.setText(DateUtils.convertDate(AppUtils.GetTimeForForApp(
                    pastJobModel.getJobEndTime()), DateUtils.DATE_JOB_SUMMARY));
            jobHolder.tvLocation.setText(pastJobModel.getJobAddressModel().getPrimaryAddress());
            jobHolder.tvServiceRate.setText(AppUtils.getAmount(pastJobModel.getSpEarnedAmount()));
            jobHolder.setJobStatus(pastJobModel.getSpJobStatus());

        }
    }

    private class JobHolder extends BaseRecyclerViewHolder {
        private TextView tvJobId;

        private LinearLayout llJob;
        private TextView tvName;
        private TextView tvServiceRate;
        private MaterialRatingBar rbRating;
        private TextView tvRating;
        private TextView tvStatus;
        private TextView tvServiceName;
        private TextView tvTime;
        private TextView tvLocation;


        public JobHolder(View view) {
            super(view);
            tvJobId = (TextView) view.findViewById(R.id.tv_job_id);

            llJob = (LinearLayout) view.findViewById(R.id.ll_job);
            tvName = (TextView) view.findViewById(R.id.tv_profile_name);
            tvServiceRate = (TextView) view.findViewById(R.id.tv_service_rate);
            rbRating = (MaterialRatingBar) view.findViewById(R.id.rb_profile_rating);
            tvRating = (TextView) view.findViewById(R.id.tv_profile_rating);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            tvServiceName = (TextView) view.findViewById(R.id.tv_service_name);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
            tvLocation = (TextView) view.findViewById(R.id.tv_location);
            llJob.setOnClickListener(this);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return JobHolder.this;
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            if (getItemClickListener() != null)
                getItemClickListener().onRecyclerViewItemClick(JobHolder.this);
        }

        public void setJobStatus(int spJobStatus) {
            if (spJobStatus == AppConstants.STATUS_OPEN) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_blue);
                tvStatus.setText(R.string.str_status_open);
            } else if (spJobStatus == AppConstants.STATUS_ACCEPTED) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_blue);
                tvStatus.setText(R.string.str_status_accepted);
            } else if (spJobStatus == AppConstants.STATUS_ON_THE_WAY) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_blue);
                tvStatus.setText(R.string.str_status_on_way);
            } else if (spJobStatus == AppConstants.STATUS_ARRIVED) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_blue);
                tvStatus.setText(R.string.str_status_arrived);
            } else if (spJobStatus == AppConstants.STATUS_STARTED) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_blue);
                tvStatus.setText(R.string.str_status_on_going);
            } else if (spJobStatus == AppConstants.STATUS_COMPLETED) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_blue);
                tvStatus.setText(R.string.str_status_completed);
            } else if (spJobStatus == AppConstants.STATUS_REJECTION) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_red);
                tvStatus.setText(R.string.str_status_rejected);
            } else if (spJobStatus == AppConstants.STATUS_CANCELLED) {
                tvStatus.setBackgroundResource(R.drawable.bg_listing_status_red);
                tvStatus.setText(R.string.str_status_cancelled);
            }
        }
    }

    public class ProgressViewHolder extends BaseRecyclerViewHolder {
        private ProgressViewHolder(View view) {
            super(view);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    public class EmptyViewHolder extends BaseRecyclerViewHolder {
        private TextView textViewEmpty;


        private EmptyViewHolder(View view) {
            super(view);
            textViewEmpty = view.findViewById(R.id.tv_message);
            textViewEmpty.setText(R.string.str_no_weekly_job);

        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }


        @Override
        public void onClick(View v) {
            super.onClick(v);
            if (getItemClickListener() != null)
                getItemClickListener().onRecyclerViewChildItemClick(EmptyViewHolder.this, v.getId());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getItemAt(position).getItemType();
    }
}
