package com.apps.ondemand.app.data.models;

import com.apps.ondemand.common.business.BaseItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceModel implements BaseItem {

    public ServiceStateEnum state = ServiceStateEnum.UNSELECTED;
    @SerializedName("isSelected")
    @Expose
    boolean isSelected = false;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("serviceImage")
    @Expose
    private String serviceImage;
    @SerializedName("serviceDescription")
    @Expose
    private String serviceDescription;
    @SerializedName("serviceName")
    @Expose
    private String serviceName;
    @SerializedName("serviceHourlyRate")
    @Expose
    double serviceHourlyRate = 0.0;

    public ServiceModel() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServiceImage() {
        return serviceImage;
    }

    public void setServiceImage(String serviceImage) {
        this.serviceImage = serviceImage;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public ServiceStateEnum getState() {
        return state;
    }

    public void setState(ServiceStateEnum state) {
        this.state = state;
    }

    public double getServiceHourlyRate() {
        return serviceHourlyRate;
    }

    public void setServiceHourlyRate(double serviceHourlyRate) {
        this.serviceHourlyRate = serviceHourlyRate;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public int getItemType() {
        return ITEM_SERVICE;
    }

}

