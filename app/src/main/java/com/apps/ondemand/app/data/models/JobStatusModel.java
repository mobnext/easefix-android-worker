package com.apps.ondemand.app.data.models;

import com.apps.ondemand.common.business.BaseItem;

public class JobStatusModel implements BaseItem {
    int status;

    public JobStatusModel(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public int getItemType() {
        return ITEM_JOB_STATUS;
    }
}
