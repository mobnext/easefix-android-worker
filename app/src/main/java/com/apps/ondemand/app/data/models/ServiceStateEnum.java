package com.apps.ondemand.app.data.models;

//Service States
public enum ServiceStateEnum {
    UNSELECTED,
    SELECTED,
    SAVED,
    EDIT
}
