package com.apps.ondemand.app.ui.dialogs;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.ui.interfaces.CautionLeftListener;
import com.apps.ondemand.app.ui.interfaces.CautionRightListener;


public class DialogCaution extends BaseDialog implements View.OnClickListener {


    Context context;
    BottomSheetBehavior mBehavior;
    Button btnLeft;
    Button btnRight;
    View contentView;
    private CautionLeftListener cautionLeftListener = null;
    private CautionRightListener cautionRightListener = null;

    private String title = "";
    private String message = "";
    private String leftButtonString = "";
    private String rightButtonString = "";
    TextView tvMessage;
    private TextView tvTitle;
    int iconId = -1;


    private DialogCaution(@NonNull Context context) {
        super(context);
        this.context = context;


    }

    private DialogCaution(@NonNull CautionBuilder builder) {
        super(builder.context);
        this.context = builder.context;
        cautionLeftListener = builder.cautionLeftListener;
        cautionRightListener = builder.cautionRightListener;
        message = builder.message;
        title = builder.title;
        leftButtonString = builder.leftButtonString;
        rightButtonString = builder.rightButtonString;
        iconId = builder.iconId;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        contentView = View.inflate(getContext(), R.layout.caution_bottom_sheet, null);
        setContentView(contentView);
        mBehavior = BottomSheetBehavior.from((View) contentView.getParent());
        ((View) contentView.getParent()).setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));

    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = DialogCaution.this;
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setLayout(width, height);
        dialog.setCanceledOnTouchOutside(false);
        populateViews();
        setCancelable(false);
    }

    @Override
    public void onBackPressed() {
        dismiss();
    }

    private void populateViews() {
        mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING)
                    bottomSheet.post(new Runnable() {
                        @Override
                        public void run() {
                            mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    });
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
        mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        tvTitle = findTextViewById(R.id.tv_title);
        tvMessage = findTextViewById(R.id.tv_message);
        btnLeft = findViewById(R.id.btn_left);
        btnLeft.setOnClickListener(this);
        btnRight = findViewById(R.id.btn_right);
        btnRight.setOnClickListener(this);

        if (TextUtils.isEmpty(title)) {
            tvTitle.setVisibility(View.GONE);
        } else {
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(title);
        }

        if (TextUtils.isEmpty(message)) {
            tvMessage.setVisibility(View.GONE);
        } else {
            tvMessage.setVisibility(View.VISIBLE);
            tvMessage.setText(message);
        }

        if (TextUtils.isEmpty(leftButtonString)) {
            btnLeft.setVisibility(View.GONE);
        } else {
            btnLeft.setVisibility(View.VISIBLE);
            btnLeft.setText(leftButtonString);
        }

        if (TextUtils.isEmpty(rightButtonString)) {
            btnRight.setVisibility(View.GONE);
        } else {
            btnRight.setVisibility(View.VISIBLE);
            btnRight.setText(rightButtonString);
        }
        if (iconId == -1) {
            findImageViewById(R.id.iv_icon).setVisibility(View.GONE);
        } else {
            findImageViewById(R.id.iv_icon).setVisibility(View.VISIBLE);
            findImageViewById(R.id.iv_icon).setImageResource(iconId);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_left:
                if (cautionLeftListener != null)
                    cautionLeftListener.onClicked();
                onBackPressed();
                break;
            case R.id.btn_right:
                if (cautionRightListener != null)
                    cautionRightListener.onClicked();
                onBackPressed();
                break;
        }
    }


    //Builder Class
    public static class CautionBuilder {

        Context context;
        private CautionLeftListener cautionLeftListener = null;
        private CautionRightListener cautionRightListener = null;
        int iconId = -1;
        private String title = "";
        private String message = "";
        private String leftButtonString = "";
        private String rightButtonString = "";

        public static CautionBuilder newInstance(Context context) {
            return new CautionBuilder(context);
        }

        public CautionBuilder(Context context) {
            this.context = context;
        }

        public CautionBuilder setIcon(@DrawableRes int iconId) {
            this.iconId = iconId;
            return this;
        }

        public CautionBuilder setTitle(String title) {
            this.title = title;
            return this;
        }

        public CautionBuilder setTitle(@StringRes int title) {
            this.title = context.getString(title);
            return this;
        }


        public CautionBuilder setMessage(@StringRes int message) {
            this.message = context.getString(message);
            return this;
        }

        public CautionBuilder setMessage(String message) {
            this.message = message;
            return this;
        }

        public CautionBuilder setLeftButton(@StringRes int leftButtonString, CautionLeftListener cautionLeftListener) {
            this.leftButtonString = context.getString(leftButtonString);
            this.cautionLeftListener = cautionLeftListener;
            return this;
        }

        public CautionBuilder setLeftButton(String leftButtonString, CautionLeftListener cautionLeftListener) {
            this.leftButtonString = leftButtonString;
            this.cautionLeftListener = cautionLeftListener;
            return this;
        }

        public CautionBuilder setRightButton(String rightButtonString, CautionRightListener cautionRightListener) {
            this.rightButtonString = rightButtonString;
            this.cautionRightListener = cautionRightListener;
            return this;
        }

        public CautionBuilder setRightButton(@StringRes int rightButtonString, CautionRightListener cautionRightListener) {
            this.rightButtonString = context.getString(rightButtonString);
            this.cautionRightListener = cautionRightListener;
            return this;
        }

        public CautionBuilder setLeftButton(@StringRes int leftButtonString) {
            this.leftButtonString = context.getString(leftButtonString);
            cautionLeftListener = null;
            return this;
        }

        public CautionBuilder setLeftButton(String leftButtonString) {
            this.leftButtonString = leftButtonString;
            cautionLeftListener = null;
            return this;
        }

        public CautionBuilder setRightButton(@StringRes int rightButtonString) {
            this.rightButtonString = context.getString(rightButtonString);
            cautionRightListener = null;
            return this;
        }

        public CautionBuilder setRightButton(String rightButtonString) {
            this.rightButtonString = rightButtonString;
            cautionRightListener = null;
            return this;
        }


        public DialogCaution build() {
            return new DialogCaution(this);
        }
    }
}