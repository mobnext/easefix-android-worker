package com.apps.ondemand.app.ui.views;

import android.animation.Animator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.apps.ondemand.R;
import com.apps.ondemand.common.utils.AppUtils;

/**
 * Created by FSC on 1/16/2018.
 */

public class CustomToggle extends RelativeLayout {

    private float indicatorRadius = 0;
    private int indicatorColor = 0;
    private int normalColor = 0;
    private float rootRadius = 0;
    private int rootRadiusWidth = 0;
    private int rootRadiusColor = 0;

    private int textColorPrimary = 0;
    private int textColorSecondary = 0;
    private float textSize = 0;
    private float width = 0;

    private CharSequence[] titles;
    private CharSequence[] values;

    private LayoutInflater inflater;
    LinearLayout mLayoutToggle;
    private Button buttonWhatGender;
    private String value  = "";

    private OnToggleChangeListener onToggleChangeListener;

    public CustomToggle(Context context) {
        super(context);
        inflateView(context);
    }

    public CustomToggle(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setupAttributes(attrs);
        inflateView(context);
        setBackgroundShape();
        setIndicatorShape();
    }

    public CustomToggle(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupAttributes(attrs);
        inflateView(context);
        setBackgroundShape();
        setIndicatorShape();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CustomToggle(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setupAttributes(attrs);
        inflateView(context);
        setBackgroundShape();
        setIndicatorShape();
    }

    public void addToggleChangeListener(OnToggleChangeListener onToggleChangeListener){
        this.onToggleChangeListener = onToggleChangeListener;
    }

    private void inflateView(Context context){
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_toggle,this,true);
        bindViews();
    }

    private void setupAttributes(AttributeSet attrs) {
        TypedArray array = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.GenderToggle,0,0);
        try {
            indicatorColor = array.getColor(R.styleable.GenderToggle_toggle_indicator_color, getResources().getColor(R.color.colorPrimary));
            indicatorRadius = array.getFloat(R.styleable.GenderToggle_toggle_indicator_radius, 50);
            rootRadius = array.getInteger(R.styleable.GenderToggle_toggle_root_radius,50);
            rootRadiusWidth = array.getInteger(R.styleable.GenderToggle_toggle_root_radius_width,3);
            rootRadiusColor = array.getColor(R.styleable.GenderToggle_toggle_root_radius_color, getResources().getColor(R.color.colorPrimary));
            titles = array.getTextArray(R.styleable.GenderToggle_toggle_titles);
            values = array.getTextArray(R.styleable.GenderToggle_toggle_values);
            textColorPrimary = array.getColor(R.styleable.GenderToggle_toggle_text_color_primary, getResources().getColor(R.color.text_primary));
            textColorSecondary = array.getColor(R.styleable.GenderToggle_toggle_text_color_secondary, getResources().getColor(R.color.text_primary_light));
            normalColor = array.getColor(R.styleable.GenderToggle_toggle_normal_color, getResources().getColor(R.color.colorPrimary));
            textSize = array.getDimension(R.styleable.GenderToggle_toggle_text_size, 10f);
            width = array.getDimension(R.styleable.GenderToggle_toggle_width, 0);
            assignValue(0);
        }finally {
            array.recycle();
        }
    }

    public void selectOption(boolean isInsideFragment, final float animationpos, final String title) {
        int duration = 0;
        if(isInsideFragment)
            duration = 500;

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                animateOption(animationpos, title);
            }
        },duration);
    }

    private void animateOption(float animationpos, final String title) {
        buttonWhatGender.animate().x(animationpos).setDuration(100).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
//                buttonWhatGender.setText("");
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                buttonWhatGender.setText(title);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        }).start();
    }

    private void bindViews() {
        mLayoutToggle = findViewById(R.id.layout_toggle);
        buttonWhatGender = (Button) findViewById(R.id.button_whatGender);
        buttonWhatGender.setText(titles.length > 0 ? titles[0].toString() : "");
        buttonWhatGender.setTextColor(textColorPrimary);
        buttonWhatGender.setTextSize(textSize);
        if (width != 0)
            buttonWhatGender.getLayoutParams().width = (int) width;

        for (int i=0 ; i < titles.length ; i++){
            View view = inflater.inflate(R.layout.row_toggle,mLayoutToggle,false);
            Button textView = view.findViewById(R.id.btn);
            textView.setBackground(new ColorDrawable(Color.TRANSPARENT));
            textView.setTextSize(textSize);
            textView.setTextColor(textColorSecondary);
            textView.setAllCaps(false);
            textView.setMinimumWidth(AppUtils.dpToPx(0, getContext()));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            textView.setText(titles[i].toString());
            textView.setTag(titles[i].toString());
            textView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    String title = v.getTag().toString();
                    assignValue(getIndexFromTitle(title));
                    selectOption(true, v.getX(), title);
                    if (onToggleChangeListener != null)
                        onToggleChangeListener.onValueChange(getIndex(value), value);
                }
            });
            mLayoutToggle.addView(view, params);
            if (width != 0)
                view.getLayoutParams().width = (int) width;
        }
    }


    private void setBackgroundShape(){
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadius(rootRadius);
        shape.setStroke(rootRadiusWidth,rootRadiusColor);
        shape.setColor(normalColor);
        setBackgroundDrawable(shape);
    }

    private void setIndicatorShape(){
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadius(indicatorRadius);
        shape.setColor(indicatorColor);
        buttonWhatGender.setBackgroundDrawable(shape);
    }

    public String getValue(){
        return value;
    }

    public void setValueFromTitle(String title){
        if (title == null)
            return;
        for (int i = 0; i < mLayoutToggle.getChildCount(); i++) {
            String tag = mLayoutToggle.getChildAt(i).getTag().toString();
            if (tag.equalsIgnoreCase(title)){
                assignValue(i);
                selectOption(true, mLayoutToggle.getChildAt(i).getX(), title);
                return;
            }
        }
    }

    public void setValue(String value){
        int index = getIndex(value);
        assignValue(index);
        selectOption(true, mLayoutToggle.getChildAt(index).getX(), titles[index].toString());
    }

    private void assignValue(int index){
        if (values != null && values.length > 0 && index < values.length )
            value = values[index].toString();
        else if (titles != null && titles.length > 0 && index < titles.length)
            value = titles[index].toString();
        else
            value = "";
    }

    private int getIndexFromTitle(String title){
         if (titles != null && titles.length > 0){
            for (int i = 0; i < titles.length; i++) {
                if (titles[i].toString().equalsIgnoreCase(title))
                    return i;
            }
        }
        return 0;
    }

    private int getIndex(String value){
        if (value == null)
            return 0;
        if (values != null && values.length > 0){
            for (int i = 0; i < values.length; i++) {
                if (values[i].toString().equalsIgnoreCase(value))
                    return i;
            }
        }
        return 0;
    }

}

interface OnToggleChangeListener{
    public void onValueChange(int index, String value);
}

