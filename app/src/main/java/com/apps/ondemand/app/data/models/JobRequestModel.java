package com.apps.ondemand.app.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobRequestModel {

    @SerializedName("title")
    @Expose
    private String title = "";
    @SerializedName("jobId")
    @Expose
    private String jobId = "";
    @SerializedName("spJobStatus")
    @Expose
    private int spJobStatus = 0;
    @SerializedName("name")
    @Expose
    private String name = "";
    @SerializedName("profileImage")
    @Expose
    private String profileImage = "";
    @SerializedName("avgRating")
    @Expose
    private float avgRating = 0;
    @SerializedName("serviceName")
    @Expose
    private String serviceName = "";
    @SerializedName("subService")
    @Expose
    private String subService = "";
    @SerializedName("jobScheduleTime")
    @Expose
    private int jobScheduleTime = 0;
    @SerializedName("jobDuration")
    @Expose
    private float jobDuration = 0;
    @SerializedName("jobAddress")
    @Expose
    private String jobAddress = "";
//    private JobAddressModel jobAddress;
    @SerializedName("specialInstruction")
    @Expose
    private String specialInstruction = "";
    @SerializedName("workDescription")
    @Expose
    private String workDescription = "";
    @SerializedName("message")
    @Expose
    private String message = "";

    @SerializedName("fixedJobAmount")
    @Expose
    private String jobFixedPrice = "";

    @SerializedName("isJobFixed")
    @Expose
    private boolean isJobFixed = false;

    @SerializedName("JobIdIdentifier")
    @Expose
    private String jobIdIdentifier = "";
    @SerializedName("isJobDurationKnown")
    @Expose
    private Boolean isJobDurationKnown = false;

    public Boolean getJobDurationKnown() {
        return isJobDurationKnown;
    }

    public void setJobDurationKnown(Boolean jobDurationKnown) {
        isJobDurationKnown = jobDurationKnown;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public int getSpJobStatus() {
        return spJobStatus;
    }

    public void setSpJobStatus(int spJobStatus) {
        this.spJobStatus = spJobStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public float getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(float avgRating) {
        this.avgRating = avgRating;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getSubService() {
        return subService;
    }

    public void setSubService(String subService) {
        this.subService = subService;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public int getJobScheduleTime() {
        return jobScheduleTime;
    }

    public void setJobScheduleTime(int jobScheduleTime) {
        this.jobScheduleTime = jobScheduleTime;
    }

    public float getJobDuration() {
        return jobDuration;
    }

    public void setJobDuration(float jobDuration) {
        this.jobDuration = jobDuration;
    }

    public String getJobAddress() {
        return jobAddress;
    }

    public void setJobAddress(String jobAddress) {
        this.jobAddress = jobAddress;
    }

    public String getSpecialInstruction() {
        return specialInstruction;
    }

    public void setSpecialInstruction(String specialInstruction) {
        this.specialInstruction = specialInstruction;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getWorkDescription() {
        return workDescription;
    }

    public void setWorkDescription(String workDescription) {
        this.workDescription = workDescription;
    }

    public String getJobIdIdentifier() {
        return jobIdIdentifier;
    }

    public void setJobIdIdentifier(String jobIdIdentifier) {
        this.jobIdIdentifier = jobIdIdentifier;
    }

    public String getJobFixedPrice() {
        return jobFixedPrice;
    }

    public void setJobFixedPrice(String jobFixedPrice) {
        this.jobFixedPrice = jobFixedPrice;
    }

    public boolean getIsJobFixed() {
        return isJobFixed;
    }

    public void setIsJobFixed(boolean isFixed) {
        this.isJobFixed = isFixed;
    }
}
