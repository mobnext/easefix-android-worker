package com.apps.ondemand.app.ui.dialogs;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.TermsPrivacyModel;
import com.apps.ondemand.app.ui.interfaces.TermsPrivacyListener;
import com.apps.ondemand.app.ui.main.TermsPrivacyActivity;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.utils.AppConstants;


public class DialogTermsAndPrivacy extends BaseDialog implements View.OnClickListener {


    BaseActivity activity;
    BottomSheetBehavior mBehavior;
    View contentView;
    TermsPrivacyModel model;
    boolean isAgreedToTerms;
    boolean isAgreedToPrivacy;
    TermsPrivacyListener listener;

    private DialogTermsAndPrivacy(@NonNull BaseActivity activity, TermsPrivacyModel model, TermsPrivacyListener listener) {
        super(activity);
        this.activity = activity;
        this.model = model;
        this.listener = listener;


    }

    public static DialogTermsAndPrivacy newInstance(BaseActivity activity, TermsPrivacyModel model, TermsPrivacyListener listener) {
        return new DialogTermsAndPrivacy(activity, model, listener);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        contentView = View.inflate(getContext(), R.layout.terms_privacy_bottom_sheet, null);

        setContentView(contentView);

        mBehavior = BottomSheetBehavior.from((View) contentView.getParent());
        ((View) contentView.getParent()).setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));
    }

    @Override
    public void onStart() {
        super.onStart();
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        getWindow().setLayout(width, height);
        setCanceledOnTouchOutside(false);
        populateViews();
        setCancelable(false);
    }

    @Override
    public void onBackPressed() {
        activity.onBackPressed();
    }

    private void populateViews() {
        mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING)
                    bottomSheet.post(new Runnable() {
                        @Override
                        public void run() {
                            mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    });
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
        mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        findViewById(R.id.tv_terms).setOnClickListener(this);
        findViewById(R.id.tv_privacy).setOnClickListener(this);
        findViewById(R.id.iv_terms_selected).setOnClickListener(this);
        findViewById(R.id.iv_privacy_selected).setOnClickListener(this);
        findViewById(R.id.btn_agree).setOnClickListener(this);

        if (model.getIsPrivacyPolicyUpdated()) {
            findViewById(R.id.cv_privacy).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.cv_privacy).setVisibility(View.GONE);
        }

        if (model.getIsTermAndConditionUpdated()) {
            findViewById(R.id.cv_terms).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.cv_terms).setVisibility(View.GONE);
        }


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_terms:
                Intent intent = new Intent(activity, TermsPrivacyActivity.class);
                intent.putExtra(AppConstants.KEY_SHOW_TERMS, true);
                activity.startActivity(intent);
                break;
            case R.id.tv_privacy:
                intent = new Intent(activity, TermsPrivacyActivity.class);
                intent.putExtra(AppConstants.KEY_SHOW_TERMS, false);
                activity.startActivity(intent);
                break;
            case R.id.iv_terms_selected:
                isAgreedToTerms = !isAgreedToTerms;
                findImageViewById(R.id.iv_terms_selected).setSelected(isAgreedToTerms);
                break;
            case R.id.iv_privacy_selected:
                isAgreedToPrivacy = !isAgreedToPrivacy;
                findImageViewById(R.id.iv_privacy_selected).setSelected(isAgreedToPrivacy);
                break;
            case R.id.btn_agree:
                if (model.getIsTermAndConditionUpdated() && !isAgreedToTerms) {
                    showSnackBar(activity.getString(R.string.error_term_not_agreed));
                    return;
                }
                if (model.getIsPrivacyPolicyUpdated() && !isAgreedToPrivacy) {
                    showSnackBar(activity.getString(R.string.error_privacy_not_agreed));
                    return;
                }
                listener.onAgree(model.getIsTermAndConditionUpdated(),model.getIsPrivacyPolicyUpdated());
                dismiss();
                break;

        }
    }
}