package com.apps.ondemand.app.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.apps.ondemand.common.business.BaseItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobAddressModel implements Parcelable {

    @SerializedName("type")
    @Expose
    private int type;

    @SerializedName("latitude")
    @Expose
    private double latitude;
    @SerializedName("longitude")
    @Expose
    private double longitude;
    @SerializedName("primaryAddress")
    @Expose
    private String primaryAddress;
    @SerializedName("streetAddressLine1")
    @Expose
    private String streetAddressLine1;
    @SerializedName("streetAddressLine2")
    @Expose
    private String streetAddressLine2;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("country")
    @Expose
    private String country;

    public JobAddressModel() {
        type = 1;
        latitude = 0.0;
        longitude = 0.0;
        primaryAddress = "";
        streetAddressLine1 = "";
        streetAddressLine2 = "";
        city = "";
        state = "";
        country = "";
    }

    protected JobAddressModel(Parcel in) {
        type = in.readInt();
        latitude = in.readDouble();
        longitude = in.readDouble();
        primaryAddress = in.readString();
        streetAddressLine1 = in.readString();
        streetAddressLine2 = in.readString();
        city = in.readString();
        state = in.readString();
        country = in.readString();
    }

    public static final Creator<JobAddressModel> CREATOR = new Creator<JobAddressModel>() {
        @Override
        public JobAddressModel createFromParcel(Parcel in) {
            return new JobAddressModel(in);
        }

        @Override
        public JobAddressModel[] newArray(int size) {
            return new JobAddressModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(type);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(primaryAddress);
        dest.writeString(streetAddressLine1);
        dest.writeString(streetAddressLine2);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(country);
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getPrimaryAddress() {
        return primaryAddress;
    }

    public void setPrimaryAddress(String primaryAddress) {
        this.primaryAddress = primaryAddress;
    }

    public String getStreetAddressLine1() {
        return streetAddressLine1;
    }

    public void setStreetAddressLine1(String streetAddressLine1) {
        this.streetAddressLine1 = streetAddressLine1;
    }

    public String getStreetAddressLine2() {
        return streetAddressLine2;
    }

    public void setStreetAddressLine2(String streetAddressLine2) {
        this.streetAddressLine2 = streetAddressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

}
