package com.apps.ondemand.app.calling;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.RequiresApi;

import com.apps.ondemand.R;

public class RingtonePlayingService extends Service {
    public static final String ARG_STOP = "argStop";
    public static Ringtone ringtone;
    public static MediaPlayer mMediaPlayer = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        RingtoneManager ringMan = new RingtoneManager(this);
        ringMan.stopPreviousRingtone();
        Uri ringtoneUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.ringing);
        if (ringtone != null && ringtone.isPlaying())
            ringtone.stop();

        this.ringtone = ringMan.getRingtone(this, ringtoneUri);
        ringtone.setLooping(true);
        ringtone.play();

        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy() {
        ringtone.stop();
    }
}
