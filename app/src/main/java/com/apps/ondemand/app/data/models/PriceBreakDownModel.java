package com.apps.ondemand.app.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PriceBreakDownModel implements Parcelable {


    @SerializedName("totalAmount")
    @Expose
    private double totalAmount;
    @SerializedName("serviceCharges")
    @Expose
    private double serviceCharges;
    @SerializedName("discountAmount")
    @Expose
    private double discountAmount;
    @SerializedName("serviceHourlyRate")
    @Expose
    private double serviceHourlyRate;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("totalHoursSpent")
    @Expose
    private int totalHoursSpent;
    @SerializedName("totalMinutesSpent")
    @Expose
    private int totalMinutesSpent;

    @SerializedName("totalJobAmount")
    @Expose
    private double totalJobAmount;

    @SerializedName("serviceName")
    @Expose
    private String serviceName;

    @SerializedName("totalLineItemAmount")
    @Expose
    private double totalLineItemAmount;

    @SerializedName("spAmount")
    @Expose
    private double spAmount;

    @SerializedName("lineItems")
    @Expose
    private List<LineItemModel> lineItems = new ArrayList<>();

    protected PriceBreakDownModel(Parcel in) {
        totalAmount = in.readDouble();
        serviceCharges = in.readDouble();
        discountAmount = in.readDouble();
        serviceHourlyRate = in.readDouble();
        currency = in.readString();
        totalHoursSpent = in.readInt();
        totalMinutesSpent = in.readInt();
        totalJobAmount = in.readDouble();
        serviceName = in.readString();
        totalLineItemAmount = in.readDouble();
        spAmount = in.readDouble();
        lineItems = in.createTypedArrayList(LineItemModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(totalAmount);
        dest.writeDouble(serviceCharges);
        dest.writeDouble(discountAmount);
        dest.writeDouble(serviceHourlyRate);
        dest.writeString(currency);
        dest.writeInt(totalHoursSpent);
        dest.writeInt(totalMinutesSpent);
        dest.writeDouble(totalJobAmount);
        dest.writeString(serviceName);
        dest.writeDouble(totalLineItemAmount);
        dest.writeDouble(spAmount);
        dest.writeTypedList(lineItems);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PriceBreakDownModel> CREATOR = new Creator<PriceBreakDownModel>() {
        @Override
        public PriceBreakDownModel createFromParcel(Parcel in) {
            return new PriceBreakDownModel(in);
        }

        @Override
        public PriceBreakDownModel[] newArray(int size) {
            return new PriceBreakDownModel[size];
        }
    };

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getServiceCharges() {
        return serviceCharges;
    }

    public void setServiceCharges(double serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public double getServiceHourlyRate() {
        return serviceHourlyRate;
    }

    public void setServiceHourlyRate(double serviceHourlyRate) {
        this.serviceHourlyRate = serviceHourlyRate;
    }


    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getTotalHoursSpent() {
        return totalHoursSpent;
    }

    public void setTotalHoursSpent(int totalHoursSpent) {
        this.totalHoursSpent = totalHoursSpent;
    }

    public int getTotalMinutesSpent() {
        return totalMinutesSpent;
    }

    public void setTotalMinutesSpent(int totalMinutesSpent) {
        this.totalMinutesSpent = totalMinutesSpent;
    }

    public double getTotalJobAmount() {
        return totalJobAmount;
    }

    public void setTotalJobAmount(double totalJobAmount) {
        this.totalJobAmount = totalJobAmount;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public double getTotalLineItemAmount() {
        return totalLineItemAmount;
    }

    public void setTotalLineItemAmount(double totalLineItemAmount) {
        this.totalLineItemAmount = totalLineItemAmount;
    }

    public List<LineItemModel> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItemModel> lineItems) {
        this.lineItems = lineItems;
    }

    public double getSpAmount() {
        return spAmount;
    }

    public void setSpAmount(double spAmount) {
        this.spAmount = spAmount;
    }
}
