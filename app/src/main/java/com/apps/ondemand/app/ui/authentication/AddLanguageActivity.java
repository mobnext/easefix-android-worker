package com.apps.ondemand.app.ui.authentication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.ProficiencyModel;
import com.apps.ondemand.app.data.models.SpokenLanguageModel;
import com.apps.ondemand.app.ui.adapters.SpokenLanguageAdapter;
import com.apps.ondemand.app.ui.dialogs.DialogLanguageProficiency;
import com.apps.ondemand.app.ui.interfaces.OnRecycleViewItemClickListener;
import com.apps.ondemand.app.ui.interfaces.ProficiencySelectionCallback;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.utils.AppConstants;

import java.util.ArrayList;

import static com.apps.ondemand.common.utils.AppConstants.KEY_PROFICIENCIES;
import static com.apps.ondemand.common.utils.AppConstants.KEY_SPOKEN_LANGUAGES;

public class AddLanguageActivity extends BaseActivity implements OnRecycleViewItemClickListener {
    ArrayList<SpokenLanguageModel> spokenLanguageModels;
    private RecyclerView recyclerView;
    SpokenLanguageAdapter spokenLanguageAdapter;
    private String fetchLevelUrl = "";
    ArrayList<ProficiencyModel> proficiencyModels;
    private EditText etSearch ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_language);
        setActionBar(R.string.str_add_languages);
        setSubTitle(R.string.str_add_language_desc);
        spokenLanguageModels = getIntent().getParcelableArrayListExtra(KEY_SPOKEN_LANGUAGES);
        proficiencyModels = getIntent().getParcelableArrayListExtra(KEY_PROFICIENCIES);
        initViews();
    }

    private void initViews() {
        etSearch =  findViewById(R.id.et_search);
        etSearch.addTextChangedListener(textWatcher);
        etSearch.setImeOptions(EditorInfo.IME_ACTION_DONE);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        spokenLanguageAdapter = new SpokenLanguageAdapter(this, spokenLanguageModels, this);
        recyclerView.setAdapter(spokenLanguageAdapter);
    }

    int selectedPosition;

    @Override
    public void onRecyclerViewItemClick(RecyclerView.ViewHolder holder) {
        selectedPosition = holder.getAdapterPosition();
        SpokenLanguageModel spokenLanguageModel = spokenLanguageModels.get(selectedPosition);
        if (spokenLanguageModel.isAdded()) {
            spokenLanguageModel.setAdded(false);
            spokenLanguageAdapter.notifyItemChanged(selectedPosition);
        } else {
            DialogLanguageProficiency.newInstance(this, proficiencyModels, spokenLanguageModel, callback).show();
        }
    }

    ProficiencySelectionCallback callback = new ProficiencySelectionCallback() {
        @Override
        public void onAddLanguage(ProficiencyModel proficiencyModel) {
            SpokenLanguageModel selectedModel = spokenLanguageModels.get(selectedPosition);
            selectedModel.setAdded(true);
            selectedModel.setProficiencyName(proficiencyModel.getLabel());
            selectedModel.setProficiencyId(proficiencyModel.getId());
            setResultAndGoBack();
        }
    };

    private void setResultAndGoBack() {
        Intent intent = new Intent();
        intent.putExtra(AppConstants.KEY_SPOKEN_LANGUAGES, spokenLanguageModels);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }


        @Override
        public void afterTextChanged(Editable editable) {
            if (spokenLanguageAdapter != null) {
                spokenLanguageAdapter.getFilter().filter(editable.toString());
            }
        }
    };

}
