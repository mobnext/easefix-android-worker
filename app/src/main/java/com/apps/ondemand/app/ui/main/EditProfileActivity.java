package com.apps.ondemand.app.ui.main;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.RequiresApi;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.common.base.TakePhotoActivity;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.BitmapUtils;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.apps.ondemand.common.utils.VizImageView;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
@RequiresApi(api = Build.VERSION_CODES.S)
public class EditProfileActivity extends TakePhotoActivity implements VizImageView.OnImageUploadingListener {

    private String saveProfile;
    private String profileImage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        setActionBar(R.string.str_edit_profile);
        setProfileData();
        bindListeners();

    }


    private void bindListeners() {
        findViewById(R.id.iv_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChoosePhotoDialog();
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.S)
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.m_save) {
            updateProfile();
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateProfile() {
        if (!areParamsValid())
            return;

        saveProfile = AppConstants.getServerUrl(String.format(AppConstants.UPDATE_PROFILE, UserManager.getUserId()));
        HttpRequestItem requestItem = new HttpRequestItem(saveProfile);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_PUT);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        requestItem.setParams(getProfileParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    private boolean areParamsValid() {
        AppUtils.hideSoftKeyboard(this);
        if (profileImage.isEmpty()) {
            showSnackBar(R.string.error_empty_image);
            return false;
        } else if (findFieldById(R.id.et_first_name).getText().toString().isEmpty()) {
            findFieldById(R.id.et_first_name).setError(getString(R.string.error_first_name));
            return false;
        } else if (findFieldById(R.id.et_last_name).getText().toString().isEmpty()) {
            findFieldById(R.id.et_last_name).setError(getString(R.string.error_last_name));
            return false;
        } else if (TextUtils.isEmpty(findFieldById(R.id.et_email).getText().toString())) {
            setError(R.id.et_email, R.string.error_empty_email);
            return false;
        } else if (!AppUtils.isEmailValid(findFieldById(R.id.et_email).getText().toString())) {
            setError(R.id.et_email, R.string.error_invalid_email);
            return false;
        } else if (TextUtils.isEmpty(findFieldById(R.id.et_bio).getText().toString())) {
            setError(R.id.et_bio, R.string.error_empty_bio);
            return false;
        }
        return true;
    }


    private Map<String, Object> getProfileParams() {
        Map<String, Object> map = new HashMap<>();
        map.put("companyName", findFieldById(R.id.et_company_name).getText());
        map.put("firstName", findFieldById(R.id.et_first_name).getText());
        map.put("lastName", findFieldById(R.id.et_last_name).getText());
        map.put("email", findFieldById(R.id.et_email).getText());
        map.put("about", findFieldById(R.id.et_bio).getText());
        map.put("profileImage", profileImage);
        return map;
    }

    private void setProfileData() {
        findFieldById(R.id.et_bio).addTextChangedListener(bioTextListener);


        if (UserManager.getIsCompany()) {
            findViewById(R.id.cv_company_name).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.cv_company_name).setVisibility(View.GONE);
        }
        findFieldById(R.id.et_company_name).setText(UserManager.getCompanyName());
        findFieldById(R.id.et_first_name).setText(UserManager.getFirstName());
        findFieldById(R.id.et_last_name).setText(UserManager.getLastName());

        profileImage = UserManager.getImage();
        VizImageView vizImageView = findViewById(R.id.iv_profile);
        vizImageView.setImage(profileImage, false);
        findFieldById(R.id.et_email).setText(UserManager.getEmail());
        findFieldById(R.id.et_bio).setText(UserManager.getBio());
    }

    private void loadImageInView() {
        if (selectedPhotoUri == null)
            return;

        VizImageView imageView = findViewById(R.id.iv_profile);
        imageView.loadImageInView(imageView.getId(), "", selectedPhotoUri, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CHOOSE_PHOTO_RESULT_CODE) {
                if (data.getData() != null) {
                    selectedPhotoUri = data.getData();
                } else {
                    selectedPhotoUri = null;
                }
            } else {
                if (requestCode == TAKE_PHOTO_RESULT_CODE) {
                    selectedPhotoUri = BitmapUtils.getImageUri(this, (Bitmap) data.getExtras().get("data"));
                } else {
                    selectedPhotoUri = null;
                }
            }
//            filePath = FileUtils.getPath(this, selectedPhotoUri);
            loadImageInView();
        } else {
            selectedPhotoUri = null;
        }
//        if (resultCode == Activity.RESULT_OK) {
//            switch (requestCode) {
//                case CHOOSE_PHOTO_RESULT_CODE:
//                case TAKE_PHOTO_RESULT_CODE: {
//                    if (requestCode == CHOOSE_PHOTO_RESULT_CODE)
//                        selectedPhotoUri = data.getData();
//                    loadImageInView();
//                    break;
//                }
//            }
//        }
    }


    @Override
    public void onUploadingResponse(boolean isSuccessful, String imageURL, int imageViewID, String message) {
        if (isSuccessful) {
            profileImage = imageURL;
        } else {
            profileImage = "";
            showSnackBar(getString(R.string.error_image_uploading_failed));
        }
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            JSONObject jsonObjectResponse = new JSONObject(response.getResponse());
            int success = jsonObjectResponse.getInt("success");
            if (success == 1) {
                //    JSONObject jsonObjectData = jsonObjectResponse.getJSONObject("data");
                //  UserManager.saveUserData(jsonObjectData);
                showSnackBar(jsonObjectResponse.getString("message"));
            } else {
                showSnackBar(jsonObjectResponse.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    TextWatcher bioTextListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            findTextViewById(R.id.tv_bio_limit).setText(
                    s.length() + "/" + getResources().getInteger(R.integer.desc_words_limit));
        }
    };
}
