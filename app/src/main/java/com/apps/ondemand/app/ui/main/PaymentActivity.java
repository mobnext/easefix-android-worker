package com.apps.ondemand.app.ui.main;

import static com.apps.ondemand.BuildConfig.stripe_key;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;

import com.apps.ondemand.R;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.DateUtils;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.BankAccount;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@RequiresApi(api = Build.VERSION_CODES.S)
public class PaymentActivity extends BaseActivity implements View.OnClickListener {
    int value = 1;

    private static final int AUTOCOMPLETE_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        setActionBar(R.string.str_bank_account);
        setSubTitle(R.string.str_add_bank_account);
        if (getIntent().hasExtra("first")) {
            value = 2;
        }
        findViewById(R.id.btn_proceed).setOnClickListener(this);
        findViewById(R.id.et_dateOfbirth).setOnClickListener(this);
        getBanksApiCall();
        calendarNew = Calendar.getInstance();
        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        findViewById(R.id.cardViewer).setOnClickListener(view -> {
            if (!Places.isInitialized()) {
                Places.initialize(getApplicationContext(), getString(R.string.google_key), Locale.US);
            }
            List<Place.Field> fields = Arrays.asList(Place.Field.NAME, Place.Field.ADDRESS, Place.Field.ADDRESS_COMPONENTS, Place.Field.LAT_LNG);
            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                    .build(this);
            startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
        });
    }

    private void getBanksApiCall() {
        value = 1;
        HttpRequestItem requestItem = new HttpRequestItem(AppConstants.getServerUrl(AppConstants.FETCH_BANK_ACCOUNT));
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);

        requestItem.setHeaderParams(AppUtils.getHeaderParams());

        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    private boolean validateData() {

        String accountNumber = ((EditText) findViewById(R.id.et_account_number)).getText().toString();
        String sortNumber = ((EditText) findViewById(R.id.et_sortNumber)).getText().toString();
        String primaryAddress = ((EditText) findViewById(R.id.et_primary_address)).getText().toString();
        String cityName = ((EditText) findViewById(R.id.et_city)).getText().toString();
        String stateName = ((EditText) findViewById(R.id.et_state)).getText().toString();
        String postalCode = ((EditText) findViewById(R.id.et_postal)).getText().toString();
        String dateofBirth = ((TextView) findViewById(R.id.et_dateOfbirth)).getText().toString();

        if (!AppUtils.ifNotNullEmpty(accountNumber)) {
            showSnackBar("Please enter account Number");
            return false;
        } else if (!AppUtils.ifNotNullEmpty(sortNumber)) {
            showSnackBar("Please enter Sort Code");
            return false;
        } else if (!AppUtils.ifNotNullEmpty(primaryAddress)) {
            showSnackBar("Please enter Address");
            return false;
        } else if (!AppUtils.ifNotNullEmpty(cityName)) {
            showSnackBar("Please enter City");
            return false;
        } else if (!AppUtils.ifNotNullEmpty(stateName)) {
            showSnackBar("Please enter State");
            return false;
        } else if (!AppUtils.ifNotNullEmpty(postalCode)) {
            showSnackBar("Please enter postal Code");
            return false;
        } else if (!AppUtils.ifNotNullEmpty(dateofBirth)) {
            showSnackBar("Please Select date of Birth");
            return false;
        } else
            return true;
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            JSONObject jsonObject = new JSONObject(response.getResponse());
            if (jsonObject.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equalsIgnoreCase(AppConstants.getServerUrl(AppConstants.FETCH_BANK_ACCOUNT))) {
                    showSnackBar(jsonObject.getString("message"));
                    if (value == 2) {
                        onBackPressed();
                    } else {
                        value = 2;
                        JSONObject data = jsonObject.getJSONObject("data");
                        String last4 = data.getString("last4");
                        String routingNum = data.getString("routingNum");
                        String city = data.getString("city");
                        String postalCode = data.getString("postalCode");
                        String address = data.getString("address");
                        String state = data.getString("state");
                        long dateOfBirth = data.getLong("dateOfBirth");
                        Log.d("TAG", "onNetworkSuccess: "+last4+ routingNum+city+postalCode+address+state+dateOfBirth);
                        populateData(last4, routingNum,city,postalCode,address,state,dateOfBirth);
                    }
                }

            }
        } catch (Exception e) {

        }
    }

    private void updateBankInfoApiCall(String token) {
        HttpRequestItem requestItem = new HttpRequestItem(
                AppConstants.getServerUrl(AppConstants.FETCH_BANK_ACCOUNT));
        requestItem.setHttpRequestType(NetworkUtils.HTTP_POST);     //by default its GET
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        requestItem.setParams(getServerParams(token));
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    private Map<String, Object> getServerParams(String token) {
        Map<String, Object> params = new HashMap<>();
        Log.d("TAG", "getServerParams: "+calendarNew.getTimeInMillis()/1000);
        params.put("bankToken", "" + token);
        params.put("primaryAddress", "" + findFieldById(R.id.et_primary_address).getText().toString());
        params.put("city", "" + findFieldById(R.id.et_city).getText().toString());
        params.put("postalCode", "" + findFieldById(R.id.et_postal).getText().toString());
        params.put("state", "" + findFieldById(R.id.et_state).getText().toString());
        params.put("dateOfBirth", "" + calendarNew.getTimeInMillis() / 1000);

        return params;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_proceed:
                if (validateData())
                    bankInf0();
                break;
            case R.id.et_dateOfbirth:
                showDatePickerDialog();
                break;
        }
    }

    private void bankInf0() {

        String sortNum = ((EditText) findViewById(R.id.et_sortNumber)).getText().toString();
        String accountNumber = ((EditText) findViewById(R.id.et_account_number)).getText().toString();

        Stripe stripe = new Stripe(this);
        stripe.setDefaultPublishableKey(stripe_key);
        BankAccount bankAccount = new BankAccount(accountNumber, "GB", "GBP", sortNum);
        stripe.createBankAccountToken(bankAccount, new TokenCallback() {
            @Override
            public void onError(Exception error) {
                Log.e("Stripe Error", error.getMessage());
                showSnackBar(error.getMessage());
            }

            @Override
            public void onSuccess(com.stripe.android.model.Token token) {
                Log.e("Bank Token", token.getId());
                updateBankInfoApiCall(token.getId());
            }
        });
    }

    private void populateData(String last4, String routingNum,String city,String postalCode,String address,String state,long dateOfBirth) {
        ((EditText) findViewById(R.id.et_sortNumber)).setText("" + routingNum);
        ((EditText) findViewById(R.id.et_primary_address)).setText("" + address);
        ((EditText) findViewById(R.id.et_city)).setText("" + city);
        ((EditText) findViewById(R.id.et_state)).setText("" + state);
        ((EditText) findViewById(R.id.et_postal)).setText("" + postalCode);
        String curTime = DateUtils.convertDate(dateOfBirth*1000, DateUtils.DATE_FORMAT_PROFILER);
        ((TextView) findViewById(R.id.et_dateOfbirth)).setText("" + curTime);

        if (!TextUtils.isEmpty(last4)) {
            ((EditText) findViewById(R.id.et_account_number)).setText("****" + last4);
        }
    }

    private Calendar calendarNew;

    private void showDatePickerDialog() {
        DatePickerDialog pickDate = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog,(DatePickerDialog.OnDateSetListener) (view, year, monthOfYear, dayOfMonth) -> {
            calendarNew = Calendar.getInstance();
            calendarNew.set(Calendar.YEAR, year);
            calendarNew.set(Calendar.MONTH, monthOfYear);
            calendarNew.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            String curTime = DateUtils.convertDate(calendarNew.getTimeInMillis(), DateUtils.DATE_FORMAT_PROFILER);
            Log.d("TAG", "onDateSet: "+curTime);
             ((TextView) findViewById(R.id.et_dateOfbirth)).setText(curTime);
        },
                calendarNew.get(Calendar.YEAR), calendarNew.get(Calendar.MONTH), calendarNew.get(Calendar.DAY_OF_MONTH));
        Calendar maxDate = Calendar.getInstance();
        maxDate.set(Calendar.YEAR, maxDate.get(Calendar.YEAR) - 14);
        pickDate.getDatePicker().setMaxDate(maxDate.getTime().getTime());
        pickDate.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pickDate.show();

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                ((EditText) findViewById(R.id.et_primary_address)).setText(place.getName());
                LatLng latLng= place.getLatLng();
                try {
                    getCityNameByCoordinates(latLng.latitude, latLng.longitude);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("places", place.toString());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                //Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

// ...

    private String getCityNameByCoordinates(double lat, double lon) throws IOException {
        Geocoder mGeocoder = new Geocoder(this, Locale.getDefault());

        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
        if (addresses != null && addresses.size() > 0) {
            Log.e("city", ""+addresses.get(0).toString());
            ((EditText) findViewById(R.id.et_city)).setText(addresses.get(0).getLocality());
            ((EditText) findViewById(R.id.et_state)).setText(addresses.get(0).getCountryName());
            ((EditText) findViewById(R.id.et_postal)).setText(addresses.get(0).getPostalCode());
            return addresses.get(0).getLocality();
        }
        return null;
    }
}