package com.apps.ondemand.app.ui.jobs;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.models.LineItemModel;
import com.apps.ondemand.app.data.models.PriceBreakDownModel;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.utils.AppUtils;

import static com.apps.ondemand.common.utils.AppConstants.KEY_PRICE_BREAKDOWN;

import androidx.annotation.RequiresApi;

@RequiresApi(api = Build.VERSION_CODES.S)
public class PriceBreakdownActivity extends BaseActivity implements View.OnClickListener {
    PriceBreakDownModel priceBreakDownModel;
    String JobIdentifier;
    boolean isFixed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_breakdown);
        setActionBar(R.string.str_price_breakdown);
        setSubTitle(R.string.str_price_breakdown_sub);
        isFixed = getIntent().getBooleanExtra("isFixed", false);
        priceBreakDownModel = getIntent().getParcelableExtra(KEY_PRICE_BREAKDOWN);
        JobIdentifier=getIntent().getStringExtra("JobIdentifier");
        initViews();
        setCosting();
    }

    private void initViews() {
        findViewById(R.id.rl_total_item_cost).setOnClickListener(this);
    }

    @SuppressLint("SetTextI18n")
    public void setCosting() {
        findTextViewById(R.id.tv_total_cost).setText(AppUtils.getAmount(priceBreakDownModel.getSpAmount()));
        findTextViewById(R.id.invoiceId).setText("Invoice Id: "+JobIdentifier+"");
        findTextViewById(R.id.tv_job_charges).setText(AppUtils.getAmount(priceBreakDownModel.getTotalJobAmount()));
        findTextViewById(R.id.tv_discount).setText(AppUtils.getAmount(priceBreakDownModel.getDiscountAmount()));
        setLineItemCosting();

        findTextViewById(R.id.tv_job_charges_label).setText(priceBreakDownModel.getServiceName()
                + " (" + AppUtils.getAmount(priceBreakDownModel.getServiceHourlyRate()) + "/hr) x "
                + priceBreakDownModel.getTotalHoursSpent() + ":" + priceBreakDownModel.getTotalMinutesSpent() + " hr "
        );
        if(isFixed){
            findTextViewById(R.id.tv_job_charges_label).setText(priceBreakDownModel.getServiceName());
        }
    }

    private void setLineItemCosting() {
        if (priceBreakDownModel.getLineItems() != null) {
            if (priceBreakDownModel.getLineItems().size() == 0) {
                findViewById(R.id.rl_total_item_cost).setVisibility(View.GONE);
                findViewById(R.id.ll_line_items).setVisibility(View.GONE);
            } else {
                findViewById(R.id.ll_line_items).setVisibility(View.GONE);
                findTextViewById(R.id.tv_total_item_cost).setText(AppUtils.getAmount(priceBreakDownModel.getTotalLineItemAmount()));
                LinearLayout lineItemsWrapper = findViewById(R.id.ll_line_items);
                lineItemsWrapper.removeAllViews();
                for (int i = 0; i < priceBreakDownModel.getLineItems().size(); i++) {
                    LineItemModel lineItemModel = priceBreakDownModel.getLineItems().get(i);
                    inflateLineItem(i + 1, lineItemsWrapper, lineItemModel);
                }

            }
        } else {
            findViewById(R.id.rl_total_item_cost).setVisibility(View.GONE);
            findViewById(R.id.ll_line_items).setVisibility(View.GONE);
        }
    }

    private void inflateLineItem(int itemNo, LinearLayout wrapper, LineItemModel lineItemModel) {
        View lineItem = getLayoutInflater().inflate(R.layout.row_line_item, null);
        TextView label = lineItem.findViewById(R.id.tv_line_item_label);
        TextView cost = lineItem.findViewById(R.id.tv_line_item_cost);
        label.setText(Html.fromHtml(lineItemModel.getItemName() + " <sup>x" + lineItemModel.getItemQuantity() + "</sup>"));
        cost.setText(AppUtils.getAmount(lineItemModel.getItemPrice() * lineItemModel.getItemQuantity()));
        wrapper.addView(lineItem);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_total_item_cost) {
            int visibility = findViewById(R.id.ll_line_items).getVisibility();
            if (visibility == View.GONE) {
                AppUtils.openDropdown(findViewById(R.id.ll_line_items));
                findImageViewById(R.id.iv_total_item_arrow).setImageResource(R.drawable.arrow_up);
            } else {
                AppUtils.closeDropdown(findViewById(R.id.ll_line_items));
                findImageViewById(R.id.iv_total_item_arrow).setImageResource(R.drawable.arrow_down);
            }
        }
    }


}