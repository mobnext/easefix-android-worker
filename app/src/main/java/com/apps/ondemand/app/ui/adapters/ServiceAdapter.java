package com.apps.ondemand.app.ui.adapters;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.LanguageManager;
import com.apps.ondemand.app.data.models.ServiceModel;
import com.apps.ondemand.app.data.models.ServiceStateEnum;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewAdapter;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.BorderErrorEditText;
import com.apps.ondemand.common.utils.GlideUtils;

import java.util.List;


public class ServiceAdapter extends BaseRecyclerViewAdapter {

    BaseActivity context;

    public ServiceAdapter(BaseActivity context, List<BaseItem> items, OnRecyclerViewItemClickListener itemClickListener) {
        super(items, itemClickListener);
        this.context = context;
    }

    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        BaseRecyclerViewHolder holder;
        if (viewType == BaseItem.ITEM_SERVICE) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_service, parent, false);
            holder = new ServiceHolder(view);
        } else if (viewType == BaseItem.ITEM_EMPTY) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
            holder = new EmptyViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_progress, parent, false);
            holder = new ProgressViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder holder, int position) {
        if (holder instanceof ServiceHolder) {
            ServiceHolder serviceHolder = (ServiceHolder) holder;
            ServiceModel serviceModel = (ServiceModel) getItemAt(position);
            GlideUtils.glideLoadImage(context,
                    serviceHolder.ivService,
                    serviceModel.getServiceImage(),
                    (int) context.getResources().getDimension(R.dimen.dimen_50));
            serviceHolder.tvServiceName.setText(serviceModel.getServiceName());
            serviceHolder.tvServiceDesc.setText(serviceModel.getServiceDescription());
            serviceHolder.ivSelected.setSelected(serviceModel.isSelected());
            if (serviceModel.getServiceHourlyRate() == 0) {
                serviceHolder.etHourlyRate.setText("");
                serviceHolder.tvHourlyRate.setText("");
            } else {
                serviceHolder.etHourlyRate.setText(String.valueOf(serviceModel.getServiceHourlyRate()));
                serviceHolder.etHourlyRate.setSelection(serviceHolder.etHourlyRate.getText().length());
                serviceHolder.tvHourlyRate.setText(AppUtils.getAmount(serviceModel.getServiceHourlyRate()) + "/hr");
            }
       /**     if (serviceModel.getState() == ServiceStateEnum.SELECTED) {
                serviceHolder.llHourlyRate.setVisibility(View.VISIBLE);
                serviceHolder.tvHourlyRate.setVisibility(View.GONE);
                serviceHolder.ivEditIcon.setVisibility(View.GONE);
            } else if (serviceModel.getState() == ServiceStateEnum.UNSELECTED) {
                serviceHolder.llHourlyRate.setVisibility(View.GONE);
                serviceHolder.tvHourlyRate.setVisibility(View.GONE);
                serviceHolder.ivEditIcon.setVisibility(View.GONE);
            } else if (serviceModel.getState() == ServiceStateEnum.EDIT) {
                serviceHolder.llHourlyRate.setVisibility(View.VISIBLE);
                serviceHolder.tvHourlyRate.setVisibility(View.GONE);
                serviceHolder.ivEditIcon.setVisibility(View.GONE);
            } else if (serviceModel.getState() == ServiceStateEnum.SAVED) {
                serviceHolder.llHourlyRate.setVisibility(View.GONE);
                serviceHolder.tvHourlyRate.setVisibility(View.VISIBLE);
                serviceHolder.ivEditIcon.setVisibility(View.VISIBLE);
            }
        */
        }

    }

    private class ServiceHolder extends BaseRecyclerViewHolder {

        private CardView llService;
        private ImageView ivSelected;
        private ImageView ivService;
        private TextView tvServiceName;
        private TextView tvServiceDesc;
        private TextView tvHourlyRate;
        private ImageView ivEditIcon;
        private LinearLayout llHourlyRate;
        private BorderErrorEditText etHourlyRate;
        private Button btnSave;


        public ServiceHolder(View view) {
            super(view);
            llService = (CardView) view.findViewById(R.id.ll_card);
            ivSelected = (ImageView) view.findViewById(R.id.iv_selected);
            ivService = (ImageView) view.findViewById(R.id.iv_service);
            tvServiceName = (TextView) view.findViewById(R.id.tv_service_name);
            tvServiceDesc = (TextView) view.findViewById(R.id.tv_service_desc);
            tvHourlyRate = (TextView) view.findViewById(R.id.tv_hourly_rate);
            ivEditIcon = (ImageView) view.findViewById(R.id.iv_edit_icon);
            llHourlyRate = (LinearLayout) view.findViewById(R.id.ll_hourly_rate);
            etHourlyRate = (BorderErrorEditText) view.findViewById(R.id.et_hourly_rate);
            btnSave = (Button) view.findViewById(R.id.btn_save);
            ivSelected.setOnClickListener(this);
            btnSave.setOnClickListener(this);
            ivEditIcon.setOnClickListener(this);
            llService.setOnClickListener(this);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return ServiceHolder.this;
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            AppUtils.hideSoftKeyboard(context);
            int position = getAdapterPosition();
            ServiceModel model = (ServiceModel) getItemAt(position);
            switch (v.getId()) {
                case R.id.ll_card:
                case R.id.iv_selected:
                    if (model.isSelected()) {
                        model.setState(ServiceStateEnum.UNSELECTED);
                        model.setServiceHourlyRate(0);
                        model.setSelected(false);
                    }
                    else {
                        model.setState(ServiceStateEnum.SELECTED);
                        String hourlyRate = "1";
                        model.setServiceHourlyRate(Double.parseDouble(hourlyRate));
                        model.setState(ServiceStateEnum.SAVED);
                        model.setSelected(true);
                    }
                    break;
                case R.id.btn_save:
//                    String hourlyRate = etHourlyRate.getText().toString();
                    String hourlyRate = "1";
                    if (TextUtils.isEmpty(hourlyRate)) {
                        etHourlyRate.setError("");
                        context.showSnackBar(R.string.error_empty_hourly_rate);
                        return;
                    }
                    float rate = Float.parseFloat(hourlyRate);
                    if(rate==0){
                        etHourlyRate.setError("");
                        context.showSnackBar(R.string.error_empty_hourly_rate);
                        return;
                    }
                    model.setServiceHourlyRate(Double.parseDouble(hourlyRate));
                    model.setState(ServiceStateEnum.SAVED);
                    break;
                case R.id.iv_edit_icon:
                    model.setState(ServiceStateEnum.EDIT);
                    break;
            }
            notifyItemChanged(position);
        }
    }

    public class ProgressViewHolder extends BaseRecyclerViewHolder {
        private ProgressViewHolder(View view) {
            super(view);
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    public class EmptyViewHolder extends BaseRecyclerViewHolder {
        private TextView textViewEmpty;

        private EmptyViewHolder(View view) {
            super(view);
            textViewEmpty = view.findViewById(R.id.tv_message);
            textViewEmpty.setText("");
        }

        @Override
        protected BaseRecyclerViewHolder populateView() {
            return this;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getItemAt(position).getItemType();
    }
}
