package com.apps.ondemand.app.ui.authentication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.PopupMenu;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.app.ui.interfaces.ResourceSpanListener;
import com.apps.ondemand.app.ui.main.TermsPrivacyActivity;
import com.apps.ondemand.common.base.TakePhotoActivity;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.BitmapUtils;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.apps.ondemand.common.utils.VizImageView;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.apps.ondemand.common.utils.AppConstants.REGISTER_AS_COMPANY;
import static com.apps.ondemand.common.utils.AppConstants.REGISTER_PERSONAL;

public class SignUpDetailsActivity extends TakePhotoActivity implements View.OnClickListener, VizImageView.OnImageUploadingListener {
    private String profileImage = "";
    private String basicInfoUrl = "";
    int registerAs = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_sign_up_details);
        setActionBar(R.string.str_sign_up);
        setSubTitle(R.string.str_tell_us_about_you);
        initView();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.cv_register_as) {
            showAccountTypeMenu();
        } else if (view.getId() == R.id.btn_sign_up) {
            validateAndCallService();
        } else if (view.getId() == R.id.iv_profile
                || view.getId() == R.id.iv_camera) {
            showChoosePhotoDialog();
        }
    }

    private void showAccountTypeMenu() {
        Context wrapper = new ContextThemeWrapper(this, R.style.PopupStyle);

        PopupMenu popup = new PopupMenu(wrapper, findViewById(R.id.cv_register_as));

        popup.getMenuInflater()
                .inflate(R.menu.account_type_menu, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_personal:
                        setRegisterAsFields(REGISTER_PERSONAL);
                        break;
                    case R.id.menu_company:
                        setRegisterAsFields(REGISTER_AS_COMPANY);
                        break;
                }
                return true;
            }
        });
        popup.show();
    }

    private void setRegisterAsFields(int registerAs) {
        animate();
        this.registerAs = registerAs;
        if (registerAs == REGISTER_PERSONAL) {
            findFieldById(R.id.et_register_as).setText(R.string.str_personal);
            findViewById(R.id.cv_company_name).setVisibility(View.GONE);
        } else if (registerAs == REGISTER_AS_COMPANY) {
            findFieldById(R.id.et_register_as).setText(R.string.str_company);
            findViewById(R.id.cv_company_name).setVisibility(View.VISIBLE);
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        findFieldById(R.id.et_bio).addTextChangedListener(bioTextListener);
        findViewById(R.id.btn_sign_up).setOnClickListener(this);
        findViewById(R.id.cv_register_as).setOnClickListener(this);
        findViewById(R.id.iv_profile).setOnClickListener(this);
        findViewById(R.id.iv_camera).setOnClickListener(this);
        AppUtils.setSpannableTextView(this, R.color.colorPrimaryBlue, R.id.tv_term_conditions, termSpanListener,
                getString(R.string.str_agree_terms_desc) + " " + getString(R.string.str_terms_condition) + " and " + getString(R.string.str_privacy_policy),
                R.string.str_terms_condition, R.string.str_privacy_policy);

       findErrorFieldById(R.id.et_bio).setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (findErrorFieldById(R.id.et_bio).hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            return true;
                    }
                }
                return false;
            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CHOOSE_PHOTO_RESULT_CODE) {
                if (data.getData() != null) {
                    selectedPhotoUri = data.getData();
                } else {
                    selectedPhotoUri = null;
                }
            } else {
                if (requestCode == TAKE_PHOTO_RESULT_CODE) {
                    selectedPhotoUri = BitmapUtils.getImageUri(this, (Bitmap) data.getExtras().get("data"));
                } else {
                    selectedPhotoUri = null;
                }
            }
//            filePath = FileUtils.getPath(this, selectedPhotoUri);
            loadImageInView();
        } else {
            selectedPhotoUri = null;
        }
    }

    private void loadImageInView() {
        if (selectedPhotoUri == null)
            return;

        VizImageView imageView = findViewById(R.id.iv_profile);
        imageView.loadImageInView(imageView.getId(), AppConstants.UPLOAD_PROFILE_IMAGE, selectedPhotoUri, this);
    }

    @Override
    public void onUploadingResponse(boolean isSuccessful, String imageURL, int imageViewID, String message) {
        if (isSuccessful) {
            profileImage = imageURL;
        } else {
            profileImage = "";
        }
    }

    private void validateAndCallService() {
        AppUtils.hideSoftKeyboard(this);
        String firstName = getFieldText(R.id.et_first_name).trim();
        String companyName = getFieldText(R.id.et_company_name).trim();
        String lastName = getFieldText(R.id.et_last_name).trim();
        String email = getFieldText(R.id.et_email).trim();
        String bio = getFieldText(R.id.et_bio).trim();
        String referralCode = getFieldText(R.id.et_referral).trim();
      /*  if (TextUtils.isEmpty(profileImage)) {
            showSnackBar(R.string.error_empty_image);
            return;
        } */
        if (registerAs == REGISTER_AS_COMPANY) {
            if (TextUtils.isEmpty(companyName)) {
                setError(R.id.et_company_name, R.string.error_empty_company_name);
                return;
            }
        }

        if (TextUtils.isEmpty(firstName)) {
            setError(R.id.et_first_name, R.string.error_empty_first_name);
            return;
        }
        if (TextUtils.isEmpty(lastName)) {
            setError(R.id.et_last_name, R.string.error_empty_last_name);
            return;
        }

        if (TextUtils.isEmpty(email)) {
            setError(R.id.et_email, R.string.error_empty_email);
            return;
        }
        if (!AppUtils.isEmailValid(email)) {
            setError(R.id.et_email, R.string.error_invalid_email);
            return;
        }
        if (TextUtils.isEmpty(bio)) {
            setError(R.id.et_bio, R.string.error_empty_bio);
            return;
        }
        boolean isAgreed = ((CheckBox) findViewById(R.id.cb_term_conditions)).isChecked();
        if (!isAgreed) {
            showSnackBar(R.string.error_not_agreed);
            return;
        }

        basicInfoUrl = AppConstants.getServerUrl(AppConstants.BASIC_INFO);

        Map<String, Object> map = new HashMap<>();
        if (registerAs == REGISTER_AS_COMPANY) {
            map.put("companyName", companyName);
        }
        map.put("firstName", firstName);
        map.put("lastName", lastName);
        map.put("email", email);
        map.put("registerAs", registerAs);
        map.put("profileImageUrl", profileImage);
        map.put("referralCode", referralCode);
        map.put("about", bio);

        HttpRequestItem requestItem = new HttpRequestItem(basicInfoUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_POST);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);

    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            JSONObject jsonObjectResponse = new JSONObject(response.getResponse());
            int success = jsonObjectResponse.getInt("success");
            if (success == 1) {
                JSONObject data = jsonObjectResponse.getJSONObject("data");
                if (response.getHttpRequestUrl().equals(basicInfoUrl)) {
                    UserManager.saveUserData(data);
                    moveToNextActivity();
                }
            } else {
                showSnackBar(jsonObjectResponse.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void moveToNextActivity() {
        Intent intent = new Intent(this, SelectServiceActivity.class);
        startActivity(intent);
    }

    ResourceSpanListener termSpanListener = new ResourceSpanListener() {
        @Override
        public void onSpanClicked(int clicked) {
            switch (clicked) {
                case R.string.str_terms_condition:
                    Intent intent = new Intent(SignUpDetailsActivity.this, TermsPrivacyActivity.class);
                    intent.putExtra(AppConstants.KEY_SHOW_TERMS, true);
                    startActivity(intent);
                    break;
                case R.string.str_privacy_policy:
                    intent = new Intent(SignUpDetailsActivity.this, TermsPrivacyActivity.class);
                    intent.putExtra(AppConstants.KEY_SHOW_TERMS, false);
                    startActivity(intent);
                    break;
                case R.string.str_sign_in:
                    intent = new Intent(SignUpDetailsActivity.this, AuthenticationActivity.class);
                    intent.putExtra(AppConstants.KEY_EXTRA_IS_SIGN_UP, false);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    break;
            }
        }
    };
    TextWatcher bioTextListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            findTextViewById(R.id.tv_bio_limit).setText(
                    s.length() + "/" + getResources().getInteger(R.integer.desc_words_limit));
        }
    };
}
