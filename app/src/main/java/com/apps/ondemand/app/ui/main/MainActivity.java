package com.apps.ondemand.app.ui.main;

import static com.apps.ondemand.common.utils.AppConstants.KEY_SHOW_TERMS;
import static com.apps.ondemand.common.utils.AppConstants.STATUS_OFFLINE;
import static com.apps.ondemand.common.utils.AppConstants.STATUS_ONLINE;
import static com.apps.ondemand.common.utils.AppConstants.TYPE_SP;
import static com.apps.ondemand.common.utils.AppConstants.TYPE_USER;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.apps.ondemand.BuildConfig;
import com.apps.ondemand.MyApplication;
import com.apps.ondemand.R;
import com.apps.ondemand.app.calling.sendBird.utils.AuthenticationUtils;
import com.apps.ondemand.app.calling.sendBird.utils.BroadcastUtils;
import com.apps.ondemand.app.communication.SocketService;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.app.data.models.JobRequestModel;
import com.apps.ondemand.app.data.models.TermsPrivacyModel;
import com.apps.ondemand.app.data.preferences.SharedPreferenceManager;
import com.apps.ondemand.app.ui.SplashActivity;
import com.apps.ondemand.app.ui.authentication.SelectLanguageActivity;
import com.apps.ondemand.app.ui.chat.ChatActivity;
import com.apps.ondemand.app.ui.dialogs.DialogJobCompleted;
import com.apps.ondemand.app.ui.dialogs.DialogJobRequest;
import com.apps.ondemand.app.ui.dialogs.DialogTermsAndPrivacy;
import com.apps.ondemand.app.ui.earnings.EarningActivity;
import com.apps.ondemand.app.ui.interfaces.TermsPrivacyListener;
import com.apps.ondemand.app.ui.myAllBids.AllBidsActivity;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.apps.ondemand.common.utils.VizImageView;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

@RequiresApi(api = Build.VERSION_CODES.S)
public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {


    private int selectedItemId = 0;
    private boolean isItemSelected = false;
    TextView tvOnline;
    @SuppressLint("UseSwitchCompatOrMaterialCode")
    Switch switchOnline;
    private String logoutUrl = "";
    private String fetchProfileUrl = "";
    private String lastUnRatedJobUrl = "";
    private String notificationCountUrl = "";
    private String setSpStatusUrl = "";
    private String getSpStatusUrl = "";
    private String getPrivacyAndTermsUrl = "";
    private String updatePrivacyAndTermsUrl = "";
    private String checkVersionUrl = "";
    DialogTermsAndPrivacy termsAndPrivacy;
    private String lastUnHandledJobUrl = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_main);
        setActionBar(R.string.str_jobs, false);
        setSubTitle(R.string.str_jobs_sub);
        setBackStackListener();

        populateView();
        setDrawerToggle();
        checkPermissions();
        initView();
        onAddFragment(new HomeFragment(), R.id.content_frame);
        setUpBasicInfo();
        getSpStatus();

        if (getIntent().hasExtra(AppConstants.FROM_WHICH_COMPONENT) &&
                getIntent().getStringExtra(AppConstants.FROM_WHICH_COMPONENT).equalsIgnoreCase(AppConstants.COMPONENT_GCM)) {
            handlePushEvent(getIntent());
        }
        //        sendBird
        authenticateSend();
        registerReceiver();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra(AppConstants.FROM_WHICH_COMPONENT) &&
                intent.getStringExtra(AppConstants.FROM_WHICH_COMPONENT).equalsIgnoreCase(AppConstants.COMPONENT_GCM)) {
            handlePushEvent(intent);
        }
    }

    private void handlePushEvent(Intent intent) {
        String eventType = intent.getStringExtra(SocketService.BROADCAST_INTENT_TYPE);
        String jobId = intent.getStringExtra(SocketService.BROADCAST_JOB_ID);
        if (!AppUtils.ifNotNullEmpty(jobId))
            return;
        if (!eventType.equalsIgnoreCase(SocketService.KEY_JOB_CHAT))
            return;

        Intent chatIntent = new Intent(this, ChatActivity.class);
        chatIntent.putExtra(AppConstants.KEY_JOB_ID, jobId);
        startActivity(chatIntent);
    }

    @Override
    public void onBindService(SocketService mService) {
        super.onBindService(mService);
        this.mService = mService;
        if (getCurrentFragment() != null && getCurrentFragment() instanceof HomeFragment) {
            HomeFragment homeFragment = (HomeFragment) getCurrentFragment();
            homeFragment.emitLocation(mService);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (UserManager.isVerifiedByAdmin()) {
            fetchProfile();
            getNotificationCount();
            // sequential calls
            // 1.check app version update
            // 2.terms and policy update
            // 3.last unhandled job
            // 4.last unrated job
            checkAppVersion();
        }
    }

    @Override
    public void updateHomeFragment() {
        super.updateHomeFragment();
        if (getCurrentFragment() != null && getCurrentFragment() instanceof HomeFragment) {
            HomeFragment homeFragment = (HomeFragment) getCurrentFragment();
            homeFragment.clearData();
            homeFragment.fetchScheduleJobsData();
            if (approved == 1) {
                homeFragment.setApproved();
            }
        }
    }


    private void setUpBasicInfo() {
        RelativeLayout spProfileWrapper = findViewById(R.id.sp_profile_wrapper);
        ((TextView) spProfileWrapper.findViewById(R.id.tv_profile_name)).setText(UserManager.getUserName());
        VizImageView imageView = spProfileWrapper.findViewById(R.id.iv_profile_image);
        ImageView certificateVerifiedImg = spProfileWrapper.findViewById(R.id.certificateVerifiedImg);
        if (UserManager.getIsCertificateVerified()) {
            certificateVerifiedImg.setVisibility(View.VISIBLE);
        } else {
            certificateVerifiedImg.setVisibility(View.GONE);
        }
        if (!(MainActivity.this).isFinishing()) {
        imageView.setImage(UserManager.getImage(), false);
        }
        float rating = 0.0f;
        try {
            rating = Float.parseFloat(UserManager.getAverageRating());
        } catch (Exception e) {
            e.printStackTrace();
        }
        ((MaterialRatingBar) spProfileWrapper.findViewById(R.id.rb_profile_rating)).setRating(rating);
        ((TextView) spProfileWrapper.findViewById(R.id.tv_profile_rating)).setText(AppUtils.formatRating(rating));
    }


    private void populateView() {
        mDrawerLayout = findViewById(R.id.drawer_layout);
        NavigationView mNavigationView = findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        mNavigationView.setItemIconTintList(null);
        mNavigationView.setCheckedItem(R.id.home);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        NavigationView mNavigationView = findViewById(R.id.nav_view);
        mNavigationView.setCheckedItem(item.getItemId());
        selectedItemId = item.getItemId();
        isItemSelected = true;
        mDrawerLayout.closeDrawers();
        return true;
    }

    @Override
    public void onDrawerClosed() {
        super.onDrawerClosed();
        if (!isItemSelected)
            return;
        isItemSelected = false;
        if (selectedItemId == R.id.home) {
            clearContentView();
            onReplaceFragment(new Fragment(), true);
        }
    }

    // Called to remove all previously loaded fragments from the backStack along with
    // clearing the content view that loads the fragments(For memory crashing issues)
    private void clearContentView() {
        onClearBackStack();
        ((ViewGroup) findViewById(R.id.content_frame)).removeAllViews();
    }

    private void fetchProfile() {
        fetchProfileUrl = AppConstants.getServerUrl(String.format(AppConstants.FETCH_SP_PROFILE, UserManager.getUserId()));
        HttpRequestItem requestItem = new HttpRequestItem(fetchProfileUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(null, this);
        appNetworkTask.execute(requestItem);
    }



    private void getLastUnHandleJob() {
        lastUnHandledJobUrl = AppConstants.getServerUrl(AppConstants.FETCH_LAST_UNHANDLED_JOB);

        Map<String, Object> map = new HashMap<>();
        map.put("userType", TYPE_SP);
        HttpRequestItem requestItem = new HttpRequestItem(lastUnHandledJobUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(null, this);
        appNetworkTask.execute(requestItem);
    }

    private void getLastUnRatedJob() {
        lastUnRatedJobUrl = AppConstants.getServerUrl(AppConstants.FETCH_LAST_UNRATED_JOB);

        Map<String, Object> map = new HashMap<>();
        map.put("userType", TYPE_SP);
        HttpRequestItem requestItem = new HttpRequestItem(lastUnRatedJobUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(null, this);
        appNetworkTask.execute(requestItem);
    }

    private void getNotificationCount() {
        notificationCountUrl = AppConstants.getServerUrl(AppConstants.GET_NOTIFICATION_COUNT);

        Map<String, Object> map = new HashMap<>();
        map.put("userType", TYPE_SP);
        HttpRequestItem requestItem = new HttpRequestItem(notificationCountUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(null, this);
        appNetworkTask.execute(requestItem);
    }

    private void getSpStatus() {
        getSpStatusUrl = AppConstants.getServerUrl(AppConstants.GET_SP_STATUS);

        Map<String, Object> map = new HashMap<>();
        map.put("userType", TYPE_USER);
        HttpRequestItem requestItem = new HttpRequestItem(getSpStatusUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    private void setOnlineStatus(int status) {
        setSpStatusUrl = AppConstants.getServerUrl(AppConstants.SET_SP_STATUS);

        Map<String, Object> map = new HashMap<>();
        map.put("spStatus", status);
        HttpRequestItem requestItem = new HttpRequestItem(setSpStatusUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }


    private void logout() {

        logoutUrl = AppConstants.getServerUrl(AppConstants.LOG_OUT);
        HttpRequestItem requestItem = new HttpRequestItem(logoutUrl);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());    // Add header params (Cookies)
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false),
                this);
        appNetworkTask.execute(requestItem);
    }


    private void callRateWorker(String jobId, String userProfileId, float rating, String review) {
        String rateUserUrl = AppConstants.getServerUrl(AppConstants.RATE_USER);

        Map<String, Object> map = new HashMap<>();
        map.put("review", review);
        map.put("rating", rating);
        map.put("jobId", jobId);
        map.put("userProfileId", userProfileId);

        HttpRequestItem requestItem = new HttpRequestItem(rateUserUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_PUT);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);

    }

    private void checkAppVersion() {
        checkVersionUrl = AppConstants.getServerUrl(AppConstants.CHECK_APP_VERSION);

        Map<String, Object> map = new HashMap<>();
        map.put("versionCode", BuildConfig.VERSION_CODE);
        map.put("deviceType", AppConstants.DEVICE_TYPE);
        map.put("userType", AppConstants.TYPE_SP);

        HttpRequestItem requestItem = new HttpRequestItem(checkVersionUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_PUT);
        requestItem.setParams(map);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(null, this);
        appNetworkTask.execute(requestItem);

    }

    private void getPrivacyAndTerms() {
        getPrivacyAndTermsUrl = AppConstants.getServerUrl(AppConstants.GET_PRIVACY_AND_TERMS);
        HttpRequestItem requestItem = new HttpRequestItem(getPrivacyAndTermsUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(null, this);
        appNetworkTask.execute(requestItem);
    }

    private void updateTermsAndCondition(boolean isAgreedToTerms, boolean
            isAgreedToPrivacy) {
        updatePrivacyAndTermsUrl = AppConstants.getServerUrl(AppConstants.UPDATE_PRIVACY_AND_TERMS);

        Map<String, Object> map = new HashMap<>();
        map.put("isTermAndConditionUpdate", isAgreedToTerms);
        map.put("isPrivacyPolicyUpdate", isAgreedToPrivacy);

        HttpRequestItem requestItem = new HttpRequestItem(updatePrivacyAndTermsUrl);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_PUT);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        requestItem.setParams(map);
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            JSONObject responseJson = new JSONObject(response.getResponse());
            if (responseJson.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equals(logoutUrl)) {
                    deAuthenticate();
                    stopService(new Intent(this, SocketService.class));
                    SharedPreferenceManager.getInstance().clearPreferences();
                    startActivity(new Intent(this, SplashActivity.class));
                    finish();
                } else if (response.getHttpRequestUrl().equals(setSpStatusUrl)) {
                    boolean isOnline = switchOnline.isChecked();
                    UserManager.setOnline(isOnline);
                    handleForeGroundService(isOnline);
                } else if (response.getHttpRequestUrl().equals(getSpStatusUrl)) {
                    JSONObject data = responseJson.getJSONObject("data");
                    boolean isOnline = data.getBoolean("isSpOnline");
                    setStatusData(isOnline);
                    UserManager.setOnline(isOnline);
                    handleForeGroundService(isOnline);
                } else if (response.getHttpRequestUrl().equals(fetchProfileUrl)) {
                    JSONObject data = responseJson.getJSONObject("data");
                    UserManager.saveUserData(data);
                    setUpBasicInfo();
                } else if (response.getHttpRequestUrl().equals(notificationCountUrl)) {
                    JSONObject data = responseJson.getJSONObject("data");
                    int notificationCount = data.getInt("notificationCount");
                    if (notificationCount > 0) {
                        findViewById(R.id.drawer_notification_badge).setVisibility(View.VISIBLE);
                        findTextViewById(R.id.drawer_notification_badge).setText(String.valueOf(notificationCount));
                    } else {
                        findTextViewById(R.id.drawer_notification_badge).setVisibility(View.GONE);
                    }
                    updateBadgeCount(notificationCount > 0, notificationCount);
                } else if (response.getHttpRequestUrl().equals(checkVersionUrl)) {
                    JSONObject data = responseJson.getJSONObject("data");
                    boolean forcefullyUpdateRequired = data.getBoolean("forcefullyUpdateRequired");
                    if (forcefullyUpdateRequired) {
                        showUpdateDialog();
                    } else {
                        // after checking version update now check terms and condition
                        getPrivacyAndTerms();
                    }
                } else if (response.getHttpRequestUrl().equals(getPrivacyAndTermsUrl)) {
                    JSONObject data = responseJson.getJSONObject("data");
                    TermsPrivacyModel termsPrivacyModel = new Gson().fromJson(data.toString(), TermsPrivacyModel.class);
                    if (termsPrivacyModel.getIsPrivacyPolicyUpdated() || termsPrivacyModel.getIsTermAndConditionUpdated()) {
                        if (checkForTermsPrivacyUpdates()) {
                            termsAndPrivacy = DialogTermsAndPrivacy.newInstance(this, termsPrivacyModel, termsPrivacyListener);
                            termsAndPrivacy.show();
                        }
                    } else {
                        // after checking term/privacy update now unhandled job
                        getLastUnHandleJob();
                    }
                } else if (response.getHttpRequestUrl().equals(lastUnHandledJobUrl)) {
                    JSONObject data = responseJson.getJSONObject("data");
                    boolean isUnhandledJobExist = data.getBoolean("isUnhandledJobExist");
                    if (isUnhandledJobExist) {
                        JobRequestModel jobRequestModel = new Gson().fromJson(data.getJSONObject("unhandledJobData").toString(), JobRequestModel.class);
                        //jobRequestListener BaseActivity listener to handle last job request
                        DialogJobRequest.newInstance(this, jobRequestModel, jobRequestListener).show();
                    } else {
                        // after checking last job now check last unrated job
                        getLastUnRatedJob();
                    }
                } else if (response.getHttpRequestUrl().equals(lastUnRatedJobUrl)) {
                    JSONObject data = responseJson.getJSONObject("data");
                    boolean isUnratedJobExist = data.getBoolean("isUnratedJobExist");
                    if (isUnratedJobExist) {
                        final String jobId = data.getString("jobId");
                        final String userProfileId = data.getString("userProfileId");
                        String profileName = data.getString("name");
                        String profileImage = data.getString("profileImage");
                        float avgRating = (float) data.getDouble("avgRating");
                        if (checkForTermsPrivacyUpdates()) {
                            DialogJobCompleted.newInstance(this,
                                    profileImage,
                                    profileName,
                                    avgRating,
                                    (rating, reviewStr) -> callRateWorker(jobId, userProfileId, rating, reviewStr)).show(jobId);
                        }
                    }
                } else if (response.getHttpRequestUrl().equals(updatePrivacyAndTermsUrl)) {
                    showSnackBar(responseJson.getString("message"));
                }
            } else
                showSnackBar(responseJson.getString("message"));
        } catch (JSONException e) {
            Logger.error(false, e);
        }
    }

    private void openHelpDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        dialog.setContentView(R.layout.dialog_help);
        TextView emergency = dialog.findViewById(R.id.emergerncy);
        TextView email = dialog.findViewById(R.id.tv_email);
        TextView phone = dialog.findViewById(R.id.tv_phone);
        String text = "Note: In case of Emergency always call ";
        String text2 = text + "999";

        Spannable spannable = new SpannableString(text2);
        spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#3254d7")), text.length(), (text + "999").length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        emergency.setText(spannable, TextView.BufferType.SPANNABLE);

        SpannableString content = new SpannableString("info@easefix.com");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        email.setText(content);

        content = new SpannableString("+44 7897 012234");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        phone.setText(content);

        email.setOnClickListener(view ->{
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.setData(Uri.parse("mailto:"+email.getText().toString()));
            startActivity(Intent.createChooser(emailIntent, "Send Email"));
            dialog.dismiss();
        });
        phone.setOnClickListener(view ->{
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone.getText().toString(), null));
            startActivity(intent);
            dialog.dismiss();
        });
        emergency.setOnClickListener(view ->{
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "999", null));
            startActivity(intent);
            dialog.dismiss();
        });
        dialog.findViewById(R.id.btn_close).setOnClickListener(view -> dialog.dismiss());
        dialog.show();
    }

    @SuppressLint("RtlHardcoded")
    @Override
    public void onClick(View view) {
        boolean closeDrawer = false;
        Intent intent = null;
        if (view.getId() == R.id.ll_home) {
            closeDrawer = true;
        } else if (view.getId() == R.id.ll_help) {
            closeDrawer = true;
            openHelpDialog();
        } else if (view.getId() == R.id.ll_myJobs) {
            intent = new Intent(this, AllJobsActivity.class);
            closeDrawer = true;
        } else if (view.getId() == R.id.ll_myBids) {
            intent = new Intent(this, AllBidsActivity.class);
            closeDrawer = true;
        } else if (view.getId() == R.id.ll_notification) {
            intent = new Intent(this, NotificationsActivity.class);
            closeDrawer = true;
        } else if (view.getId() == R.id.ll_my_profile) {
            intent = new Intent(this, MyProfileActivity.class);
            closeDrawer = true;
        } else if (view.getId() == R.id.ll_earnings) {
            intent = new Intent(this, EarningActivity.class);
            closeDrawer = true;
        } else if (view.getId() == R.id.ll_language) {
            intent = new Intent(this, SelectLanguageActivity.class);
            closeDrawer = true;
        } else if (view.getId() == R.id.ll_share) {
            intent = new Intent(this, ShareDiscountActivity.class);
            closeDrawer = true;
        } else if (view.getId() == R.id.ll_identity_doc) {
            intent = new Intent(this, IdentityDocActivity.class);
            closeDrawer = true;
        } else if (view.getId() == R.id.ll_portfolio) {
            intent = new Intent(this, PortfolioActivity.class);
            closeDrawer = true;
        } else if (view.getId() == R.id.ll_payment) {
            intent = new Intent(this, PaymentActivity.class);
            closeDrawer = true;
        } else if (view.getId() == R.id.ll_services) {
            intent = new Intent(this, ServicesActivity.class);
            closeDrawer = true;
        } else if (view.getId() == R.id.ll_service_cert) {
            intent = new Intent(this, ServiceCertificateActivity.class);
            closeDrawer = true;
        } else if (view.getId() == R.id.ll_personal) {
            intent = new Intent(this, PersonalDetailsActivity.class);
            closeDrawer = true;
        } else if (view.getId() == R.id.ll_terms) {
            intent = new Intent(this, TermsPrivacyActivity.class);
            intent.putExtra(KEY_SHOW_TERMS, true);
            closeDrawer = true;
        } else if (view.getId() == R.id.ll_privacy) {
            intent = new Intent(this, TermsPrivacyActivity.class);
            intent.putExtra(KEY_SHOW_TERMS, false);
            closeDrawer = true;
        } else if (view.getId() == R.id.ll_logout) {
            closeDrawer = true;
            logout();
        }
        if (closeDrawer) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
            if (intent != null) {
                startActivity(intent);
            }
        }
    }

    private void initView() {
        findViewById(R.id.ll_help).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_home).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_myJobs).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_myBids).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_notification).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_my_profile).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_earnings).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_language).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_share).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_identity_doc).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_portfolio).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_payment).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_services).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_service_cert).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_personal).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_terms).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_privacy).setOnClickListener(MainActivity.this);
        findViewById(R.id.ll_logout).setOnClickListener(MainActivity.this);
        tvOnline = findViewById(R.id.tv_online);
        switchOnline = findViewById(R.id.switch_online);
        switchOnline.setOnCheckedChangeListener(onCheckChangeListener);
    }

    public void setStatusData(boolean isOnline) {
        if (switchOnline == null || tvOnline == null) {
            return;
        }
        switchOnline.setOnCheckedChangeListener(null);
        switchOnline.setChecked(isOnline);
        switchOnline.setOnCheckedChangeListener(onCheckChangeListener);
        if (isOnline) {
            tvOnline.setText(R.string.str_online);
        } else {
            tvOnline.setText(R.string.str_offline);
        }
    }

    @SuppressLint("ObsoleteSdkInt")
    public void showUpdateDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name))
                .setMessage(R.string.str_update_message)
                .setPositiveButton(R.string.str_update, (dialog, which) -> {
                    // continue with delete
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(AppConstants.PLAY_STORE_URL_PREFIX + getPackageName()));
                    startActivity(i);
                })
                .setCancelable(true)
                .show();
    }

    public boolean checkForTermsPrivacyUpdates() {
        return (termsAndPrivacy == null || !termsAndPrivacy.isShowing());
    }

    TermsPrivacyListener termsPrivacyListener = this::updateTermsAndCondition;
    CompoundButton.OnCheckedChangeListener onCheckChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                tvOnline.setText(R.string.str_online);
                setOnlineStatus(STATUS_ONLINE);
            } else {
                tvOnline.setText(R.string.str_offline);
                setOnlineStatus(STATUS_OFFLINE);
            }
        }
    };
    int approved = 0;

    @Override
    public void onEventReceived(Intent intent) {
        super.onEventReceived(intent);
        try {
            String action = intent.getStringExtra(SocketService.BROADCAST_INTENT_TYPE);

            if (action.equalsIgnoreCase(SocketService.KEY_ADMIN_APPROVED)) {
                updateHomeFragment();
                approved = 1;

            } else if (action.equalsIgnoreCase(SocketService.KEY_ADMIN_JOB_COMPLETE)) {
                updateHomeFragment();
            }
            else {
                getNotificationCount();
            }

        } catch (Exception e) {
            Logger.caughtException(e);
        }
    }


//SendBirdcall
    protected void authenticateSend() {
        String appId;
        String userId;
        String accessToken = "";
        appId = MyApplication.APP_ID;
        userId = UserManager.getUserId();
        if (!TextUtils.isEmpty(appId) && !TextUtils.isEmpty(userId) && ((MyApplication) getApplication()).initSendBirdCall(appId)) {
            AuthenticationUtils.authenticate(this, userId, accessToken, isSuccess -> {
                if (isSuccess) {
                    info();
                    Log.d(MyApplication.TAG, "authenticateSend: ");
                } else {
                    authenticateSend();
                }
            });


        }
    }
    private BroadcastReceiver mReceiver;
    private static final String[] MANDATORY_PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,   // for VoiceCall and VideoCall
            Manifest.permission.CAMERA          // for VideoCall
    };
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;

    protected void registerReceiver() {
        Log.i(MyApplication.TAG, "[MainActivity] registerReceiver()");

        if (mReceiver != null) {
            return;
        }

        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i(MyApplication.TAG, "[MainActivity] onReceive()");
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BroadcastUtils.INTENT_ACTION_ADD_CALL_LOG);
        registerReceiver(mReceiver, intentFilter);


    }

    protected void checkPermissions() {
        ArrayList<String> deniedPermissions = new ArrayList<>();
        for (String permission : MANDATORY_PERMISSIONS) {
            if (checkCallingOrSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                deniedPermissions.add(permission);
            }
        }

        if (deniedPermissions.size() > 0) {
            requestPermissions(deniedPermissions.toArray(new String[0]), REQUEST_PERMISSIONS_REQUEST_CODE);
        }

    }
    private void info(){


    }
}
