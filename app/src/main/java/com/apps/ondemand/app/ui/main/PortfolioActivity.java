package com.apps.ondemand.app.ui.main;

import static com.apps.ondemand.common.utils.AppConstants.DELETE_PORTFOLIO_IMAGE;
import static com.apps.ondemand.common.utils.AppConstants.UPLOAD_PORTFOLIO_IMAGE;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.ondemand.R;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.app.data.models.PortfolioModel;
import com.apps.ondemand.app.ui.adapters.PortfolioAdapter;
import com.apps.ondemand.app.ui.videoUpload.Util;
import com.apps.ondemand.common.base.TakePhotoVideoActivity;
import com.apps.ondemand.common.base.recycler_view.BaseRecyclerViewHolder;
import com.apps.ondemand.common.base.recycler_view.OnRecyclerViewItemClickListener;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.BitmapUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.apps.ondemand.common.utils.VizImageVideoView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.iceteck.silicompressorr.SiliCompressor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.net.URISyntaxException;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiresApi(api = Build.VERSION_CODES.S)
public class PortfolioActivity extends TakePhotoVideoActivity implements View.OnClickListener, VizImageVideoView.OnImageVideoUploadingListener, OnRecyclerViewItemClickListener {
    private String fetchPortfolio = "";
    private String deletePortfolio = "";
    private String videoUploadResponse = "";
    List<BaseItem> serviceModels = new ArrayList<>();
    List<BaseItem> portfolioModelsArrayList = new ArrayList<>();
    private PortfolioAdapter portfolioAdapter;
    private RecyclerView recyclerView;
    private boolean isCommingFromActivityResult = false;
    int type = 2;
    ArrayList<Uri> imagesEncodedList = new ArrayList<Uri>();
    int cout = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portfolio);
        setActionBar(R.string.str_portfolio);
        setSubTitle(R.string.str_img_video);
        initView();
        fetchDocumentImages();
        initVideoThings();
    }

    @Override
    public void onClick(View v) {
        isCommingFromActivityResult = true;
        showChoosePhotoDialog();
    }

    int j = 0;
    int secondCount = 0;

    @Override
    public void onUploadingResponse(boolean isSuccessful, String imageURL, int imageViewID, String message) {
        if (isSuccessful) {
            if (j < secondCount) {
                try {
                    inputPath = Util.getFilePath(this, imagesEncodedList.get(j));
                    tv_output.setText(inputPath);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                selectedPhotoUri = imagesEncodedList.get(j);
                findViewById(R.id.et_add_attachment).setVisibility(View.VISIBLE);
                loadImageInView();
                j++;
            } else {
                serviceModels.clear();
                fetchDocumentImages();
            }
        }

    }

    private void initView() {
        findViewById(R.id.btn_add).setOnClickListener(this);
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
    }

    File mediaFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/easefix.mp4");
        boolean value=false;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CHOOSE_MULTIPLE_RESULT_CODE) {
                ClipData cd = data.getClipData();
                if (cd==null){
                    selectedPhotoUri = data.getData();
                    imagesEncodedList.add(selectedPhotoUri);
                }else
                {
                    if (data.getClipData() != null && data.getClipData().getItemCount() < 6) {
                    value=false;
                    ClipData mClipData = data.getClipData();
                    cout = data.getClipData().getItemCount();
                    int ido = secondCount;
                    secondCount = cout + secondCount;
                    Uri imageUrl;
                    for (int i = 0; i < cout; i++) {
                        // adding imageuri in array
                        imageUrl = data.getClipData().getItemAt(i).getUri();
                        imagesEncodedList.add(imageUrl);
                    }

                    selectedPhotoUri = data.getClipData().getItemAt(0).getUri();
                    j++;

                    try {
                        inputPath = Util.getFilePath(this, data.getClipData().getItemAt(0).getUri());
                        tv_output.setText(inputPath);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                }
                else if (data.getClipData().getItemCount() > 6) {
                    showToast("Max of 5 Images or videos can be selected");
                    value=true;
                } else
                    selectedPhotoUri = null;}
            } else {
                if (requestCode == TAKE_PHOTO_RESULT_CODE) {
                    selectedPhotoUri = BitmapUtils.getImageUri(this, (Bitmap) data.getExtras().get("data"));
                } else if (requestCode == REQUEST_VIDEO_CAPTURE) {
                    selectedPhotoUri = data.getData();
                    try {
                        inputPath = Util.getFilePath(this, selectedPhotoUri);
                        tv_output.setText(inputPath);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                } else selectedPhotoUri = null;
            }
            if (!value){
            loadImageInView();
            }
        } else {
            selectedPhotoUri = null;
        }
        isCommingFromActivityResult = true;
    }

    private void loadImageInView() {
        if (selectedPhotoUri == null)
            return;
        VizImageVideoView imageView = findViewById(R.id.et_add_attachment);
        imageView.setVisibility(View.VISIBLE);
        String mimeType = getContentResolver().getType(selectedPhotoUri);
        Log.d("mimiType", "loadImageInView: " + mimeType);
        if (mimeType.equalsIgnoreCase("image/jpg") || mimeType.equalsIgnoreCase("image/jpeg") || mimeType.equalsIgnoreCase("image/png")) {
            imageView.loadImageVideoInView(imageView.getId(), UPLOAD_PORTFOLIO_IMAGE, selectedPhotoUri, this);
            type = 0;
        } else {
            type = 1;
            compressFolder();
        }

    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        super.onNetworkSuccess(response);
        try {
            JSONObject responseJson = new JSONObject(response.getResponse());
            if (responseJson.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equalsIgnoreCase(fetchPortfolio)) {
                    JSONArray services = responseJson.getJSONObject("data").getJSONArray("portfolio");
                    if (services.length() != 0) {
                        recyclerView.setVisibility(View.VISIBLE);
                        serviceModels = new Gson().fromJson(services.toString(), new TypeToken<List<PortfolioModel>>() {
                        }.getType());
                        populateList(serviceModels);
                    } else {
                        setEmptyLayout();
                    }

                }
                else if (response.getHttpRequestUrl().equalsIgnoreCase(deletePortfolio)) {
                    serviceModels.clear();
                    fetchDocumentImages();
                }
                else if (response.getHttpRequestUrl().equalsIgnoreCase(videoUploadResponse)) {
                    if (j < secondCount) {
                        try {
                            inputPath = Util.getFilePath(this, imagesEncodedList.get(j));
                            tv_output.setText(inputPath);
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }
                        selectedPhotoUri=imagesEncodedList.get(j);
                        loadImageInView();
                        j++;
                    } else {
                        serviceModels.clear();
                        fetchDocumentImages();
                    }

                }
            } else
                showSnackBar(responseJson.getString("message"));
        } catch (JSONException e) {
            Logger.error(false, e);
        }
    }

    private void fetchDocumentImages() {
        fetchPortfolio = AppConstants.getServerUrl(AppConstants.FETCH_PORTFOLIO_LIST);
        HttpRequestItem requestItem = new HttpRequestItem(fetchPortfolio);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_GET);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    private void populateList(List<BaseItem> itemList) {
        if (portfolioAdapter == null) {
            portfolioAdapter = new PortfolioAdapter(this, itemList, this);
            recyclerView.setItemAnimator(null);
            recyclerView.setAdapter(portfolioAdapter);
        } else {

            portfolioAdapter.clearItems();
            portfolioAdapter.addAll(itemList);
        }
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);

    }

    private void setEmptyLayout() {
        if (recyclerView.getContext() == null)
            return;
        if (portfolioAdapter == null || portfolioAdapter.getItemCount() == 0) {
//            List<BaseItem> itemList = new ArrayList<>();
//            String message = getString(R.string.str_no_past_job);
//            itemList.add(new PortfolioModel());
            findViewById(R.id.et_add_attachment).setVisibility(View.GONE);
//            populateList(itemList);
        }
        recyclerView.setVisibility(View.GONE);
        findViewById(R.id.et_add_attachment).setVisibility(View.GONE);
    }

    @Override
    public void onRecyclerViewItemClick(BaseRecyclerViewHolder holder) {

    }

    @Override
    public void onRecyclerViewChildItemClick(BaseRecyclerViewHolder holder, int resourceId) {
        switch (resourceId) {
            case R.id.deleteImg:
                PortfolioModel jobModel = (PortfolioModel) portfolioAdapter.getItemAt(holder.getAdapterPosition());
                animate();
                deleteImages(jobModel.getId());
                break;
            case R.id.iv_upload_image_port:
                moveToFullScreenDocument(holder);
                break;
        }
    }


    private void moveToFullScreenDocument(BaseRecyclerViewHolder holder) {
        int num = holder.getAdapterPosition();
        Intent intent = new Intent(this, ImageActivity.class);
        intent.putExtra("type", 1);
        intent.putExtra("holderPosition", num);
        intent.putExtra("ImageVideo", (Serializable) serviceModels);
        startActivity(intent);
    }

    ////////
    private TextView tv_output;

    private void initVideoThings() {
        tv_output = (TextView) findViewById(R.id.tv_output);
    }

    private String outputDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    private String inputPath;
    private RelativeLayout pb_compress;

    public void compressFolder() {
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
        // Create compress video method
        new CompressVideo().execute("false", selectedPhotoUri.toString(), file.getPath());
//        pb_compress = findViewById(R.id.progress_circular);
//        String destPath = outputDir + File.separator + "VID_" + new SimpleDateFormat("yyyyMMdd_HHmmss", getLocale()).format(new Date()) + ".mp4";
//        VideoCompress.compressVideoLow(tv_output.getText().toString(), destPath, new VideoCompress.CompressListener() {
//            @Override
//            public void onStart() {
//                showToast("please wait while video compressing and uploading");
//                pb_compress.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void onSuccess() {
//                pb_compress.setVisibility(View.GONE);
//                showSnackBar("compress complete uploading wait...");
//                uploadImageToServer(destPath);
////                uploadVideo(destPath);
//            }
//
//            @Override
//            public void onFail() {
//                showSnackBar("failed to upload");
//                pb_compress.setVisibility(View.GONE);
//                Log.d("TAG", "onFail: Failed");
//            }
//
//            @Override
//            public void onProgress(float percent) {
//                pb_compress.setVisibility(View.VISIBLE);
////                tv_progress.setText(String.valueOf(percent) + "%");
//            }
//        });
    }

    public void uploadImageToServer(String filePath) {
        videoUploadResponse = AppConstants.getServerUrl(String.format(AppConstants.UPLOAD_PORTFOLIO_IMAGE, 1, UserManager.getUserId()));
        HttpRequestItem requestItem = new HttpRequestItem(videoUploadResponse);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_MULTIPART2);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        Map<String, Object> data = new HashMap<>();
        data.put("path", filePath);
        requestItem.setParams(data);
        requestItem.setHttpRequestTimeout(NetworkUtils.HTTP_MULTIPART_TIMEOUT);
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    /////////
    private void deleteImages(String filePath) {
        deletePortfolio = AppConstants.getServerUrl(DELETE_PORTFOLIO_IMAGE);
        HttpRequestItem requestItem = new HttpRequestItem(deletePortfolio);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_POST);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        Map<String, Object> data = new HashMap<>();
        data.put("portfolioId", filePath);
        data.put("id", UserManager.getUserId());
        requestItem.setParams(data);
        AppNetworkTask appNetworkTask = new AppNetworkTask(getProgressDialog(false), this);
        appNetworkTask.execute(requestItem);
    }

    ///////////CompressVideo
    private class CompressVideo extends AsyncTask<String, String, String> {

        // Initialize dialog
        Dialog dialog;

        @Override protected void onPreExecute()
        {
            super.onPreExecute();
            // Display dialog
            dialog = ProgressDialog.show(PortfolioActivity.this, "", "Compressing...");
        }

        @Override
        protected String doInBackground(String... strings)
        {
            // Initialize video path
            String videoPath = null;

            try {
                // Initialize uri
                Uri uri = Uri.parse(strings[1]);
                // Compress video
                videoPath = SiliCompressor.with(PortfolioActivity.this).compressVideo(uri, strings[2]);
            }
            catch (URISyntaxException e) {
                e.printStackTrace();
            }
            // Return Video path
            return videoPath;
        }

        @Override protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            dialog.dismiss();
            File file = new File(s);
            Uri uri = Uri.fromFile(file);
            uploadImageToServer(s);
            float size = file.length() / 1024f;

        }
    }
}