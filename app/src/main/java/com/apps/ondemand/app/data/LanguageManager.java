package com.apps.ondemand.app.data;


import com.apps.ondemand.app.data.models.LanguageModel;
import com.apps.ondemand.app.data.preferences.LanguageSharedPreferenceManager;
import com.apps.ondemand.app.data.preferences.PreferenceUtils;
import com.apps.ondemand.common.business.BaseItem;
import com.apps.ondemand.common.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;


public class LanguageManager {

    public static String getUserPreferredLanguage() {
        return LanguageSharedPreferenceManager.getInstance().read(PreferenceUtils.PREFERRED_LANGUAGE, AppConstants.LANGUAGE_EN);
    }

    public static void setUserPreferredLanguage(String languageCode) {
        LanguageSharedPreferenceManager.getInstance().save(PreferenceUtils.PREFERRED_LANGUAGE, languageCode);
    }

    public static boolean isLanguageSelected() {
        return LanguageSharedPreferenceManager.getInstance().read(PreferenceUtils.IS_LANGUAGE_SELECTED, false);
    }

    public static void setLanguageSelected() {
        LanguageSharedPreferenceManager.getInstance().save(PreferenceUtils.IS_LANGUAGE_SELECTED, true);
    }

    public static String getCurrency() {
        return LanguageSharedPreferenceManager.getInstance().read(PreferenceUtils.CURRENCY, AppConstants.POND_CURRENCY);
    }

    public static void setCurrency(String currency) {
        LanguageSharedPreferenceManager.getInstance().save(PreferenceUtils.CURRENCY, currency);
    }

    public static List<BaseItem> getLanguageList() {
        ArrayList<BaseItem> languageModels = new ArrayList<>();
        languageModels.add(new LanguageModel(AppConstants.ENGLISH, AppConstants.LANGUAGE_EN, true));
      //  languageModels.add(new LanguageModel(AppConstants.SPANISH, AppConstants.LANGUAGE_ES, false));
//        languageModels.add(new LanguageModel(AppConstants.ESTONIAN, AppConstants.LANGUAGE_ET, false));
//        languageModels.add(new LanguageModel(AppConstants.RUSSIAN, AppConstants.LANGUAGE_RU, false));
        return languageModels;
    }
}
