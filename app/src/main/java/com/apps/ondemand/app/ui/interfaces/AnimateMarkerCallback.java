package com.apps.ondemand.app.ui.interfaces;

public interface AnimateMarkerCallback {
    void animationEnd();
}
