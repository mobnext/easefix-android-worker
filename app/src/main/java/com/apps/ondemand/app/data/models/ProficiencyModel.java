package com.apps.ondemand.app.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.apps.ondemand.common.business.BaseItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProficiencyModel implements Parcelable{

    @SerializedName("label")
    @Expose
    private String label;

    @SerializedName("_id")
    @Expose
    private String id;


    boolean isSelected = false;

    public ProficiencyModel() {
    }

    protected ProficiencyModel(Parcel in) {
        label = in.readString();
        id = in.readString();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(label);
        dest.writeString(id);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProficiencyModel> CREATOR = new Creator<ProficiencyModel>() {
        @Override
        public ProficiencyModel createFromParcel(Parcel in) {
            return new ProficiencyModel(in);
        }

        @Override
        public ProficiencyModel[] newArray(int size) {
            return new ProficiencyModel[size];
        }
    };

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
