package com.apps.ondemand.app.data.models;

import com.apps.ondemand.common.business.BaseItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReviewModel implements BaseItem {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("rating")
    @Expose
    private float rating;
    @SerializedName("review")
    @Expose
    private String review;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("profileImage")
    @Expose
    private String profileImage;
    @SerializedName("avgRating")
    @Expose
    private float avgRating;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public float getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public float getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(float avgRating) {
        this.avgRating = avgRating;
    }


    @Override
    public int getItemType() {
        return ITEM_REVIEWS;
    }
}

