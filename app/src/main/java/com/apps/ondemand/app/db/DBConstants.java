package com.apps.ondemand.app.db;

/**
 * A helper class to store all database contracts in one place. Each
 * class in {@link DBConstants} represents a table will fields equal to
 * columns.
 * <p>
 * Created on 2016-12-03 15:01.
 *
 * @author Rana
 */
public class DBConstants {

    public static class Localisation {
        public static final String TABLE_NAME = "FeedTable";

        public static final String ID = "id";
        public static final String KEY = "key";
        public static final String VALUE = "value";
    }//Localisation


} // DBConstants
