package com.apps.ondemand.common.base;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.apps.ondemand.R;
import com.apps.ondemand.common.utils.DateUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.PermissionUtils;


import java.io.File;
import java.io.IOException;

/**
 * Created by bilal on 26/03/2018.
 */

public class TakePhotoVideoActivity extends BaseActivity {

    /**
     * Flag to identify camera access permission is requested
     */
    public static final int TAKE_PHOTO_PERMISSION = 101;
    /**
     * Flag to identify gallery access permission is requested
     */
    public static final int CHOOSE_PHOTO_PERMISSION = 102;
    /**
     * Flag to identify camera intent is called
     */
    public static final int TAKE_PHOTO_RESULT_CODE = 110;
    /**
     * Flag to identify gallery intent is called
     */
    public static final int CHOOSE_PHOTO_RESULT_CODE = 120;
    /**
     * Flag to identify gallery intent is called
     */
    public static final int CHOOSE_MULTIPLE_RESULT_CODE = 130;

    public static final int REQUEST_VIDEO_CAPTURE = 140;

    public Uri selectedPhotoUri;
    public String selectedVideoUri;

    /**
     * show user dialog to select either open camera Or gallery
     */
    protected void showChoosePhotoDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        dialog.setContentView(R.layout.choose_photo_video_dialog);
        TextView takePhotoTv = (TextView) dialog.findViewById(R.id.tv_take_photo);
        TextView takeVideoTv = (TextView) dialog.findViewById(R.id.tv_take_video);
        TextView choosePhotoTv = (TextView) dialog
                .findViewById(R.id.tv_photo_lib);
        takePhotoTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (PermissionUtils.checkAndRequestPermissions(TakePhotoVideoActivity.this
                        , new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE}, TAKE_PHOTO_PERMISSION))
                    dispatchTakePictureIntent();
            }
        });

        choosePhotoTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (PermissionUtils.checkAndRequestPermissions(TakePhotoVideoActivity.this
                        , new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE}, CHOOSE_PHOTO_PERMISSION))
//                    checkAndDispatchGalleryIntent();
//                    dispatchChoosePictureIntent();
                dispatchMultipleChoosePictureIntent();
            }
        });
        takeVideoTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (PermissionUtils.checkAndRequestPermissions(TakePhotoVideoActivity.this
                        , new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE}, TAKE_PHOTO_PERMISSION))
                    dispatchRecordVideoIntent();
            }
        });
        dialog.findViewById(R.id.cancel_dialog_tv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == TAKE_PHOTO_PERMISSION && PermissionUtils.verifyPermission(grantResults)) {
            checkPermissionDispatchTakePictureIntent();
        } else if (requestCode == CHOOSE_PHOTO_PERMISSION && PermissionUtils.verifyPermission(grantResults)) {
//            dispatchChoosePictureIntent();
            dispatchMultipleChoosePictureIntent();
        }
    }


    public void checkPermissionDispatchTakePictureIntent() {
        if (PermissionUtils.checkAndRequestPermissions(TakePhotoVideoActivity.this
                , new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, TAKE_PHOTO_RESULT_CODE))
//            dispatchTakePictureIntent();
            dispatchRecordVideoIntent();
    }
    /**
     * Check build os version and then dispatch single or multiple choose picture intent
     */
    private void checkAndDispatchGalleryIntent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
            dispatchMultipleChoosePictureIntent();
        else
            dispatchChoosePictureIntent();
        ;
    }

    /**
     * create and call open camera intent
     */
    private static final int MAXIMUM_VIDEO_RECORDING_LIMIT = 30;
    private static final int VIDEO_RECORDING_QUALITY = 0;


    public void dispatchTakePictureIntent() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, TAKE_PHOTO_RESULT_CODE);

    }

    /**
     * create and open choose from gallery/photos intent
     */
    public void dispatchRecordVideoIntent(){
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, MAXIMUM_VIDEO_RECORDING_LIMIT);
        takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, VIDEO_RECORDING_QUALITY);

        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }

    public void dispatchChoosePictureIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/* video/*");
        startActivityForResult(Intent.createChooser(intent, "Choose Picture"), CHOOSE_PHOTO_RESULT_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void dispatchMultipleChoosePictureIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/* video/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(Intent.createChooser(intent, "Choose Picture"), CHOOSE_MULTIPLE_RESULT_CODE);
    }

    /**
     * create and return a file to store the taken image in the device storage
     *
     * @return created file in device storage
     */
    final public File getPhotoFile() {
        String timeStamp = DateUtils.convertDate(System.currentTimeMillis(), DateUtils.FILE_NAME);
        String imageFileName = "IMG" + timeStamp + "";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File photoFile = null;
        try {
            if(!storageDir.exists())
                storageDir.mkdirs();
            photoFile = File.createTempFile(imageFileName, ".jpg", storageDir);
        } catch (IOException e) {
            Logger.caughtException(e);
        }
        return photoFile;
    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Video.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }
}
