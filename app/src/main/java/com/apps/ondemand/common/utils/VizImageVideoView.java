package com.apps.ondemand.common.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import com.apps.ondemand.R;
import com.apps.ondemand.app.communication.SocketService;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.app.data.preferences.SharedPreferenceManager;
import com.apps.ondemand.app.ui.SplashActivity;
import com.apps.ondemand.common.business.OnNetworkTaskListener;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONObject;

import java.io.File;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by JunaidAhmed on 8/7/2018.
 */
public class VizImageVideoView extends RelativeLayout implements OnNetworkTaskListener {

    private boolean isCircular = false;
    private boolean enableProgress = true;
    private float radius = 0;
    private int placeholder = 0;
    private int resource = 0;

    private ProgressBar loadingView = null;
    private RoundedImageView roundedImageView = null;
    private OnImageVideoUploadingListener onImageUploadingListener;
    private @IdRes
    int imageViewID;
    private String imageURL = "";
    private String serverURL = "";

    public interface OnImageVideoUploadingListener {
        void onUploadingResponse(boolean isSuccessful, String imageURL, @IdRes int imageViewID, String message);
    }

    public VizImageVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupAttributes(attrs);
        inflateLayout(context);
        initLoadingView();
        setProperties();
    }

    private void setProperties() {
        RoundedImageView roundedImageView = findViewById(R.id.iv_picture);

        if (isCircular)
            roundedImageView.setCornerRadius(200);
        else
            roundedImageView.setCornerRadius(radius);

        if (resource != 0)
            roundedImageView.setImageResource(resource);
        else if (placeholder != 0) {
            roundedImageView.setImageResource(placeholder);
        }
    }

    private void setupAttributes(AttributeSet attrs) {
        TypedArray array = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.VizImageView, 0, 0);
        try {
            isCircular = array.getBoolean(R.styleable.VizImageView_viv_setCircular, false);
            enableProgress = array.getBoolean(R.styleable.VizImageView_viv_enableProgress, true);
            radius = array.getDimensionPixelSize(R.styleable.VizImageView_viv_radius, 20);
            placeholder = array.getResourceId(R.styleable.VizImageView_viv_placeholder, 0);
            resource = array.getResourceId(R.styleable.VizImageView_viv_src, 0);
        } finally {
            array.recycle();
        }
    }

    private void initLoadingView() {
        loadingView = getAvLoadingIndicatorView();
    }

    private void inflateLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_imageview, this, true);
    }

    public void setPlaceholder(int placeholder) {
        RoundedImageView roundedImageView = findViewById(R.id.iv_picture);
        roundedImageView.setImageResource(placeholder);
    }

    public void setImage(final String url, boolean resizeImage) {
        if (url.isEmpty())
            return;

        this.imageURL = url;
        RoundedImageView roundedImageView = findViewById(R.id.iv_picture);
        showProgress();
        RequestOptions requestOptions = new RequestOptions();
        if (placeholder != 0) {
            requestOptions.placeholder(placeholder);
        }
        if (resizeImage) {
            requestOptions.override((int) getContext().getResources().getDimension(R.dimen.dimen_40), (int) getContext().getResources().getDimension(R.dimen.dimen_40));
        } else {
            requestOptions.fitCenter();
        }
        requestOptions.centerCrop();
        Glide.with(getContext()).load(url).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                hideProgress();
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                hideProgress();
                return false;
            }
        }).apply(requestOptions).into(roundedImageView);
    }

    public void removeImage(final @DrawableRes int resource, boolean resizeImage) {
        this.imageURL = "";
        RoundedImageView roundedImageView = findViewById(R.id.iv_picture);
        showProgress();
        RequestOptions requestOptions = new RequestOptions();
        if (placeholder != 0) {
            requestOptions.placeholder(placeholder);
        }
        if (resizeImage) {
            requestOptions.override((int) getContext().getResources().getDimension(R.dimen.dimen_40), (int) getContext().getResources().getDimension(R.dimen.dimen_40));
        } else {
            requestOptions.fitCenter();
        }
        requestOptions.centerCrop();
        Drawable myIcon = getResources().getDrawable(resource);
        Glide.with(getContext()).load(myIcon).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                hideProgress();
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                hideProgress();
                return false;
            }
        }).apply(requestOptions).into(roundedImageView);
    }

    public void setImage(final String url, int width, int height, @ColorRes int progressColor) {
        if (!URLUtil.isValidUrl(url))
            return;

        this.imageURL = url;
        RoundedImageView roundedImageView = findViewById(R.id.iv_picture);
        showProgress();
        loadingView.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), progressColor),
                android.graphics.PorterDuff.Mode.MULTIPLY);
        RequestOptions requestOptions = new RequestOptions();
        if (placeholder != 0) {
            requestOptions.placeholder(placeholder);
        }

        loadingView.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), progressColor),
                android.graphics.PorterDuff.Mode.MULTIPLY);

        requestOptions.override(width, height);
        requestOptions.centerCrop();
        Glide.with(getContext()).load(url).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                hideProgress();
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                hideProgress();
                return false;
            }
        }).apply(requestOptions).into(roundedImageView);
    }

    private void hideProgress() {
        if (enableProgress)
            loadingView.setVisibility(GONE);
    }

    private void showProgress() {
        if (enableProgress)
            loadingView.setVisibility(VISIBLE);
    }

    private ProgressBar getAvLoadingIndicatorView() {
        return findViewById(R.id.photo_loader);
    }

    public void uploadImageToServer(String filePath,int typesMod) {
        this.serverURL = AppConstants.getServerUrl(String.format(AppConstants.UPLOAD_PORTFOLIO_IMAGE, typesMod,UserManager.getUserId()));
        HttpRequestItem requestItem = new HttpRequestItem(serverURL);
        requestItem.setHttpRequestType(NetworkUtils.HTTP_MULTIPART2);
        requestItem.setHeaderParams(AppUtils.getHeaderParams());
        Map<String, Object> data = new HashMap<>();
        data.put("path", filePath);
        requestItem.setParams(data);
        requestItem.setHttpRequestTimeout(NetworkUtils.HTTP_MULTIPART_TIMEOUT);

        AppNetworkTask appNetworkTask = new AppNetworkTask(null, this);
        appNetworkTask.execute(requestItem);
    }

    public void loadImageVideoInView(@IdRes int imageViewID, final String serverURL, final Uri imageURL, final OnImageVideoUploadingListener onImageLoadingListener) {
        fadeIn();
        final String filePath = FileUtils.getPath(getContext(), imageURL);
        this.imageViewID = imageViewID;
        this.onImageUploadingListener = onImageLoadingListener;
        this.serverURL = (serverURL.isEmpty()) ? AppConstants.UPLOAD_PORTFOLIO_IMAGE : serverURL;
        roundedImageView = findViewById(R.id.iv_picture);
        showProgress();
        if (URLUtil.isValidUrl(imageURL.toString())) {
            Glide.with(getContext()).load(AppUtils.preparePathForPicture(imageURL.toString(), AppConstants.IMAGE_MINI_THUMB)).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    hideProgress();
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    uploadImageToServer(filePath,0);
                    return false;
                }
            }).into(roundedImageView);
        } else
            createAndSetBitmap(imageURL, filePath);
    }

    public void loadVideoView(final String serverURL, final String imageURL, int type,final OnImageVideoUploadingListener onImageLoadingListener)
    {

        this.onImageUploadingListener = onImageLoadingListener;
        this.serverURL = (serverURL.isEmpty()) ? AppConstants.UPLOAD_PORTFOLIO_IMAGE : serverURL;
        if (URLUtil.isValidUrl(imageURL.toString())) {
            uploadImageToServer(imageURL,1);
        }
    }
    private void fadeIn() {
        RoundedImageView roundedImageView = findViewById(R.id.iv_picture);
        roundedImageView.setAlpha(0.5f);
    }

    private void fadeOut() {
        RoundedImageView roundedImageView = findViewById(R.id.iv_picture);
        roundedImageView.setAlpha(1.0f);
    }

    /**
     * Create bitmap from Uri and set it to Image View
     *
     * @param uri photo Uri
     */
    private void createAndSetBitmap(Uri uri, String filePath) {
        String[] imageInfo = BitmapUtils.getRealPathWithIdFromURI(getContext(), uri,
                MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID);
        if (imageInfo != null && imageInfo.length > 1) {
            ImageView ivPhoto = findViewById(R.id.iv_profile);
            Bitmap bitmap = BitmapUtils.getThumbnail(getContext().getContentResolver(),
                    Long.parseLong(imageInfo[1]), MediaStore.Images.Thumbnails.MICRO_KIND);
            if (bitmap != null) {
                ivPhoto.setImageBitmap(bitmap);
                uploadImageToServer(filePath,0);
            }
        }
    }

    @Override
    public void onNetworkResponse(HttpResponseItem response) {
        boolean status = response.getResponseCode() == HttpURLConnection.HTTP_OK;
        if (status)
            onNetworkSuccess(response);
        else
            onNetworkError(response);
    }

    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        fadeOut();
        hideProgress();
        String imageURL = getImageURL(response.getResponse());
        this.imageURL = imageURL;
        try {
            String message = new JSONObject(response.getResponse()).getString("message");
            if (onImageUploadingListener != null)
                onImageUploadingListener.onUploadingResponse(true, imageURL, imageViewID, message);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onNetworkError(HttpResponseItem response) {
        if (response.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED) {
            showSessionExpiredDialog();
            return;
        }
        try {
            String message = "Image uploading failed";
            if (onImageUploadingListener != null)
                onImageUploadingListener.onUploadingResponse(false, imageURL, imageViewID, message);
        } catch (Exception e) {
            e.printStackTrace();
        }

        hideProgress();
        fadeOut();
        if (imageURL.isEmpty()) {
            if (placeholder == 0)
                roundedImageView.setImageDrawable(null);
            else
                roundedImageView.setImageResource(placeholder);
        } else {
            setImage(imageURL, false);
        }
    }

    private String getImageURL(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject data = jsonObject.getJSONObject("data");
            return data.getString("url");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    @Override
    public void onNetworkCanceled(HttpResponseItem response) {
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.hasNetworkConnection(getContext(), true);
    }

    public String getImageURL() {
        return imageURL;
    }

    /**
     * If server response code is unauthorized (session expired)
     */
    private void showSessionExpiredDialog() {
        SharedPreferenceManager.getInstance().clearPreferences();
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(false);
        builder.setTitle("Session expired");
        builder.setMessage("Your session got expired kindly sign in again.");
        builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getContext().stopService(new Intent(getContext(), SocketService.class));
                Intent intent = new Intent(getContext(), SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(intent);
            }
        });
        builder.create().show();
    }

}
