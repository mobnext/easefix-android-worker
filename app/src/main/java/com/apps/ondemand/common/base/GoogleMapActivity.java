package com.apps.ondemand.common.base;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import android.util.Log;
import android.view.animation.LinearInterpolator;

import com.apps.ondemand.R;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.PermissionUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import static com.google.android.gms.location.LocationServices.FusedLocationApi;

/**
 * Created by T520 on 1/13/2016.
 */
public class GoogleMapActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, LocationSource {

    private static final int REQUEST_CHECK_SETTINGS = 0;
    private static final long DEFAULT_RIDE_LOCATION_INTERVAL_SECONDS = 10000;
    public static final int PERMISSION_LOCATION_REQUEST = 1010;

    private static final double LATITUDE_DEFAULT = 53.483959;
    private static final double LONGITUDE_DEFAULT = -2.244644;

    protected GoogleApiClient mGoogleApiClient;
    protected MapView mapView;
    protected GoogleMap googleMap;
    private String TAG = "GoogleMapActivity";
    private OnLocationChangedListener mMapLocationListener = null;
    private LocationRequest mLocationRequest;
    public Location mLocation;
    public boolean needToCheckPermission = true;
    private Bundle savedInstanceState;

    public boolean needCurrentLocation = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        mapView = (MapView) findViewById(R.id.map);
        initializeGoogleMap(savedInstanceState);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mapView != null)
            mapView.onPause();
        if (needCurrentLocation && mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mapView != null)
            mapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (needCurrentLocation)
            connectGoogleClient();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (needCurrentLocation)
            disconnectGoogleClient();
    }

    private void createLocationRequest() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(DEFAULT_RIDE_LOCATION_INTERVAL_SECONDS);
        mLocationRequest.setFastestInterval(DEFAULT_RIDE_LOCATION_INTERVAL_SECONDS);
        createLocationAccessBuilder();
    }

    private void createLocationAccessBuilder() {
        if (!needToCheckPermission)
            return;
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        onApiClientConnected();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(GoogleMapActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        onApiClientConnected();
                        break;
                    case Activity.RESULT_CANCELED:
                        //rejectLocationServicesRequest();
                        createLocationRequest();//keep asking if imp or do whatever
                        break;
                }
                break;
        }
    }

    public void rejectLocationServicesRequest() {

    }

    private void connectGoogleClient() {
        mGoogleApiClient.registerConnectionCallbacks(this);
        mGoogleApiClient.registerConnectionFailedListener(this);
        mGoogleApiClient.connect();
    }

    private void disconnectGoogleClient() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
            mGoogleApiClient.unregisterConnectionCallbacks(this);
            mGoogleApiClient.unregisterConnectionFailedListener(this);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        checkPermission();
    }

    public void checkPermission() {
        if (PermissionUtils.hasLocationPermissionGranted(GoogleMapActivity.this)) {
            if (!mGoogleApiClient.isConnected())
                connectGoogleClient();
            else
                createLocationRequest();
        } else {
            PermissionUtils.requestLocationPermissions(GoogleMapActivity.this, PERMISSION_LOCATION_REQUEST);
        }
//        if (PermissionUtils.hasLocationPermissionGranted(GoogleMapActivity.this))
//            onLocationPermissionGranted();
//        else
//            PermissionUtils.requestLocationPermissions(GoogleMapActivity.this, PERMISSION_LOCATION_REQUEST);
    }

    @SuppressLint("MissingPermission")
    private void onApiClientConnected() {
//        createLocationRequest();
        if (!mGoogleApiClient.isConnected())
            return;
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        if (googleMap == null)
            return;
        if (!googleMap.isMyLocationEnabled())
            googleMap.setMyLocationEnabled(true);
        googleMap.setLocationSource(this);
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            mLocation = location;
            moveToMyLocation(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(GoogleMapActivity.this, REQUEST_CHECK_SETTINGS);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.d(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLocation = location;
        if (mMapLocationListener != null)
            mMapLocationListener.onLocationChanged(location);
        LatLng lng = new LatLng(location.getLatitude(), location.getLongitude());
        moveToMyLocation(lng);
    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        mMapLocationListener = onLocationChangedListener;
    }

    @Override
    public void deactivate() {
        mMapLocationListener = null;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null)
            mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (googleMap != null) {
            if (PermissionUtils.hasLocationPermissionGranted(GoogleMapActivity.this))
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
            googleMap.setMyLocationEnabled(false);
            googleMap.clear();
        }
        googleMap = null;
        if (mapView != null)
            mapView.onDestroy();
        mapView = null;
        mGoogleApiClient = null;
        mLocationRequest = null;
        mMapLocationListener = null;
    }

    private void initializeGoogleMap(Bundle savedInstanceState) {
        MapsInitializer.initialize(GoogleMapActivity.this);
        Log.e("setting", "anjum1");
        if (PermissionUtils.checkPlayServices(GoogleMapActivity.this)) {

            Log.e("setting", "anjum2");
            if (mapView != null) {
                Log.e("setting", "anjum3");
                mapView.onCreate(savedInstanceState);
                mapView.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        GoogleMapActivity.this.googleMap = googleMap;
                        setUpMap();
                    }
                });
            }
        }
    }

    private void setUpMap() {
        if (googleMap == null)
            return;
        changeMapTheme();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setMapToolbarEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.setLocationSource(this);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(
                new CameraPosition.Builder()
                        .target(new LatLng(LATITUDE_DEFAULT, LONGITUDE_DEFAULT))
                        .zoom(12.0f).build()));
        if (PermissionUtils.hasLocationPermissionGranted(GoogleMapActivity.this))
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
        googleMap.setMyLocationEnabled(true);
        setMapProperties();
    }

    private void changeMapTheme() {
        try {
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle));
            if (!success) {
                // Handle map style load failure
            }
        } catch (Resources.NotFoundException e) {
            // Oops, looks like the map style resource couldn't be found!
        }
    }

    /**
     * Setup MapView properties according to own requirement
     */
    public void setMapProperties() {
    }

    public void onMyLocationReceived(CameraPosition cameraPosition) {
    }

    public void moveToMyLocation(LatLng location) {
//        try {
//            Marker marker = googleMap.addMarker(new MarkerOptions().position(location).title(""));
//            marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));
//            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 11.0f));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    protected void moveToCurrentLocation() {
        if (googleMap != null && PermissionUtils.hasLocationPermissionGranted(GoogleMapActivity.this)) {
            Location location;
            if (!googleMap.isMyLocationEnabled())
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
            googleMap.setMyLocationEnabled(true);
            location = googleMap.getMyLocation();
            // if current location is null then get last location
            if (location == null) {
                if (mGoogleApiClient == null)
                    return;
                location = FusedLocationApi.getLastLocation(mGoogleApiClient);
            }
            if (location != null)
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                        new CameraPosition.Builder().target(new LatLng(location.getLatitude(), location.getLongitude())).zoom(16.0f).build()));
        }
    }

    public void setHighAccuracyLocationRequest() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(DEFAULT_RIDE_LOCATION_INTERVAL_SECONDS);
        mLocationRequest.setFastestInterval(DEFAULT_RIDE_LOCATION_INTERVAL_SECONDS);
        createLocationAccessBuilder();
    }

    public void onLocationPermissionGranted() {
        if (!mGoogleApiClient.isConnected())
            connectGoogleClient();
        else
            createLocationRequest();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_LOCATION_REQUEST:
                // if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (PermissionUtils.verifyPermission(grantResults)) {
                    onLocationPermissionGranted();
                } else {
                    String text = getString(R.string.location_permissions_not_granted);
                    showSnackBar(text);
//                    onLocationPermissionDisabled(requestCode);
                }
                break;
        }
    }

    public void onLocationPermissionDisabled(int requestCode) {
    }

    public void animateMarker(final Marker marker, final double latitude, final double longitude,
                              final boolean hideMarker) {
        if (googleMap == null || marker == null)
            return;
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = googleMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;

        final LinearInterpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * longitude + (1 - t) * startLatLng.longitude;
                double lat = t * latitude + (1 - t) * startLatLng.latitude;

                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }

    public List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        if (!AppUtils.ifNotNullEmpty(encoded))
            return poly;
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }
}
