package com.apps.ondemand.common.utils;

import android.content.Context;
import android.widget.ImageView;

import com.apps.ondemand.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import static com.apps.ondemand.common.utils.AppUtils.imageLowerSizeReplace;


/**
 * Created by Rana Sadam
 */
public class GlideUtils {

    public static void glideLoadImage(Context context, final ImageView imageView, final String url,
                                      int size) {
        if (!AppUtils.ifNotNullEmpty(url) || size <= 0) {
            imageView.setImageResource(R.color.background);
        } else {
            RequestOptions requestOptions = new RequestOptions().override(size, size);
            Glide.with(context).load(imageLowerSizeReplace(url)).apply(requestOptions).into(imageView);
        }
    }

    public static void glideLoadImageOrPlaceHolderWithSize(Context context, final ImageView imageView, final String url,
                                                   final int errorResId, int size) {
        if (!AppUtils.ifNotNullEmpty(url) || size <= 0) {
            imageView.setImageResource(errorResId);
        } else {
            RequestOptions requestOptions = new RequestOptions().placeholder(errorResId).override(size, size);
            Glide.with(context).load(imageLowerSizeReplace(url)).apply(requestOptions).into(imageView);
        }
    }

    public static void glideLoadImageOrPlaceHolder(Context context, final ImageView imageView, final String url,
                                                   final int errorResId, int width, int height) {
        if (!AppUtils.ifNotNullEmpty(url) || width <= 0 || height <= 0) {
            imageView.setImageResource(errorResId);
        } else {
            RequestOptions requestOptions = new RequestOptions().placeholder(errorResId).override(width, height);
            Glide.with(context).load(imageLowerSizeReplace(url)).apply(requestOptions).into(imageView);
        }
    }

    public static void glideLoadImageOrPlaceHolder(Context context, final ImageView imageView, final String url) {
        if (!AppUtils.ifNotNullEmpty(url)) {
            return;
        } else {
            Glide.with(context).load(imageLowerSizeReplace(url)).into(imageView);
        }
    }

    public static void glideLoadImageOrPlaceHolderNoFade(Context context, final ImageView imageView, final String url, int width, int height) {
        if (!AppUtils.ifNotNullEmpty(url)) {
            return;
        } else {
            RequestOptions requestOptions = new RequestOptions().override(width, height).dontAnimate();
            Glide.with(context).load(imageLowerSizeReplace(url)).apply(requestOptions).into(imageView);
        }
    }

    public static void glideLoadImageOrPlaceHolder(Context context, final ImageView imageView, final String url,
                                                   int width, int height) {
        if (!AppUtils.ifNotNullEmpty(url) || width <= 0 || height <= 0) {
            return;
        } else {
            RequestOptions requestOptions = new RequestOptions().override(width, height);
            Glide.with(context).load(imageLowerSizeReplace(url)).apply(requestOptions).into(imageView);
        }
    }
}
