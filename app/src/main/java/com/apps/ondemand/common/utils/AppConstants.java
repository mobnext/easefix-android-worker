package com.apps.ondemand.common.utils;

import com.apps.ondemand.BuildConfig;
import com.apps.ondemand.app.data.LanguageManager;

/**
 * Contains constants used in application
 */
public class AppConstants {
    public static final float ZOOM_LEVEL = 13;

    //Language Constants
    public static final String ENGLISH = "English";
    public static final String SPANISH = "Spanish";
    public static final String ESTONIAN = "Estonian";
    public static final String RUSSIAN = "Russian";

    public static final String LANGUAGE_EN = "en";//for english
    public static final String LANGUAGE_ES = "es";//for spanish
    public static final String LANGUAGE_ET = "et";//for estonian
    public static final String LANGUAGE_RU = "ru";//for russian

    public static final String POND_CURRENCY = "£";

    //Twilio Implementation
    public static int line_Visible = 0;
    public static final int STATE_WIFI = 1;
    public static final int STATE_MIC = 2;
    public static final String DISPLAY_NAME = "displayName";
    public static final String PROFILE_IMAGE = "profileImage";
    public static String PARTICIPANT_ID = "participantId";

    public static String VIEW_DISPLAY_NAME = "displayName";
    public static String VIEW_PROFILE_IMAGE = "profileImage";
    public static String VIEW_PARTICIPANT_ID = "participantId";


    //twilio Video
    public static final String GET_TOKEN = "videoCal/generate-token";
    public static final String VIDEO_CALL_STATUS = "videoCal/%s/complete?status=%s";
    public static final String CONFLICT = "sp/job/dispute/%s";

    public static String VIDEO_NAME_ = "name";
    public static String VIDEO_IMAGE_ = "image";
    public static String VIDEO_USER_ID_ = "user_id";
    public static String VIDEO_DISTANCE_ = "distance";
    public static String VIDEO_UNIQUE_ID_ = "uniqueId";



    //SP Status
    public static final int STATUS_ONLINE = 1;
    public static final int STATUS_OFFLINE = 0;

    public static final int ACCEPT_JOB = 2;
    public static final int REJECT_JOB = 8;

    public static final float MAP_ZOOM_LEVEL = 13;

    public static final String TYPE_USER = "user";
    public static final String TYPE_SP = "sp";
    public static final String DEVICE_TYPE = "android";

    public static final int REGISTER_PERSONAL = 1;
    public static final int REGISTER_AS_COMPANY = 2;

    //Chat constant
    public static final int INCOMING_MESSAGE = 2;
    public static final int OUTGOING_MESSAGE = 1;

    //Video Image
    public static final int IMAGE_PORT = 0;
    public static final int VIDEO_PORT = 1;

    //Notification Actions
    public static final int MARKS_AS_READ = 1;
    public static final int CLEAR_ALL = 2;

    //Job Fragment Indexes
    public static final int ACTIVE_INDEX = 0;
    public static final int PAST_INDEX = 1;

    //reasons constants
    public static final int CANCELLATION_REASON = 1;
    public static final int REPORT_ISSUE_REASON = 2;

    //Job Statuses
    public static final int STATUS_OPEN = 1; //For Open,
    public static final int STATUS_ACCEPTED = 2; //For Accepted
    public static final int STATUS_ON_THE_WAY = 3; //For Completed
    public static final int STATUS_ARRIVED = 4; //For Completed
    public static final int STATUS_STARTED = 5; //For Rated By User
    public static final int STATUS_COMPLETED = 6; //For Cancellation
    public static final int STATUS_RATE_BY_USER = 7; //For Rate by user
    public static final int STATUS_REJECTION = 8; //For Rejection
    public static final int STATUS_CANCELLED = 9; //For Cancellation
    public static final int STATUS_TIME_OUT = 10;//TimeOut
    public static final int STATUS_DISPUTE = 11;//TimeOut


    //FCM and Sockets Components
    public static final String FROM_WHICH_COMPONENT = "support_comp";
    public static final String COMPONENT_GCM = "gcm";
    public static final String COMPONENT_SOCKET = "socket";
    public static final String TITLE_GCM = "socket";
    public static final String MESSAGE_GCM = "socket";

    public static final String PLAY_STORE_URL_PREFIX = "https://play.google.com/store/apps/details?id=";
    /**
     * Web server fields
     */
    public static final String GOOGLE_BASE_URL = "https://maps.googleapis.com/";
    public static final String GET_DIRECTIONS = "maps/api/directions/json";

    public static final String LOG_OUT = "logout";
    public static final String IMG_UPLOAD = "upload/profile-image";

    public static final String SIGN_IN_CODE = "sign-in/send-verification-code";
    public static final String SIGN_UP_CODE = "sign-up/send-verification-code";

    public static final String SIGN_IN_VERIFY_CODE = "sign-in/verify-verification-code";
    public static final String SIGN_UP_VERIFY_CODE = "sign-up/verify-verification-code";

    public static final String BASIC_INFO = "sp/basic-info";

    public static final String UPLOAD_PROFILE_IMAGE = "upload/profile-image";
    public static final String UPLOAD_IDENTITY_IMAGE = "sp/upload-identity-images";
    public static final String UPLOAD_LICENCE_IMAGE = "sp/upload-license-images";

    public static final String UPLOAD_PORTFOLIO_IMAGE = "sp/upload-portfolio?type=%s&id=%s";
    public static final String DELETE_PORTFOLIO_IMAGE = "sp/upload-portfolio";
    public static final String FETCH_PORTFOLIO_LIST = "sp/fetch-portfolio";

    public static final String FETCH_SERVICES = "services";
    public static final String SELECT_SERVICES = "sp/select-services";

    public static final String FETCH_IDENTITY_TYPES = "identity-doc";

    public static final String UPLOAD_IDENTITY_DOCUMENT = "sp/select-identity-doc";
    public static final String UPLOAD_SERVICE_CERTIFICATES = "sp/service-images";
    public static final String UPDATE_SERVICE_CERTIFICATES = "sp/update/service-images";

    public static final String GET_ACTIVE_LANGUAGES = "active-languages";
    public static final String GET_LANGUAGES_LEVELS = "language-level";

    public static final String ADD_PERSONAL_DETAILS = "sp/add-bank-info";

    public static final String GET_TERM_PRIVACY = "privacy-term-conditions";
    public static final String SET_SP_STATUS = "sp/update-status";
    public static final String GET_SP_STATUS = "sp/status";

    public static final String FETCH_SP_PROFILE = "fetch-sp-profile/%s";
    public static final String FETCH_PROFILE_DATA = "sp/fetch-profile";
    public static final String FETCH_SP_SERVICES = "sp/services";

    public static final String DELETE_ACCOUNT = "delete/account";

    public static final String FETCH_LAST_UNRATED_JOB = "last-unrated-job";
    public static final String FETCH_LAST_UNHANDLED_JOB = "sp/job/unhandled-job";

    public static final String RATE_USER = "sp/job/rate-user";
    public static final String UPDATE_JOB_REQUEST = "sp/job/update-offer";

    public static final String GET_JOB_DETAILS = "sp/job/%s/detail";
    public static final String FETCH_CANCELLATION_REASONS = "cancellation-reasons?userType=%s";
    public static final String JOB_CANCEL = "sp/job/cancel";

    public static final String UPDATE_JOB_STATUS = "sp/job/update-status";
    public static final String COMPLETE_JOB = "sp/job/%s/complete-job";
    public static final String PAUSE_START_JOB = "sp/job/%s/pause-start";

    public static final String ADD_LINE_ITEM = "sp/job/add-line-item";
    public static final String EDIT_LINE_ITEM = "sp/job/edit-line-item";
    public static final String DELETE_LINE_ITEM = "sp/job/delete-line-item";

    public static final String GET_THREAD_CHATS = "chatting/thread/%s?userType=%s&offset=%s&limit=%s";

    public static final String GET_COUNTRIES = "country/codes";
    public static final String HELPER_LOGIN_VERIFICATION = "helper/login/verification";
    public static final String UPDATE_PROFILE = "sp/update-profile/%s";

    public static final String FETCH_SCHEDULE_JOBS = "sp/job/listing?listType=%s";
    public static final String FETCH_AVAILABLE_JOBS = "sp/job/bids?listType=%s";

    public static final String GET_EARNINGS = "sp/job/weekly-earning";
    public static final String GET_ALL_WEEK_LIST = "sp/job/week-list";
    public static final String GET_EARNING_LISTING = "sp/job/weekly-earning-detail";

    public static final String GET_ALL_REVIEWS = "%s/%s/over-all-rating?offset=%s&limit=%s";

    public static final String BID_AMOUNT_ = "sp/job/update/bid";
    public static final String REJECT_BID = "sp/job/reject/bid";

    public static final String FETCH_BANK_ACCOUNT = "bank/account";

    public static final String GET_NOTIFICATIONS = "sp-notification?offset=%s&limit=%s";
    public static final String NOTIFICATION_ACTION = "action-notifications";
    public static final String GET_NOTIFICATION_COUNT = "notification-count";

    public static final String GET_PRIVACY_AND_TERMS = "current/privacy-termConditions?userType=sp";
    public static final String UPDATE_PRIVACY_AND_TERMS = "update/privacy-termConditions?userType=sp";
    public static final String CHECK_APP_VERSION = "check-version";


    //image type
    public static final String IMAGE_THUMB = "_thumb";
    public static final String IMAGE_MINI_THUMB = "_mini_thumb";


    //Extras keys
    public static final String KEY_EXTRA_IS_SIGN_UP = "key_extra_is_sign_up";
    public static final String KEY_EXTRA_COUNTRY_CODE = "key_extra_country_code";
    public static final String KEY_EXTRA_PHONE_NO = "key_extra_phone_no";
    public static final String KEY_EXTRA_COUNTRY = "key_extra_country";
    public static final String KEY_SPOKEN_LANGUAGES = "key_spoken_languages";
    public static final String KEY_PROFICIENCIES = "key_proficiencies";
    public static final String IS_ONLINE_EXTRA_KEY = "is_online_extra_key";
    public static final String KEY_JOB_STATUS = "key_job_status";
    public static final String KEY_PRICE_BREAKDOWN = "key_price_breakdown";
    public static final String KEY_JOB_ID = "key_job_id";
    public static final String KEY_BID_ID = "key_bid_id";
    public static final String KEY_LOCATION_MODEL = "key_location_model";
    public static final String KEY_JOB_DETAILS = "key_job_details";
    public static final String KEY_CAN_APPLY_PROMO = "key_can_apply_promo";
    public static final String KEY_SHOW_TERMS = "key_show_terms";
    public static final String KEY_TERMS_PRIVACY_STRING = "key_terms_privacy_model";
    public static final String KEY_EXTRA_WEEK_MODEL = "key_extra_week_model";
    public static final String KEY_EXTRA_WEEK_NUMBER = "key_extra_week_number";
    public static final String KEY_EXTRA_YEAR = "key_extra_year";
    public static final String KEY_USER_TYPE = "key_user_type";
    public static final String KEY_USER_ID = "key_user_id";
    public static final String KEY_EDIT_SERVICE = "key_edit_service";

    public static final String KEY_CONFIGURATION = "https://strapi.easefix.com/api/configuration";


    public static final int SPOKEN_LANGUAGE_CODE = 100;
    public static final int SHOW_PRICE_BREAKDOWN_CODE = 400;
    public static final int WEEK_CODE = 400;
    public static final int EDIT_PROFILE_REQUEST_CODE = 500;


    /**
     * Create a full server rest api url by embedding provide end point with the server url
     *
     * @param api Rest api endpoint
     * @return Full rest api url
     */
    public static String getServerUrl(String api) {
        return String.format(BuildConfig.SERVER_URL, LanguageManager.getUserPreferredLanguage()) + api;
    }
}
