package com.apps.ondemand.common.base;

import static android.text.TextUtils.isEmpty;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.annotation.StringRes;


import com.apps.ondemand.app.calling.sendBird.utils.AuthenticationUtils;
import com.apps.ondemand.app.ui.jobs.JobDetailsActivity;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import androidx.transition.Fade;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.provider.Settings;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.ondemand.R;
import com.apps.ondemand.app.communication.SocketService;
import com.apps.ondemand.app.data.UserManager;
import com.apps.ondemand.app.data.models.JobRequestModel;
import com.apps.ondemand.app.data.preferences.SharedPreferenceManager;
import com.apps.ondemand.app.ui.SplashActivity;
import com.apps.ondemand.app.ui.dialogs.DialogJobRequest;
import com.apps.ondemand.app.ui.interfaces.JobRequestListener;
import com.apps.ondemand.common.business.OnContentChangeListener;
import com.apps.ondemand.common.business.OnNetworkTaskListener;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.network.HttpRequestItem;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.BadgeDrawable;
import com.apps.ondemand.common.utils.BorderErrorEditText;
import com.apps.ondemand.common.utils.CustomSnackBar;
import com.apps.ondemand.common.utils.ErrorEditText;
import com.apps.ondemand.common.utils.GlideUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.apps.ondemand.common.utils.NotificationManager;
import com.apps.ondemand.common.utils.ViewUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Locale;

import io.socket.client.Ack;

import static com.apps.ondemand.common.utils.AppConstants.GOOGLE_BASE_URL;
import static com.apps.ondemand.common.utils.AppConstants.KEY_JOB_ID;

/**
 * Class is responsible to manage all fragment transactions
 * broadcast receiver, view manager etc.
 */
@RequiresApi(api = Build.VERSION_CODES.S)
abstract public class BaseActivity extends AppCompatActivity implements OnContentChangeListener, OnNetworkTaskListener {

    // region VARIABLESø
    private Toolbar toolbar;
    private CharSequence appTitle;
    private boolean allowedToExit = false;
    public int width = 0, height = 0;
    // endregion

    public DrawerLayout mDrawerLayout;
    public BadgeDrawerToggle mDrawerToggle;

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //here you can update the RecyclerView as you desire using intent's data
            onEventReceived(intent);
            //Replace actionable notification with simple not actionable notification if that broadcast is listened through  FireBase service
            if (intent.hasExtra(AppConstants.FROM_WHICH_COMPONENT) &&
                    intent.getStringExtra(AppConstants.FROM_WHICH_COMPONENT).equalsIgnoreCase(AppConstants.COMPONENT_GCM))
                NotificationManager.createNotification(getBaseContext(), null,
                        intent.getStringExtra(AppConstants.TITLE_GCM),
                        intent.getStringExtra(AppConstants.MESSAGE_GCM),
                        intent.getStringExtra(SocketService.BROADCAST_JOB_ID));
        }
    };

    public SocketService mService;
    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            mService = ((SocketService.SocketBinder) service).getService();
            onBindService(mService);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
        }
    };
    private String updateJobStatusUrl = "";

    protected BaseActivity() {
    }

    public void handleForeGroundService(boolean isOnline) {
        if (UserManager.isUserLoggedIn()) {
            Intent intent = new Intent(this, SocketService.class);
            intent.putExtra(AppConstants.IS_ONLINE_EXTRA_KEY, isOnline);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(intent);
            } else {
                startService(intent);
            }
        }
    }

    // region NETWORK_RECEIVER
    private BroadcastReceiver networkConnectivityReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            onConnectivityChanged();
        }
    };

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
                if (mDrawerToggle != null) {
                    mDrawerToggle.onOptionsItemSelected(item);
                    if (backStackEntryCount > 1)
                        onRemoveCurrentFragment();
                } else {
                    if (backStackEntryCount > 1)
                        onRemoveCurrentFragment();
                    else
                        finish();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onConnectivityChanged() {
        TextView tvNoInterConnectivity = (TextView) findViewById(R.id.tv_no_internet_connect);
        if (tvNoInterConnectivity != null) {
            if (NetworkUtils.hasNetworkConnection(BaseActivity.this, true))
                tvNoInterConnectivity.setVisibility(View.VISIBLE);
            else
                tvNoInterConnectivity.setVisibility(View.GONE);
        }
    }
    // endregion

    //region LIFE_CYCLE
    @Override
    protected void onStart() {
        super.onStart();
        if (UserManager.isUserLoggedIn()) {
            Intent intent = new Intent(this, SocketService.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            IntentFilter filter = new IntentFilter(SocketService.BROADCAST_NOTIFICATION);
            registerReceiver(mReceiver, filter);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (mService != null)
                unbindService(mConnection);
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
            Logger.error("BaseActivity", e.toString());
        }
    }

    @Override
    public void onBackPressed() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntryCount <= 1) {
            if (allowedToExit || !isTaskRoot()) {
                finish();
                return;
            } else {
                allowedToExit = true;
                ViewUtils.showToast(BaseActivity.this, "Press again to exit", Toast.LENGTH_SHORT);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        allowedToExit = false;
                    }
                }, 1000);
                return;
            }
        }
        // we have more than 1 fragments in back stack
        if (backStackEntryCount > 1) {
            AppUtils.hideSoftKeyboard(BaseActivity.this);
            onRemoveCurrentFragment();
        } else
            super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (getCurrentFragment() != null)
            getCurrentFragment().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        toolbar = null;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }


    // endregion

    public String getLogTag() {
        return getClass().getSimpleName();
    }

    // region CONTENT_CHANGE_LISTENER


    @Override
    public void onPopTillSpecificFragment(Fragment fragment) {
        String state = fragment.getClass().getSimpleName().toLowerCase(Locale.getDefault());
        getSupportFragmentManager().popBackStack(state, 0);
        getSupportFragmentManager().beginTransaction().commit();


    }

    /**
     * Get {@link Toolbar} of {@link BaseActivity}
     *
     * @return Toolbar
     */
    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    /**
     * Remove current fragment from {@link FragmentTransaction}
     */
    @Override
    public void onRemoveCurrentFragment() {
        // some times FragmentActivity.getLoaderManager returns null
        try {
            AppUtils.hideSoftKeyboard(this);
            getSupportFragmentManager().popBackStackImmediate();
            getSupportFragmentManager().beginTransaction().commit();
        } catch (Exception e) {
            Logger.caughtException(e);
        }
    }

    /**
     * Remove given fragment from {@link FragmentTransaction}
     *
     * @param fragment {@link Fragment}
     */
    @Override
    public void onRemoveCurrentFragment(Fragment fragment) {
        // some time FragmentActivity.getLoaderManager returns null
        try {
            if (fragment == null) return;
            View view = fragment.getView();
            if (view != null) {
                ((ViewGroup) view).removeAllViews();
                view.invalidate();
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.remove(fragment).commit();
            AppUtils.hideSoftKeyboard(this);
        } catch (Exception e) {
            Logger.caughtException(e);
        }
    }

    /**
     * Add fragment in given {@link android.widget.FrameLayout}
     *
     * @param fragment  {@link Fragment}
     * @param contentId Id of {@link android.widget.FrameLayout}
     */
    @Override
    public void onAddFragment(Fragment fragment, int contentId) {
        onAddFragment(fragment, contentId, false);
    }

    /**
     * Add fragment in given {@link android.widget.FrameLayout}
     *
     * @param fragment   {@link Fragment}
     * @param addToStack true add else do not add
     */
    @Override
    public void onAddFragment(Fragment fragment, boolean addToStack) {
        onAddFragment(fragment, R.id.content_frame, addToStack);
    }

    /**
     * Add fragment in given {@link android.widget.FrameLayout}
     *
     * @param fragment       {@link Fragment}
     * @param contentId      Id of {@link android.widget.FrameLayout}
     * @param addToBackStack true add else do not add
     */
    @Override
    public void onAddFragment(Fragment fragment, int contentId, boolean addToBackStack) {
        if (fragment == null) return;
        String tag = fragment.getClass().getSimpleName().toLowerCase(Locale.getDefault());
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(contentId, fragment, tag);
        if (addToBackStack) transaction.addToBackStack(tag);
        transaction.commit();
    }

    /**
     * Replace fragment in given {@link android.widget.FrameLayout}
     *
     * @param fragment  {@link Fragment}
     * @param contentId Id of {@link android.widget.FrameLayout}
     */
    @Override
    public void onReplaceFragment(Fragment fragment, int contentId) {
        onReplaceFragment(fragment, contentId, false);
    }

    /**
     * Replace fragment in given {@link android.widget.FrameLayout}
     *
     * @param fragment   {@link Fragment}
     * @param addToStack true add else do not add
     */
    @Override
    public void onReplaceFragment(Fragment fragment, boolean addToStack) {
        onReplaceFragment(fragment, R.id.content_frame, addToStack);
    }

    /**
     * Replace fragment in given {@link android.widget.FrameLayout}
     *
     * @param fragment   {@link Fragment}
     * @param contentId  Id of {@link android.widget.FrameLayout}
     * @param addToStack true add else do not add
     */
    @Override
    public void onReplaceFragment(Fragment fragment, int contentId, boolean addToStack) {
        if (fragment == null) return;
        String tag = fragment.getClass().getSimpleName().toLowerCase(Locale.getDefault());
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (getSupportFragmentManager().getBackStackEntryCount() < 1)
            transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
                    android.R.anim.fade_in, android.R.anim.fade_out);
        else
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                    R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.setReorderingAllowed(true);
        transaction.replace(contentId, fragment, tag);
        if (addToStack) transaction.addToBackStack(null);
        transaction.commit();
    }

    /**
     * Remove current fragment from {@link FragmentTransaction}
     * & Replace fragment in given {@link android.widget.FrameLayout}
     *
     * @param curFragment    Current Fragment
     * @param newFragment    {@link Fragment}
     * @param addToBackStack true add else do not add
     */
    public void onRemoveAndReplaceFragment(Fragment curFragment, Fragment newFragment, boolean addToBackStack) {
        onRemoveCurrentFragment();
        onReplaceFragment(newFragment, addToBackStack);
    }

    /**
     * Get fragment from given id of {@link android.widget.FrameLayout}
     *
     * @param contentId Id of {@link android.widget.FrameLayout}
     * @return Fragment
     */
    @Override
    public Fragment getCurrentFragment(int contentId) {
        return getSupportFragmentManager().findFragmentById(contentId);
    }

    /**
     * Get Fragment from default {@link android.widget.FrameLayout}
     *
     * @return Fragment
     */
    @Override
    public Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.content_frame);
    }

    @Override
    public void onClearBackStack() {
        try {
            FragmentManager manager = getSupportFragmentManager();
            if (manager.getBackStackEntryCount() > 0) {
                FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
                manager.popBackStackImmediate(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        } catch (Exception e) {
            Logger.error("onClearBackStack", e.toString());
        }
    }

    /**
     * @param title title of {@link NavigationView}
     */
    @Override
    public void setDrawerTitle(CharSequence title) {
        if (title != null && !title.toString().isEmpty())
            setTitle(title);
        else
            setTitle("");
    }

    /**
     * Get parent view of {@link BaseActivity}
     *
     * @return View
     */
    @Override
    public View getParentView() {
        View view = findViewById(R.id.layout_main);
        if (view == null)
            view = findViewById(android.R.id.content).getRootView();
        if (view == null)
            view = getWindow().getDecorView().findViewById(android.R.id.content);
        if (view == null)
            view = ((ViewGroup) this
                    .findViewById(android.R.id.content)).getChildAt(0);
        return view;
    }

    //endregion


    /**
     * Set action bar drawer toggle
     */
    public void setDrawerToggle() {
        mDrawerToggle = new BadgeDrawerToggle(this, mDrawerLayout,
                R.string.str_open, R.string.str_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                AppUtils.hideSoftKeyboard(BaseActivity.this);
                ActivityCompat.invalidateOptionsMenu(BaseActivity.this);
                syncState();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                setTitle(appTitle);
                ActivityCompat.invalidateOptionsMenu(BaseActivity.this);
                syncState();
                int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
                // backStackCount == 1 we choose fragment from drawer
                if (backStackCount > 1)
                    // we have more than 2 fragments so we must show back button instead of hum burger icon
                    animDrawerIcon(backStackCount < 2);
                BaseActivity.this.onDrawerClosed();
            }

            @Override
            public boolean onOptionsItemSelected(MenuItem item) {
                // boolean isDrawerOpen = mDrawerLayout.isDrawerOpen(GravityCompat.START);
                if (getSupportFragmentManager().getBackStackEntryCount() > 1)// && isDrawerOpen)
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                else //if (!isDrawerOpen)
                    mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.setHomeAsUpIndicator(R.drawable.btn_menu);
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        mDrawerToggle.syncState();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    public void onDrawerClosed() {
    }

    final public void setBackStackListener() {
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {

            @Override
            public void onBackStackChanged() {
                // int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
                int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
                Log.e("count", "" + backStackCount);
                if (backStackCount == 0)
                    return;
                onDrawerEnabled(backStackCount == 1, backStackCount);
                changeTollBar();
            }
        });
    }

    private void onDrawerEnabled(boolean isEnabled, int backStackCount) {
        Log.e("count", "" + isEnabled);
        if (mDrawerToggle != null && backStackCount <= 2)
            animDrawerIcon(isEnabled);
    }

    public void setCount(int count) {
        if (mDrawerToggle == null)
            return;
        if (count == 0) {
            mDrawerToggle.setBadgeEnabled(false);
            mDrawerToggle.setBadgeText("");
        } else {
            mDrawerToggle.setBadgeEnabled(true);
            mDrawerToggle.setBadgeText("" + count);
        }
    }
    public void updateBadgeCount(boolean showBadge, int badgeCount) {
        if (toolbar != null) {
            if (showBadge) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowCustomEnabled(true);   // enable overriding the default toolbar layout
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                updateDrawerBadgeCount(badgeCount);
            } else {
                mDrawerToggle.setHomeAsUpIndicator(R.drawable.btn_menu);
                mDrawerToggle.setDrawerIndicatorEnabled(false);
                mDrawerToggle.syncState();
            }
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    public void updateDrawerBadgeCount(int badgeCount) {
        if (mDrawerToggle == null)
            return;
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(setBadgeCount(this, R.drawable.btn_menu, badgeCount));
        }

    }

    private Drawable setBadgeCount(Context context, int res, int badgeCount) {
        LayerDrawable icon = (LayerDrawable) ContextCompat.getDrawable(context, R.drawable.badge_placeholders);
        Drawable mainIcon = ContextCompat.getDrawable(context, res);
        BadgeDrawable badge = new BadgeDrawable(context);
        if (badgeCount > 0)
            badge.setCount("");
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
        icon.setDrawableByLayerId(R.id.ic_main_icon, mainIcon);
        return icon;
    }

    private void animDrawerIcon(boolean isEnabled) {
        ValueAnimator anim = ValueAnimator.ofFloat(isEnabled ? 0 : 1, isEnabled ? 0 : 1);
        Log.e("count", "" + (isEnabled ? 1 : 0) + "-->" + (isEnabled ? 0 : 1));
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float slideOffset = (Float) valueAnimator.getAnimatedValue();
                if (mDrawerLayout != null && mDrawerToggle != null)
                    mDrawerToggle.onDrawerSlide(mDrawerLayout, slideOffset);
            }
        });
        anim.setInterpolator(new DecelerateInterpolator());
        anim.setDuration(0);
        anim.start();
    }

    private void changeTollBar() {
        Fragment oldFragment = getCurrentFragment();
        if (oldFragment instanceof BaseFragment) {
            BaseFragment fragment = (BaseFragment) oldFragment;
            // fragment.onChangeOptionsMenuVisibility(true);
            setTitle(AppUtils.ifNotNullEmpty(fragment.getTitle()) ? fragment.getTitle() : "");
        } else
            setTitle("");
    }
    // endregion

    // region NETWORK_OPERATIONS

    /**
     * @return true is connected else not
     */
    @Override
    final public boolean isNetworkConnected() {
        return NetworkUtils.hasNetworkConnection(BaseActivity.this, true);
    }

    /**
     * HTTP response call back from {@link AppNetworkTask}
     *
     * @param response {@link HttpResponseItem}
     */
    @Override
    public void onNetworkResponse(HttpResponseItem response) {
        boolean status = response.getResponseCode() == HttpURLConnection.HTTP_OK;
        if (status)
            onNetworkSuccess(response);
        else
            onNetworkError(response);
    }

    /**
     * HTTP network operation is successfully completed
     *
     * @param response {@link HttpResponseItem}
     */
    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        try {
            if (response.getHttpRequestUrl().contains(GOOGLE_BASE_URL))
                return;
            JSONObject responseJson = new JSONObject(response.getResponse());
            if (responseJson.getInt("success") == 1) {
                if (response.getHttpRequestUrl().equals(updateJobStatusUrl)) {
                    updateHomeFragment();
                }
            } else
                showSnackBar(responseJson.getString("message"));
        } catch (JSONException e) {
            Logger.error(false, e);
        }
    }

    public void updateHomeFragment() {

    }

    /**
     * For some reasons there is/are some error(s) in network operation
     *
     * @param response {@link HttpResponseItem}
     */
    @Override
    public void onNetworkError(HttpResponseItem response) {
        Logger.error(getLogTag(), response.getDefaultResponse() + "(network error)");
        if (response.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED){
            deAuthenticate();
            showSessionExpiredDialog();
        }
        else
            showSnackBar(getString(R.string.error_message), Snackbar.LENGTH_SHORT);

    }

    /**
     * For some reasons network operation has been cancelled
     *
     * @param response {@link HttpResponseItem}
     */
    @Override
    final public void onNetworkCanceled(HttpResponseItem response) {
        Logger.error(getLogTag(), response.getDefaultResponse() + " (operation cancelled by user)");
    }
    // endregion

    /**
     * Show snackBar in current view
     *
     * @param message  message to be displayed
     * @param duration defined snackBar duration flags
     */
    public void showSnackBar(String message, int duration) {
        ViewGroup view = findViewById(R.id.layout_main);
        if (view == null)
            return;
        CustomSnackBar customSnackbar = CustomSnackBar.make(view, duration);
        customSnackbar.setText(message);
        customSnackbar.show();
        //  Snackbar.make(view, message, (duration <= 0 && duration >= -2 ? duration : Snackbar.LENGTH_SHORT)).show();
    }

    /**
     * Show snackBar in current view
     *
     * @param message message to be displayed
     */
    public void showSnackBar(String message) {
        showSnackBar(message, Snackbar.LENGTH_LONG);
    }

    /**
     * Show snackBar in current view
     *
     * @param id string id of message to be displayed
     */
    public void showSnackBar(@StringRes int id) {
        showSnackBar(getString(id), Snackbar.LENGTH_LONG);
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    /**
     * If server response code is unauthorized (session expired)
     */
    AlertDialog sessionExpiryDialog = null;

    private void showSessionExpiredDialog() {
        if (sessionExpiryDialog != null && sessionExpiryDialog.isShowing()) {
            return;
        }
        if (!UserManager.isUserLoggedIn()) {
            return;
        }
        SharedPreferenceManager.getInstance().clearPreferences();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle("Session expired");
        builder.setMessage("Your session got expired kindly sign in again.");
        builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                stopService(new Intent(BaseActivity.this, SocketService.class));
                startActivity(new Intent(BaseActivity.this, SplashActivity.class));
                finish();
            }
        });
        sessionExpiryDialog = builder.create();
        if (!(BaseActivity.this).isFinishing()) {
        sessionExpiryDialog.show();
        }


    }

    /**
     * Create and return a waiting progress dialog
     *
     * @param cancelable is dialog cancelable
     * @return a new progress dialog
     */
//    public ProgressDialog getProgressDialog(boolean cancelable) {
//        ProgressDialog dialog = new ProgressDialog(this);
//        dialog.setMessage("Please wait!");
//        dialog.setCancelable(cancelable);
//        return dialog;
//    }
    public Dialog getProgressDialog(boolean cancelable) {
        Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.app_dialog);
        dialog.setCancelable(cancelable);
        return dialog;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (getCurrentFragment() instanceof BaseFragment)
            getCurrentFragment().onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    public void animate() {
        if (getParentView() != null && getParentView() instanceof ViewGroup)
            TransitionManager.beginDelayedTransition((ViewGroup) getParentView());
    }

    public void animate(int view_group_id) {
        View viewGroup = findViewById(view_group_id);
        Transition transition = new Fade();
        transition.setStartDelay(100);
        transition.setDuration(500);
        if (viewGroup != null && viewGroup instanceof ViewGroup)
            TransitionManager.beginDelayedTransition((ViewGroup) viewGroup, transition);
    }

    public void animateViewChange(int view_group_id) {
        View viewGroup = findViewById(view_group_id);
        if (viewGroup != null && viewGroup instanceof ViewGroup)
            TransitionManager.beginDelayedTransition((ViewGroup) viewGroup);
    }

    public void animate(int view_group_id, Transition transition) {
        View viewGroup = findViewById(view_group_id);
        if (viewGroup != null && viewGroup instanceof ViewGroup)
            TransitionManager.beginDelayedTransition((ViewGroup) viewGroup);
    }

    public void emitFromSocket(String key, JSONObject data) {
        if (mService != null)
            mService.emitResults(key, data);
    }

    public void emitFromSocket(String key, JSONObject data, Ack ack) {
        if (mService != null)
            mService.emitResults(key, data, ack);
    }

    public void onBindService(SocketService mService) {
    }

    public void onRatingApplied(String orderId) {
    }

    /*
     * action bar generic methods starts
     */

    public void setActionBar(@StringRes int id, boolean isBackEnabled) {
        setActionBar(getString(id), isBackEnabled);
    }

    public void setActionBar(boolean isBackEnabled) {
        setActionBar("", isBackEnabled);
    }

    private ImageView toolbarBack;
    private ImageView toolbarIcon;
    private TextView toolbarTitle;
    private TextView toolbarSubTitle;

    public void setActionBar(String title, boolean isBackEnabled) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbarBack = toolbar.findViewById(R.id.iv_toolbar_back);
            toolbarIcon = toolbar.findViewById(R.id.iv_toolbar);
            toolbarTitle = toolbar.findViewById(R.id.tv_toolbar_title);
            toolbarSubTitle = toolbar.findViewById(R.id.tv_toolbar_sub_title);
            setTitle(title);
            setBackListener(isBackEnabled);
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    public void setActionBar(@StringRes int id) {
        setActionBar(getString(id));
    }

    public void setActionBar() {
        setActionBar("");
    }

    public void setActionBar(String title) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbarBack = toolbar.findViewById(R.id.iv_toolbar_back);
            toolbarIcon = toolbar.findViewById(R.id.iv_toolbar);
            toolbarTitle = toolbar.findViewById(R.id.tv_toolbar_title);
            toolbarSubTitle = toolbar.findViewById(R.id.tv_toolbar_sub_title);
            setTitle(title);
            setBackListener(true);
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    public void setBackListener(boolean isBackEnabled) {
        if (toolbarBack != null && isBackEnabled) {
            toolbarBack.setVisibility(View.VISIBLE);
            toolbarBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }
    }

    public void setToolbarIcon(@DrawableRes int id) {
        if (toolbarIcon != null) {
            toolbarIcon.setVisibility(View.VISIBLE);
            toolbarIcon.setImageResource(id);
        }
    }

    public void setToolbarIcon(String url) {
        if (toolbarIcon != null && URLUtil.isValidUrl(url)) {
            toolbarIcon.setVisibility(View.VISIBLE);
            GlideUtils.glideLoadImageOrPlaceHolder(this, toolbarIcon, url,
                    (int) getResources().getDimension(R.dimen.toolbar_icon_size),
                    (int) getResources().getDimension(R.dimen.toolbar_icon_size));
        }
    }

    public void setTitle(@StringRes int id) {
        setTitle(getString(id));
    }

    @Override
    public void setTitle(CharSequence title) {
        if (toolbarTitle != null && !TextUtils.isEmpty(title)) {
            toolbarTitle.setVisibility(View.VISIBLE);
            toolbarTitle.setText(title);
        }
    }

    public void setSubTitle(@StringRes int id) {
        setSubTitle(getString(id));
    }

    public void setSubTitle(String subTitle) {
        if (toolbarSubTitle != null && !TextUtils.isEmpty(subTitle)) {
            toolbarSubTitle.setVisibility(View.VISIBLE);
            toolbarSubTitle.setText(subTitle);
        }
    }

    /*
     * action bar generic methods ends
     */
    public void setError(@IdRes int fieldId, @StringRes int errorId) {
        findErrorFieldById(fieldId).setError(errorId);
    }

    public String getFieldText(@IdRes int id) {
        return findErrorFieldById(id).getText().toString();
    }

    public ErrorEditText findErrorFieldById(@IdRes int id) {
        return (ErrorEditText) findViewById(id);
    }

    public BorderErrorEditText findBorderErrorFieldById(@IdRes int id) {
        return (BorderErrorEditText) findViewById(id);
    }

    public EditText findFieldById(@IdRes int id) {
        return (EditText) findViewById(id);
    }

    public TextView findTextViewById(@IdRes int id) {
        return ((TextView) findViewById(id));
    }

    public ImageView findImageViewById(@IdRes int id) {
        return ((ImageView) findViewById(id));
    }

    public Button findButtonById(@IdRes int id) {
        return (Button) findViewById(id);
    }

    public ImageButton findImageButtonById(@IdRes int id) {
        return (ImageButton) findViewById(id);
    }

    public RadioButton findRadioButtonById(@IdRes int id) {
        return (RadioButton) findViewById(id);
    }

    public String format(@StringRes int stringId, Object... args) {
        return String.format(getString(stringId), args);
    }

    public void onEventReceived(Intent intent) {
        String event = intent.getStringExtra(SocketService.BROADCAST_INTENT_TYPE);
        if (AppUtils.ifNotNullEmpty(event)) {
            if (event.equalsIgnoreCase(SocketService.KEY_JOB_OFFER)) {
                String jobId = intent.getStringExtra(SocketService.BROADCAST_JOB_ID);
                String data = intent.getStringExtra(SocketService.BROADCAST_INTENT_DATA);
                JobRequestModel jobRequestModel = new Gson().fromJson(data, JobRequestModel.class);
                DialogJobRequest.newInstance(this, jobRequestModel, jobRequestListener).show();
            }
        }

    }
    protected void updateCallStatus(String uniqueId, int status) {
        Log.d("VideoStatus", "updateCallStatus");
        String updateUrl = String.format(AppConstants.VIDEO_CALL_STATUS, uniqueId, status);
        HttpRequestItem item = new HttpRequestItem(updateUrl, "");
        item.setHttpRequestType(NetworkUtils.HTTP_GET);
        item.setHeaderParams(AppUtils.getHeaderParams());
        AppNetworkTask appNetworkTask = new AppNetworkTask(null, null);
        appNetworkTask.execute(item);
    }
    public JobRequestListener jobRequestListener = (jobId, status) -> moveToNext(jobId);
    void moveToNext(String jobId){
        Intent intent= new Intent(this, JobDetailsActivity.class);
        intent.putExtra(KEY_JOB_ID,jobId);
        startActivity(intent);
    }

    //Twilio Calling Implementation
    public Dialog dialogGeneral;
    private static final int REQUEST_WRITE_PERMISSION = 786;

    protected void requestPermission() {
        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
    }

    public boolean checkPermissionForMicrophone() {
        int resultMic = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        return resultMic == PackageManager.PERMISSION_GRANTED;
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public void showGeneralDialog(final int state, String title, String message, String positiveButton, int iconId, boolean cancelable) {
        if (dialogGeneral != null && dialogGeneral.isShowing()) {
            return;
        }
        TextView tv_title, tv_message, tv_positive, tv_negative;
        ImageView iv_icon;
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_selfie_messages, null);
        dialogGeneral = new Dialog(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        dialogGeneral.setCanceledOnTouchOutside(true);

        dialogGeneral.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogGeneral.getWindow().setBackgroundDrawableResource(R.drawable.bg_verify_now);

        tv_title = dialogView.findViewById(R.id.tv_title);
        tv_message = dialogView.findViewById(R.id.tv_message);
        tv_positive = dialogView.findViewById(R.id.tv_positive);
        tv_negative = dialogView.findViewById(R.id.tv_negative);
        iv_icon = dialogView.findViewById(R.id.iv_icon);

        if (isEmpty(title))
            tv_title.setVisibility(View.GONE);
        tv_negative.setVisibility(View.GONE);
        tv_title.setText(title);
        tv_message.setText(message);
        tv_positive.setText(positiveButton);
        if (iconId != -1)
            iv_icon.setImageDrawable(getDrawable(iconId));

        tv_positive.setOnClickListener(view -> {
            dialogGeneral.dismiss();
            // if (!checkPermissionForMicrophone()) {
            if (state == AppConstants.STATE_WIFI) {
                return;
            }
            moveToAppSetting();
            // }
        });

        dialogGeneral.setContentView(dialogView);
        dialogGeneral.setCancelable(cancelable);
        dialogGeneral.setCanceledOnTouchOutside(cancelable);
        dialogGeneral.show();
    }

    public void moveToAppSetting() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.fromParts("package", getPackageName(), null));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private static final String[] MANDATORY_PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.RECORD_AUDIO,// for VoiceCall and VideoCall
            Manifest.permission.BLUETOOTH_SCAN,
            Manifest.permission.BLUETOOTH_CONNECT,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;


    protected void checkPermissions() {
        ArrayList<String> deniedPermissions = new ArrayList<>();
        for (String permission : MANDATORY_PERMISSIONS) {
            if (checkCallingOrSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                deniedPermissions.add(permission);
            }
        }

        if (deniedPermissions.size() > 0) {
            requestPermissions(deniedPermissions.toArray(new String[0]), REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }
    public void deAuthenticate(){
        AuthenticationUtils.deauthenticate(this, isSuccess -> {
        });
    }
}
