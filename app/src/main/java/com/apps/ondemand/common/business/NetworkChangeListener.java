package com.apps.ondemand.common.business;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.apps.ondemand.common.utils.MobileNetworkUtil;

/**
 * Created by $Bilal on 4/6/2016.
 */
public class NetworkChangeListener extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent brodIntent = new Intent("network_status");
        brodIntent.putExtra("isConnected", MobileNetworkUtil.isNetworkConnected(context));
        context.sendBroadcast(brodIntent);

        Log.d("Network", ""+MobileNetworkUtil.isNetworkConnected(context) +"----"+
        MobileNetworkUtil.hasWLANConnection(context)+"-----"+MobileNetworkUtil.hasMobileConnection(context));
    }
}
