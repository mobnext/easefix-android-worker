package com.apps.ondemand.common.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.net.Uri;
import androidx.annotation.ColorRes;
import androidx.annotation.IdRes;
import androidx.annotation.StringRes;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.ondemand.app.data.LanguageManager;
import com.apps.ondemand.app.data.models.ScheduleJobModel;
import com.apps.ondemand.app.data.models.JobStatusModel;
import com.apps.ondemand.app.data.models.LanguageModel;
import com.apps.ondemand.app.data.models.ReviewModel;
import com.apps.ondemand.app.data.models.ServiceModel;
import com.apps.ondemand.app.data.preferences.PreferenceUtils;
import com.apps.ondemand.app.data.preferences.SharedPreferenceManager;
import com.apps.ondemand.app.ui.interfaces.ClickableSpanListener;
import com.apps.ondemand.app.ui.interfaces.ResourceSpanListener;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.business.BaseItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static android.webkit.URLUtil.isNetworkUrl;
import static com.apps.ondemand.common.utils.AppConstants.STATUS_ARRIVED;
import static com.apps.ondemand.common.utils.AppConstants.STATUS_CANCELLED;
import static com.apps.ondemand.common.utils.AppConstants.STATUS_COMPLETED;
import static com.apps.ondemand.common.utils.AppConstants.STATUS_ON_THE_WAY;
import static com.apps.ondemand.common.utils.AppConstants.STATUS_STARTED;

/**
 * Contains utility methods used in application
 */
public class AppUtils {

    public static void hideSoftKeyboard(Activity activity) {
        if (activity == null)
            return;
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view != null)
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showSoftKeyboard(Activity activity) {
        if (activity == null)
            return;
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view != null) {
            view.requestFocus();
            inputMethodManager.showSoftInput(view, 0);
        }
    }

    public static boolean validateUri(Uri uri) {
        if (uri == null)
            return false;
        else {
            String path = uri.getPath();
            return !(uri.equals(Uri.EMPTY) || path == null || path.equals("null"));
        }
    }

    public static boolean ifNotNullEmpty(String text) {
        return text != null && !text.isEmpty();
    }

    public static String toTitleCase(String givenString) {
        if (!AppUtils.ifNotNullEmpty(givenString) || givenString.equalsIgnoreCase(" "))
            return "";
        else {
            String[] arr = givenString.trim().split("\\s+");
            StringBuilder sb = new StringBuilder();
            for (String anArr : arr)
                sb.append(Character.toUpperCase(anArr.charAt(0)))
                        .append(anArr.substring(1)).append(" ");
            return sb.toString().trim();
        }
    }

    public static boolean validateEmptyEditText(EditText et) {
        return (et.getText().toString().equals("")) ? false : true;
    }

    public static boolean validateEmptyString(String string) {
        return (string.equals("")) ? false : true;
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;
        if (ifNotNullEmpty(email)) {
            try {
                String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
                Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(email);
                if (matcher.matches())
                    isValid = true;
            } catch (Exception e) {
                Logger.error("isEmailValid", e.toString());
            }
        }
        return isValid;
    }

    public static String preparePathForPicture(String path, String imageType) {
        String preparedLink;
        if (isNetworkUrl(path)) {
            String[] arr = path.split("\\.(?=[^\\.]+$)");
            preparedLink = arr[0] + imageType + ".jpg";
        } else
            preparedLink = path;
        return preparedLink;
    }

    public static Map<String, String> getHeaderParams() {
        Map<String, String> map = new HashMap<>();
        map.put("cookie", SharedPreferenceManager.getInstance().read(PreferenceUtils.COOKIE, ""));  // Add required header parameters (Cookies) here
        Log.e("COOKIE", map.get("cookie"));
        return map;
    }

    /**
     * Converting dp to pixel
     */
    public static int dpToPx(int dp, Context context) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public static String getStringFromJson(JSONObject data, String key) {
        try {
            return data.has(key) && !data.get(key).equals(null) ?
                    data.getString(key) : "";
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
        return "";
    }

    public static int getIntFromJson(JSONObject data, String key) {
        try {
            return data.has(key) && !data.get(key).equals(null) ?
                    data.getInt(key) : 0;
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
        return 0;
    }

    public static long getLongFromJson(JSONObject data, String key) {
        try {
            return data.has(key) && !data.get(key).equals(null) ?
                    data.getLong(key) : 0;
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
        return 0;
    }

    public static double getDoubleFromJson(JSONObject data, String key) {
        try {
            return data.has(key) && !data.get(key).equals(null) ?
                    data.getDouble(key) : 0;
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
        return 0;
    }

    public static boolean getBooleanFromJson(JSONObject data, String key) {
        try {
            return data.has(key) && !data.get(key).equals(null) ?
                    data.getBoolean(key) : false;
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
        return false;
    }

    public static void showWebDialog(Context context, String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(browserIntent);

    }

    public static void showFacebookDialog(Context context, String pageUrl, String packageID) {
        Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
        String facebookUrl = getFacebookPageURL(context, pageUrl, packageID);
        facebookIntent.setData(Uri.parse(facebookUrl));
        try {
            context.startActivity(facebookIntent);
        } catch (ActivityNotFoundException ex) {
            // start web browser and the facebook mobile page as fallback
            String uriMobile = "http://touch.facebook.com/pages/x/" + packageID;
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(uriMobile));
            context.startActivity(i);
        }
    }

    public static void showInstaGramDialog(Context context, String url) {
        Intent intent = getInstaGramIntent(context, url);
        if (intent == null)
            return;
        context.startActivity(intent);
    }

    private static String getFacebookPageURL(Context context, String pageUrl, String packageID) {

        String FACEBOOK_URL = pageUrl;
        String FACEBOOK_PAGE_ID = packageID;
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            boolean activated = packageManager.getApplicationInfo("com.facebook.katana", 0).enabled;
            if (activated) {
//                if ((versionCode >= 3002850)) {
//                    return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
//                } else {
                return "fb://page/" + FACEBOOK_PAGE_ID;
//                }
            } else {
                return FACEBOOK_URL;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL;
        }
    }

    private static Intent getInstaGramIntent(Context context, String url) {
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager.getPackageInfo("com.instagram.android", 0) != null) {
                if (url.endsWith("/")) {
                    url = url.substring(0, url.length() - 1);
                }
                final String username = url.substring(url.lastIndexOf("/") + 1);
                // http://stackoverflow.com/questions/21505941/intent-to-open-instagram-user-profile-on-android
                intent.setData(Uri.parse("http://instagram.com/_u/" + username));
                intent.setPackage("com.instagram.android");
                return intent;
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        intent.setData(Uri.parse(url));
        return intent;
    }

    public static void openMailIntent(Context context, String emailTo, String subject) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", emailTo, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    public static void openShareDialog(Context context, String message) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, message);
        context.startActivity(Intent.createChooser(share, ""));
    }

    public static int parseStringToInteger(String value) {
        if (value.trim().equalsIgnoreCase(""))
            return 0;
        try {
            int intValue = Integer.parseInt(value);
            return intValue;
        } catch (Exception e) {
            return 0;
        }
    }

    public static void disableField(EditText etField) {
        etField.setEnabled(false);
        etField.setClickable(false);
        etField.setFocusable(false);
        etField.setFocusableInTouchMode(false);
    }

    public static boolean isNotFieldEmpty(EditText editText) {
        if (editText == null)
            return false;
        return ifNotNullEmpty(editText.getText().toString());
    }

    public static Bitmap createDrawableFromView(View view, Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));
        //   view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    public static void setSpannableTextView(BaseActivity activity, @ColorRes int highlightColor, @IdRes int textViewId, final ResourceSpanListener clickableSpanListener, String fullString, @StringRes int... highlightedParts) {

        SpannableString sb = new SpannableString(fullString);


        for (@StringRes int highlightedPartID : highlightedParts) {
            ForegroundColorSpan fcs = new ForegroundColorSpan(activity.getResources().getColor(highlightColor));
            StyleSpan bss = new StyleSpan(Typeface.BOLD);

            String highlightedPart = activity.getString(highlightedPartID);
            sb.setSpan(fcs, fullString.indexOf(highlightedPart), fullString.indexOf(highlightedPart) + highlightedPart.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            sb.setSpan(bss, fullString.indexOf(highlightedPart), fullString.indexOf(highlightedPart) + highlightedPart.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            sb.setSpan(new ResourceClickableSpan(highlightedPartID, clickableSpanListener), fullString.indexOf(highlightedPart), fullString.indexOf(highlightedPart) + highlightedPart.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        }


        TextView bySigningUp = activity.findViewById(textViewId);
        bySigningUp.setText(sb);
        bySigningUp.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public static void setSpannableTextView(BaseActivity activity, String fullString, String highlightedPart, @ColorRes int highlightColor, @IdRes int textViewId, final ClickableSpanListener clickableSpanListener) {

        SpannableString sb = new SpannableString(fullString);
        MyClickableSpan clickableSpan = new MyClickableSpan(highlightedPart) {
            @Override
            public void onClick(View textView) {
                clickableSpanListener.onSpanClicked();
            }
        };

        ForegroundColorSpan fcs = new ForegroundColorSpan(activity.getResources().getColor(highlightColor));
        StyleSpan bss = new StyleSpan(Typeface.BOLD);

        sb.setSpan(fcs, fullString.indexOf(highlightedPart), fullString.indexOf(highlightedPart) + highlightedPart.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        sb.setSpan(bss, fullString.indexOf(highlightedPart), fullString.indexOf(highlightedPart) + highlightedPart.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        sb.setSpan(clickableSpan, fullString.indexOf(highlightedPart), fullString.indexOf(highlightedPart) + highlightedPart.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        TextView bySigningUp = activity.findViewById(textViewId);
        bySigningUp.setText(sb);
        bySigningUp.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public static List<BaseItem> getServiceDummyData() {
        List<BaseItem> baseItems = new ArrayList<>();
        baseItems.add(new ServiceModel());
        baseItems.add(new ServiceModel());
        baseItems.add(new ServiceModel());
        baseItems.add(new ServiceModel());
        baseItems.add(new ServiceModel());
        baseItems.add(new ServiceModel());
        return baseItems;
    }


    public static List<BaseItem> getReviewDummyData() {
        List<BaseItem> baseItems = new ArrayList<>();
        baseItems.add(new ReviewModel());
        baseItems.add(new ReviewModel());
        return baseItems;
    }

    public static List<BaseItem> getLanguageDummyData() {
        List<BaseItem> baseItems = new ArrayList<>();
        baseItems.add(new LanguageModel());
        baseItems.add(new LanguageModel());
        baseItems.add(new LanguageModel());
        baseItems.add(new LanguageModel());
        baseItems.add(new LanguageModel());
        return baseItems;
    }

    public static List<BaseItem> getJobStatusDummyData() {
        List<BaseItem> baseItems = new ArrayList<>();
        baseItems.add(new JobStatusModel(STATUS_ON_THE_WAY));
        baseItems.add(new JobStatusModel(STATUS_ARRIVED));
        baseItems.add(new JobStatusModel(STATUS_STARTED));
        baseItems.add(new JobStatusModel(STATUS_COMPLETED));
        baseItems.add(new JobStatusModel(STATUS_CANCELLED));
        return baseItems;
    }

    public static List<BaseItem> getJobDummyData() {
        List<BaseItem> baseItems = new ArrayList<>();
        baseItems.add(new ScheduleJobModel());
        baseItems.add(new ScheduleJobModel());
        baseItems.add(new ScheduleJobModel());
        baseItems.add(new ScheduleJobModel());
        baseItems.add(new ScheduleJobModel());


        return baseItems;
    }

    public static String getLabel(int position) {
        ArrayList<String> labels = new ArrayList<>();
        labels.add("");
        labels.add("A");
        labels.add("B");
        labels.add("C");
        labels.add("D");
        labels.add("E");
        labels.add("F");
        labels.add("G");
        labels.add("H");
        labels.add("J");
        labels.add("K");
        labels.add("L");
        labels.add("M");
        labels.add("N");
        labels.add("O");
        labels.add("P");
        labels.add("Q");
        labels.add("R");
        labels.add("S");
        labels.add("T");
        labels.add("U");
        labels.add("V");
        labels.add("W");
        labels.add("X");
        labels.add("Y");
        labels.add("Z");
        return labels.get(position);
    }

    public static String getAmount(double amount) {
        if (amount == 0) {
            return LanguageManager.getCurrency() + " " + "0.0";
        }
        NumberFormat formatter = NumberFormat.getNumberInstance();
        formatter.setMinimumFractionDigits(2);
        formatter.setMaximumFractionDigits(2);
        String outputAmount = formatter.format(amount);
        return LanguageManager.getCurrency() + " " + outputAmount;

    }

    public static String formatRating(double amount) {
        if (amount == 0) {
            return "0.00";
        }
        NumberFormat formatter = NumberFormat.getNumberInstance();
        formatter.setMinimumFractionDigits(2);
        formatter.setMaximumFractionDigits(2);
        String outputAmount = formatter.format(amount);
        return outputAmount;

    }

    public static String formatNumberToTwoDigits(double amount) {
        if (amount == 0) {
            return "0.00";
        }
        NumberFormat formatter = NumberFormat.getNumberInstance();
        formatter.setMinimumFractionDigits(2);
        formatter.setMaximumFractionDigits(2);
        String outputAmount = formatter.format(amount);
        return outputAmount;

    }


    public static String formatAndRoundOff(double amount) {
        if (amount == 0) {
            return "0";
        }
        return String.valueOf(Math.round(amount));

    }

    public static String formatNumberToOneDigits(double amount) {
        if (amount == 0) {
            return "0.0";
        }
        NumberFormat formatter = NumberFormat.getNumberInstance();
        formatter.setMinimumFractionDigits(1);
        formatter.setMaximumFractionDigits(1);
        String outputAmount = formatter.format(amount);
        return outputAmount;

    }

    public static long GetTimeForServer(long timeInMillis) {
        if (timeInMillis != 0)
            return timeInMillis / 1000;
        return timeInMillis;
    }

    public static long GetTimeForForApp(long timeInMillis) {
        return timeInMillis * 1000;
    }

    public static void setResultAndFinish(Activity activity) {
        Intent intent = new Intent();
        activity.setResult(Activity.RESULT_OK, intent);
        activity.finish();
    }


    /**
     * Animates in the dropdown list
     */
    public static void openDropdown(final View view) {
        if (view.getVisibility() != View.VISIBLE) {
            ScaleAnimation anim = new ScaleAnimation(1, 1, 0, 1);
            anim.setDuration(200);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    view.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            view.startAnimation(anim);


        }
    }

    /**
     * Animates out the dropdown list
     */
    public static void closeDropdown(final View view) {
        if (view.getVisibility() == View.VISIBLE) {
            ScaleAnimation anim = new ScaleAnimation(1, 1, 1, 0);
            anim.setDuration(200);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    view.setVisibility(View.GONE);
                }
            });

            view.startAnimation(anim);
        }
    }
    public static int getHoursDifference(long jobStartTime, long jobEndTime) {
        Calendar start = Calendar.getInstance();
        start.setTimeInMillis(AppUtils.GetTimeForForApp(jobStartTime));
        Calendar end = Calendar.getInstance();
        end.setTimeInMillis(AppUtils.GetTimeForForApp(jobEndTime));
        return Math.abs(end.get(Calendar.HOUR_OF_DAY) - start.get(Calendar.HOUR_OF_DAY));
    }

    public static String imageLowerSizeReplace(String url) {
        if (ifNotNullEmpty(url)) {
            if (url.contains("1000X1000"))
                return url.replace("1000X1000", "1000X1000");
            else
                return url;
        } else
            return url;
    }
}
