package com.apps.ondemand.common.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import android.util.Log;

import com.apps.ondemand.BuildConfig;
import com.apps.ondemand.R;
import com.apps.ondemand.app.data.preferences.PreferenceUtils;
import com.apps.ondemand.app.data.preferences.SharedPreferenceManager;
import com.apps.ondemand.app.communication.SocketService;
import com.apps.ondemand.app.ui.SplashActivity;
import com.apps.ondemand.app.ui.main.AllJobsActivity;
import com.apps.ondemand.app.ui.main.MainActivity;

import static android.app.PendingIntent.getActivity;
import static android.content.Context.NOTIFICATION_SERVICE;
import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.media.RingtoneManager.TYPE_NOTIFICATION;
import static android.media.RingtoneManager.getDefaultUri;

/**
 * Created by bilal on 26/01/2018.
 */

public class NotificationManager {

    public static final int NOTIFICATION_ID = 1010;
    public static final String NOTIFICATION_CHANNEL = "channel";

    private static final String NOTIFICATION_GROUP = "ondemand_group";
    private static final int SUMMARY_NOTIFICATION_ID = 1111;

    public static void createNotification(Context context, String job_id, String event,
                                          String mTitle, String mBody, String data) {

        String title = AppUtils.ifNotNullEmpty(mTitle) ? mTitle : context.getString(R.string.app_name);
        String body = AppUtils.ifNotNullEmpty(mBody) ? mBody : "You have a new notification";

        Intent intent;
        if (!SharedPreferenceManager.getInstance().read(PreferenceUtils.COOKIE, "").equalsIgnoreCase("")) {
            if (SharedPreferenceManager.getInstance().read(PreferenceUtils.IS_LOGGED_IN, false)){
                if (event.equalsIgnoreCase(SocketService.KEY_OFFER_ACCEPTED)
                ){
                    intent=new Intent(context, AllJobsActivity.class);
                }else {
                    intent = new Intent(context, MainActivity.class);
                }
            } else
                intent = new Intent(context, SplashActivity.class);
        } else
            intent = new Intent(context, SplashActivity.class);
        intent.addFlags(FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(SocketService.BROADCAST_JOB_ID, job_id);
        intent.putExtra(SocketService.BROADCAST_INTENT_TYPE, event);
        intent.putExtra(SocketService.BROADCAST_INTENT_DATA, data);
        intent.putExtra(AppConstants.FROM_WHICH_COMPONENT, AppConstants.COMPONENT_GCM);
  
        int uniqueInt = (int) (System.currentTimeMillis() & 0xfffffff);

        PendingIntent pendingIntent = getActivity(context, uniqueInt, intent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        createNotification(context, pendingIntent, title, body, job_id);

    }

    public static void createNotification(Context context, PendingIntent intent, String title, String body) {

        Log.e("NOTIFICATION:", "Title ->" + title + "\n" + "Body->" + body);
        int uniqueInt = (int) (System.currentTimeMillis() & 0xfffffff);
        if (intent == null)
            intent = PendingIntent.getActivity(context, uniqueInt,
                    new Intent(), // add this
                   PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = getDefaultUri(TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, BuildConfig.APPLICATION_ID);
        notificationBuilder.setSmallIcon(getNotificationIcon(context, notificationBuilder)).setContentTitle(title)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                .setContentText(body).setAutoCancel(true)
                .setSound(defaultSoundUri).setContentIntent(intent);
//                .setNumber(SharedPreferenceManager.getSharedPreferenceInstance(context).read(ZemCarConsts.PREF_ZEM_NOT_COUNT, 0));
        android.app.NotificationManager notificationManager =
                (android.app.NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        if (notificationManager == null)
            return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            notificationManager.createNotificationChannel(getNotificationChannel(context));
        notificationBuilder.setAutoCancel(true);
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }

    public static void createNotification(Context context, PendingIntent intent, String title, String body, String jobID){
        if (intent == null) {
            int uniqueInt = (int) (System.currentTimeMillis() & 0xfffffff);
            intent = PendingIntent.getActivity(context, uniqueInt,
                    new Intent(), // add this
                   PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        }
        Uri defaultSoundUri = getDefaultUri(TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, BuildConfig.APPLICATION_ID);
        notificationBuilder.setSmallIcon(getNotificationIcon(context, notificationBuilder)).setContentTitle(title)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                .setContentText(body).setAutoCancel(true)
                .setGroup(NOTIFICATION_GROUP)
                .setPriority(android.app.NotificationManager.IMPORTANCE_HIGH)
                .setVibrate(new long[]{100,100,100,100})
                .setSound(defaultSoundUri).setContentIntent(intent);
//                .setNumber(SharedPreferenceManager.getSharedPreferenceInstance(context).read(ZemCarConsts.PREF_ZEM_NOT_COUNT, 0));
        android.app.NotificationManager notificationManager =
                (android.app.NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        if (notificationManager == null)
            return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            notificationManager.createNotificationChannel(getNotificationChannel(context));
        notificationBuilder.setAutoCancel(true);
        notificationManager.notify(createNotificationId(jobID), notificationBuilder.build());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Notification summaryNotification =
                    new NotificationCompat.Builder(context, BuildConfig.APPLICATION_ID).setSmallIcon(getNotificationIcon(context, notificationBuilder)).setContentTitle(title)
                            .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher))
                            .setContentTitle(context.getString(R.string.app_name) + " Notification(s)")
                            //set content text to support devices running API level < 24
                            .setContentText("You have new notifications")
                            //build summary info into InboxStyle template
                            .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                            //specify which group this notification belongs to
                            .setGroup(NOTIFICATION_GROUP)
                            //set this notification as the summary for the group
                            .setGroupSummary(true)
                            .setAutoCancel(true)
                            .build();
            notificationManager.notify(SUMMARY_NOTIFICATION_ID, summaryNotification);
        }
    }

    private static int getNotificationIcon(Context context, NotificationCompat.Builder notificationBuilder) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
            return R.drawable.ic_launcher;

        } else {
            return R.drawable.ic_launcher;
        }
    }

    private static NotificationChannel getNotificationChannel(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
            return null;
        CharSequence name = context.getString(R.string.app_name);// The user-visible name of the channel.
        int importance = android.app.NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel notificationChannel = new NotificationChannel(BuildConfig.APPLICATION_ID, name, importance);
        notificationChannel.setShowBadge(true);
        return notificationChannel;
    }

    private static int createNotificationId(String jobId) {
        if (!AppUtils.ifNotNullEmpty(jobId))
            return NOTIFICATION_ID;
        if (jobId.length() > 12) {
            try {
                String subString = jobId.substring(jobId.length() - 10, jobId.length());
                int id = 0;
                for (int a : subString.toCharArray()) {
                    id += a;
                }
                return id;
            }catch (Exception e){
                return NOTIFICATION_ID;
            }
        } else
            return NOTIFICATION_ID;
    }

//    ////////////?VideoCall Notification
//    public static Notification createNotificationVideo(Context context, String userID, String name, String image, String distance, String uniqueID, int notificationId, int channelImportance) {
////    private Notification createNotificationVideo(Context context,CallInvite callInvite, int notificationId, int channelImportance) {
//        Intent intent = new Intent(context, VideoCallRequest.class);
//        intent.setAction(Constants.ACTION_INCOMING_CALL_NOTIFICATION);
//        intent.putExtra(Constants.INCOMING_CALL_NOTIFICATION_ID, notificationId);
//        intent.putExtra(VIDEO_USER_ID_, userID);
//        intent.putExtra(VIDEO_NAME_, name);
//        intent.putExtra(VIDEO_IMAGE_, image);
//        intent.putExtra(VIDEO_DISTANCE_, distance);
//        intent.putExtra(VIDEO_UNIQUE_ID_, uniqueID);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//        PendingIntent pendingIntentActivity = PendingIntent.getActivity(context, notificationId, intent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
//
//        TaskStackBuilder resultPendingIntent = TaskStackBuilder.create(context);
//        resultPendingIntent.addNextIntentWithParentStack(intent);
//        resultPendingIntent.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
//
//        /*
//         * Pass the notification id and call sid to use as an identifier to cancel the
//         * notification later
//         */
//        Bundle extras = new Bundle();
////        extras.putString(Constants.CALL_SID_KEY, callInvite.getCallSid());
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            return buildNotification(AppConstants.VIEW_DISPLAY_NAME + " is calling.",
//                    pendingIntentActivity,
//                    extras,
//                    userID, name, image, distance, uniqueID,
//                    notificationId,
//                    createChannel(channelImportance,context));
//        } else {
//            //noinspection deprecation
//            return new NotificationCompat.Builder(context)
//                    .setSmallIcon(R.drawable.ic_call_end_white_24dp)
//                    .setContentTitle(context.getString(R.string.app_name))
//                    .setContentText(AppConstants.VIEW_DISPLAY_NAME + " is calling.")
//                    .setAutoCancel(true)
////                    .setExtras(extras)
//                    .setContentIntent(pendingIntentActivity)
//                    .setGroup("test_app_notification")
//                    .setColor(Color.rgb(214, 10, 37)).build();
//        }
//    }
//
//
//
//
//    @TargetApi(Build.VERSION_CODES.O)
//    private Notification buildNotification(String text, PendingIntent pendingIntentActivity, Bundle extras,
//                                           final CallInvite callInvite,
//                                           int notificationId,
//                                           Context context,
//                                           String channelId) {
//
//        Intent rejectIntent = new Intent(context, IncomingCallNotificationService.class);
//        rejectIntent.setAction(Constants.ACTION_REJECT);
//        rejectIntent.putExtra(Constants.INCOMING_CALL_INVITE, callInvite);
//        rejectIntent.putExtra(Constants.INCOMING_CALL_NOTIFICATION_ID, notificationId);
//        PendingIntent piRejectIntent = PendingIntent.getService(context, 0, rejectIntent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
//
//        Intent acceptIntent = new Intent(context, IncomingCallNotificationService.class);
//        acceptIntent.setAction(Constants.ACTION_ACCEPT);
//        acceptIntent.putExtra(Constants.INCOMING_CALL_INVITE, callInvite);
//        acceptIntent.putExtra(Constants.INCOMING_CALL_NOTIFICATION_ID, notificationId);
//        PendingIntent piAcceptIntent = PendingIntent.getService(context, 0, acceptIntent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
//
//        Notification.Builder builder =
//                new Notification.Builder(context, channelId)
//                        .setSmallIcon(R.drawable.ic_call_end_white_24dp)
//                        .setContentTitle(context.getString(R.string.app_name))
//                        .setContentText(text)
//                        .setCategory(Notification.CATEGORY_CALL)
//                        .setExtras(extras)
//                        .setAutoCancel(true)
//                        .addAction(android.R.drawable.ic_menu_delete, context.getString(R.string.decline), piRejectIntent)
//                        .addAction(android.R.drawable.ic_menu_call, context.getString(R.string.answer), pendingIntentActivity)
//                        .setFullScreenIntent(pendingIntentActivity, true);
//
//        return builder.build();
//    }
//    @TargetApi(Build.VERSION_CODES.O)
//    private String createChannel(int channelImportance,Context context) {
//        Uri Emergency_sound_uri= Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.uber_driver_2019);
//        AudioAttributes att = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).setContentType(AudioAttributes.USAGE_MEDIA).build();
//        NotificationChannel callInviteChannel = new NotificationChannel(Constants.VOICE_CHANNEL_HIGH_IMPORTANCE,
//                "Primary Voice Channel", android.app.NotificationManager.IMPORTANCE_HIGH);
//        String channelId = Constants.VOICE_CHANNEL_HIGH_IMPORTANCE;
//
//        if (channelImportance == android.app.NotificationManager.IMPORTANCE_LOW) {
//            callInviteChannel = new NotificationChannel(Constants.VOICE_CHANNEL_LOW_IMPORTANCE,
//                    "Primary Voice Channel", android.app.NotificationManager.IMPORTANCE_LOW);
//            channelId = Constants.VOICE_CHANNEL_LOW_IMPORTANCE;
//        }
//        callInviteChannel.setLightColor(Color.GREEN);
//        callInviteChannel.setSound(Emergency_sound_uri,att);
//        callInviteChannel.enableVibration(true);
//        callInviteChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
//        callInviteChannel.setVibrationPattern(new long[] { 1000, 1000, 1000, 1000, 1000 });
//        android.app.NotificationManager notificationManager = (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.createNotificationChannel(callInviteChannel);
//
//        return channelId;
//    }

}
