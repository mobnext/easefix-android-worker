package com.apps.ondemand.common.business;

public interface BaseItem {


    int ITEM_LANGUAGE = 1;
    int ITEM_COUNTRY = 2;
    int ITEM_SERVICE = 3;
    int ITEM_TIME_SLOT = 4;
    int ITEM_IDENTITY_TYPE = 5;
    int ITEM_REVIEWS = 6;
    int ITEM_JOB_STATUS = 7;
    int ITEM_NOTIFICATION = 8;
    int ITEM_JOB = 9;
    int ITEM_SERVICE_CERTIFICATES = 10;
    int ITEM_SPOKEN_LANGUAGE = 11;
    int ITEM_LINE_ITEM = 12;
    int ITEM_WEEK = 13;

    int ITEM_OUTGOING = 38;
    int ITEM_INCOMING = 83;

    int ITEM_HEADER = 50;
    int ITEM_EMPTY = 60;
    int ITEM_PROGRESS = 70;
    int ITEM_PORTFOLIO = 80;
    int ITEM_ATTACHMENTS = 90;

    int getItemType();
}
