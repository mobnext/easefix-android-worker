package com.apps.ondemand.common.base;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;

import com.apps.ondemand.app.ui.SplashActivity;
import com.apps.ondemand.common.network.AppNetworkTask;
import com.apps.ondemand.common.utils.NetworkUtils;
import com.apps.ondemand.R;
import com.apps.ondemand.common.business.OnContentChangeListener;
import com.apps.ondemand.common.business.OnNetworkTaskListener;
import com.apps.ondemand.app.data.preferences.SharedPreferenceManager;
import com.apps.ondemand.common.network.HttpResponseItem;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.ViewUtils;

/***
 * This Fragment contains {@link WeakReference} of {@link OnContentChangeListener}.
 * Which we are using for Activity and fragment communication.
 */
@SuppressWarnings({"ConstantConditions", "NullableProblems"})
public abstract class BaseFragment extends Fragment implements OnNetworkTaskListener {

    // region CONTENT_CHANGE_LISTENER
    private WeakReference<OnContentChangeListener> reference = null;

    final protected OnContentChangeListener getReference() {
        return reference != null ? reference.get() : null;
    }
    // endregion

    // region ABSTRACT

    /**
     * @return Title of given fragment
     */
    abstract public String getTitle();
    // endregion

    // region LIFE_CYCLE
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            reference = new WeakReference<>((OnContentChangeListener) context);
        } catch (ClassCastException e) {
            Logger.caughtException(e);
            throw new ClassCastException(context.toString() + " must implement OnContentChangeListener");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Logger.error("onLowMemory", String.format("%s is running on low memory", getLogTag()));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        reference.clear();
        reference = null;
        Logger.info("onDestroy", getLogTag());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    // endregion

    //region VIEW_HELPERS
    public View findViewById(int resourceId) {
        try {
            return getView().findViewById(resourceId);
        }catch (Exception ex){
            return null;
        }
    }

    public TextView findTextViewById(int resourceId) {
        return (TextView) findViewById(resourceId);
    }

    public EditText findEditTextById(int resourceId) {
        return (EditText) findViewById(resourceId);
    }

    public void setText(int resourceId, String text) {
        findTextViewById(resourceId).setText(text);
    }

    public Button findButtonById(int resourceId) {
        return (Button) findViewById(resourceId);
    }

    public LinearLayout findLinearLayoutById(int resourceId) {
        return (LinearLayout) findViewById(resourceId);
    }

    public RelativeLayout findRelativeLayoutById(int resourceId) {
        return (RelativeLayout) findViewById(resourceId);
    }
    //endregion

    // region ACTIVITY_VIEW_NULL
    final protected boolean isActivityNotNull() {
        return getActivity() != null;
    }

    final protected boolean isViewNull() {
        return getView() == null;
    }

    final protected boolean isActivityAndViewNotNull() {
        return isActivityNotNull() && isAdded() && !isViewNull();
    }

    final public String getLogTag() {
        return getClass().getSimpleName();
    }
    //endregion

    // region NETWORK_OPERATIONS

    /**
     * @return true is connected else not
     */
    @Override
    final public boolean isNetworkConnected() {
        return NetworkUtils.hasNetworkConnection(getActivity(), true);
    }

    /**
     * HTTP response call back from {@link AppNetworkTask}
     *
     * @param response {@link HttpResponseItem}
     */
    @Override
    public void onNetworkResponse(HttpResponseItem response) {
        boolean status = response.getResponseCode() == HttpURLConnection.HTTP_OK;
        if (status)
            onNetworkSuccess(response);
        else
            onNetworkError(response);
    }

    /**
     * HTTP network operation is successfully completed
     *
     * @param response {@link HttpResponseItem}
     */
    @Override
    public void onNetworkSuccess(HttpResponseItem response) {
        Logger.info(getLogTag(), "Network operation (" + response.getHttpRequestUrl() + ") successfully completed");
    }

    /**
     * For some reasons there is/are some error(s) in network operation
     *
     * @param response {@link HttpResponseItem}
     */
    @Override
    public void onNetworkError(HttpResponseItem response) {
        Logger.error(getLogTag(), response.getDefaultResponse() + "(network error)");
        if (response.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED)
            showSessionExpiredDialog();
        else if (getView() != null)
            ViewUtils.showSnackBar(getView(), getString(R.string.error_message), Snackbar.LENGTH_SHORT);
    }

    /**
     * For some reasons network operation has been cancelled
     *
     * @param response {@link HttpResponseItem}
     */
    @Override
    final public void onNetworkCanceled(HttpResponseItem response) {
        Logger.error(getLogTag(), response.getDefaultResponse() + " (operation cancelled by user)");
    }
    // endregion

    /**
     * If server response code is unauthorized (session expired)
     */
    private void showSessionExpiredDialog() {
        if (!isActivityAndViewNotNull())
            return;
        SharedPreferenceManager.getInstance().clearPreferences();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        builder.setTitle("Session expired");
        builder.setMessage("Your session got expired kindly sign in again.");
        builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getActivity().startActivity(new Intent(getActivity(), SplashActivity.class));
                getActivity().finish();
            }
        });
        builder.create().show();
    }

    public void setTitle(String title){
        if (getActivity() == null)
            return;
        BaseActivity baseActivity = (BaseActivity) getActivity();
        if (baseActivity != null)
            baseActivity.setTitle(title);
    }

    public void showSnackBar(String message){
        if (getView() == null)
            return;
        Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
    }

    public void showToast(String message){
        if (getContext() == null)
            return;
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Create and return a waiting progress dialog
     *
     * @param cancelable    is dialog cancelable
     * @return      a new progress dialog
     */
//    public ProgressDialog getProgressDialog(boolean cancelable){
//        ProgressDialog dialog = new ProgressDialog(getActivity());
//        dialog.setMessage("Please wait!");
//        dialog.setCancelable(cancelable);
//        return dialog;
//    }

    public Dialog getProgressDialog(boolean cancelable) {
        Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.app_dialog);
        dialog.setCancelable(cancelable);
        return dialog;
    }

    public void animate() {
        if (getView()!= null && getView() instanceof ViewGroup)
            TransitionManager.beginDelayedTransition((ViewGroup) getView());
    }

    public void animate(int view_group_id) {
        View viewGroup = findViewById(view_group_id);
        if (viewGroup != null && viewGroup instanceof ViewGroup)
            TransitionManager.beginDelayedTransition((ViewGroup) viewGroup);
    }

    public void animate(int view_group_id, Transition transition) {
        View viewGroup = findViewById(view_group_id);
        if (viewGroup != null && viewGroup instanceof ViewGroup)
            TransitionManager.beginDelayedTransition((ViewGroup) viewGroup);
    }

    public void onEventReceived(Intent intent){};

}
