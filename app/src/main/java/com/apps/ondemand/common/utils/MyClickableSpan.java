package com.apps.ondemand.common.utils;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

public class MyClickableSpan extends ClickableSpan {
    // extend ClickableSpan

    String clicked;

    public MyClickableSpan(String string) {
        super();
        clicked = string;
    }

    public void onClick(View tv) {

    }

    public void updateDrawState(TextPaint ds) {
        // override updateDrawState
        ds.setUnderlineText(false); // set to false to remove underline
    }
}