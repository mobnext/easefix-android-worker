package com.apps.ondemand.common.network;

import android.app.Activity;
import android.app.Dialog;
import android.os.AsyncTask;

import androidx.annotation.NonNull;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;

import com.apps.ondemand.BuildConfig;
import com.apps.ondemand.common.base.BaseActivity;
import com.apps.ondemand.common.business.OnNetworkTaskListener;
import com.apps.ondemand.common.utils.AppConstants;
import com.apps.ondemand.common.utils.AppUtils;
import com.apps.ondemand.common.utils.Logger;
import com.apps.ondemand.common.utils.NetworkUtils;

public final class AppNetworkTask extends AsyncTask<HttpRequestItem, Void, HttpResponseItem> {
    private static final String LOGGING_TAG = "LOGGING";

    private Dialog dialog = null;
    private String httpRequestUrl = null;
    private WeakReference<OnNetworkTaskListener> onNetworkTaskListener = null;
    private HttpRequestItem httpRequestItem;

    public AppNetworkTask(Dialog dialog, @NonNull OnNetworkTaskListener taskListener) {
        this.dialog = dialog;
        this.onNetworkTaskListener = new WeakReference<>(taskListener);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        OnNetworkTaskListener listener = getNetworkTaskListener();
        if (listener != null)
            listener.onNetworkCanceled(getDefaultHttpResponse("Operation canceled by user"));
        clearResources();
    }

    @Override
    protected HttpResponseItem doInBackground(HttpRequestItem... params) {
        httpRequestItem = params[0];
        if (httpRequestItem == null)
            return getDefaultHttpResponse("http request item is null");

        // region INTERNET_CONNECTIVITY_CHECK
        OnNetworkTaskListener listener = getNetworkTaskListener();
        if (listener != null && !listener.isNetworkConnected()) {
            HttpResponseItem item = new HttpResponseItem(HttpURLConnection.HTTP_UNAVAILABLE, "http unavailable/not connected");
            item.setHttpRequestUrl(httpRequestItem.getHttpRequestUrl());
            item.setHttpRequestEndPoint(httpRequestItem.getHttpRequestEndPoint());
            item.setInBackGround(httpRequestItem.isInBackGround());
            return item;
        }
        // endregion

        // execute network request
        String networkResponse = NetworkUtils.executeNetworkRequest(httpRequestItem);
        // if we have some response from network executor service
        if (!AppUtils.ifNotNullEmpty(networkResponse))
            return getDefaultHttpResponse("network executor service returned nothing");
        try {
            JSONObject json = new JSONObject(networkResponse);
            if (json.has("response_code")) {
                int code = json.getInt("response_code");
                HttpResponseItem item = new HttpResponseItem();
                item.setResponseCode(code);
                item.setResponse(json.getString("data"));
                item.setHttpRequestUrl(httpRequestItem.getHttpRequestUrl());
                item.setHttpRequestEndPoint(httpRequestItem.getHttpRequestEndPoint());
                item.setHttpRequestType(httpRequestItem.getHttpRequestType());
                item.setInBackGround(httpRequestItem.isInBackGround());
                return item;
            } else
                Logger.error(getClass().getSimpleName(), "");
        } catch (JSONException e) {
            Logger.caughtException(e);
        }
        return getDefaultHttpResponse("Internal error");
    }

    @Override
    protected void onPostExecute(HttpResponseItem response) {
        super.onPostExecute(response);
        try {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog=null;
        }
        }catch (Exception e){
            Log.d("TAG", "onPostExecute: "+e);
        }
        dialog = null;
        OnNetworkTaskListener listener = getNetworkTaskListener();
        if (response != null && listener != null)
            listener.onNetworkResponse(response);
        clearResources();
        if (response != null && BuildConfig.IS_DEBUG_ABLE) {
            Log.d(LOGGING_TAG, "URL: " + httpRequestItem.getHttpRequestUrl());
            Log.d(LOGGING_TAG, "CODE: " + response.getResponseCode());
            if (httpRequestItem.getParams() != null)
                Log.d(LOGGING_TAG, "Params: " + httpRequestItem.getParams().toString());
            if (!httpRequestItem.getHttpRequestUrl().contains(AppConstants.GOOGLE_BASE_URL))
                Log.d(LOGGING_TAG, "RESPONSE: " + response.getResponse());
        }
    }

    /**
     * Clear all resources
     */
    private void clearResources() {
        if (onNetworkTaskListener != null)
            onNetworkTaskListener.clear();
        onNetworkTaskListener = null;
    }

    private OnNetworkTaskListener getNetworkTaskListener() {
        if (onNetworkTaskListener != null)
            return onNetworkTaskListener.get();
        return null;
    }

    /**
     * Default {@link HttpResponseItem} for error
     *
     * @param error error string
     * @return HttpResponseItem
     */
    @NonNull
    private HttpResponseItem getDefaultHttpResponse(String error) {
        HttpResponseItem item = new HttpResponseItem(HttpURLConnection.HTTP_INTERNAL_ERROR, error);
        item.setHttpRequestUrl(httpRequestUrl);
        return item;
    }
}
