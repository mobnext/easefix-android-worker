package com.apps.ondemand.common.base;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.material.snackbar.Snackbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;

import com.apps.ondemand.app.ui.interfaces.AnimateMarkerCallback;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import com.apps.ondemand.R;
import com.apps.ondemand.common.utils.PermissionUtils;
import com.google.android.gms.maps.model.Marker;

import static com.google.android.gms.location.LocationServices.FusedLocationApi;

import java.util.Objects;

/**
 * Created by T520 on 1/13/2016.
 */
public class GoogleMapFragment extends BaseFragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, LocationSource {

    private static final int REQUEST_CHECK_SETTINGS = 0;
    private static final long DEFAULT_RIDE_LOCATION_INTERVAL_SECONDS = 10000;
    private static final int PERMISSION_LOCATION_REQUEST = 1010;

    private static final double LATITUDE_DEFAULT = 53.483959;
    private static final double LONGITUDE_DEFAULT = -2.244644;

    protected GoogleApiClient mGoogleApiClient;
    protected MapView mapView;
    protected GoogleMap googleMap;
    private String TAG = "GoogleMapFragment";
    private OnLocationChangedListener mMapLocationListener = null;
    private LocationRequest mLocationRequest;
    public Location mLocation;
    public boolean needToCheckPermission = true;
    public boolean needCurrentLocation = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeGoogleMap(savedInstanceState);

        mGoogleApiClient = new GoogleApiClient.Builder(requireActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mapView != null)
            mapView.onPause();
        if (needCurrentLocation && mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mapView != null)
            mapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (needCurrentLocation)
            connectGoogleClient();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (needCurrentLocation)
            disconnectGoogleClient();
    }

    private void createLocationRequest() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(DEFAULT_RIDE_LOCATION_INTERVAL_SECONDS);
        mLocationRequest.setFastestInterval(DEFAULT_RIDE_LOCATION_INTERVAL_SECONDS);
        createLocationAccessBuilder();
    }

    private void createLocationAccessBuilder() {
        if (!needToCheckPermission)
            return;
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        onApiClientConnected();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        onApiClientConnected();
                        break;
                    case Activity.RESULT_CANCELED:
//                        rejectLocationServicesRequest();

                        createLocationRequest();//keep asking if imp or do whatever
                        break;
                }
                break;
        }
    }

    public void rejectLocationServicesRequest() {

    }

    private void connectGoogleClient() {
        mGoogleApiClient.registerConnectionCallbacks(this);
        mGoogleApiClient.registerConnectionFailedListener(this);
        mGoogleApiClient.connect();
    }

    private void disconnectGoogleClient() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
            mGoogleApiClient.unregisterConnectionCallbacks(this);
            mGoogleApiClient.unregisterConnectionFailedListener(this);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        checkPermission();
    }

    public void checkPermission() {
        if (PermissionUtils.hasLocationPermissionGranted(getActivity())) {
            if (!mGoogleApiClient.isConnected())
                connectGoogleClient();
            else
                createLocationRequest();
        } else {
            PermissionUtils.requestLocationPermissions(getActivity(), PERMISSION_LOCATION_REQUEST);
        }
    }

    @SuppressLint("MissingPermission")
    private void onApiClientConnected() {
//        createLocationRequest();
        if (!mGoogleApiClient.isConnected())
            return;
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        if (googleMap == null)
            return;
        if (!googleMap.isMyLocationEnabled())
            googleMap.setMyLocationEnabled(true);
        googleMap.setLocationSource(this);
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            mLocation = location;
            moveToMyLocation(location);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.d(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLocation = location;
        if (mMapLocationListener != null)
            mMapLocationListener.onLocationChanged(location);
        moveToMyLocation(location);
    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        mMapLocationListener = onLocationChangedListener;
    }

    @Override
    public void deactivate() {
        mMapLocationListener = null;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null)
            mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (googleMap != null) {
            if (PermissionUtils.hasLocationPermissionGranted(getActivity()))
                if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
            googleMap.setMyLocationEnabled(false);
            googleMap.clear();
        }
        googleMap = null;
        if (mapView != null)
            mapView.onDestroy();
        mapView = null;
        mGoogleApiClient = null;
        mLocationRequest = null;
        mMapLocationListener = null;
    }

    private void initializeGoogleMap(Bundle savedInstanceState) {
        MapsInitializer.initialize(requireActivity());
        if (PermissionUtils.checkPlayServices(getContext())) {
            mapView = (MapView) requireView().findViewById(R.id.map);
            if (mapView != null) {
                mapView.onCreate(savedInstanceState);
                mapView.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(@NonNull GoogleMap googleMap) {
                        GoogleMapFragment.this.googleMap = googleMap;
                        setUpMap();
                    }
                });
            }
        }
    }

    private void setUpMap() {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.setLocationSource(this);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(
                new CameraPosition.Builder()
                        .target(new LatLng(LATITUDE_DEFAULT, LONGITUDE_DEFAULT))
                        .zoom(12.0f).build()));
        if (PermissionUtils.hasLocationPermissionGranted(getContext()))
            if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }googleMap.setMyLocationEnabled(false);
        setMapProperties();
    }

    /**
     * Setup MapView properties according to own requirement
     */
    public void setMapProperties() {
    }

    public void onMyLocationReceived(CameraPosition cameraPosition) {
    }

    public void moveToMyLocation(Location location) {
    }

    protected void moveToCurrentLocation() {
        if (googleMap != null && PermissionUtils.hasLocationPermissionGranted(getActivity())) {
            Location location;
            if (!googleMap.isMyLocationEnabled())
                googleMap.setMyLocationEnabled(false);
            location = googleMap.getMyLocation();
            // if current location is null then get last location
            if (location == null) {
                if (mGoogleApiClient == null)
                    return;
                location = FusedLocationApi.getLastLocation(mGoogleApiClient);
            }
            if (location != null)
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                        new CameraPosition.Builder().target(new LatLng(location.getLatitude(), location.getLongitude())).zoom(16.0f).build()));
        }
    }

    public void setHighAccuracyLocationRequest() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(DEFAULT_RIDE_LOCATION_INTERVAL_SECONDS);
        mLocationRequest.setFastestInterval(DEFAULT_RIDE_LOCATION_INTERVAL_SECONDS);
        createLocationAccessBuilder();
    }

    public void onLocationPermissionGranted() {
        if (!mGoogleApiClient.isConnected())
            connectGoogleClient();
        else
            createLocationRequest();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_LOCATION_REQUEST:
                // if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (PermissionUtils.verifyPermission(grantResults)) {
                    onLocationPermissionGranted();
                } else {
                    String text = getString(R.string.location_permissions_not_granted);
                    Snackbar.make(getView(), text, Snackbar.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void animateMarker(final GoogleMap map, final Marker marker, final LatLng toPosition,
                              final AnimateMarkerCallback animateMarkerCallback) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = map.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 1000;

        final LinearInterpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * toPosition.longitude + (1 - t) * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t) * startLatLng.latitude;

                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    animateMarkerCallback.animationEnd();
                    marker.setVisible(true);
                }
            }
        });
    }


    private boolean isMarkerRotating = false;

    public void rotateMarker(final Marker marker, final float toRotation) {
        if (!isMarkerRotating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = marker.getRotation();
            final long duration = 1000;

            final LinearInterpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    isMarkerRotating = true;

                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);

                    float rot = t * toRotation + (1 - t) * startRotation;

                    marker.setRotation(-rot > 180 ? rot / 2 : rot);
                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    } else {
                        isMarkerRotating = false;
                    }
                }
            });
        }
    }

    public double angleToRotate = 0.0f;

    public double bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;
        angleToRotate = brng;
        return brng;
    }

}
