package com.apps.ondemand.common.utils;

import androidx.annotation.StringRes;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import com.apps.ondemand.app.ui.interfaces.ResourceSpanListener;

public class ResourceClickableSpan extends ClickableSpan {

    @StringRes
    int clickedId;
    ResourceSpanListener resourceSpanListener;

    public ResourceClickableSpan(@StringRes int clickedId, ResourceSpanListener resourceSpanListener) {
        super();
        this.clickedId = clickedId;
        this.resourceSpanListener = resourceSpanListener;
    }

    public void onClick(View tv) {
        resourceSpanListener.onSpanClicked(clickedId);
    }


    public void updateDrawState(TextPaint ds) {
        // override updateDrawState
        ds.setUnderlineText(false); // set to false to remove underline
    }
}