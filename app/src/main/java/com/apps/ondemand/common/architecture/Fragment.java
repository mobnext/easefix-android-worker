package com.apps.ondemand.common.architecture;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps.ondemand.R;
import com.apps.ondemand.app.ui.main.MainActivity;
import com.apps.ondemand.common.base.BaseFragment;

/**
 * Created by bilal on 26/12/2017.
 *
 * Common practice when creating fragments
 */

public class Fragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container,false);
        /**
         *  Only inflate the layout in this method. All view initialization are done onViewCreated()
         */
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /**
         *  Initialize/populate UI views here
         */
        //.......................
        /**
         *  Need to call parent activity methods
         */
        callParentActivityMethods();
    }

    /**
     *  Title associated with the fragment (This title will be displayed on actionbar)
     * @return  Fragment title
     */
    @Override
    public String getTitle() {
        return "New Fragment";
    }

    /**
     *  Practice to use when calling parent activity methods OR
     *  opening fragments, getting current fragments
     */
    private void callParentActivityMethods(){
        if (getReference() == null)
            return;
        /**
         *  getReference() has access to some common activity methods @{@link com.apps.ondemand.common.base.BaseActivity}
         *  {@link com.apps.ondemand.common.business.OnContentChangeListener}
         */
        getReference().onReplaceFragment(new Fragment(), true);

        /**
         *  Else if there are some methods that are not provided by getReference then
         */
        if ( getActivity() != null &&
                getActivity() instanceof MainActivity){     // Check parent activity is instance of that activity you are trying to call
            ((MainActivity) getActivity()).onNavigationItemSelected(null);
        }
    }
}
