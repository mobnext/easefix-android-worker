package com.apps.ondemand.common.base.recycler_view;

/**
 * Created by bilal on 05/12/2017.
 */

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * RecyclerView item decoration - give equal margin around grid item
 */
public class GridSpacingItemDecorator extends RecyclerView.ItemDecoration {

    private int spanCount;
    private int spacing;

    public GridSpacingItemDecorator(int spanCount, int spacing) {
        this.spanCount = spanCount;
        this.spacing = spacing;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position
        int column = position % spanCount; // item column

        if (position == 0) { // top edge
                outRect.top = spacing;
            }
    }
}