package com.apps.ondemand.common.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.text.TextUtils;

import com.apps.ondemand.app.data.LanguageManager;
import com.apps.ondemand.app.ui.SplashActivity;

import java.util.Locale;

/**
 * Created by bilal on 26/02/2018.
 */

public class LocaleUtils {

    public static Locale getLocale() {
        String lang = LanguageManager.getUserPreferredLanguage();
        return new Locale(lang);
    }

    public static void setLocale(Context context) {

        final Resources resources = context.getResources();
        final Configuration configuration = resources.getConfiguration();
        final Locale locale = getLocale();

        if (!configuration.locale.equals(locale)) {
            configuration.setLocale(locale);
            resources.updateConfiguration(configuration, null);
        }
    }


    public static void changeAppLocale(Activity activity) {
        setLocale(activity);
        Intent intent = new Intent(activity, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.finish();
    }

}
