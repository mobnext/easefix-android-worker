package com.apps.ondemand;

import android.content.Context;
import android.content.res.Configuration;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.apps.ondemand.app.calling.sendBird.call.CallService;
import com.apps.ondemand.app.calling.sendBird.utils.BroadcastUtils;
import com.apps.ondemand.app.calling.sendBird.utils.PrefUtils;
import com.apps.ondemand.app.data.preferences.LanguageSharedPreferenceManager;
import com.apps.ondemand.app.data.preferences.SharedPreferenceManager;
import com.apps.ondemand.common.utils.LocaleUtils;
import com.sendbird.calls.DirectCall;
import com.sendbird.calls.SendBirdCall;
import com.sendbird.calls.handler.DirectCallListener;
import com.sendbird.calls.handler.SendBirdCallListener;

import java.util.UUID;

/**
 * Created by $Bilal on 11/24/2017.
 */

public class MyApplication extends MultiDexApplication {
    private static final String DATABASE_NAME = "OnDemand";
    private static final int DATABASE_VERSION = 1;

    public static final String VERSION = "1.4.0";
    public static final String TAG = "SendBirdCalls";
    // Refer to "https://github.com/sendbird/quickstart-calls-android".
    public static final String APP_ID = "1E6D4A31-FEA6-4DB3-A695-BC9A90297849";
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
        SharedPreferenceManager.setSingletonInstance(context);
        LanguageSharedPreferenceManager.setSingletonInstance(context);
        LocaleUtils.setLocale(context);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        LocaleUtils.setLocale(getApplicationContext());
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        // these 2 lines are necessary if you want to use database in your project
        /*
            SQliteHelper helper = SQliteHelper.newInstance(this, DATABASE_NAME, DATABASE_VERSION);
            DatabaseConnection.init(helper);
        */
        initSendBirdCall(PrefUtils.getAppId(getApplicationContext()));
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleUtils.setLocale(getApplicationContext());

    }
    public boolean initSendBirdCall(String appId) {
        Log.i(MyApplication.TAG, "[MyApplication] initSendBirdCall(appId: " + appId + ")");
        Context context = getApplicationContext();

        if (TextUtils.isEmpty(appId)) {
            appId = APP_ID;
        }

        if (SendBirdCall.init(context, appId)) {
            SendBirdCall.removeAllListeners();
            SendBirdCall.addListener(UUID.randomUUID().toString(), new SendBirdCallListener() {
                @Override
                public void onRinging(DirectCall call) {
                    int ongoingCallCount = SendBirdCall.getOngoingCallCount();
                    Log.i(MyApplication.TAG, "[MyApplication] onRinging() => callId: " + call.getCallId() + ", getOngoingCallCount(): " + ongoingCallCount);

                    if (ongoingCallCount >= 2) {
                        call.end();
                        return;
                    }

                    call.setListener(new DirectCallListener() {
                        @Override
                        public void onConnected(DirectCall call) {
                        }

                        @Override
                        public void onEnded(DirectCall call) {
                            int ongoingCallCount = SendBirdCall.getOngoingCallCount();
                            Log.i(MyApplication.TAG, "[MyApplication] onEnded() => callId: " + call.getCallId() + ", getOngoingCallCount(): " + ongoingCallCount);

                            BroadcastUtils.sendCallLogBroadcast(context, call.getCallLog());

                            if (ongoingCallCount == 0) {
                                CallService.stopService(context);
                            }
                        }
                    });

                    CallService.onRinging(context, call);
                }
            });

            SendBirdCall.Options.addDirectCallSound(SendBirdCall.SoundType.DIALING, R.raw.dialing);
            SendBirdCall.Options.addDirectCallSound(SendBirdCall.SoundType.RINGING, R.raw.ringing);
            SendBirdCall.Options.addDirectCallSound(SendBirdCall.SoundType.RECONNECTING, R.raw.reconnecting);
            SendBirdCall.Options.addDirectCallSound(SendBirdCall.SoundType.RECONNECTED, R.raw.reconnected);
            return true;
        }
        return false;
    }
}
